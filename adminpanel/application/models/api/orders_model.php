<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders_model extends CI_Model
{

    public function createOrder($data)
    {
        $this->db->insert('orders', $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        }
        return $this->db->_error_message();
    }


    public function updateOrder($data)
    {
        $this->db->where('id', $data['id']);
        return $this->db->update('orders', $data);
    }

    public function getPaymentInfoForProvider($providerId, $paymentMethodId)
    {
        $this->db->select('field_name, field_value');
        $this->db->from('payment_config');
        $this->db->where("user_id", $providerId);
        $this->db->where("payment_method_id", $paymentMethodId);
        $query = $this->db->get();

        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }
}

?>
