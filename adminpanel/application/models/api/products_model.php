<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products_model extends CI_Model
{

    public function checkProductAvailability($productId, $cityId)
    {
        $queryStr = "*";
        $this->db->select($queryStr);
        $this->db->from('product');
        $this->db->join('assign_city as city', 'city.admin_name=product.user_id', 'inner');
        $this->db->where('city.city', $cityId);
        $this->db->where('product.id', $productId);
        $this->db->where('product.outofstock', 0);

        $query = $this->db->get();

        if ($query->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllProducts($catId, $subCategoryId, $cityId, $searchTxt)
    {

        $deliveryCostSubQuery = "";
        if ($cityId) {
            $deliveryCostSubQuery = "(SELECT cost from delivery_cost WHERE city_id=$cityId AND user_id=product.user_id)";
        } else {
            $deliveryCostSubQuery = "(SELECT cost from delivery_cost WHERE city_id=0 AND user_id=0)";
        }

        $queryStr = "product.*, cat.category_name, users.name as sold_by, $deliveryCostSubQuery as delievery_price";
        $this->db->select($queryStr);
        $this->db->distinct();
        $this->db->from('product');
        $this->db->join('category as cat', 'cat.id=product.category_id', 'inner');
        $this->db->join('users', 'users.id=product.user_id', 'inner');

        if ($cityId) {
            $this->db->join('assign_city as city', 'city.admin_name=product.user_id', 'inner');
            $this->db->where('city.city', $cityId);
        }
        if ($searchTxt) {
            $this->db->like('product.product_name', $searchTxt);
        }
        if ($subCategoryId) {
            $this->db->where('product.subcategory_id', $subCategoryId);
        }
        if ($catId) {
            $this->db->where('product.category_id', $catId);
        }
        $this->db->where('product.outofstock', 0);


        $query = $this->db->get();

        if ($query->num_rows() >= 1) {
            $result = $query->result();

            $resultArray = array();

            for ($x = 0; $x < count($result); $x++) {

                $data = $result[$x];

                $newData = array();

                foreach ($data as $key => $value) {
                    $newData[$key] = $value;
                }


                $subQuery = "pm.*";
                $this->db->select($subQuery);
                $this->db->from('product_payoptions as ppo');
                $this->db->join('payment_methods as pm', 'ppo.paymentmethod_id=pm.id', 'inner');
                $this->db->where('ppo.product_id', $data->id);
                $res = $this->db->get();


                $suppliesQuery = "psup.*";
                $this->db->select($suppliesQuery);
                $this->db->from('product_supplies as psup');
                $this->db->where('psup.product_id', $data->id);
                $psupRes = $this->db->get();

                $newData['payment_options'] = $res->result();
                $newData['supplies'] = $psupRes->result();

                $resultArray[] = $newData;

            }
            return $resultArray;
        } else {
            return false;
        }
    }
}

?>
