<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cities_model extends CI_Model
{

	public function getAllCities() {
		$this->db->select('*');
		$this->db->from('city');
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		} else {
			return false;
		}
	}

}
?>