<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    public function login($conditions)
    {
        $this->db->select('email');

        if (!empty($conditions)):
            foreach ($conditions as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;

        $this->db->from('users');
        $query = $this->db->get();

        if ($query->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }

    public function checkAlreadyExist($email)
    {
        $condition = "email = '" . $email . "'";
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $query = $this->db->get();

        if ($query->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }

    public function signup($data)
    {
        $this->db->insert('users', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return $this->db->_error_message();
    }

    public function updateUser($data)
    {
        $this->db->where('id', $data['id']);
        return $this->db->update('users', $data);
    }

    public function updatePasswordByResetCode($resetCode, $data)
    {
        $this->db->where('reset_password_code', $resetCode);
        return $this->db->update('users', $data);
    }

    public function resetPasswordCode($email, $data)
    {
        $this->db->where('email', $email);
        return $this->db->update('users', $data);
    }

    public function checkResetPasswordCode($code)
    {
        $condition = "reset_password_code = '" . $code . "'";
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $query = $this->db->get();

        if ($query->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getUser($conditions)
    {
        $this->db->select('users.*, city.city_name as city_name');
        $this->db->from('users');
        if (!empty($conditions)):
            foreach ($conditions as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;
        $this->db->join('city', 'city.id=users.city_id', 'left');
        $query = $this->db->get();

        if ($query->num_rows() >= 1) {
            return $query->result()[0];
        } else {
            return false;
        }
    }

    public function sendEmail($email, $subject, $message)
    {
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.snappypanel.com',
            'smtp_port' => 465,
            'smtp_user' => 'info@snappypanel.com',
            'smtp_pass' => 'txRG3q0r54!!',
            'mailtype' => 'html'
        );
        $this->load->library('email', $config);
        $this->email->from('info@snappypanel.com', 'Snappy App');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->set_mailtype("html");
        return $this->email->send();
    }
}


?>
