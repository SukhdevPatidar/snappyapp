<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Feedback_model extends CI_Model
{
	public function addFeedback($data) {
		$this->db->insert('feedback', $data);
		if ($this->db->affected_rows() > 0) {
			return $this->db->insert_id();
		}
		return false;
	}
}
?>