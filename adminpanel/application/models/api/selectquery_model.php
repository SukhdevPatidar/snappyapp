<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Selectquery_model extends CI_Model {

	public function get_row($table_name='', $conditions='',$columns=array(), $order_by = array(), $joins = array(), $group_by = array()) 
	{
		if(!empty($columns)):
			$all_columns = implode(",", $columns);
			$this->db->select($all_columns);
		endif; 
		if(!empty($conditions)):		
			foreach ($conditions as $key => $value){
				$this->db->where($key, $value);
			}
		endif;
		if(!empty($joins)):
			foreach ($joins as $key => $value) {
				$this->db->join($value[0], $value[1], $value[2]); 
			}
		endif;
		if(!empty($order_by)):
			foreach ($order_by as $key => $value) {
				$this->db->order_by($value[0], $value[1]); 
			}
		endif;
		if(!empty($group_by)):
			foreach ($group_by as $key => $value) {
				$this->db->group_by($value); 
			}
		endif;
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->row();
		else
			return FALSE;
	}


	public function getResults($table_name = '', $conditions = '', $columns = array(), $order_by = array(), $joins = array(), $group_by = array())
	{
		if(!empty($columns)):
			$all_columns = implode(",", $columns);
			$this->db->select($all_columns);
		endif;
		if(!empty($conditions)):
			foreach ($conditions as $key => $value) {
				$this->db->where($key, $value);
			}
		endif;
		if(!empty($joins)):
			foreach ($joins as $key => $value) {
				$this->db->join($value[0], $value[1], $value[2]); 
			}
		endif;
		if(!empty($order_by)):
			foreach ($order_by as $key => $value) {
				$this->db->order_by($value[0], $value[1]); 
			}
		endif;
		if(!empty($group_by)):
			foreach ($group_by as $key => $value) {
				$this->db->group_by($value); 
			}
		endif;

		$query = $this->db->get($table_name);
		if ($query->num_rows() > 0) return $query->result();
		else return FALSE;
	}
}