<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Address_model extends CI_Model
{

	public function addAddress($data) {
		$this->db->insert('addresses', $data);
		if ($this->db->affected_rows() > 0) {
			return $this->db->insert_id();
		}
		return false;
	}


	public function updateAddress($data)
	{
		$this->db->where('id', $data['id']);
		return $this->db->update('addresses', $data);
	}


	public function resetDefaultAddress($userId)
	{
		$data =array("is_default"=>0);
		$this->db->where('user_id', $userId);
		return $this->db->update('addresses', $data);
	}


	public function deleteAddress($addressId) {
		return $this->db->delete('addresses', array("id"=>$addressId));
	}

	public function getAddress($addressId) {
		$condition = "id = '".$addressId."'";
		$this->db->select('*');
		$this->db->from('addresses');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		} else {
			return false;
		}
	}


	public function getAddresses($userId) {
		$condition = "user_id = '".$userId."'";
		$this->db->select('*');
		$this->db->from('addresses');
		$this->db->where($condition);
		$query = $this->db->get();

		if ($query->num_rows() >= 1) {
			return $query->result();
		} else {
			return false;
		}
	}

}
?>