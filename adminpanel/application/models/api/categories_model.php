<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends CI_Model
{


    public function getAllCategories($cityId, $countryId = "", $businessGroupId = null, $businessGroupSubCatId = null, $searchTxt = null)
    {
        $this->db->select('category.*');
        $this->db->distinct();
        $this->db->from('category');

        $this->db->join('assign_cat as acat', 'acat.category=category.id', 'inner');
        $this->db->join('assign_city as city', 'city.admin_name=acat.admin_name', 'inner');
        $this->db->join('users', 'users.id=acat.added_by', 'inner');

        if ($cityId) {
            $this->db->where('city.city', $cityId);
        }
        if (!empty($countryId)) {
            $this->db->where('users.country_id', $countryId);
        }
        if (!empty($businessGroupSubCatId)) {
            $this->db->where('category.businesssubcat_id', $businessGroupSubCatId);
        }
        if (!empty($businessGroupId)) {
            $this->db->where('category.businesstype_id', $businessGroupId);
        } else {
            $this->db->where('category.onlycalltaxi', 0);
        }

        if ($searchTxt) {
            $this->db->like('category.category_name', $searchTxt);
        }

        $query = $this->db->get();

        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getAllBusinessGroups($countryId = "")
    {
        $this->db->select('businesstype.*');
        $this->db->distinct();
        $this->db->from('businesstype');

        $this->db->join('users', 'users.id=businesstype.added_by', 'inner');

        if (!empty($countryId)) {
            $this->db->where('users.country_id', $countryId);
        }
        $query = $this->db->get();

        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }
}

?>
