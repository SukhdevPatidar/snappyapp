<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Insertquery_model extends CI_Model {
	public function insertRow($table_name='', $data=array()) {
		$this->db->insert($table_name, $data);
		if ($this->db->affected_rows() > 0) {
			return $this->db->insert_id();
		}
		return false;
	}
	
	public function insertMultipleRow($table_name='', $data=array()) {
		$res = $this->db->insert_batch($table_name, $data);
		if ($res) return $this->db->insert_id();
		return false;
	}
	
	
	public function updateRow($table_name='', $conditions=array(), $data=array()) {
		if(!empty($conditions)):
            foreach ($conditions as $key => $value) {
                $this->db->where($key, $value);
            }
    endif;
		return $this->db->update($table_name, $data);
	}
	
	
}