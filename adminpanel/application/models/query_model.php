<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Query_model extends CI_Model
{


    // Select query

    public function getRow($tableName = '', $conditions = '', $columns = array(), $orderBy = array(), $joins = array(), $groupBy = array())
    {
        if (!empty($columns)):
            $allColumns = implode(",", $columns);
            $this->db->select($allColumns);
        endif;

        if (!empty($conditions)):
            foreach ($conditions as $key => $value) {
                if (is_array($value)) {
                    $this->db->where_in($key, $value);
                } else {
                    if (!$value) {
                        $this->db->where($key, $value, false);
                    } else {
                        $this->db->where($key, $value);
                    }

                }
            }
        endif;

        if (!empty($joins)):
            foreach ($joins as $value) {
                $this->db->join($value[0], $value[1], $value[2]);
            }
        endif;
        if (!empty($orderBy)):
            foreach ($orderBy as $value) {
                $this->db->order_by($value[0], $value[1]);
            }
        endif;
        if (!empty($groupBy)):
            foreach ($groupBy as $value) {
                $this->db->group_by($value);
            }
        endif;
        $query = $this->db->get($tableName);
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }


    public function getRows($tableName = '', $conditions = '', $columns = array(), $orderBy = array(), $joins = array(), $groupBy = array())
    {
        if (!empty($columns)):
            $all_columns = implode(",", $columns);
            $this->db->select($all_columns);
        endif;

        if (!empty($conditions)):
            foreach ($conditions as $key => $value) {
                if (is_array($value)) {
                    $this->db->where_in($key, $value);
                } else {
                    if (!$value) {
                        $this->db->where($key, $value, false);
                    } else {
                        $this->db->where($key, $value);
                    }

                }
            }
        endif;

        if (!empty($joins)):
            foreach ($joins as $value) {
                $this->db->join($value[0], $value[1], $value[2]);
            }
        endif;
        if (!empty($orderBy)):
            foreach ($orderBy as $value) {
                $this->db->order_by($value[0], $value[1]);
            }
        endif;
        if (!empty($groupBy)):
            foreach ($groupBy as $value) {
                $this->db->group_by($value);
            }
        endif;

        $query = $this->db->get($tableName);
        if ($query->num_rows() > 0) return $query->result();
        else return FALSE;
    }


    // Insert Query

    public function insertRow($tableName = '', $data = array())
    {
        $this->db->insert($tableName, $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function insertMultipleRow($tableName = '', $data = array())
    {
        $res = $this->db->insert_batch($tableName, $data);
        if ($res) return $this->db->insert_id();
        return false;
    }

    // Update Query

    public function updateRow($tableName = '', $conditions = array(), $data = array())
    {
        if (!empty($conditions)):
            foreach ($conditions as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;
        return $this->db->update($tableName, $data);
    }

    // Delete Query

    public function deleteAddress($tableName = '', $conditions = array())
    {
        return $this->db->delete($tableName, $conditions);
    }

    public function deleteRow($tableName = '', $conditions = array())
    {
        return $this->db->delete($tableName, $conditions);
    }
}
