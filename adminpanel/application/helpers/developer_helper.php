<?php
define('API_ACCESS_KEY_SNAPPY', 'AAAAHVgD9fk:APA91bEbwcF_SppkObrptDZdr9X6A-IugMaL_o4n8193RAI3dEh75yEpQ8PO1Cj_XvPFRsxBkkTpS9IhJZn-51D4BotjFPtFJntSuAWGl1wkElmw6kwL__ov169whOojGO0iVNzalSYp');

function _check_user_login()
{

    if (check_user_login() === FALSE) redirect('user/index');
}

if (!function_exists('check_user_login')) {

    function check_user_login()
    {
        $CI = &get_instance();
        $user_info = $CI->session->userdata('user_info');
        //&& $user_info['user_role']=='manager'
        if ($user_info['logged_in'] === TRUE && isset($user_info['is_user']) && $user_info['is_user'] === TRUE) {
            return TRUE;
        } else {
            return FALSE;
        }

    }
}

if (!function_exists('get_city_name')) {
    function get_city_name($city_id)
    {
        $ci = &get_instance();
        $city = $ci->db->get_where('city', array('id' => $city_id));
        if ($city->result() != '') {
            $data = $city->result();
            return $data ? $data[0] : false;
        } else {
            return false;
        }
    }
}

if (!function_exists('get_cat_name')) {
    function get_cat_name($cat_id)
    {
        $ci = &get_instance();
        $cat = $ci->db->get_where('category', array('id' => $cat_id));
        if ($cat->result() != '') {
            $data = $cat->result();
            return $data ? $data[0] : false;
        } else {
            return false;
        }
    }
}

if (!function_exists('get_user_name')) {
    function get_user_name($user_id)
    {
        $ci = &get_instance();
        $user = $ci->db->get_where('users', array('id' => $user_id));
        if ($user->result() != '') {
            //print_r($user->result());die;
            $data = $user->result();
            if (isset($data[0]) && !empty($data[0])) {
                return $data[0];
            }
        } else {
            return false;
        }
    }
}

if (!function_exists('msg_alert')) {


    function msg_alert_backend()

    {

        $CI = &get_instance();


        ?>

        <?php if ($CI->session->flashdata('msg_success')): ?>

        <div class="alert alert-success">

            <button type="button" class="close" data-dismiss="alert">&times;</button>

            <!-- <strong>Success :</strong> <br> --> <?php echo $CI->session->flashdata('msg_success'); ?>

        </div>

    <?php endif; ?>

        <?php if ($CI->session->flashdata('msg_info')): ?>

        <div class="alert alert-info">

            <button type="button" class="close" data-dismiss="alert">&times;</button>

            <!-- <strong>Info :</strong> <br> --> <?php echo $CI->session->flashdata('msg_info'); ?>

        </div>

    <?php endif; ?>

        <?php if ($CI->session->flashdata('msg_warning')): ?>

        <div class="alert alert-warning">

            <button type="button" class="close" data-dismiss="alert">&times;</button>

            <!--  <strong>Warning :</strong> <br> --> <?php echo $CI->session->flashdata('msg_warning'); ?>

        </div>

    <?php endif; ?>

        <?php if ($CI->session->flashdata('msg_error')): ?>

        <div class="alert alert-danger">

            <button type="button" class="close" data-dismiss="alert">&times;</button>

            <!-- <strong>Error :</strong> <br> --> <?php echo $CI->session->flashdata('msg_error'); ?>

        </div>

    <?php endif; ?>

        <?php


    }


}

if (!function_exists('upload_file')) {

    function upload_file($param = null)
    {


        $CI = &get_instance();

        $config['upload_path'] = './assets/uploads/';

        $config['allowed_types'] = 'gif|jpg|png|xls|xlsx|csv|jpeg|pdf|doc|docx';

        $config['max_size'] = 1024 * 90;

        $config['image_resize'] = TRUE;

        $config['resize_width'] = 200;

        $config['resize_height'] = 200;

        if ($param) {

            $config = $param + $config;

        }


        if (!file_exists($config['upload_path'])) {
            mkdir($config['upload_path'], 0777, true);
        }


        $CI->load->library('upload');

        $CI->upload->initialize($config);

        if (!empty($config['file_name'])) $file_Status = $CI->upload->do_upload($config['file_name']);

        else $file_Status = $CI->upload->do_upload();

        if (!$file_Status) {

            return array('STATUS' => FALSE, 'FILE_ERROR' => $CI->upload->display_errors());

        } else {


            $uplaod_data = $CI->upload->data();

            // if(empty($param['resize_width'])&&empty($param['resize_height'])){
            //      // $original_height = $uplaod_data['image_height'];
            //      // $original_width =  $uplaod_data['image_width'];
            //         $config['resize_width']=175;
            //         $config['resize_height']=150;
            //  }

            $upload_file = explode('.', $uplaod_data['file_name']);


            if ($config['image_resize'] && in_array($upload_file[1], array('gif', 'jpeg', 'jpg', 'png', 'bmp', 'jpe'))) {

                $param2 = array(

                    'source_image' => $config['source_image'] . $uplaod_data['file_name'],

                    'new_image' => $config['new_image'] . $uplaod_data['file_name'],

                    'create_thumb' => FALSE,

                    'maintain_ratio' => TRUE,

                    'width' => $config['resize_width'],

                    'height' => $config['resize_height'],

                );


                image_resize($param2);

            }

            if (empty($config['image_resize']) && in_array($upload_file[1], array('gif', 'jpeg', 'jpg', 'png', 'bmp', 'jpe'))) {

                $param3 = array(
                    'file_name' => $uplaod_data['file_name'],

                    'source_image' => $config['source_image'] . $uplaod_data['file_name'],

                    'new_image' => $config['new_image'] . $uplaod_data['file_name'],

                    'maintain_ratio' => 0,

                    'width' => $config['resize_width'],

                    'height' => $config['resize_height'],

                );

                create_frontend_thumbnail($param3);

            }
            return array('STATUS' => TRUE, 'UPLOAD_DATA' => $uplaod_data);

        }

    }


}

function create_frontend_thumbnail($config_img = '')

{


    $CI = &get_instance();

    $config_image['image_library'] = 'gd2';

    $config_image['source_image'] = $config_img['source_image'];

    $config_image['file_name'] = $config_img['file_name'];

    $config_image['new_image'] = $config_img['new_image'];

    $config_image['height'] = $config_img['height'];

    $config_image['width'] = $config_img['width'];


    list($width, $height, $type, $attr) = getimagesize($config_image['source_image']);

    if ($width < $height) {

        $cal = $width / $height;

        $config_image['width'] = $config_img['width'] * $cal;

    }

    if ($height < $width) {

        $cal = $height / $width;

        $config_image['height'] = $config_img['height'] * $cal;

    }

    $CI->load->library('image_lib');
    $CI->image_lib->initialize($config_image);
    if (!$CI->image_lib->resize()) return array('status' => FALSE, 'error_msg' => $CI->image_lib->display_errors());

    else return array('status' => TRUE, 'file_name' => $config_image['file_name']);

}


function weekDays()
{
    return array(
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Firday",
        "Saturday",
        "Sunday",
    );
}

function checkedValue($value1, $value2)
{
    return ($value1 == $value2) ? "checked" : "";
}

function selectedValue($value1, $value2)
{
    return ($value1 == $value2) ? "selected" : "";
}

function datetimeFormat($str)
{
    $CI = &get_instance();
    $user_info = $CI->session->userdata('user_info');

    $datetime = new DateTime($str);
    $timeZone = "America/Argentina/Buenos_Aires";
    if (!empty($user_info['timezone'])) {
        $timeZone = $user_info['timezone'];
    }
    $la_time = new DateTimeZone($timeZone);
    $datetime->setTimezone($la_time);
    return $datetime->format("d-m-Y H:i:s");
}

function GoogleMapKey()
{
    return "AIzaSyDGh18z6Flp61MclXTRY_HhIJkEu5jjAhM";
}

?>
