<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}

	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Privacy Policy</h1>

	<div id="body">
		<p>
            Privacy Policy
            We are committed to protecting and respecting your privacy. This Privacy Policy (this “Policy”)
            describes how we collect, use and disclose personal information of users of our services,
            including our Website (our “Site”). Please read this Privacy Policy carefully.
            By using our Site, you agree to the terms of this Policy. If you do not agree with the terms of
            this Policy, do not use our Site. Your use of our Site for the limited and exclusive purpose of
            reviewing this Policy does not constitute your agreement to this Policy unless you make further
            use of our Site. This Policy may be updated from time to time.
            This Policy describes:
            • The personal information we collect
            • How we collect such information
            • How such information is used
            • How such information is shared
            • How you can contact us
            INFORMATION WE COLLECT
            We and our third-party service providers may collect and process the following types of
            personal information about you:
            • Name
            • Phone number
            • Email address
            • Social network profile information
            • Information we collect when you use our Site, such as your domain name, Internet
            protocol (IP) address, mobile device model, Internet service provider, Site access times,
            websites that referred you to us and web pages within our Site that you visit
            • Information you communicate to us through our Site, our social media pages or through
            other means
            HOW WE COLLECT INFORMATION
            We and our third-party service providers may collect personal information as follows:
            • We may collect information you provide when you use our Site, including when you fill
            in forms on our Site, subscribe to any of our services, complete a survey on our Site,
            post material to our Site or download content from our Site.
            • We may collect information you provide when you contact us by phone, email, text
            message or messaging application on social media.
            • We may collect social network profile information that you have made publicly available
            through your social network account settings if you choose to access our social media
            pages or connect or otherwise link to our Site with your own social media page. We also
            may collect customers’ and public feedback on social media.
            • Our Site may use cookies, tracking pixels and other similar technologies to collect
            information about visitors to our Site. A cookie is a small amount of data that is sent to
            your browser from a Web server and stored on your computer's hard drive. A tracking
            pixel is a graphic that is loaded when a user visits a website or opens an email and is
            used to track certain user activities.
            HOW WE USE THE INFORMATION COLLECTED
            We and our third-party service providers may use information collected to:
            • Operate, maintain and improve our Site.
            • Conduct analytics to help us better understand our customers and improve our products
            and services.
            • Process and manage purchases made by you.
            • Respond to your customer service inquiries, post your comments related to our
            products and services on our social network pages and take other actions in response to
            your questions, comments or Site activity.
            • Communicate with you about special offers, services and promotions that may be of
            interest to you.
            • Help us develop, customize, deliver, support and improve our services.
            • Allow you to participate in interactive features of our service when you choose to do so.
            • Notify you about changes to our service.
            • Conduct market research in order to serve targeted advertisements.
            We and our third-party service providers may use the information collected from cookies,
            tracking pixels and other similar technologies to target advertising to you personally, through
            online and offline methods including email, display media, video media and direct mail. You
            may opt out of receiving direct mail from us or our third-party service providers by clicking
            here. For detailed information on the cookies we use and the purposes for which we use them
            see our Cookie Policy.
            When you log in to or visit our Site, your IP address may be combined with other de-identified
            data (such as a hashed, non-readable email or postal address) and such information may be
            used by third-party service providers to send ads and materials to you based on your
            preferences, interests and attributes. Such information may also be combined with aggregate
            information collected from other users or sources and used by third-party service providers to
            conduct market research and to better target their advertising. Although you may not opt out
            of receiving online advertisements generally, you may find out how to opt out of having your
            online behavior collected by third-party advertisers for advertising purposes. You may visit each
            ad network's website individually to opt-out and review their privacy policies, or you may visit
            the Digital Advertising Alliance’s opt-out website at http://www.aboutads.info/ or the Network
            Advertising Initiative’s opt-out website at http://networkadvertising.org/.
            HOW WE DISCLOSE THE INFORMATION COLLECTED
            We and our third-party service providers may share your personal information under the
            following circumstances:
            • We may share your personal information with any member of our group of companies
            (our subsidiaries and our ultimate parent company and its subsidiaries).
            • We may share your personal information with companies that provide services to us,
            such as credit card processors, website hosts, email vendors and other companies that
            help us provide our services or our Site.
            • We may disclose your personal information in response to legal process, when required
            to comply with laws, to combat fraudulent or criminal activity, to enforce our
            agreements, corporate policies and the terms of use of our Site, and to protect the
            rights, property and safety of our business, our employees, agents, customers or others.
            • We may share technical data that we collect about your browsing habits and your
            device (such as data collected via our cookies, tracking pixels and similar technologies,
            as discussed above) with third-party service providers and other advertising companies.
            This enables them and us to better target ads to you and other consumers.
            HOW TO CONTACT US
            If you have any questions, comments or requests regarding this Policy, please contact us using
            the contact information shown on our Site.
            Last Updated: August 2018
        </p>
	</div>

</div>

</body>
</html>
