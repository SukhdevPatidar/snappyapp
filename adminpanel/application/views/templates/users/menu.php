<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">

            <li class="active">
                <a href="<?php echo base_url(''); ?>admin/dashboard">
                    <i class="fa fa-dashboard"></i> <span>Panel administrador</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cube"></i> <span>Categorias</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>users/category/add_category"><i class="fa fa-circle-o"></i>
                            Agregar categoría</a></li>
                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-envelope"></i> <span>Ordenes</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>users/order/add_order"><i class="fa fa-circle-o"></i> Lista
                            de ordenes</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-id-card-o"></i> <span>Personal de envío</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>users/delivery/add_boy"><i class="fa fa-circle-o"></i>
                            Agregar personal de envío</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-shopping-bag"></i> <span>Productos</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>users/product/add_product"><i class="fa fa-circle-o"></i>
                            Agrear productos</a></li>
                    <li><a href="<?php echo base_url(''); ?>users/product/product_list"><i class="fa fa-circle-o"></i>
                            Lista de productos</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-money"></i> <span>Método de pago</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>users/payment/show_ass_pay"><i class="fa fa-circle-o"></i>Asignar
                            método de pago</a></li>
                    <li><a href="<?php echo base_url(''); ?>users/payment/add_paypal_info"><i
                                    class="fa fa-circle-o"></i>Dátos de método de pago</a></li>
                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dropbox"></i> <span>Envíos</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>users/deliverycost"><i class="fa fa-circle-o"></i>Costo de
                            envíos</a></li>
                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
