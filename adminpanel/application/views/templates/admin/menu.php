<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="http://localhost/sughandi_enterprice/assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
      
        <?php
        $menu = $this->frontend_model->get_result('page',array('parent_page_id'=>'0'));
        foreach ($menu as $parent_id) {
         // $parent_id = $this->frontend_model->get_row('page',array('parent_page_id'=>'0'));
          $sub_menu = $this->frontend_model->get_row('page',array('parent_page_id'=>$parent_id->id));
          //print_r("<pre/>");
          //print_r($page);
          
               
       ?>
        <li class="treeview">
          <a href="#">
            <?php echo $parent_id->icon; ?> <span><?php echo $parent_id->page_name; ?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
         <?php
         if(isset($sub_menu) && !empty($sub_menu)){
         ?>
          <ul class="treeview-menu">
            <li><a href=""><?php echo $sub_menu->icon; ?> <?php echo $sub_menu->page_name; ?></a></li>
            
          </ul>
          <?php
        }
        ?>
        </li>
        <?php
  
 }       
        ?>
        <li class="treeview">
       
          <a href="#">
            <i class="fa fa-th"></i> <span>manage menu</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(''); ?>index.php/admin/menu/create_menu"><i class="fa fa-circle-o"></i> add Menu</a></li>
            <li><a href="<?php echo base_url(''); ?>index.php/admin/menu/show_menu"><i class="fa fa-circle-o"></i> Show</a></li>
          </ul>
        </li>
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>