<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">

            <li class="active">
                <a href="<?php echo base_url(''); ?>superadmin/dashboard">
                    <i class="fa fa-dashboard"></i> <span>Panel administrador</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-github-alt"></i> <span>Publicidad</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>backend/sliders/add_slider/1"><i class="fa fa-circle-o"></i>
                            Publicidad 1</a></li>
                    <li><a href="<?php echo base_url(''); ?>backend/sliders/add_slider/2"><i class="fa fa-circle-o"></i>
                            Publicidad 2</a></li>
                    <li><a href="<?php echo base_url(''); ?>backend/sliders/add_slider/3"><i class="fa fa-circle-o"></i>
                            Publicidad 3</a></li>
                    <li><a href="<?php echo base_url(''); ?>backend/sliders/add_slider/4"><i class="fa fa-circle-o"></i>
                            Publicidad 4</a></li>
                </ul>
            </li>





            <li class="treeview">
                <a href="#">
                    <i class="fa fa-shirtsinbulk"></i> <span>Grupos</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo base_url(''); ?>backend/businesstype/add_businesstype">
                            <i class="fa fa-circle-o"></i> Asignar grupo
                        </a>
                    </li>

                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-shirtsinbulk"></i> <span>Business Sub Categorías</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>backend/businesssubcat">
                            <i class="fa fa-circle-o"></i> Agregar Subcategory</a></li>
                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-shirtsinbulk"></i> <span>Categorías</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>backend/category">
                            <i class="fa fa-circle-o"></i> Agregar categorías</a></li>
                </ul>
            </li>




            <li class="treeview">
                <a href="#">
                    <i class="fa fa-shirtsinbulk"></i> <span>Sub Categorías</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>backend/subcategory">
                            <i class="fa fa-circle-o"></i> Agregar Subcategorías</a></li>
                </ul>
            </li>



            <li class="treeview">
                <a href="#">
                    <i class="fa fa-building"></i> <span>Ciudad</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>backend/city/add_city"><i class="fa fa-circle-o"></i>
                            Agregar ciudades</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-circle"></i> <span>Administradores</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>backend/admin/add_admin"><i class="fa fa-circle-o"></i>
                            Agregar administradores</a></li>
                </ul>
            </li>


            <!--li class="treeview">
        <a href="#">
          <i class="fa fa-th"></i> <span>Delivery Boy</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php //echo base_url(''); ?>backend/delivery/add_boy"><i class="fa fa-circle-o"></i> Add</a></li>
        </ul>
      </li-->

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-american-sign-language-interpreting"></i> <span>Asignaciones</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>backend/admin/assign"><i class="fa fa-circle-o"></i>Asignar
                            categorías</a></li>
                    <li><a href="<?php echo base_url(''); ?>backend/admin/assign_city"><i class="fa fa-circle-o"></i>Asignar
                            ciudades</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-superpowers"></i> <span>Ordenes</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>backend/order/order_list"><i class="fa fa-circle-o"></i>Lista
                            de ordenes</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dropbox"></i> <span>Productos</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>backend/order/product_list"><i class="fa fa-circle-o"></i>Lista
                            de productos</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-credit-card-alt"></i> <span>Métodos de pago</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>backend/payment/add_payment"><i class="fa fa-circle-o"></i>Agregar
                            método de pago</a></li>
                    <li><a href="<?php echo base_url(''); ?>backend/payment/assign_payment"><i
                                    class="fa fa-circle-o"></i>Asignar método de pago</a></li>
                </ul>
            </li>

            <!--       <li class="treeview">
        <a href="#">
          <i class="fa fa-th"></i> <span>Delivery</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url(''); ?>backend/deliverycost"><i class="fa fa-circle-o"></i>Delivery Cost</a></li>
        </ul>
      </li> -->

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-plus"></i> <span>Usuarios</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>backend/user_list/user_history"><i
                                    class="fa fa-circle-o"></i>Lista de usuarios</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-commenting-o"></i> <span>Notificaciones</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>backend/notifications/addNotifications"><i
                                    class="fa fa-circle-o"></i>Enviar notificaciones</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-angellist"></i> <span>Retroalimentación</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(''); ?>backend/feedback/show"><i class="fa fa-circle-o"></i>Ver
                            retroalimentación</a></li>
                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
