<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Reset Password</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
  <style>
    .success {
          text-align: center;
    padding: 10px;
    font-size: 18px;
    color: green;
    }
    .error {
          text-align: center;
    padding: 10px;
    font-size: 18px;
    color: red;
    }
  </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>Reset Password</b>
  </div>
  <?php
  if(isset($error)) {
    ?>
    <p class="login-box-msg">Invalid Request</p>
   <?php
 } else { 
    if(isset($resetId)) { ?>
  <!-- /.login-logo -->

  <div class="login-box-body">
    <p class="login-box-msg">Enter New Password</p>

    <form action="<?php echo htmlspecialchars(base_url('resetpassword/updatedPassword')); ?>" method="post">
        <input type="hidden" name="resetId" class="form-control" placeholder="Password" value="<?php echo $resetId;?>">
     
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        
        <!-- /.col -->
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Update Password</button>
        </div>
        <!-- /.col -->
      </div>
       
    </form>

   
   
  </div>
  
  <?php 
      } 
  }  ?>
  
  <!-- /.login-box-body -->
</div>
  
   <?php
         if(isset($updateError)) {
        ?>
      <div class="error">
        <?php echo $updateError;?>
      </div>
        <?php  } else if (isset($updateSuccess)){
           ?>
      <div class="success">
        <?php echo $updateSuccess;?>
      </div>
      <?php           }    ?>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url(''); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(''); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(''); ?>assets/plugins/iCheck/icheck.min.js"></script>
<script>
  $(document).ready(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
