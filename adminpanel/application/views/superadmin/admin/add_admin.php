<section class="content">
<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Nuevo administrador</li>
      </ol>
       <?php echo msg_alert_backend(); ?>
 <!-- general form elements -->
          <div class="box box-primary">

            <div class="box-header with-border form-heading ">
              <h3 class="box-title">Agregar administrador</h3>
            </div>
              <div id="alert"></div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id ="form" action="<?php //echo htmlspecialchars(current_url()); ?>" method="post"  enctype="multipart/form-data">
              <div class="box-body">
              <div class="form-group">
                <div class="col-xs-6 col-sm-4 col-md-4">
                  <label for="inputtext">Nombre</label>
                  <input type="text" required="" class="form-control" id="f_name" name="f_name" placeholder="Ingresar nombre">
                </div>
                <?php echo form_error('f_name'); ?>
                <div class="col-xs-6 col-sm-4 col-md-4">
                  <label for="inputtext">Apellidos</label>
                  <input type="text" required="" class="form-control" id="l_name" name="l_name" placeholder="Ingresar apellidos">
                </div>
                <?php echo form_error('l_name'); ?>
                <div class="col-xs-6 col-sm-4 col-md-4">
                  <label for="inputtext">Correo electrónico</label>
                  <input type="email" required="" class="form-control" id="email" name="email" placeholder="Ingresar correo electrónico">
                </div>
                <?php echo form_error('email'); ?>
               <div class="col-xs-6 col-sm-4 col-md-4">
                  <label for="inputtext">Número de teléfono</label>
                  <input type="text" required="" class="form-control" id="mob" name="mob" placeholder="Ingresar número de teléfono">
                </div>
                <?php echo form_error('mob'); ?>
                <div class="col-xs-6 col-sm-4 col-md-4">
                  <label for="inputtext">Contraseña</label>
                  <input type="password" required="" class="form-control" id="password" name="password" placeholder="Ingresar contraseña">
                </div>
               <?php echo form_error('password'); ?>
               <div class="col-xs-6 col-sm-4 col-md-4">
                  <label for="inputtext">Nombre de ciudad</label>
                  <input type="text" required="" class="form-control" id="city_name" name="city_name" placeholder="Ingresar ciudad">
                </div>
                <?php echo form_error('city_name'); ?>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer form-actions">
              	<div class="col-xs-6 col-sm-12 col-md-12">
               		<button type="submit" name="add_admin" id="add_admin" class="btn btn-success btn btn-primary">Guardar</button>
                </div>
              </div>
            </form>
     </div>
          <!-- /.box -->
</section>  

 <section class="content">
<div class="box box-primary">
      <div class="table-responsive">
          <table class="table table-hover data-table-export">
            <thead>
              <th>#</th>
              <th>Nombre</th>
              <th>Correo electrónico</th>                        
              <th>Número de teléfono</th>                        
              <th>Acción</th>                        
            </thead>
            <tbody id="data">
            <?php
            $i="1";
            if(isset($admin_list) && !empty($admin_list)){
            foreach($admin_list as $data){
            ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $data->name; ?></td>
                <td><?php echo $data->email; ?></td>
                <td><?php echo $data->mobile; ?></td>
                <td><button type="button" value="<?php echo $data->id; ?>" class="btn btn-warning btn btn_edit edit_admin">Editar</button>
                  <button type="button" value="<?php echo $data->id; ?>" id="del_admin" class="btn btn-danger btn btn_edit get_city">Borrar</button>
            
              </td>
                </tr>
              <?php
            }
          }
              ?>
                </tbody>
            </table>

            </div>
            </div>
</section>
<script type="text/javascript">
  $(document).on("click","#add_admin",function(){
    var f_name = $("#f_name").val();
    var l_name = $("#l_name").val();
    var email = $("#email").val();
    var mob = $("#mob").val();
    var password = $("#password").val();
    var city_name = $("#city_name").val();
    var c = 1;
    $("#alert").html('');
    $("#alert").fadeIn();
    if(f_name == '' || l_name == '' || email == '' || mob == '' || password == '' || city_name == ''){
        alert("Por favor ingresar información completa");
    }
    else{
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/admin/insert_admin'); ?>",
          data : {'f_name' : f_name , 'l_name' : l_name ,'email' : email ,'mob' : mob ,'password' : password ,'city_name' : city_name ,'add_admin' : '1'},
          success : function(data){
            if(data != false){
              $("#f_name").val('');
              $("#l_name").val('');
              $("#email").val('');
              $("#mob").val('');
              $("#password").val('');
              $("#city_name").val('');
              $("#alert").html(data);
              $("#alert").fadeOut(8000);
              setInterval (loadLog(c), 7500);
            }
          } 
        });
        return false;
    }
    function loadLog(){
    if(c == 1){   
    $.ajax({
      url: "<?php echo base_url('backend/admin/get_update_admin_value'); ?>",
      cache: false,
      success: function(html){    
        $("#data").html(html); //Insert chat log into the #chatbox div        
          
        },
    });
  }
  return 2;
  }
});
</script>
<script type="text/javascript">
  $(document).on("click",".edit_admin",function(){
      var row = $(this).val();
      var c = 1;
      $('div.form-heading').html('<h3 class="box-title">Agregar administrador</h3>');
      $('#f_name').val("");
      $('#l_name').val("");
      $('#email').val("");
      $('#mob').val("");
      $('#city_name').val("");
      $("div.form-actions").html('<button type="submit" name="add_admin" class="btn btn-success btn btn-primary">Guardar</button>');
      $("form#form").attr('action',"<?php echo htmlspecialchars(base_url('backend/admin/add_admin')); ?>");
      $("input#row").remove();
      if(row != ''){
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/admin/get_edit_admin'); ?>",
          dataType:"JSON",
          data : {'row' : row , 'edit_form' : '1'},
          success : function(data){         
            if(data != false){
              $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Editar administrador');
              
              $('#f_name').val(data.name);
              $('#l_name').val(data.l_name);
              $('#email').val(data.email);
              $('#mob').val(data.mobile);
              $('#city_name').val(data.city_name);                
              $('#password').val(data.password);                
             // $("#password").removeAttr("required");
              $('div.form-actions').html('<button type="submit" class="btn btn-warning btn submit_btn" id="update_admin">Guardar</button>');
              $("form#form").attr('action',"");
              $('form#form').append('<input type="hidden" name="row" id="row" value="'+data.id+'" />');
              
            } 
            
          }
        })
      }
     
  });
</script>
<script type="text/javascript">
  $(document).on("click","#update_admin",function(){
    var id = $("#row").val();
    var f_name = $("#f_name").val();
    var l_name = $("#l_name").val();
    var email = $("#email").val();
    var mob = $("#mob").val();
    var password = $("#password").val();
    var city_name = $("#city_name").val();
    var c = 1;
    $("#alert").html('');
    $("#alert").fadeIn();
    if(id == '' || f_name == '' || l_name == '' || email == '' || mob == '' || password == '' || city_name == ''){
        alert("Por favor ingresar información completa");
    }
    else{
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/admin/update_admin'); ?>",
          data : {'id' : id , 'f_name' : f_name , 'l_name' : l_name ,'email' : email ,'mob' : mob ,'password' : password ,'city_name' : city_name ,'edit_admin' : '1'},
          success : function(data){
            if(data != false){
              $("#f_name").val('');
              $("#l_name").val('');
              $("#email").val('');
              $("#mob").val('');
              $("#password").val('');
              $("#city_name").val('');
              $("#alert").html(data);
              $("#alert").fadeOut(8000);
              $("div.form-actions").html('<button type="submit" name="add_admin" class="btn btn-success btn btn-primary">Guardar</button>');
              $('div.form-heading').html('<h3 class="box-title">Agregar administrador</h3>');
              $("input#row").remove();
              setInterval (loadLog(c), 7500);
            }
          } 
        });
        return false;
    }
    function loadLog(){
    if(c == 1){   
    $.ajax({
      url: "<?php echo base_url('backend/admin/get_update_admin_value'); ?>",
      cache: false,
      success: function(html){    
        $("#data").html(html); //Insert chat log into the #chatbox div        
          
        },
    });
  }
  return 2;
  }
});
</script>
          
<script type="text/javascript">
  $(document).on("click","#del_admin",function(){
    var id = $(this).val();
    var c = 1;
    $("#alert").html('');
    $("#alert").fadeIn();
    if(id == ''){
        alert("ID no puede estar sin información");
    }
    else{
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/admin/delete_admin'); ?>",
          data : {'id' : id , 'delete_admin' : '1'},
          success : function(data){
            if(data != false){
              $("#alert").html(data);
              $("#alert").fadeOut(8000);
              setInterval (loadLog(c), 7500);
            }
          } 
        });
        return false;
    }
     function loadLog(){
    if(c == 1){   
    $.ajax({
      url: "<?php echo base_url('backend/admin/get_update_admin_value'); ?>",
      cache: false,
      success: function(html){    
        $("#data").html(html); //Insert chat log into the #chatbox div        
          
        },
    });
  }
  return 2;
  }
});
</script>    