<section class="content1">
<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Asignar</li>
      </ol>
       <?php echo msg_alert_backend(); ?>
 <!-- general form elements -->
       <div class="box-header with-border form-heading ">
              <h3 class="box-title">Administrador : <?php $name = get_user_name($admin_id);
                echo isset($name->name)?$name->name:''; ?></h3><br/>
                <a class="btn btn-success" href="<?php echo base_url('backend/admin/assign_city') ?>">Regresar</a>
            </div>   
          <!-- /.box -->
</section>  

 <section class="content">
<div class="box box-primary">
      <div class="table-responsive">
          <table class="table table-hover data-table-export">
            <thead>
              <th>#</th>
              <th>Ciudad</th>                                            
                                      
            </thead>
            <tbody>
            <?php
            $i="1";
            if(isset($city_list) && !empty($city_list)){
            foreach($city_list as $data){
            $ct = array();
              $cat_name = explode(",",$data->city);
              
              foreach ($cat_name as $name) {
                $cate = get_city_name($name);
                $ct[] = isset($cate->city_name)?$cate->city_name:'';
              } 
            ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo implode(",",$ct); ?></td>
                
                </tr>
              <?php
            }
          }
              ?>
                </tbody>
            </table>

            </div>
            </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
    $('.edit_event').click(function(){
      var row = $(this).val();
      $('div.form-heading').html('<h3 class="box-title">Agregar ciudad</h3>');
      $('#f_name').val("");
      $('#l_name').val("");
      $('#email').val("");
      $('#mob').val("");
      $('#city_name').val("");
      $("div.form-actions").html('<button type="submit" name="add_admin" class="btn btn-success">Guardar</button>');
      $("form#form").attr('action',"<?php echo htmlspecialchars(base_url('backend/admin/add_admin')); ?>");
      $("input#row").remove();
      if(row != ''){
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/admin/get_edit_admin'); ?>",
          dataType:"JSON",
          data : {'row' : row , 'edit_form' : '1'},
          success : function(data){         
            if(data != false){
              $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Editar administrador');
              $('#event_name').val(data.event_name);
              $('#ttl_days').val(data.total_days);
              
              $('#f_name').val(data.name);
              $('#l_name').val(data.l_name);
              $('#email').val(data.email);
              $('#mob').val(data.mobile);
              $('#city_name').val(data.city_name);                
              
              $('div.form-actions').html('<button type="submit" class="btn btn-success submit_btn">Actualizar</button>');
              $("form#form").attr('action',"<?php echo htmlspecialchars(base_url('backend/admin/edit_admin')); ?>");
              $('form#form').append('<input type="hidden" name="row" id="row" value="'+data.id+'" />');
              

            } 
            
          }
        })
      }
    });
  });

</script>
          