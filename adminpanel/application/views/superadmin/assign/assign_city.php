<section class="content">
<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Asignar</li>
      </ol>
       <?php echo msg_alert_backend(); ?>
 <!-- general form elements -->
          <div class="box box-primary">

            <div class="box-header with-border form-heading ">
              <h3 class="box-title">Asignar ciudad </h3>
            </div>
            <div id="alert"></div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id ="form" action="" method="post"  enctype="multipart/form-data">
              <div class="box-body">
              <div class="form-group">
                <div class="col-xs-6 col-sm-4 col-md-4">
                    <label for="inputPassword3" >Seleccionar administrador </label>
                    <select  class="form-control" required  name="admin" id="admin">
                    <option value="" selected>-- Seleccionar administrador --</option>
                    <?php
                      if(!empty($admin_list))
                      {
                        foreach($admin_list as $row)
                        {
                          
                          ?>
                          <option value="<?php echo $row->id; ?>"><?php echo $row->name." ".$row->l_name; ?></option>
                          <?php
                        }
                      }
                    ?>
                    </select>
                    
                    
                    <?php echo form_error('admin'); ?>
                  </div>
                
                <div class="col-xs-6 col-sm-4 col-md-4">
                   <label for="inputPassword3" >Seleccionar ciudad </label>
                    <select  class="form-control" required name="city" id="city">
                    <option value="" selected>-- Seleccionar ciudad --</option>
                    <?php
                      if(!empty($city_list))
                      {
                        foreach($city_list as $row)
                        {
                          
                          ?>
                          <option value="<?php echo $row->id; ?>"><?php echo $row->city_name; ?></option>
                          <?php
                        }
                      }
                    ?>
                    </select>
                    
                    
                    <?php echo form_error('city'); ?>
                </div>
               
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer form-actions">
                <button type="submit" name="assign_role" id="assgn_city_role" class="btn btn-success">Guardar</button>
              </div>
            </form>
     </div>
          <!-- /.box -->
</section>  

 <section class="content">
<div class="box box-primary">
      <div class="table-responsive">
          <table class="table table-hover data-table-export">
            <thead>
              <th>#</th>
              <th>Nombre</th>                       
              <th>Acción</th>                        
            </thead>
            <tbody id="data">
            <?php
            $i="1";
            if(isset($role_list) && !empty($role_list)){
            foreach($role_list as $data){
               $c = array();
            $ct = array();
              $city_name = explode(",",$data->city);
              foreach ($city_name as $name) {
                if(!empty($name)){
                  $city = get_city_name($name);                 
                $cname = isset($city->city_name)?$city->city_name:'';
                $c[] = $cname; 
              }
           }
             
            ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php 
                $name = get_user_name($data->admin_name);
                echo isset($name->name)?($name->name." ".$name->l_name):'';
                 ?></td>
                <td> <a href="<?php echo base_url('backend/admin/view_assign_city/'.$data->admin_name) ?>" class="btn btn-success btn cancel_btn">Ver</a>
                 <button type="button" value="<?php echo $data->id; ?>" id="del_city" class="btn btn-danger btn btn_edit get_city">Borrar</button>
            
              </td>
                </tr>
              <?php
            }
          }
              ?>
                </tbody>
            </table>

            </div>
            </div>
</section>
<script type="text/javascript">
  $(document).on("click","#assgn_city_role",function(){
    var admin = $("#admin").val();
    var city = $("#city").val();
    var c = 1;
    $("#alert").html('');
    $("#alert").fadeIn();
    if(admin == '' || city == ''){
        alert("Por favor ingresar información completa");
    }
    else{
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/admin/insert_city_role'); ?>",
          data : {'admin' : admin ,'city' : city , 'assign_role_city' : '1'},
          success : function(data){
            if(data != false){
              $("#admin").val('');
              $("#city").val('');
              $("#alert").html(data);
              $("#alert").fadeOut(8000);
              setInterval (loadLog(c), 7500);
            }
          } 
        });
        return false;
    }
    function loadLog(){
    if(c == 1){   
    $.ajax({
      url: "<?php echo base_url('backend/admin/get_update_assign_city'); ?>",
      cache: false,
      success: function(html){    
        $("#data").html(html); //Insert chat log into the #chatbox div        
          
        },
    });
  }
  return 2;
  }
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.edit_event').click(function(){
      var row = $(this).val();
      $('div.form-heading').html('<h3 class="box-title">Agregar ciudad</h3>');
      $('#f_name').val("");
      $('#l_name').val("");
      $('#email').val("");
      $('#mob').val("");
      $('#city_name').val("");
      $("div.form-actions").html('<button type="submit" name="add_admin" class="btn btn-success">Guardar</button>');
      $("form#form").attr('action',"<?php echo htmlspecialchars(base_url('backend/admin/add_admin')); ?>");
      $("input#row").remove();
      if(row != ''){
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/admin/get_edit_admin'); ?>",
          dataType:"JSON",
          data : {'row' : row , 'edit_form' : '1'},
          success : function(data){         
            if(data != false){
              $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Editar administrador');
              $('#event_name').val(data.event_name);
              $('#ttl_days').val(data.total_days);
              
              $('#f_name').val(data.name);
              $('#l_name').val(data.l_name);
              $('#email').val(data.email);
              $('#mob').val(data.mobile);
              $('#city_name').val(data.city_name);                
              
              $('div.form-actions').html('<button type="submit" class="btn btn-success btn submit_btn">Actualizar</button>');
              $("form#form").attr('action',"<?php echo htmlspecialchars(base_url('backend/admin/edit_admin')); ?>");
              $('form#form').append('<input type="hidden" name="row" id="row" value="'+data.id+'" />');
              

            } 
            
          }
        })
      }
    });
  });

</script>
<script type="text/javascript">
  $(document).on("click","#del_city",function(){
    var id = $(this).val();
    var c = 1;
    $("#alert").html('');
    $("#alert").fadeIn();
    if(id == ''){
        alert("id is blank");
    }
    else{
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/admin/delete_assign_city'); ?>",
          data : {'id' : id , 'delete_city' : '1'},
          success : function(data){
            if(data != false){
              $("#alert").html(data);
              $("#alert").fadeOut(8000);
              setInterval (loadLog(c), 7500);
            }
          } 
        });
        return false;
    }
    function loadLog(){
    if(c == 1){   
    $.ajax({
      url: "<?php echo base_url('backend/admin/get_update_assign_city'); ?>",
      cache: false,
      success: function(html){    
        $("#data").html(html); //Insert chat log into the #chatbox div        
          
        },
    });
  }
  return 2;
  }
});
</script>       