<section class="conteant">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Lista de ordenes</li>
    </ol>
    <?php echo msg_alert_backend(); ?>
    <!-- general form elements -->
    <div class="box box-primary">

        <div class="box-header with-border form-heading ">
            <h3 class="box-title">Lista de ordenes</h3>
            <form action="<?php echo htmlspecialchars(current_url()) ?>" method="post">
                <div class="form-group">
                    <div class="col-md-4">
                        <label>Administrador</label>
                        <select class="form-control" name="admin_id">
                            <option value="">Seleccionar administrador</option>
                            <?php
                            if (isset($admins_list) && !empty($admins_list)) {
                                foreach ($admins_list as $admin) {
                                    ?>
                                    <option value="<?php echo $admin->id; ?>"><?php echo $admin->name . " " . $admin->l_name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label>Estatus de orden</label>
                        <select class="form-control" name="order_sta">
                            <option value="">Seleccionar estatus</option>
                            <?php
                            if (isset($order_status) && !empty($order_status)) {
                                foreach ($order_status as $st_list) {
                                    ?>
                                    <option value="<?php echo $st_list->order_status; ?>"><?php echo $st_list->order_status; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label></label>
                        <button type="submit" name="search" id="search" class="btn btn-primary">Buscar</button>
                    </div>

                </div>
            </form>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

    </div>
    <!-- /.box -->
</section>

<section class="content">
    <div class="box box-primary">
        <div class="table-responsive">
            <table id="example2" class="table table-hover data-table-export">
                <thead>
                <th>#</th>
                <th>Nombre de administrador</th>
                <th>Número de orden</th>
                <th>Usuario</th>
                <th>Metodo de pago</th>
                <th>Total de la orden</th>
                <th>Estatus de la orden</th>
                <th>Estatus del pago</th>
                <th>Fecha de creación</th>
                <th>Acción</th>
                </thead>
                <tbody>
                <?php
                $i = "1";

                if (isset($search_list) && !empty($search_list)) {
                    foreach ($search_list as $data) {
                        if (!empty($data->provider_id)) {
                            $admin = get_user_name($data->provider_id);
                            if (!empty($admin)) ;
                            $a_name = isset($admin->name) ? $admin->name : '';
                            $al_name = isset($admin->l_name) ? $admin->l_name : '';
                            $name = $a_name . " " . $al_name;
                        } else {
                            $name = "-";
                        }
                        $user = $this->city_model->get_row('users', array('id' => $data->user_Id));
                        $order_prc = $this->city_model->get_result('order_products', array('order_id' => $data->id), array("*", "price as product_price"));

                        $tot_order_prc = '';
                        foreach ($order_prc as $price) {
                            $tot_order_prc = $tot_order_prc + intval($price->quantity) * floatval($price->product_price);
                        }
                        $total = $tot_order_prc + $data->delievery_price;
                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $name; ?></td>
                            <td><?php echo $data->id; ?></td>
                            <td><?php echo $user->name . " " . $user->l_name; ?></td>
                            <td><?php echo $data->name; ?></td>
                            <td><?php echo $total; ?></td>
                            <td><?php echo $data->order_status; ?></td>
                            <td><?php echo $data->payment_status; ?></td>
                            <td><?php echo $data->created_at; ?></td>
                            <td><a href="<?php echo base_url('backend/order/product_view/' . $data->id) ?>"
                                   class="btn btn_success">Ver</a>&nbsp;
                                <!--a href="<?php //echo base_url('backend/order/user_view/'.$data->id)
                                ?>" class="btn btn_success">User Detail</a-->
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    if (isset($order_list) && !empty($order_list)) {
                        foreach ($order_list as $data) {
                            if (!empty($data->provider_id)) {
                                $admin = get_user_name($data->provider_id);
                                if (!empty($admin)) ;
                                $a_name = isset($admin->name) ? $admin->name : '';
                                $al_name = isset($admin->l_name) ? $admin->l_name : '';
                                $name = $a_name . $al_name;
                            } else {
                                $name = "-";
                            }
                            $user = $this->city_model->get_row('users', array('id' => $data->user_Id));
                            $order_prc = $this->city_model->get_result('order_products', array('order_id' => $data->id), array("*", "price as product_price"));

                            $tot_order_prc = 0;
                            foreach ($order_prc as $price) {
                                $tot_order_prc = $tot_order_prc + intval($price->quantity) * floatval($price->product_price);
                            }
                            $total = $tot_order_prc + floatval($data->delievery_price);
                            ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $name; ?></td>
                                <td><?php echo $data->id; ?></td>
                                <td><?php echo $user->name . " " . $user->l_name; ?></td>
                                <td><?php echo $data->name; ?></td>
                                <td><?php echo $total; ?></td>
                                <td><?php echo $data->order_status; ?></td>
                                <td><?php echo $data->payment_status; ?></td>
                                <td><?php echo $data->created_at; ?></td>
                                <td><a href="<?php echo base_url('backend/order/product_view/' . $data->id) ?>"
                                       class="btn btn_success">Ver</a></td>
                            </tr>
                            <?php
                        }
                    }
                }
                ?>
                </tbody>
            </table>

        </div>
    </div>

</section>

<script type="text/javascript">
    $(document).ready(function () {
        $('.edit_event').click(function () {
            var row = $(this).val();
            $('div.form-heading').html('<h3 class="box-title">Add City</h3>');
            $('#f_name').val("");
            $('#l_name').val("");
            $('#email').val("");
            $('#mob').val("");
            $('#city_name').val("");
            $("div.form-actions").html('<button type="submit" name="add_admin" class="btn btn-primary">Submit</button>');
            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('backend/admin/add_admin')); ?>");
            $("input#row").remove();
            if (row != '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('backend/admin/get_edit_admin'); ?>",
                    dataType: "JSON",
                    data: {'row': row, 'edit_form': '1'},
                    success: function (data) {
                        if (data != false) {
                            $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Editar administrador');
                            $('#event_name').val(data.event_name);
                            $('#ttl_days').val(data.total_days);

                            $('#f_name').val(data.name);
                            $('#l_name').val(data.l_name);
                            $('#email').val(data.email);
                            $('#mob').val(data.mobile);
                            $('#city_name').val(data.city_name);
                            $("#password").removeAttr("required");
                            $('div.form-actions').html('<button type="submit" class="btn submit_btn">Actualizar</button>');
                            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('backend/admin/edit_admin')); ?>");
                            $('form#form').append('<input type="hidden" name="row" id="row" value="' + data.id + '" />');
                        }
                    }
                })
            }
        });
    });
</script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        $('#example2').DataTable();
    });
</script>
