<section class="conteant">
<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Lista de ordenes</li>
      </ol>
       <?php echo msg_alert_backend(); ?>
 <!-- general form elements -->
          <div class="box box-primary">

            <div class="box-header with-border form-heading ">
              
              <table class="table">
              <?php
              if(isset($user_history) && !empty($user_history)){
              //print_r($user_history);
                $user_city = get_city_name($user_history[0]->city_id);
                $add_city = get_city_name($user_history[0]->town_city);
                ?>
                <tr>
                  <th colspan="2">Detalles de usuario</th>
                </tr>
                  <tr>
                     <td>Usuario</td><td><?php echo $user_history[0]->user_name." ".$user_history[0]->l_name; ?></td>
                  </tr>
                  <tr>
                     <td>Correo electrónico</td><td><?php echo $user_history[0]->email; ?></td>
                  </tr>
                  <tr>
                     <td>Número de teléfono</td><td><?php echo $user_history[0]->user_mobile; ?></td>
                  </tr>
                  <tr>
                     <td>Ciudad</td><td><?php echo isset($user_city->city_name)?$user_city->city_name:'-'; ?></td>
                  </tr>
                  <tr>
                  <th colspan="2">Dirección de envío</th>
                  </tr>
                  <tr>
                     <td>Nombre de perfil</td><td><?php echo $user_history[0]->address_name; ?></td>
                  </tr>
                   <tr>
                     <td>Número de teléfono</td><td><?php echo $user_history[0]->add_mobile; ?></td>
                  </tr>
                  <tr>
                     <td>Ciudad</td><td><?php echo isset($add_city->city_name)?$add_city->city_name:'-'; ?></td>
                  </tr>
                  <tr>
                     <td>Dirección</td><td><?php echo $user_history[0]->house_no." ".$user_history[0]->colony_street." ".$user_history[0]->landmark." ".$user_history[0]->pincode; ?></td>
                  </tr>
                  <tr>
                     <td>Punto de referencia</td><td><?php echo $user_history[0]->landmark; ?></td>
                  </tr>
                   <tr>
                     <td>Otros</td><td><?php echo $user_history[0]->other; ?></td>
                  </tr>
                   <tr>
                     <td>Empresa</td><td><?php echo $user_history[0]->company; ?></td>
                  </tr>
				  <tr>
					<th>Otros</th>
				  </tr>
				  <tr>
                     <td>Otras notas</td><td><?php echo $user_history[0]->order_notes; ?></td>
                  </tr>
				  
                <?php
              }
              else{
                echo "No Records Available";
              }
              ?>
                
              </table>
              <h3 class="box-title">Lista de ordenes</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
          
     </div>
          <!-- /.box -->
</section>  

 <section class="content">
<div class="box box-primary">
      <div class="table-responsive">
          <table id="example" class="table table-hover data-table-export">
            <thead>
              <th>#</th>
              <th>Nombre del producto</th>
              <th>Método de pago</th>
              <th>Estatus de la orden</th>
              <th>Detalles extras</th>
              <th>Cantidad</th>
              <th>Precio de producto</th>
              <th>Precio de envío</th>                        
              <th>Imangen de producto</th>                                               
              <th>Precio total</th>                                               
            </thead>
            <tbody>
            <?php
            $i="1";
            $tot_quan = '';
              $tot_pro_prc = '';
              $del_chrg = '';
              $total = '';
            $total_price = '';
            if(isset($history) && !empty($history)){
            foreach($history as $data){
              $ci = & get_instance();
              $sql= $ci->db->query("select pm.name, o.payment_status, o.delievery_price from `orders` as o inner join `payment_methods` as pm on o.payment_method_id = pm.id where o.id = '".$data->order_id."'");
              $pay_name = $sql->result();

              $total_price = ($data->quantity * ($data->promotional_price?$data->promotional_price:$data->product_price)) + $pay_name[0]->delievery_price;
              $tot_quan = $tot_quan +  $data->quantity;
               $tot_pro_prc = $tot_pro_prc +  ($data->promotional_price?$data->promotional_price:$data->product_price);
               $del_chrg = $del_chrg + $pay_name[0]->delievery_price;
               $total = $total +  $total_price;
            ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $data->product_name; ?></td>
                <td><?php echo $pay_name[0]->name; ?></td>
                <td><?php echo $pay_name[0]->payment_status; ?></td>
                <td><?php echo $data->extra_detail; ?></td>
                <td><?php echo $data->quantity; ?></td>
                <td><?php echo ($data->promotional_price?$data->promotional_price:$data->product_price); ?></td>
                <td><?php echo $pay_name[0]->delievery_price; ?></td>
                <td><img src="<?php echo base_url().$data->product_img; ?>" width="50" height="50"></td>
                <td><?php echo $total_price; ?></td>
              </tr>
              <?php
            }
          }
              ?>
              <tr>
                <td>Total</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><?php echo $tot_quan; ?></td>
                <td><?php echo $tot_pro_prc; ?></td>
                <td><?php echo $del_chrg; ?></td>
                <td></td>
                <td><?php echo $total; ?></td>
              </tr>
                </tbody>
            </table>

            </div>
            </div>

</section>
