<section class="content">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Business Sub Categorias</li>
    </ol>
    <?php echo msg_alert_backend(); ?>
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border form-heading">
            <h3 class="box-title">Agregar Sub categoría</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="form" action="<?php echo base_url('backend/businesssubcat/add_category'); ?>" method="post"
              enctype="multipart/form-data">

            <?php
            if (isset($editCategoryData)) {
                ?>
                <input type="hidden" name="businesssubcat_id" value="<?= $editCategoryData->id ?>">
                <?php
            }
            ?>

            <div class="box-body">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <label for="inputtext">Business Group</label>
                    <select class="form-control" name="businesstype" id="businesstype" required>
                        <option value="">Select Business Group</option>
                        <?php
                        if (isset($businessTypes)) {
                            foreach ($businessTypes as $businessType) {
                                ?>
                                <option value="<?php echo $businessType->id; ?>" <?= isset($editCategoryData) ? selectedValue($businessType->id, $editCategoryData->businesstype_id) : '' ?>><?php echo $businessType->name; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                    <?php echo form_error('businesstype'); ?>
                </div>



                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="inputtext">Nombre de categoría</label>
                        <input type="text" value="<?= isset($editCategoryData) ? $editCategoryData->name : '' ?>"
                               class="form-control" id="businesssubcat_name" name="businesssubcat_name" required
                               placeholder="Ingresa nombre">
                        <?php echo form_error('businesssubcat_name'); ?>
                    </div>
                </div>


                <!--                <div class="col-xs-12 col-sm-12 col-md-12">-->
                <!--                    <div class="form-group">-->
                <!--                        <label for="comment">Description:</label>-->
                <!--                        <textarea class="form-control" rows="5" id="description" required-->
                <!--                                  name="description"></textarea>-->
                <!--                        --><?php //echo form_error('description'); ?>
                <!--                    </div>-->
                <!--                </div>-->

            </div>


            <div class="box-footer form-actions">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <button type="submit" name="add_subcategory" id="add_subcategory"
                            class="btn btn-success btn btn-primary">
                        Guardar
                    </button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.box -->
</section>

<section class="content">
    <div class="box box-primary">
        <div class="table-responsive">
            <table class="table  table-hover data-table-export" id="">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre de categoría</th>
                    <th>Business Group</th>
                    <th>Fecha de creación</th>
                    <th>Acción</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($category_list)) {
                    $i = 1;
                    foreach ($category_list as $row) {

                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $row->name; ?></td>
                            <td><?php echo $row->business_type; ?></td>
                            </td>
                            <td><?php echo date('d-m-Y', strtotime($row->created_at)); ?></td>
                            <td>
                                <a href="<?php echo base_url('backend/businesssubcat/edit/' . $row->id) ?>"
                                   class="btn btn-warning btn btn_edit">Editar</a>

                                <a href="<?php echo base_url('backend/businesssubcat/delete/' . $row->id) ?>"
                                   onclick="return confirm('If Category Already Assign And If You Remove This Category All Record Will Be delete');"
                                   class="btn btn-danger btn cancel_btn">Borrar</a>

                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>

            </table>

        </div>
    </div>
</section>
<script type="text/javascript">

</script>
