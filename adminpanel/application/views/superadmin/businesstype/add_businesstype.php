<section class="content">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Asignar grupo</li>
    </ol>
    <?php echo msg_alert_backend(); ?>
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border form-heading">
            <h3 class="box-title">Agregar grupo de negocios</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="form" action="<?php echo base_url('backend/businesstype/add_businesstype'); ?>"
              method="post"
              enctype="multipart/form-data">
            <div class="box-body">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="inputtext">Nombre</label>
                        <input type="text" required="" value="" class="form-control" id="group_name" name="group_name"
                               placeholder="Enter business group name">
                    </div>
                    <?php echo form_error('group_name'); ?>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="">Imagen de grupo</label>
                        <input type="file" required=""
                               value="" class="form-control" id="c_image" name="c_image">
                    </div>
                    <?php echo form_error('c_image'); ?>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 " style="padding: 16px;">
                    <label for="inputtext" style="padding-right: 16px">Mostrar grande</label>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="show_big" value="1"
                               name="show_big"> &nbsp;
                    </label>
                </div>


            </div>
            <!-- /.box-body -->
            <div id="img_show"></div>

            <div class="box-footer form-actions">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <button type="submit" name="add_businesstype" id="add_businesstype"
                            class="btn btn-success btn btn-primary">
                        Save
                    </button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.box -->
</section>

<section class="content">
    <div class="box box-primary">
        <div class="table-responsive">
            <table class="table  table-hover data-table-export" id="">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Imagen</th>
                    <th>Acción</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($businesstype_list)) {
                    $i = 1;
                    foreach ($businesstype_list as $row) {

                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $row->name; ?></td>
                            <td><img src="<?php echo base_url('') . $row->imagepath; ?>" width="100" height="100">
                            </td>
                            <td>
                                <button type="button" value="<?php echo $row->id; ?>"
                                        class="btn btn-warning btn btn_edit edit_event">Editar
                                </button>

                                <a href="<?php echo base_url('backend/businesstype/delete_businesstype/' . $row->id) ?>"
                                   onclick="return confirm('If Business Group have already business And If You Remove This Group All Record Will Be delete');"
                                   class="btn btn-danger btn cancel_btn">Borrar</a>

                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>

            </table>

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('.edit_event').click(function () {
            var row = $(this).val();
            $('div.form-heading').html('<h3 class="box-title">Add Business Group</h3>');
            $('#name').val("");
            $("div.form-actions").html('<button type="submit" name="add_businesstype" class="btn btn-primary">Save</button>');
            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('backend/businesstype/add_businesstype')); ?>");
            $("input#row").remove();
            if (row != '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('backend/businesstype/get_edit_businesstype'); ?>",
                    dataType: "JSON",
                    data: {'row': row, 'edit_form': '1'},
                    success: function (data) {
                        if (data != false) {
                            $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Edit Business Group');
                            $('#group_name').val(data.name);
                            $("#c_image").removeAttr("required");
                            $('#img_show').html('<img src="<?php echo base_url(''); ?>' + data.imagepath + '" width="50" height="50">');
                            $('div.form-actions').html('<button type="submit" style="margin-left: 15px" class="btn submit_btn">Actualizar</button>');
                            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('backend/businesstype/edit_businesstype')); ?>");
                            $('form#form').append('<input type="hidden" name="row" id="row" value="' + data.id + '" />');
                            $("#show_big").prop("checked", (data.show_big == 1));

                            $("html, body").animate({scrollTop: 0}, "slow");
                        }

                    }
                })
            }
        });
    });
</script>
