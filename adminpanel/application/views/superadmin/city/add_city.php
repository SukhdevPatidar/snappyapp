<section class="content">
<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Ciudad</li>
      </ol>
       <?php echo msg_alert_backend(); ?>
 <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border form-heading">
              <h3 class="box-title">Agregar ciudad</h3>
            </div>
            <div id="alert"></div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="form" action="" method="post"  enctype="multipart/form-data">
              <div class="box-body">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <label for="inputtext">Nombre de ciudad</label>
                  <input type="text" required="" class="form-control" id="city_name" name="city_name" placeholder="Ingresar ciudad">
                </div>
                </div>
               
              </div>
              <!-- /.box-body -->

              <div class="box-footer form-actions">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <button type="submit" name="add_city" id="add_city" class="btn btn-success">Guardar</button>
                </div>
              </div>
            </form>
     </div>
          <!-- /.box -->
</section>  

 <section class="content">
<div class="box box-primary">
      <div class="table-responsive">
          <table class="table table-hover data-table-export">
            <thead>
              <th>#</th>
              <th>Nombre de ciudad</th>
              <th>Acción</th>                        
            </thead>
            <tbody id="data">
            <?php
            $i="1";
            if(isset($city) && !empty($city)){
            foreach($city as $data){
            ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $data->city_name; ?></td>
                <td><button type="button" value="<?php echo $data->id; ?>" id="get_city" class="btn btn-warning btn btn_edit get_city">Editar</button>
                <button type="button" value="<?php echo $data->id; ?>" id="del_city" class="btn btn-danger btn btn_edit">Borrar</button>
            
              </td>
                </tr>
              <?php
            }
          }
              ?>
                </tbody>
            </table>

            </div>
            </div>
</section>
<script type="text/javascript">
  $(document).on("click","#add_city",function(){
    var city_name = $("#city_name").val();
    var user_id = $("#user_id").val();
    var c = 1;
    $("#alert").html('');
    $("#alert").fadeIn();
    if(city_name == ''){
        alert("Por favor ingresar informaci��n completa");
    }
    else{
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/city/insert_city'); ?>",
          data : {'city_name' : city_name , 'add_form' : '1'},
          success : function(data){
            if(data != false){
              $("#city_name").val('');
              $("#alert").html(data);
              $("#alert").fadeOut(8000);
              setInterval (loadLog(c), 7500);
            }
          } 
        });
        return false;
    }

    function loadLog(){
    if(c == 1){   
    $.ajax({
      url: "<?php echo base_url('backend/city/get_update_value'); ?>",
      cache: false,
      success: function(html){    
        $("#data").html(html); //Insert chat log into the #chatbox div        
          
        },
    });
  }
  return 2;
  }
});
</script>
<script type="text/javascript">
  $(document).on("click","#get_city",function(){
      var row = $(this).val();
      $('div.form-heading').html('<h3 class="box-title">Agregar ciudad</h3>');
      $('#city_name').val("");
      $("div.form-actions").html('<button type="submit" name="add_admin" class="btn btn-primary">Guardar</button>');
      $("form#form").attr('action',"<?php echo htmlspecialchars(base_url('backend/city/add_city')); ?>");
      $("input#row").remove();
      if(row != ''){
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/city/get_edit_city'); ?>",
          dataType:"JSON",
          data : {'row' : row , 'edit_form' : '1'},
          success : function(data){         
            if(data != false){
              $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Editar ciudad');
              
              $('#city_name').val(data.city_name);
              $('div.form-actions').html('<button type="submit" style="margin-left: 15px" class="btn submit_btn" id="update_city">Actualizar</button>');
              $("form#form").attr('action',"");
              $('form#form').append('<input type="hidden" name="row" id="row" value="'+data.id+'" />');
              

            } 
            
          }
        })
      }
    });
</script>
<script type="text/javascript">
  $(document).on("click","#update_city",function(){
    var city_name = $("#city_name").val();
    var id = $("#row").val();
    var c = 1;
    $("#alert").html('');
    $("#alert").fadeIn();
    if(city_name == '' || id == ''){
        alert("Por favor ingresar informaci��n completa");
    }
    else{
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/city/update_city'); ?>",
          data : {'city_name' : city_name ,'id' : id , 'edit_form' : '1'},
          success : function(data){
            if(data != false){
              $("#city_name").val('');
              $("input#row").remove();
              $("#alert").html(data);
              $("#alert").fadeOut(8000);
              $("div.form-actions").html('<button type="submit" name="add_admin" class="btn btn-primary">Guardar</button>');
              $('div.form-heading').html('<h3 class="box-title">Agregar ciudad</h3>');
              setInterval (loadLog(c), 7500);
            }
          } 
        });
        return false;
    }
    function loadLog(){
    if(c == 1){   
    $.ajax({
      url: "<?php echo base_url('backend/city/get_update_value'); ?>",
      cache: false,
      success: function(html){    
        $("#data").html(html); //Insert chat log into the #chatbox div        
          
        },
    });
  }
  return 2;
  }
});
</script>
<script type="text/javascript">
  $(document).on("click","#del_city",function(){
    var x = confirm("Si la ciudad se encuentra asignada y se elimina, todo el registro se eliminar��");
    var id = $(this).val();
    var c = 1;
    $("#alert").html('');
    $("#alert").fadeIn();
    if(x == true){
      if(id == ''){
          alert("ID no se puede dejar sin datos");
      }
      else{
          $.ajax({
            type : "POST",
            url  : "<?php echo base_url('backend/city/delete_city'); ?>",
            data : {'id' : id , 'delete_city' : '1'},
            success : function(data){
              if(data != false){
                $("#alert").html(data);
                $("#alert").fadeOut(8000);
                setInterval (loadLog(c), 7500);
              }
            } 
          });
          return false;
      }
       function loadLog(){
      if(c == 1){   
      $.ajax({
        url: "<?php echo base_url('backend/city/get_update_value'); ?>",
        cache: false,
        success: function(html){    
          $("#data").html(html); //Insert chat log into the #chatbox div        
            
          },
      });
    }
    return 2;
    }
  }else{
      return false;
  }
});
</script> 

          