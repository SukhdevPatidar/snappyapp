<section class="conteant">
<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">FeedBack</li>
      </ol>
       <?php echo msg_alert_backend(); ?>
 <!-- general form elements -->
          <div class="box box-primary">

            <div class="box-header with-border form-heading ">
              <h3 class="box-title">FeedBack</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
          
     </div>
          <!-- /.box -->
</section>  

 <section class="content">
<div class="box box-primary">
      <div class="table-responsive">
          <table id="example2" class="table table-hover data-table-export">
            <thead>
              <th>#</th>
              <th>Nombre</th>
              <th>Servicio</th>
              <th>Tiempo de entrega</th>                        
              <th>Staff</th>                                               
              <th>Comentarios</th>                                               
              <th>Fecha de comentario</th>                                                                                             
              <th>Acción</th>                                                                                             
            </thead>
            <tbody>
            <?php
            $k="1";

            if(isset($all_feedback) && !empty($all_feedback)){
                foreach($all_feedback as $data){
                  if(isset($data->user_id) && !empty($data->user_id)){
                     $user =  get_user_name($data->user_id);
                     //$u_name = isset($user->name)?$user->name:'-';
                     //echo $u_name;
                   }
            ?>
              <tr>
                <td><?php echo $k++; ?></td>
                <td><?php echo isset($user->name)?$user->name:'-'; ?></td>
                <td><?php
                for($i=1;$i<5;$i++){
                   for($j=$i;$j<=$data->service;$j++){
                      echo "<span style='color:red;font-size:55px'>*</span>";
                   $i = $data->service;
                   }
                   if($i != 5){
                      echo "<span style='color:black;font-size:55px'>*</span>";
                   }
                }
                  ?></td>
                <td>
                  <?php
                for($m=1;$m<5;$m++){
                   for($n=$m;$n<=$data->delievery_time;$n++){
                      echo "<span style='color:red;font-size:55px'>*</span>";
                   $m = $data->delievery_time;
                   }
                   if($m != 5){
                      echo "<span style='color:black;font-size:55px'>*</span>";
                   }
                }
                  ?></td>
                <td><?php
                for($o=1;$o<5;$o++){
                   for($p=$o;$p<=$data->staff_behaviour;$p++){
                      echo "<span style='color:red;font-size:55px'>*</span>";
                   $o = $data->staff_behaviour;
                   }
                   if($o != 5){
                      echo "<span style='color:black;font-size:55px'>*</span>";
                   }
                }
                  ?></td>
                <td><?php echo $data->comment; ?></td>
                <td><?php echo $data->added_date; ?></td>
                <td><a href="<?php echo base_url('backend/feedback/view_detail/'.$data->id); ?>">Ver</a></td>
                </tr>
              <?php
            }
            }
              ?>
                </tbody>
            </table>

            </div>
            </div>

</section>

<script type="text/javascript">
  $(document).ready(function(){
    $('.edit_event').click(function(){
      var row = $(this).val();
      $('div.form-heading').html('<h3 class="box-title">Agregar ciudad</h3>');
      $('#f_name').val("");
      $('#l_name').val("");
      $('#email').val("");
      $('#mob').val("");
      $('#city_name').val("");
      $("div.form-actions").html('<button type="submit" name="add_admin" class="btn btn-success">Guardar</button>');
      $("form#form").attr('action',"<?php echo htmlspecialchars(base_url('backend/admin/add_admin')); ?>");
      $("input#row").remove();
      if(row != ''){
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/admin/get_edit_admin'); ?>",
          dataType:"JSON",
          data : {'row' : row , 'edit_form' : '1'},
          success : function(data){         
            if(data != false){
              $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Editar administrador');
              $('#event_name').val(data.event_name);
              $('#ttl_days').val(data.total_days);
              
              $('#f_name').val(data.name);
              $('#l_name').val(data.l_name);
              $('#email').val(data.email);
              $('#mob').val(data.mobile);
              $('#city_name').val(data.city_name);                
              $("#password").removeAttr("required");
              $('div.form-actions').html('<button type="submit" class="btn submit_btn">Actualizar</button>');
              $("form#form").attr('action',"<?php echo htmlspecialchars(base_url('backend/admin/edit_admin')); ?>");
              $('form#form').append('<input type="hidden" name="row" id="row" value="'+data.id+'" />');
              

            } 
            
          }
        })
      }
    });
  });
</script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready( function () {
    $('#example2').DataTable();
} );
</script>