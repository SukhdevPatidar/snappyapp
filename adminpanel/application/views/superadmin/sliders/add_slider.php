<section class="content">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li>Publicidad</li>
        <li class="active"><?php echo $slideIndex; ?></li>
    </ol>

    <?php echo msg_alert_backend(); ?>
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border form-heading">
            <h3 class="box-title">Agregar publicidad</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="form" action="<?php echo base_url('backend/sliders/add_slider/' . $slideIndex); ?>"
              method="post" enctype="multipart/form-data">
            <div class="box-body">

                <!--               <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <label for="inputtext">Slider Title</label>
                  <input type="text" required="" value="" class="form-control" id="slider_title" name="slider_title" placeholder="Enter title">
                </div>
                 <?php echo form_error('slider_title'); ?>
               </div>   -->

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="">Imagen</label>
                        <input type="file" value="" class="form-control" id="slider_image" name="slider_image">
                    </div>
                    <?php echo form_error('slider_image'); ?>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="banner_link">Enlace de banner</label>
                        <input type="text" class="form-control" id="banner_link" name="banner_link"
                               placeholder="Enlace de banner" value="<?= $slider ? $slider->url : '' ?>">
                    </div>
                </div>


            </div>
            <!-- /.box-body -->
            <div id="img_show"></div>

            <div class="box-footer form-actions">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <button type="submit" name="add_slider" id="add_slider" class="btn btn-success btn btn-primary">
                        Guardar
                    </button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.box -->
</section>

<section class="content">
    <div class="box box-primary">
        <div class="table-responsive">
            <table class="table  table-hover data-table-export" id="">

                <tbody>
                <?php
                if (!empty($slider)) {
                    ?>

                    <?php
                    if ($slider->url) {
                        ?>
                        <tr>
                            <td>
                                Enlace de banner
                            </td>
                        </tr>
                        <tr>
                            <td><a href="<?= $slider->url; ?>"><?= $slider->url; ?></a></td>
                        </tr>
                        <?php
                    }
                    ?>

                    <tr>
                        <td>
                            <img src="<?php echo base_url('') . $slider->image; ?>" width="400" height="400"
                                 style="object-fit: cover;">
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>

            </table>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('.edit_event').click(function () {
            var row = $(this).val();
            $('div.form-heading').html('<h3 class="box-title">Add Category</h3>');
            $('#cat_name').val("");
            $("div.form-actions").html('<button type="submit" name="add_category" class="btn btn-primary">Submit</button>');
            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('backend/category/add_category')); ?>");
            $("input#row").remove();
            if (row != '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('backend/category/get_edit_category'); ?>",
                    dataType: "JSON",
                    data: {'row': row, 'edit_form': '1'},
                    success: function (data) {
                        if (data != false) {
                            $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Edit category');
                            $('#cat_name').val(data.category_name);
                            // $('#c_image').val(data.category_img);
                            $("#c_image").removeAttr("required");
                            $('#img_show').html('<img src="<?php echo base_url(''); ?>' + data.category_img + '" width="50" height="50">');
                            $('div.form-actions').html('<button type="submit" style="margin-left: 15px" class="btn submit_btn">Update</button>');
                            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('backend/category/edit_category')); ?>");
                            $('form#form').append('<input type="hidden" name="row" id="row" value="' + data.id + '" />');


                        }

                    }
                })
            }
        });
    });
</script>