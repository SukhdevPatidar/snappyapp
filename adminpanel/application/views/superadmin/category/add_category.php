<section class="content">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Categorias</li>
    </ol>
    <?php echo msg_alert_backend(); ?>
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border form-heading">
            <h3 class="box-title">Agregar categoría</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="form" action="<?php echo base_url('backend/category/add_category'); ?>" method="post"
              enctype="multipart/form-data">
            <div class="box-body">

                <div class="col-xs-12 " style="padding: 16px;">
                    <label class="checkbox-inline">
                        <input type="checkbox" id="categoryAvailable" value="1"
                               name="categoryAvailable"> &nbsp;
                    </label>
                    <label for="categoryAvailable" style="padding-right: 16px">Category Available</label>
                </div>


                <div class="col-xs-12 col-sm-6 col-md-4 " style="padding: 16px;">
                    <label class="checkbox-inline">
                        <input type="checkbox" id="onlycalltaxi" value="1"
                               name="onlycalltaxi"> &nbsp;
                    </label>
                    <label for="onlycalltaxi" style="padding-right: 16px">Call Taxi</label>
                </div>


                <div class="col-xs-12 col-sm-6 col-md-4 " style="padding: 16px;">
                    <label class="checkbox-inline">
                        <input type="checkbox" id="onlyopenurl" value="1"
                               name="onlyopenurl"> &nbsp;
                    </label>
                    <label for="onlyopenurl" style="padding-right: 16px">Redirect Url</label>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <label for="inputtext">Business Group</label>
                    <select class="form-control" name="businesstype" id="businesstype">
                        <option value="">Select Business Group</option>
                        <?php
                        if (isset($businessTypes)) {
                            foreach ($businessTypes as $businessType) {
                                ?>
                                <option value="<?php echo $businessType->id; ?>"><?php echo $businessType->name; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <label for="inputtext">Business Subcategory</label>
                    <select class="form-control" name="businesssubcat" id="businesssubcat" required>
                        <option value="">Select Business Subcategory</option>
                    </select>
                    <?php echo form_error('businesssubcat'); ?>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="inputtext">Nombre de categoría</label>
                        <input type="text" required="" value="" class="form-control" id="cat_name" name="cat_name"
                               placeholder="Ingresa nombre">
                    </div>`
                    <?php echo form_error('cat_name'); ?>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12" id="phonenumberField" style="display: none">
                    <div class="form-group">
                        <label for="phonenumber">Phone number</label>
                        <input type="text"  value="" class="form-control"
                               id="phonenumber" name="phonenumber"
                               placeholder="Ingresa nombre">
                    </div>
                    <?php echo form_error('phonenumber'); ?>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12" id="urlField" style="display: none">
                    <div class="form-group">
                        <label for="url">Url</label>
                        <input type="text"  value="" class="form-control"
                               id="url" name="url"
                               placeholder="Ingrese la URL de redireccionamiento">
                    </div>
                    <?php echo form_error('urlField'); ?>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="inputtext">Correo electrónico (Para recibir notificaciones de ordenes)</label>
                        <input type="email" required="" value="" class="form-control" id="email" name="email"
                               placeholder="Ingresa correo electrónico">
                    </div>
                    <?php echo form_error('email'); ?>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="comment">Description:</label>
                        <textarea class="form-control" rows="5" id="description" required
                                  name="description"></textarea>
                    </div>
                    <?php echo form_error('description'); ?>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="">Imagen de categoría</label>
                        <input type="file" required=""
                               value="" class="form-control" id="c_image" name="c_image">
                    </div>
                    <?php echo form_error('c_image'); ?>
                </div>


            </div>
            <!-- /.box-body -->
            <div id="img_show"></div>

            <div class="box-footer form-actions">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <button type="submit" name="add_category" id="add_category" class="btn btn-success btn btn-primary">
                        Guardar
                    </button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.box -->
</section>

<section class="content">
    <div class="box box-primary">
        <div class="table-responsive">
            <table class="table  table-hover data-table-export" id="">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre de categoría</th>
                    <th>Business Group</th>
                    <th>Imagen de categoría</th>
                    <th>Fecha de creación</th>
                    <th>Acción</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($category_list)) {
                    $i = 1;
                    foreach ($category_list as $row) {

                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $row->category_name; ?></td>
                            <td><?php echo $row->business_type; ?></td>
                            <td><img src="<?php echo base_url('') . $row->category_img; ?>" width="100" height="100">
                            </td>
                            <td><?php echo date('d-m-Y', strtotime($row->create_date)); ?></td>
                            <td>

                                <button type="button" value="<?php echo $row->id; ?>"
                                        class="btn btn-warning btn btn_edit edit_event">Editar
                                </button>

                                <a href="<?php echo base_url('backend/category/delete_category/' . $row->id) ?>"
                                   onclick="return confirm('If Category Already Assign And If You Remove This Category All Record Will Be delete');"
                                   class="btn btn-danger btn cancel_btn">Borrar</a>

                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>

            </table>

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#businesstype').on('change', function () {
            var businesstype = $(this).val();
            $('#businesssubcat').find('option').not(':first').remove();

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('backend/category/getBusinessSubcategories'); ?>",
                dataType: "JSON",
                data: {'businesstype_id': businesstype},
                success: function (response) {
                    if (response.status) {
                        var data = response.data;
                        if (data) {
                            for (var i = 0; i < data.length; i++) {
                                var catObj = data[i];
                                $('#businesssubcat').append($('<option>', {
                                    value: catObj.id,
                                    text: catObj.name
                                }));
                            }
                        }
                    }
                }
            });
        });

        $('#phonenumberField').hide();
        $('#urlField').hide();

        $('#onlycalltaxi').on('change', function () {
            var showPhoneField = $(this).is(':checked');
            if (showPhoneField) {
                $('#onlyopenurl').prop('checked', false);
                $('#phonenumberField').show();
                $('#urlField').hide();
            } else {
                $('#phonenumberField').hide();
            }
        });

        $('#onlyopenurl').on('change', function () {
            var showUrlField = $(this).is(':checked');
            if (showUrlField) {
                $('#onlycalltaxi').prop('checked', false);
                $('#urlField').show();
                $('#phonenumberField').hide();
            } else {
                $('#urlField').hide();
            }
        });

        $('.edit_event').click(function () {
            var row = $(this).val();
            $('div.form-heading').html('<h3 class="box-title">Agregar categoría</h3>');
            $('#cat_name').val("");
            $("div.form-actions").html('<button type="submit" name="add_category" class="btn btn-primary">Guardar</button>');
            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('backend/category/add_category')); ?>");
            $("input#row").remove();
            $('#businesssubcat').find('option').not(':first').remove();

            if (row != '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('backend/category/get_edit_category'); ?>",
                    dataType: "JSON",
                    data: {'row': row, 'edit_form': '1'},
                    success: function (response) {
                        if (response.status) {
                            var data = response.data;
                            var businesssubcat = response.businesssubcat;

                            $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Editar categoría');
                            $('#cat_name').val(data.category_name);
                            $('#description').val(data.description);
                            $('#email').val(data.notification_email);
                            $('#businesstype').val(data.businesstype_id);
                            $('#phonenumber').val(data.contact_number);
                            $('#url').val(data.url);
                            // $('#c_image').val(data.category_img);
                            $("#c_image").removeAttr("required");
                            $('#img_show').html('<img src="<?php echo base_url(''); ?>' + data.category_img + '" width="50" height="50">');
                            $('div.form-actions').html('<button type="submit" style="margin-left: 15px" class="btn submit_btn">Actualizar</button>');
                            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('backend/category/edit_category')); ?>");
                            $('form#form').append('<input type="hidden" name="row" id="row" value="' + data.id + '" />');
                            $("#onlycalltaxi").prop("checked", (data.onlycalltaxi == 1));
                            $("#categoryAvailable").prop("checked", (data.category_available == 1));
                            $("#onlyopenurl").prop("checked", (data.onlyopenurl == 1));
                            if (data.onlycalltaxi == 1) {
                                $('#phonenumberField').show();
                            } else {
                                $('#phonenumberField').hide();
                            }

                            if (data.onlyopenurl == 1) {
                                $('#urlField').show();
                            } else {
                                $('#urlField').hide();
                            }


                            //Add subcategory dropdown
                            for (var i = 0; i < businesssubcat.length; i++) {
                                var catObj = businesssubcat[i];
                                if (catObj.id == data.businesssubcat_id) {
                                    $('#businesssubcat').append($('<option>', {
                                        value: catObj.id,
                                        text: catObj.name,
                                        selected: true
                                    }));
                                } else {
                                    $('#businesssubcat').append($('<option>', {
                                        value: catObj.id,
                                        text: catObj.name
                                    }));
                                }
                            }

                            $("html, body").animate({scrollTop: 0}, "slow");
                        }

                    }
                })
            }
        });
    });
</script>
