<section class="conteant">
<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Lista de usuarios</li>
      </ol>
       <?php echo msg_alert_backend(); ?>
 <!-- general form elements -->
          <div class="box box-primary">

            <div class="box-header with-border form-heading ">
              <h3 class="box-title">Lista de usuarios</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
          
     </div>
          <!-- /.box -->
</section>  

 <section class="content">
<div class="box box-primary">
      <div class="table-responsive">
          <table id="example2" class="table table-hover data-table-export">
            <thead>
              <th>#</th>
              <th>Nombre</th>
              <th>Apellidos</th>
              <th>Correo electrónico</th>                        
              <th>Teléfono</th>                                               
              <th>Ciudad</th>                                               
              <th>Fecha de creación</th>                                                                                             
            </thead>
            <tbody>
            <?php
            $i="1";

            if(isset($all_user) && !empty($all_user)){
                foreach($all_user as $data){
                  if(isset($data->city_id) && !empty($data->city_id)){
                     $city =  get_city_name($data->city_id);
                   }
            ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $data->name; ?></td>
                <td><?php echo $data->l_name; ?></td>
                <td><?php echo $data->email; ?></td>
                <td><?php echo $data->mobile; ?></td>
                <td><?php echo isset($city->city_name)?$city->city_name:''; ?></td>
                <td><?php echo $data->user_added_at; ?></td>
                </tr>
              <?php
            }
            }
              ?>
                </tbody>
            </table>

            </div>
            </div>

</section>

<script type="text/javascript">
  $(document).ready(function(){
    $('.edit_event').click(function(){
      var row = $(this).val();
      $('div.form-heading').html('<h3 class="box-title">Agregar ciudad</h3>');
      $('#f_name').val("");
      $('#l_name').val("");
      $('#email').val("");
      $('#mob').val("");
      $('#city_name').val("");
      $("div.form-actions").html('<button type="submit" name="add_admin" class="btn btn-success">Guardar</button>');
      $("form#form").attr('action',"<?php echo htmlspecialchars(base_url('backend/admin/add_admin')); ?>");
      $("input#row").remove();
      if(row != ''){
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/admin/get_edit_admin'); ?>",
          dataType:"JSON",
          data : {'row' : row , 'edit_form' : '1'},
          success : function(data){         
            if(data != false){
              $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Editar administrador');
              $('#event_name').val(data.event_name);
              $('#ttl_days').val(data.total_days);
              
              $('#f_name').val(data.name);
              $('#l_name').val(data.l_name);
              $('#email').val(data.email);
              $('#mob').val(data.mobile);
              $('#city_name').val(data.city_name);                
              $("#password").removeAttr("required");
              $('div.form-actions').html('<button type="submit" class="btn btn-success submit_btn">Actualizar</button>');
              $("form#form").attr('action',"<?php echo htmlspecialchars(base_url('backend/admin/edit_admin')); ?>");
              $('form#form').append('<input type="hidden" name="row" id="row" value="'+data.id+'" />');
              

            } 
            
          }
        })
      }
    });
  });
</script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready( function () {
    $('#example2').DataTable();
} );
</script>