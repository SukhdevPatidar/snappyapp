<section class="content">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Pagos</li>
    </ol>
    <?php echo msg_alert_backend(); ?>
    <div id="alert"></div>
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border form-heading">
            <h3 class="box-title">Agregar pago</h3>
        </div>
        <div id="alert"></div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="form" action="" method="post" enctype="multipart/form-data">
            <div class="box-body">
                <div class="form-group">
                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <label for="inputtext">Nombre</label>
                        <input type="text" required="" class="form-control" id="name" name="name"
                               placeholder="Enter Name">
                        <?php echo form_error('name'); ?>
                    </div>


                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <label for="inputtext">Selecionar método</label>
                        <select class="form-control" id="code" name="code" required>
                            <option value="">Seleccionar método</option>
                            <option value="paypal">paypal</option>
                            <option value="cod">cod</option>
                            <option value="mercadopago">mercadopago</option>
                            <!--<option value="todopago">todopago</option-->
                            <option value="payu">payu</option>
                        </select>
                        <?php echo form_error('code'); ?>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer form-actions">
                <button type="submit" name="add_pay" id="add_pay" class="btn btn-success">Guardar</button>
            </div>
        </form>
    </div>
    <!-- /.box -->
</section>

<section class="content">
    <div class="box box-primary">
        <div class="table-responsive">
            <table class="table table-hover data-table-export">
                <thead>
                <th>#</th>
                <th>Nombre</th>
                <th>Código</th>
                <th>Acción</th>
                </thead>
                <tbody id="data">
                <?php
                $i = "1";
                if (isset($payment) && !empty($payment)) {
                    foreach ($payment as $data) {
                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data->name; ?></td>
                            <td><?php echo $data->code; ?></td>
                            <td>
                                <button type="button" value="<?php echo $data->id; ?>"
                                        class="btn btn-warning btn_edit get_pay_info">Editar
                                </button>
                                <button type="button" value="<?php echo $data->id; ?>" id="del_pay_method"
                                        class="btn btn-danger btn_edit">Borrar
                                </button>

                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).on("click", "#add_pay", function () {
        var name = $("#name").val();
        var code = $("#code").val();
        var c = 1;
        $("#alert").html('');
        $("#alert").fadeIn();
        if (name == '' || code == '') {
            alert("Por favor llenar información completa");
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('backend/payment/insert_pay'); ?>",
                data: {'name': name, 'code': code, 'add_pay': '1'},
                success: function (data) {
                    if (data != false) {
                        $("#name").val('');
                        $("#code").val('');
                        $("#alert").html(data);
                        $("#alert").fadeOut(8000);
                        setInterval(loadLog(c), 7500);
                    }
                }
            });
            return false;
        }

        function loadLog() {
            if (c == 1) {
                $.ajax({
                    url: "<?php echo base_url('backend/payment/get_update_payment_detail'); ?>",
                    cache: false,
                    success: function (html) {
                        $("#data").html(html); //Insert chat log into the #chatbox div

                    },
                });
            }
            return 2;
        }
    });
</script>
<script type="text/javascript">
    $(document).on("click", ".get_pay_info", function () {
        var row = $(this).val();
        $('div.form-heading').html('<h3 class="box-title">Agregar pago</h3>');
        $('#city_name').val("");
        $("div.form-actions").html('<button type="submit" name="add_pay" id="add_pay" class="btn btn-success">Guardar</button>');
        $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('backend/payment/add_payment')); ?>");
        $("input#row").remove();
        if (row != '') {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('backend/payment/get_edit_info'); ?>",
                dataType: "JSON",
                data: {'row': row, 'edit_form': '1'},
                success: function (data) {
                    if (data != false) {
                        $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Editar información de método de pago');

                        $('#name').val(data.name);
                        $('#code').val(data.code);
                        $('div.form-actions').html('<button type="submit" name="edit_pay" id="edit_pay" class="btn btn-success">Actualizar</button>');
                        $("form#form").attr('action', "");
                        $('form#form').append('<input type="hidden" name="row" id="row" value="' + data.id + '" />');


                    }

                }
            })
        }
    });
</script>
<script type="text/javascript">
    $(document).on("click", "#edit_pay", function () {
        var name = $("#name").val();
        var code = $("#code").val();
        var c = 1;
        var id = $("#row").val();
        $("#alert").html('');
        $("#alert").fadeIn();
        if (name == '' || id == '' || code == '') {
            alert("Por favor ingresar información completa");
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('backend/payment/update_payment_info'); ?>",
                data: {'name': name, 'code': code, 'id': id, 'edit_form': '1'},
                success: function (data) {
                    if (data != false) {
                        $("#name").val('');
                        $("#code").val('');
                        $("input#row").remove();
                        $("#alert").html(data);
                        $("#alert").fadeOut(8000);
                        $("div.form-actions").html('<button type="submit" name="add_pay" id="add_pay" class="btn btn-success">Guardar</button>');
                        $('div.form-heading').html('<h3 class="box-title">Agregar pago</h3>');
                        setInterval(loadLog(c), 7500);
                    }
                }
            });
            return false;
        }

        function loadLog() {
            if (c == 1) {
                $.ajax({
                    url: "<?php echo base_url('backend/payment/get_update_payment_detail'); ?>",
                    cache: false,
                    success: function (html) {
                        $("#data").html(html); //Insert chat log into the #chatbox div

                    },
                });
            }
            return 2;
        }
    });
</script>
<script type="text/javascript">
    $(document).on("click", "#del_pay_method", function () {
        var id = $(this).val();
        var c = 1;
        $("#alert").html('');
        $("#alert").fadeIn();
        if (id == '') {
            alert("id is blank");
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('backend/payment/delete_payment_met'); ?>",
                data: {'id': id, 'delete_pay_met': '1'},
                success: function (data) {
                    if (data != false) {
                        $("#alert").html(data);
                        $("#alert").fadeOut(8000);
                        setInterval(loadLog(c), 7500);
                    }
                }
            });
            return false;
        }

        function loadLog() {
            if (c == 1) {
                $.ajax({
                    url: "<?php echo base_url('backend/payment/get_update_payment_detail'); ?>",
                    cache: false,
                    success: function (html) {
                        $("#data").html(html); //Insert chat log into the #chatbox div

                    },
                });
            }
            return 2;
        }
    });
</script>

