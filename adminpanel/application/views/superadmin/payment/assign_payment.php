<section class="content">
<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Pagos</li>
      </ol>
       <?php echo msg_alert_backend(); ?>
 <!-- general form elements -->
          <div class="box box-primary">

            <div class="box-header with-border form-heading ">
              <h3 class="box-title">Asignar pago </h3>
            </div>
            <div id="alert"></div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id ="form" action="" method="post"  enctype="multipart/form-data">
              <div class="box-body">
              <div class="form-group">
                <div class="col-xs-6 col-sm-4 col-md-4">
                    <label for="inputPassword3" >Seleccionar administrador </label>
                    <select  class="form-control" required  name="admin" id="admin">
                    <option value="" disabled selected>-- Seleccionar administrador --</option>
                    <?php
                      if(!empty($admin_list))
                      {
                        foreach($admin_list as $row)
                        {
                          
                          ?>
                          <option value="<?php echo $row->id; ?>"><?php echo $row->name." ".$row->l_name; ?></option>
                          <?php
                        }
                      }
                    ?>
                    </select>
                    
                    
                    <?php echo form_error('admin'); ?>
                  </div>
                
                <div class="col-xs-6 col-sm-4 col-md-4">
                   <label for="inputPassword3" >Seleccionar método de págo </label>
                    <select  class="form-control" required name="pay_method" id="pay_method">
                    <option value="" disabled selected>-- Seleccionar método de págo --</option>
                    <?php
                      if(!empty($payment_list))
                      {
                        foreach($payment_list as $row)
                        {
                          
                          ?>
                          <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                          <?php
                        }
                      }
                    ?>
                    </select>
                    
                    
                    <?php echo form_error('pay_method'); ?>
                </div>
               
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer form-actions">
                <button type="submit" name="assign_payment" id="assign_payment" class="btn btn-success">Guardar</button>
              </div>
            </form>
     </div>
          <!-- /.box -->
</section>  

 <section class="content">
<div class="box box-primary">
      <div class="table-responsive">
          <table class="table table-hover data-table-export">
            <thead>
              <th>#</th>
              <th>Administrador</th>                       
              <th>Método de págo</th>                        
              <th>Acción</th>                        
            </thead>
            <tbody id="data">
            <?php
            $i="1";
            if(isset($payment) && !empty($payment)){
            foreach($payment as $data){              
             
            ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $data->admin_name." ".$data->admin_lname; ?></td>
                <td><?php echo $data->p_name; ?></td>
                <td> <button type="button" value="<?php echo $data->id; ?>" id="edit_payment" class="btn btn-warning btn_edit get_city">Editar</button>
                 <button type="button" value="<?php echo $data->id; ?>" id="del_assign" class="btn btn-danger btn_edit ">Borrar</button>
            
              </td>
                </tr>
              <?php
          }
        }
              ?>
                </tbody>
            </table>

            </div>
            </div>
</section>
<script type="text/javascript">
  $(document).on("click","#assign_payment",function(){
    var admin = $("#admin").val();
    var pay_method = $("#pay_method").val();
    var c = 1;
    $("#alert").html('');
    $("#alert").fadeIn();
    if(admin == '' || pay_method == ''){
        alert("Por favor ingresar información completa");
    }
    else{
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/payment/assign_admin_payment'); ?>",
          data : {'admin' : admin ,'pay_method' : pay_method , 'admin_payment' : '1'},
          success : function(data){
            if(data != false){
              $("#admin").val('');
              $("#pay_method").val('');
              $("#alert").html(data);
              $("#alert").fadeOut(8000);
              setInterval (loadLog(c), 7500);
            }
          } 
        });
        return false;
    }
    function loadLog(){
    if(c == 1){   
    $.ajax({
      url: "<?php echo base_url('backend/payment/get_update_payment_assign'); ?>",
      cache: false,
      success: function(html){    
        $("#data").html(html); //Insert chat log into the #chatbox div        
          
        },
    });
  }
  return 2;
  }
});
</script>
<script type="text/javascript">
  $(document).on("click","#edit_payment",function(){
      var row = $(this).val();
      $('div.form-heading').html('<h3 class="box-title">Asignar pago</h3>');
      $('#admin').val("");
      $('#pay_method').val("");
      $("div.form-actions").html('<button type="submit" name="assign_payment" id="assign_payment" class="btn btn-success">Guardar</button>');
      $("form#form").attr('action',"<?php echo htmlspecialchars(base_url('backend/payment/assign_payment')); ?>");
      $("input#row").remove();
      if(row != ''){
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/payment/get_assign_payment'); ?>",
          dataType:"JSON",
          data : {'row' : row , 'edit_form' : '1'},
          success : function(data){         
            if(data != false){
              $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Editar');
             
              $('#admin').val(data.usesrid);
              $('#pay_method').val(data.paymentmethod_id);
              $('div.form-actions').html('<button type="submit" name="edit_assign_payment" id="edit_assign_payment" class="btn btn-success">Actualizar</button>');
              $("form#form").attr("");
              $('form#form').append('<input type="hidden" name="row" id="row" value="'+data.id+'" />');
              

            } 
            
          }
        })
      }
    });
</script>

<script type="text/javascript">
  $(document).on("click","#edit_assign_payment",function(){
    var admin = $("#admin").val();
    var pay_method = $("#pay_method").val();
    var id = $("#row").val();
    var c = 1;
    $("#alert").html('');
    $("#alert").fadeIn();
    if(admin == '' || id == '' || pay_method == ''){
        alert("Por favor agregar información completa");
    }
    else{
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/payment/update_assign_payment_info'); ?>",
          data : {'admin' : admin ,'pay_method' : pay_method ,'id' : id , 'edit_form' : '1'},
          success : function(data){
            if(data != false){
              $("#admin").val('');
              $("#pay_method").val('');
              $("input#row").remove();
              $("#alert").html(data);
              $("#alert").fadeOut(8000);
              $("div.form-actions").html('<button type="submit" name="assign_payment" id="assign_payment" class="btn btn-success">Guardar</button>');
              $('div.form-heading').html('<h3 class="box-title">Asignar pago</h3>');
              setInterval (loadLog(c), 7500);
            }
          } 
        });
        return false;
    }
    function loadLog(){
    if(c == 1){   
    $.ajax({
      url: "<?php echo base_url('backend/payment/get_update_payment_assign'); ?>",
      cache: false,
      success: function(html){    
        $("#data").html(html); //Insert chat log into the #chatbox div        
          
        },
    });
  }
  return 2;
  }
});
</script>

<script type="text/javascript">
  $(document).on("click","#del_assign",function(){
    var id = $(this).val();
    var c = 1;
    $("#alert").html('');
    $("#alert").fadeIn();
    if(id == ''){
        alert("id is blank");
    }
    else{
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/payment/delete_assign_admin'); ?>",
          data : {'id' : id , 'delete_assignin' : '1'},
          success : function(data){
            if(data != false){
              $("#alert").html(data);
              $("#alert").fadeOut(8000);
              setInterval (loadLog(c), 7500);
            }
          } 
        });
        return false;
    }
    function loadLog(){
    if(c == 1){   
    $.ajax({
      url: "<?php echo base_url('backend/payment/get_update_payment_assign'); ?>",
      cache: false,
      success: function(html){    
        $("#data").html(html); //Insert chat log into the #chatbox div        
          
        },
    });
  }
  return 2;
  }
});
</script>       