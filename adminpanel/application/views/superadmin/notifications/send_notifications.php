<script type="text/javascript" src="<?php echo base_url('') ?>assets/time/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>assets/time/jquery.timepicker.css"/>
<script type="text/javascript" src="<?php echo base_url('') ?>assets/time/lib/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>assets/time/lib/bootstrap-datepicker.css"/>
<section class="content">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Notificaciones</a></li>
        <li class="active">Enviar notificación</li>
    </ol>
    <?php echo msg_alert_backend(); ?>
    <div id="alert"></div>
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border form-heading">
            <h3 class="box-title">Agregar notificación</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form">
            <div class="box-body">
                <div class="form-group">
                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <label for="inputtext">Ciudad</label>
                        <select class="form-control" name="city_id" id="city_id" required>
                            <option value="">Seleccionar ciudad</option>
                            <?php
                            if (isset($city_list)) {
                                foreach ($city_list as $city) {
                                    ?>
                                    <option value="<?php echo $city->id; ?>"><?php echo $city->city_name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <?php echo form_error('city_name'); ?>

                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <label for="inputtext">Título</label>
                        <input type="text" required="" class="form-control" id="title" name="title"
                               placeholder="Ingresar título">
                    </div>
                    <?php echo form_error('title'); ?>


                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <label for="comment">Descripción:</label>
                        <textarea class="form-control" rows="5" id="description" required name="description"></textarea>
                    </div>
                    <?php echo form_error('description'); ?>
                </div>


            </div>

            <!-- /.box-body -->
            <div id="img_show"></div>
            <div class="box-footer form-actions">
                <button type="button" name="send_notifications" id="send_notifications" class="btn btn-success">Enviar
                </button>
            </div>
    </div>

    </form>

    </div>
</section>

<script type="text/javascript">
    $(document).on("click", "#send_notifications", function () {
        var city_id = $("#city_id").val();
        var title = $("#title").val();
        var description = $("#description").val();

        $("#alert").html('');
        $("#alert").fadeIn();
        if (city_id == '' || title == '' || description == '') {
            alert("Por favor agregar información completa");
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('backend/notifications/sendNotifications'); ?>",
                data: {'city_id': city_id, 'title': title, 'description': description},
                success: function (data) {
                    if (data != false) {
                        $("#city_id").val('');
                        $("#title").val('');
                        $("#description").val('');
                        $("#alert").html(data);
                        $("#alert").fadeOut(8000);
                    }
                }
            });
            return false;
        }
    });

</script>
<script type="text/javascript">
    $(function () {
        $('#delivery_time').timepicker({'timeFormat': 'H:i'});
        //$('#delivery_time').timepicker({ 'timeFormat': 'h:i A' });
    });
</script>
