<section class="content">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Categoría</li>
    </ol>

    <div class="box box-primary">
        <div class="table-responsive">
            <table class="table  table-hover data-table-export" id="">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre de categoría</th>
                    <th>Business Group</th>
                    <th>Fecha de creación</th>
                    <th>Acción</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($ass_category_list)) {
                    $i = 1;
                    foreach ($ass_category_list as $row) {
                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $row->category_name; ?></td>
                            <td><?php echo $row->businessgroup; ?></td>
                            <td><?php echo date('d-m-Y', strtotime($row->create_date)); ?></td>
                            <td>
                                <!--button type="button" value="<?php echo $row->id; ?>" class="btn btn_edit edit_event">Edit</button-->

                                <a href="<?php echo base_url('users/category/openTimeSchedule/' . $row->category) ?>"
                                   class="btn btn-info btn cancel_btn">Configurar horario</a>

                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>

            </table>

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('.edit_event').click(function () {
            var row = $(this).val();
            $('div.form-heading').html('<h3 class="box-title">Agregar cateor��a</h3>');
            $('#cat_name').val("");
            $("div.form-actions").html('<button type="submit" name="add_category" class="btn btn-primary">Guardar</button>');
            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('users/category/add_category')); ?>");
            $("input#row").remove();
            if (row != '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('users/category/get_edit_category'); ?>",
                    dataType: "JSON",
                    data: {'row': row, 'edit_form': '1'},
                    success: function (data) {
                        if (data != false) {
                            $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Editar categor��a');
                            $('#cat_name').val(data.category_name);
                            $("#c_image").removeAttr("required");
                            $('#img_show').html('<img src="<?php echo base_url(''); ?>' + data.category_img + '" width="50" height="50">');
                            $('div.form-actions').html('<button type="submit" class="btn submit_btn">Actualizar</button>');
                            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('users/category/edit_category')); ?>");
                            $('form#form').append('<input type="hidden" name="row" id="row" value="' + data.id + '" />');


                        }

                    }
                })
            }
        });
    });
</script>
