<section class="content">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Category Time Schedule</li>
    </ol>
    <?php echo msg_alert_backend(); ?>
    <!-- general form elements -->
    <!-- /.box -->


    <h3><?= $category->category_name ?></h3>
    <div class="box box-primary" style="padding: 0 10px;">
        <div class="box-header form-actions">
            <strong>
                <div class="row" style="font-size: 16px">
                    <div class="col-lg-2">
                        Days of week
                    </div>
                    <div class="col-lg-2">
                        From
                    </div>
                    <div class="col-lg-2">
                        To
                    </div>
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
            </strong>
        </div>
        <br/>

        <form role="form" id="form"
              action="<?php echo base_url('users/category/save_timesetting?catId=' . $category->id) ?>"
              method="post"
              enctype="multipart/form-data">
            <?php
            foreach (weekDays() as $key => $day) {
                $timeSetting = $availability[$key];
                ?>
                <div class="row">
                    <div class="col-lg-2">
                        <?= $day ?>
                    </div>

                    <div class="col-lg-2">
                        <input name="openingtime<?= $key ?>" type="text" class="form-control time availibility_time"
                               placeholder="Opening time"
                               value="<?= $timeSetting ? $timeSetting->opening_time : '' ?>"/>
                    </div>
                    <div class="col-lg-2">
                        <input name="closingtime<?= $key ?>" type="text" class="form-control time availibility_time"
                               placeholder="Closing time"
                               value="<?= $timeSetting ? $timeSetting->closing_time : '' ?>"/>
                    </div>

                    <div class="col-lg-2">
                        <label class="checkbox-inline">
                            <input type="radio" value="0"
                                   name="availablestatus<?= $key ?>" id="availablestatusCustom<?= $key ?>"
                                <?= $timeSetting ? checkedValue($timeSetting->open_status, 0) : 'checked' ?>
                            > &nbsp;
                        </label>
                        <label for="availablestatusCustom<?= $key ?>" style="padding-right: 16px">Customize</label>
                    </div>

                    <div class="col-lg-2">
                        <label class="checkbox-inline">
                            <input type="radio" value="1"
                                   name="availablestatus<?= $key ?>" id="availablestatusClosed<?= $key ?>"
                                <?= $timeSetting ? checkedValue($timeSetting->open_status, 1) : '' ?>> &nbsp;
                        </label>
                        <label for="availablestatusClosed<?= $key ?>" style="padding-right: 16px">Closed</label>
                    </div>

                    <div class="col-lg-2">
                        <label class="checkbox-inline">
                            <input type="radio" value="2"
                                   name="availablestatus<?= $key ?>" id="availablestatusAllDay<?= $key ?>"
                                <?= $timeSetting ? checkedValue($timeSetting->open_status, 2) : '' ?>
                            > &nbsp;
                        </label>
                        <label for="availablestatusAllDay<?= $key ?>" style="padding-right: 16px">All Day</label>
                    </div>
                </div>
                <br/>

                <?php
            }
            ?>
            <div class="box-footer form-actions">
                <button type="submit" name="add_product" id="submit1" class="btn btn-primary">Update Settings
                </button>
            </div>
        </form>

    </div>

</section>

<script type="text/javascript">
    $(function () {
        $('.availibility_time').timepicker({'timeFormat': 'h:i A'});
        //$('#delivery_time').timepicker({ 'timeFormat': 'h:i A' });
    });
</script>

<script type="text/javascript" src="<?php echo base_url('') ?>assets/time/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>assets/time/jquery.timepicker.css"/>
<script type="text/javascript" src="<?php echo base_url('') ?>assets/time/lib/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>assets/time/lib/bootstrap-datepicker.css"/>

