<section class="content">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Agregar personal de envío</li>
    </ol>
    <?php echo msg_alert_backend(); ?>
    <div id="alert"></div>
    <!-- general form elements -->
    <div class="box-header with-border form-heading ">
        <h3 class="box-title">Agregar personal de envío</h3>
    </div>
    <div id="alert"></div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" id="form" action="<?php //echo htmlspecialchars(current_url()); ?>" method="post"
          enctype="multipart/form-data">
        <div class="box-body">
            <div class="form-group">
                <div class="col-xs-6 col-sm-4 col-md-4">
                    <label for="inputtext">Nombre</label>
                    <input type="text" required="" class="form-control" id="f_name" name="f_name"
                           placeholder="Ingresar nombre">
                </div>
                <?php echo form_error('f_name'); ?>
                <div class="col-xs-6 col-sm-4 col-md-4">
                    <label for="inputtext">Apellidos</label>
                    <input type="text" required="" class="form-control" id="l_name" name="l_name"
                           placeholder="Ingresar apellidos">
                </div>
                <?php echo form_error('l_name'); ?>
                <div class="col-xs-6 col-sm-4 col-md-4">
                    <label for="inputtext">Correo electrónico</label>
                    <input type="email" required="" class="form-control" id="email" name="email"
                           placeholder="Ingresar correo electrónico">
                </div>
                <?php echo form_error('email'); ?>
                <div class="col-xs-6 col-sm-4 col-md-4">
                    <label for="inputtext">Número de teléfono</label>
                    <input type="text" required="" class="form-control" id="mob" name="mob"
                           placeholder="Ingresar número de teléfono">
                </div>
                <?php echo form_error('mob'); ?>
                <div class="col-xs-6 col-sm-4 col-md-4">
                    <label for="inputtext">Contraseña</label>
                    <input type="password" required="" class="form-control" id="password" name="password"
                           placeholder="Ingresar contraseña">
                </div>
                <?php echo form_error('password'); ?>
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer form-actions">
            <div class="col-xs-6 col-sm-12 col-md-12">
                <button type="submit" name="add_admin" id="add_boy" class="btn btn-success">Guardar</button>
            </div>
        </div>
    </form>


</section>

<section class="content">
    <div class="box box-primary">
        <div class="table-responsive">
            <table class="table table-hover data-table-export">
                <thead>
                <th>#</th>
                <th>Nombre</th>
                <th>Correo electrónico</th>
                <th>Número de teléfono</th>
                <th>Estatus</th>
                <th>Acción</th>
                </thead>
                <tbody id="data">
                <?php
                $i = "1";
                if (isset($list) && !empty($list)) {
                    foreach ($list as $data) {
                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data->name; ?></td>
                            <td><?php echo $data->email; ?></td>
                            <td><?php echo $data->mobile; ?></td>
                            <td><?php echo ($data->user_status == 1) ? "Online" : "Offline"; ?></td>
                            <td>
                                <button type="button" value="<?php echo $data->id; ?>"
                                        class="btn btn-warning btn  btn_edit edit_boy">Editar
                                </button>
                                <button type="button" value="<?php echo $data->id; ?>" id="del_boy"
                                        class="btn btn-danger btn btn_edit get_city">Borrar
                                </button>

                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>

        </div>
    </div>
</section>


<script type="text/javascript">
    $(document).on("click", "#add_boy", function () {
        var f_name = $("#f_name").val();
        var l_name = $("#l_name").val();
        var email = $("#email").val();
        var mob = $("#mob").val();
        var password = $("#password").val();
        var c = 1;
        $("#alert").html('');
        $("#alert").fadeIn();
        if (f_name == '' || l_name == '' || email == '' || mob == '' || password == '') {
            alert("Please Fill All Information");
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('users/delivery/insert_boy'); ?>",
                data: {
                    'f_name': f_name,
                    'l_name': l_name,
                    'email': email,
                    'mob': mob,
                    'password': password,
                    'add_boy': '1'
                },
                success: function (data) {
                    if (data != false) {
                        $("#f_name").val('');
                        $("#l_name").val('');
                        $("#email").val('');
                        $("#mob").val('');
                        $("#password").val('');
                        $("#alert").html(data);
                        $("#alert").fadeOut(8000);
                        setInterval(loadLog(c), 7500);
                    }
                }
            });
            return false;
        }

        function loadLog() {
            if (c == 1) {
                $.ajax({
                    url: "<?php echo base_url('users/delivery/get_update_value'); ?>",
                    cache: false,
                    success: function (html) {
                        $("#data").html(html); //Insert chat log into the #chatbox div

                    },
                });
            }
            return 2;
        }
    });
</script>
<script type="text/javascript">
    $(document).on("click", ".edit_boy", function () {
        var row = $(this).val();
        var c = 1;
        $('div.form-heading').html('<h3 class="box-title">Agregar personal de envío</h3>');
        $('#f_name').val("");
        $('#l_name').val("");
        $('#email').val("");
        $('#mob').val("");
        $("div.form-actions").html('<button type="submit" name="add_boy" class="btn btn-success">Guardar</button>');
        $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('users/delivery/add_boy')); ?>");
        $("input#row").remove();
        if (row != '') {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('users/delivery/get_edit_boy'); ?>",
                dataType: "JSON",
                data: {'row': row, 'edit_form': '1'},
                success: function (data) {
                    if (data != false) {
                        $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Editar personal de envíoy');

                        $('#f_name').val(data.name);
                        $('#l_name').val(data.l_name);
                        $('#email').val(data.email);
                        $('#mob').val(data.mobile);
                        $('#password').val(data.password);
                        // $("#password").removeAttr("required");
                        $('div.form-actions').html('<button type="submit" class="btn btn-warning btn submit_btn" id="update_boy">Actualizar</button>');
                        $("form#form").attr('action', "");
                        $('form#form').append('<input type="hidden" name="row" id="row" value="' + data.id + '" />');

                    }

                }
            })
        }

    });
</script>
<script type="text/javascript">
    $(document).on("click", "#update_boy", function () {
        var id = $("#row").val();
        var f_name = $("#f_name").val();
        var l_name = $("#l_name").val();
        var email = $("#email").val();
        var mob = $("#mob").val();
        var password = $("#password").val();
        var c = 1;
        $("#alert").html('');
        $("#alert").fadeIn();
        if (id == '' || f_name == '' || l_name == '' || email == '' || mob == '' || password == '') {
            alert("Please Fill All Information");
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('users/delivery/update_boy'); ?>",
                data: {
                    'id': id,
                    'f_name': f_name,
                    'l_name': l_name,
                    'email': email,
                    'mob': mob,
                    'password': password,
                    'edit_boy': '1'
                },
                success: function (data) {
                    if (data != false) {
                        $("#f_name").val('');
                        $("#l_name").val('');
                        $("#email").val('');
                        $("#mob").val('');
                        $("#password").val('');
                        $("#alert").html(data);
                        $("#alert").fadeOut(8000);
                        $("div.form-actions").html('<button type="submit" name="add_boy" class="btn btn-success">Guardar</button>');
                        $('div.form-heading').html('<h3 class="box-title">Agregar personal de envío</h3>');
                        $("input#row").remove();
                        setInterval(loadLog(c), 7500);
                    }
                }
            });
            return false;
        }

        function loadLog() {
            if (c == 1) {
                $.ajax({
                    url: "<?php echo base_url('users/delivery/get_update_value'); ?>",
                    cache: false,
                    success: function (html) {
                        $("#data").html(html); //Insert chat log into the #chatbox div

                    },
                });
            }
            return 2;
        }
    });
</script>

<script type="text/javascript">
    $(document).on("click", "#del_boy", function () {
        var id = $(this).val();
        var c = 1;
        $("#alert").html('');
        $("#alert").fadeIn();
        if (id == '') {
            alert("id is blank");
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('users/delivery/delete_boy'); ?>",
                data: {'id': id, 'delete_boy': '1'},
                success: function (data) {
                    if (data != false) {
                        $("#alert").html(data);
                        $("#alert").fadeOut(8000);
                        setInterval(loadLog(c), 7500);
                    }
                }
            });
            return false;
        }

        function loadLog() {
            if (c == 1) {
                $.ajax({
                    url: "<?php echo base_url('users/delivery/get_update_value'); ?>",
                    cache: false,
                    success: function (html) {
                        $("#data").html(html); //Insert chat log into the #chatbox div

                    },
                });
            }
            return 2;
        }
    });
</script>

