
<section class="content">
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
		<li class="active">Costo de envío</li>
	</ol>


	<!-- general form elements -->
	<div class="box box-primary">
		<div class="box-header with-border form-heading">
			<h3 class="box-title">Agregar costo de envío</h3>
		</div>
		<div id="alert"></div>
		<!-- /.box-header -->
		<!-- form start -->
		<form method="post" id="deliverycost" name="deliverycost" onsubmit="return addDeliveryCost();">
			<input type="hidden" name="addUpdateType" id="addUpdateType"  class="form-control"/>
			<input type="hidden" name="delievery_id" id="delievery_id"  class="form-control"/>
			<div class="box-body">
				<div class="form-group">
					<div class="col-xs-6 col-sm-4 col-md-4">
						<label for="inputtext">Costo</label>
						<input type="text" required="" class="form-control" id="cost" name="cost" placeholder="Ingresar el costo">
						<?php echo form_error('costError');?>
					</div>
					<div class="col-md-4">
						<label>Ciudad</label>
						<input type="hidden" name="user_id" value="<?php echo $userId; ?>"/>
						<select class="form-control" name="city_id" id="city_id">
							<option value="">Seleccionar ciudad</option>
							<?php
							
							if(isset($assignedCity) && !empty($assignedCity)){
								foreach($assignedCity as $city){
									?>
									<option value="<?php echo $city->id; ?>"><?php echo $city->city_name; ?></option>
									<?php
								}
							}
							?>
						</select>
					</div>


<!-- 					<div class="col-xs-6 col-sm-4 col-md-4" style="padding: 10px">
						<br />

						<input type="checkbox" class="filled-in form-check-input"  class="form-control" id="active_status" name="active_status" >
						<label class="form-check-label" for="checkbox105" style="margin-left: 10px">Status</label>
					</div> -->

				</div>

			</div>

			<div class="box-footer form-actions">
				<button type="submit" name="deliverycostbtn" id="deliverycostbtn" class="btn btn-success">Guardar</button>
			</div>
		</form>
	</div>
	<!-- /.box -->
</section>  

<section class="content">
	<div class="box box-primary">
		<div class="table-responsive">
			<table class="table table-hover data-table-export">
				<thead>
					<th>#</th>
					<th>Costo de envío</th>
					<th>Ciudad</th>                        
					<th>Acción</th>                        
				</thead>
				<tbody id="delieveryCostTable">

				</tbody>
			</table>

		</div>
	</div>
</section>


<script src="./../assets/js/pages/delievery_cost_user.js"></script>
<script type="text/javascript">
</script>
