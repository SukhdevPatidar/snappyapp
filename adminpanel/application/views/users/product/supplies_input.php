<div class="supplies_item" style="clear:both;">
    <div class="col-xs-6 col-sm-4 col-md-4">
        <label for="inputtext">Supplies Name</label>
        <input type="text" class="form-control" name="supplies_name[]"
               placeholder="Enter Supplies Name"
               value="<?= !empty($data) ? $data->supplies_name : '' ?>"
        >
    </div>

    <div class="col-xs-6 col-sm-4 col-md-4">
        <label for="inputtext">Suppies Price($)</label>
        <input type="text" class="form-control" name="supplies_price[]"
               placeholder="Enter Supplies Price"
               value="<?= !empty($data) ? $data->supplies_price : '' ?>"
        >
    </div>
    <?php
    if (!empty($data)) {
        ?>
        <input type="hidden" class="form-control" name="supplies_id[]"
               value="<?= !empty($data) ? $data->id : '' ?>"
        <?php
    }
    ?>
    <div class="col-xs-6 col-sm-4 col-md-4">
        <button type="button" name="remove_supplies"
                class="btn btn-danger remove_supplies"
                onclick="removeSuppliesItem(this, <?= !empty($data) ? $data->id : null ?>)">
            Remove
        </button>
    </div>
</div>
