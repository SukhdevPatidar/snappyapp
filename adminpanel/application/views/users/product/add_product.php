<script type="text/javascript" src="<?php echo base_url('') ?>assets/time/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>assets/time/jquery.timepicker.css"/>
<script type="text/javascript" src="<?php echo base_url('') ?>assets/time/lib/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('') ?>assets/time/lib/bootstrap-datepicker.css"/>
<section class="content">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Category</li>
    </ol>
    <?php echo msg_alert_backend(); ?>
    <div id="alert"></div>
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border form-heading">
            <h3 class="box-title">Add Product</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="form" action="<?php echo base_url('users/product/add_product') ?>" method="post"
              enctype="multipart/form-data">
            <div class="box-body">
                <div class="form-group">
                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <label for="inputtext">Category Name</label>
                        <select class="form-control" name="cat_name" id="cat_name" required>
                            <option value="">Select Category</option>
                            <?php
                            if (isset($cat_list)) {
                                foreach ($cat_list as $list) {
                                    ?>
                                    <option value="<?php echo $list->category; ?>"><?php echo $list->category_name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <?php echo form_error('cat_name'); ?>
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <label for="inputtext">Subcategory Name</label>
                        <select class="form-control" name="subcategory" id="subcategory" required">
                            <option value="">Select Subcategory</option>
                        </select>
                        <?php echo form_error('subcategory'); ?>
                    </div>


                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <label for="inputtext">Product Name</label>
                        <input type="text" required="" class="form-control" id="p_name" name="p_name"
                               placeholder="Enter Product Name">
                        <?php echo form_error('p_name'); ?>
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <label for="inputtext">Product Image</label>
                        <input type="file" required="" class="form-control" id="p_img" name="p_img"
                               placeholder="Enter Product Image">
                        <?php echo form_error('p_img'); ?>
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <label for="inputtext">Price ($)</label>
                        <input type="text" required="" class="form-control" id="price" name="price"
                               placeholder="Enter Price">
                        <?php echo form_error('price'); ?>
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <label for="inputtext">Promotional Price (Optional) ($)</label>
                        <input type="text" class="form-control" id="promo_price" name="promo_price"
                               placeholder="Enter Price">
                    </div>
                    <?php echo form_error('promo_price'); ?>

                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <label for="inputtext">Time </label>
                        <input id="delivery_time" name="delivery_time" type="text" class="form-control time"/>
                        <?php echo form_error('delivery_time'); ?>
                    </div>

                    <div class="col-md-12">
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <label for="comment">Short Description:</label>
                        <textarea class="form-control" rows="5" id="shortDescription" required
                                  name="shortDescription"></textarea>
                        <?php echo form_error('shortDescription'); ?>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <label for="comment">Long Description(Optional):</label>
                        <textarea class="form-control" rows="5" id="longDescription" name="longDescription"></textarea>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 " style="padding: 16px;">
                        <label for="inputtext" style="padding-right: 16px">Payment Options </label>
                        <?php
                        if (!empty($paymentOptions)) {
                            $i = 1;
                            foreach ($paymentOptions as $row) {

                                ?>
                                <label class="checkbox-inline"><input type="checkbox" id="pay_<?php echo $row->id; ?>"
                                                                      value="<?php echo $row->id; ?>"
                                                                      name="product_payment_options[]"><?php echo $row->name; ?>
                                </label>
                                <?php
                            }
                        }
                        ?>
                    </div>


                    <div class="col-xs-12 col-sm-12 col-md-12 " style="padding: 16px;">
                        <label for="inputtext" style="padding-right: 16px">Out of Stock </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="outofstock" value="1"
                                   name="outofstock"> &nbsp;
                        </label>
                    </div>


                    <div id="supplies_container">
                        <div id="supplies">

                        </div>

                        <!--                        ForEdit product-->
                        <div id="removed_supplies"></div>

                        <div class="col-xs-12" style="margin-top: 8px">
                            <button type="button" name="add_supplies" id="add_supplies" class="btn btn-info"
                                    onclick="addMoreSupplies()">
                                Add Supplies
                            </button>
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <label for="noOfSuppliesAllowed">Number of supplies available</label>
                        <input type="number" class="form-control" id="noOfSuppliesAllowed" name="noOfSuppliesAllowed"
                               placeholder="Max supplies available" value="0">
                    </div>
                </div>
            </div>


            <!-- /.box-body -->
            <div id="img_show"></div>
            <div class="box-footer form-actions">
                <button type="submit" name="add_product" id="submit1" class="btn btn-primary"
                        style="width: 180px!important;">
                    Submit
                </button>
            </div>
    </div>

    </form>


    <div class="box box-primary">
        <div class="table-responsive">
            <table class="table  table-hover data-table-export">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Category Name</th>
                    <th>Subcategory Name</th>
                    <th>Product Name</th>
                    <th>Product Image</th>
                    <th>Price</th>
                    <th>Promotional Price</th>
                    <th>Delivery Time</th>
                    <th>Short Description</th>
                    <th>Long Description</th>
                    <th>Out of Stock</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($product_list)) {
                    $i = 1;
                    foreach ($product_list as $row) {

                        ?>
                        <tr>
                            <td><?php echo '#' . $i++; ?></td>
                            <td><?php echo $row->category_name; ?></td>
                            <td><?php echo $row->subcategory_name; ?></td>
                            <td><?php echo $row->product_name; ?></td>
                            <td><img src="<?php echo base_url('') . $row->product_img; ?>" width="100" height="100">
                            </td>
                            <td><?php echo $row->price . " $ "; ?></td>
                            <td><?php echo $row->promotional_price . " $ "; ?></td>
                            <td><?php echo $row->delivery_time; ?></td>
                            <td><?php echo $row->short_description; ?></td>
                            <td><?php echo $row->long_description; ?></td>
                            <td><?= ($row->outofstock == 1) ? "Yes" : "No"; ?></td>
                            <td>

                                <button type="button" value="<?php echo $row->id; ?>" class="btn btn_edit edit_event">
                                    Edit
                                </button>

                                <a href="<?php echo base_url('users/product/delete_product/' . $row->id) ?>"
                                   class="btn cancel_btn">Remove</a>

                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>

            </table>

        </div>
    </div>
    <!-- /.box -->
    </div>
</section>

<script type="text/javascript">

    $(document).on("click", "#submit", function () {
        var cat_name = $("#cat_name").val();
        var p_name = $("#p_name").val();
        $("#alert").html('');
        $("#alert").fadeIn();
        if (cat_name == '' || p_name == '') {
            alert("Please Fill All Information");
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('users/product/insert_product'); ?>",
                data: {'cat_name': cat_name, 'p_name': p_name, 'add_form': '1'},
                success: function (data) {
                    if (data != false) {
                        $("#cat_name").val('');
                        $("#p_name").val('');
                        $("#alert").html(data);
                        $("#alert").fadeOut(8000);
                    }
                }
            });
            return false;
        }
    });


    function addMoreSupplies() {
        $("<div>").load("suppliesItem", {}, function (response, error, txt) {
            $('#supplies').append(response);
        });
    }

    function removeSuppliesItem(event, suppliesId) {
        if (suppliesId) {
            $('#removed_supplies').append('<input type="hidden" name="removed_supplies_id[]" value="' + suppliesId + '"/>')
        }

        event.parentElement.closest(".supplies_item").remove();
    }

    $(document).ready(function () {
        $(document).ready(function () {
            $('#cat_name').on('change', function () {
                var catId = $(this).val();
                $('#subcategory').find('option').not(':first').remove();

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('users/product/getSubcategories'); ?>",
                    dataType: "JSON",
                    data: {'category_id': catId},
                    success: function (response) {
                        if (response.status) {
                            var data = response.data;
                            if (data) {
                                for (var i = 0; i < data.length; i++) {
                                    var catObj = data[i];
                                    $('#subcategory').append($('<option>', {
                                        value: catObj.id,
                                        text: catObj.name
                                    }));
                                }
                            }
                        }
                    }
                });
            });
        });


        $('.edit_event').click(function () {
            var row = $(this).val();
            $('div.form-heading').html('<h3 class="box-title">Add product</h3>');
            $('#cat_name').val("");
            $('#p_name').val("");
            $('#price').val("");
            $('#promo_price').val("");
            $('#delivery_time').val("");
            $('#supplies').html("");
            $("div.form-actions").html('<button type="submit" name="add_product" id="submit" class="btn btn-primary">Submit</button>');
            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('users/product/add_product')); ?>");
            $("input#row").remove();

            //uncheck all payment option
            var cbarray = document.getElementsByName('product_payment_options[]');
            for (var i = 0; i < cbarray.length; i++) {
                $("#" + cbarray[i].id).prop("checked", false);
            }
            //uncheck all payment option end
            $('#subcategory').find('option').not(':first').remove();

            if (row != '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('users/product/get_edit_product'); ?>",
                    dataType: "JSON",
                    data: {'row': row, 'edit_form': '1'},
                    success: function (response) {
                        if (response.status) {
                            var data = response.data;
                            var subcategories = response.subcategories;

                            $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Edit Product');
                            $('#cat_name').val(data.category_id);
                            $('#p_name').val(data.product_name);
                            $('#price').val(data.price);
                            $('#promo_price').val(data.promotional_price);
                            $('#noOfSuppliesAllowed').val(data.max_supplies_select);
                            $('#shortDescription').val(data.short_description);
                            $('#longDescription').val(data.long_description);
                            $('#delivery_time').val(data.delivery_time);
                            $('#img_show').html('<img src="<?php echo base_url(''); ?>' + data.product_img + '" width="50" height="50">');
                            $("#p_img").removeAttr("required");
                            $('div.form-actions').html('<button style="margin-left:15px;" type="submit" class="btn submit_btn">Update</button>');
                            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('users/product/edit_product')); ?>");
                            $('form#form').append('<input type="hidden" name="row" id="row" value="' + data.id + '" />');
                            $("#outofstock").prop("checked", (data.outofstock == 1));

                            $("<div>").load("suppliesItem?product_id=" + data.id, {}, function (response, error, txt) {
                                $('#supplies').append(response);
                            });

                            //Add subcategory dropdown
                            for (var i = 0; i < subcategories.length; i++) {
                                var catObj = subcategories[i];
                                if (catObj.id == data.subcategory_id) {
                                    $('#subcategory').append($('<option>', {
                                        value: catObj.id,
                                        text: catObj.name,
                                        selected: true
                                    }));
                                } else {
                                    $('#subcategory').append($('<option>', {
                                        value: catObj.id,
                                        text: catObj.name
                                    }));
                                }
                            }

                            $("html, body").animate({scrollTop: 0}, "slow");

                        }
                    }
                });


                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('users/product/get_edit_payment_opt'); ?>",
                    dataType: "JSON",
                    data: {'row': row, 'edit_pay': '1'},
                    success: function (data) {
                        if (data != false) {
                            for (i = 0; i < data.length; i++) {
                                //p = JSON.parse(data[0]);
                                //console.log(data[i].id);
                                //console.log(p);
                                $("#pay_" + data[i].paymentmethod_id).prop("checked", true);
                                $('form#form').append('<input type="hidden" name="p_id[]" id="row" value="' + data[i].id + '" />');
                            }
                        }
                    }
                });
            }
        });
    });

</script>
<script type="text/javascript">
    $(function () {
        $('#delivery_time').timepicker({'timeFormat': 'H:i'});
        //$('#delivery_time').timepicker({ 'timeFormat': 'h:i A' });
    });
</script>
