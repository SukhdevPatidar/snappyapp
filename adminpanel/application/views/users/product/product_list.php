<section class="conteant">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Product List</li>
    </ol>
    <?php echo msg_alert_backend(); ?>
    <!-- general form elements -->
    <div class="box box-primary">

        <div class="box-header with-border form-heading ">
            <h3 class="box-title">Product List</h3>
            <form action="<?php echo htmlspecialchars(current_url()) ?>" method="post">
                <div class="form-group">
                    <div class="col-md-4">
                        <label>Category</label>
                        <select class="form-control" name="cat_id">
                            <option value="">Select Category</option>
                            <?php
                            if (isset($category) && !empty($category)) {
                                foreach ($category as $list) {
                                    ?>
                                    <option value="<?php echo $list->id; ?>"><?php echo $list->category_name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label>Product Name</label>
                        <select class="form-control" name="pro_id">
                            <option value="">Prdouct Name</option>
                            <?php
                            if (isset($prd_list) && !empty($prd_list)) {
                                foreach ($prd_list as $p_list) {
                                    ?>
                                    <option value="<?php echo $p_list->product_name; ?>"><?php echo $p_list->product_name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label></label>
                        <button type="submit" name="search_product" id="search" class="btn btn-primary">Search</button>
                    </div>

                </div>
            </form>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

    </div>
    <!-- /.box -->
</section>

<section class="content">
    <div class="box box-primary">
        <div class="table-responsive">
            <table class="table table-hover data-table-export">
                <thead>
                <th>#</th>
                <th>Admin Name</th>
                <th>Category Name</th>
                <th>Product Name</th>
                <th>Image</th>
                <th>Out of Stock</th>
                <th>Delivery Date</th>
                </thead>
                <tbody>
                <?php
                $i = "1";
                if (isset($product_list) && !empty($product_list)) {
                    foreach ($product_list as $data) {
                        $admin = $this->city_model->get_row('users', array('id' => $data->user_id));
                        $name = isset($admin->name) ? $admin->name : '';
                        $surname = isset($admin->l_name) ? $admin->l_name : '';
                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $name . " " . $surname; ?></td>
                            <td><?php echo $data->category_name; ?></td>
                            <td><?php echo $data->product_name; ?></td>
                            <td><img src="<?php echo base_url('') . $data->product_img; ?>" width="100" height="100">
                            </td>
                            <td><?= ($data->outofstock == 1) ? "Yes" : "No"; ?></td>
                            <td><?php echo isset($data->delivery_date) ? $data->delivery_date : ''; ?></td>

                        </tr>
                        <?php
                    }
                } else {
                    if (isset($srch_product_list) && !empty($srch_product_list)) {
                        foreach ($srch_product_list as $data) {
                            $admin = $this->city_model->get_row('users', array('id' => $data->user_id));
                            $name = isset($admin->name) ? $admin->name : '';
                            $surname = isset($admin->l_name) ? $admin->l_name : '';
                            ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $name . " " . $surname; ?></td>
                                <td><?php echo $data->category_name; ?></td>
                                <td><?php echo $data->product_name; ?></td>
                                <td><img src="<?php echo base_url('') . $data->product_img; ?>" width="100"
                                         height="100"></td>
                                <td><?= ($data->outofstock == 1) ? "Yes" : "No"; ?></td>
                                <td><?php echo isset($data->delivery_date) ? $data->delivery_date : ''; ?></td>
                            </tr>
                            <?php
                        }
                    }
                }
                ?>
                </tbody>
            </table>

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('.edit_event').click(function () {
            var row = $(this).val();
            $('div.form-heading').html('<h3 class="box-title">Add City</h3>');
            $('#f_name').val("");
            $('#l_name').val("");
            $('#email').val("");
            $('#mob').val("");
            $('#city_name').val("");
            $("div.form-actions").html('<button type="submit" name="add_admin" class="btn btn-primary">Submit</button>');
            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('backend/admin/add_admin')); ?>");
            $("input#row").remove();
            if (row != '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('backend/admin/get_edit_admin'); ?>",
                    dataType: "JSON",
                    data: {'row': row, 'edit_form': '1'},
                    success: function (data) {
                        if (data != false) {
                            $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Edit Admin');
                            $('#event_name').val(data.event_name);
                            $('#ttl_days').val(data.total_days);

                            $('#f_name').val(data.name);
                            $('#l_name').val(data.l_name);
                            $('#email').val(data.email);
                            $('#mob').val(data.mobile);
                            $('#city_name').val(data.city_name);
                            $("#password").removeAttr("required");
                            $('div.form-actions').html('<button type="submit" class="btn submit_btn">Update</button>');
                            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('backend/admin/edit_admin')); ?>");
                            $('form#form').append('<input type="hidden" name="row" id="row" value="' + data.id + '" />');


                        }

                    }
                })
            }
        });
    });
</script>
