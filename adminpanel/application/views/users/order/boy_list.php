<section class="conteant">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Lista de pedidos</li>
    </ol>
    <?php echo msg_alert_backend(); ?>
    <!-- general form elements -->
    <div class="box box-primary">

        <div class="box-header with-border form-heading">
            <h3 class="box-title">Lista de pedidos</h3>
            <form action="<?php echo base_url('users/order/insert_delivery_boy?redirect=1'); ?>" method="post">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Personal de envío</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <input type="hidden" name="orderId" value="<?= isset($order_id) ? $order_id : ''; ?>"/>
                            <select class="form-control" name="boy_id">
                                <option value="">Seleccionar personal de envío</option>
                                <?php
                                if (isset($boylist) && !empty($boylist)) {
                                    foreach ($boylist as $list) {
                                        ?>
                                        <option value="<?php echo $list->id; ?>"><?php echo $list->name . " " . $list->l_name; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label></label>
                            <button type="submit" name="add_deliveryBoy" id="add" class="btn btn-primary">Agregar
                            </button>
                        </div>
                    </div>

                </div>
            </form>
        </div>

        <div class="form-group">
            <div id="locationMap" style="height: 450px;margin: 22px"
                 latitude="<?php echo isset($handymanData) ? $handymanData->latitude : '-35.2618026'; ?>"
                 longitude="<?php echo isset($handymanData) ? $handymanData->longitude : '-64.9258587'; ?>"></div>
        </div>
        <br/>

        <!-- /.box-header -->

    </div>
    <!-- /.box -->


</section>


<script src="<?php echo base_url(); ?>assets/js/pages/assign_delivery_boy.js"></script>
<script>
    setTimeout(function () {
        var boylistStr = '<?= json_encode($boylist) ?>';
        if (boylistStr) {
            var boylist = JSON.parse(boylistStr);
            addDeliveryBoysToMap(boylist, "<?= base_url()?>", "<?= $order_id ?>", true);
        }
    }, 1000);

    setInterval(function () {
        $.ajax({
            type: "POST",
            url: "<?= base_url()?>" + "users/order/fetchDeliveryBoyList",
            success: function (data) {
                var response = JSON.parse(data);

                if (response.status) {
                    var boylist = response.data;
                    addDeliveryBoysToMap(boylist, "<?= base_url()?>", "<?= $order_id ?>", false);
                }
            }
        });
    }, 10000);


</script>
<script
        src="https://maps.googleapis.com/maps/api/js?key=<?= GoogleMapKey() ?>&libraries=places&callback=initMap"
        async defer></script>
