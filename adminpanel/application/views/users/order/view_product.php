<section class="conteant">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Lista de pedidos</li>
    </ol>
    <?php echo msg_alert_backend(); ?>
    <!-- general form elements -->
    <div class="box box-primary">

        <div class="box-header with-border form-heading ">

            <table class="table">
                <?php
                if (isset($user_history) && !empty($user_history)) {
                    $user_city = get_city_name($user_history[0]->city_id);
                    $add_city = get_city_name($user_history[0]->town_city);
                    // echo "<pre/>";
                    // print_r($user_history);
                    ?>
                    <tr>
                        <th colspan="2">Detalles de usuario</th>
                    </tr>
                    <tr>
                        <td>Usuario</td>
                        <td><?php echo $user_history[0]->user_name . " " . $user_history[0]->l_name; ?></td>
                    </tr>
                    <tr>
                        <td>Correo electrónico</td>
                        <td><?php echo $user_history[0]->email; ?></td>
                    </tr>
                    <tr>
                        <td>Número de teléfono</td>
                        <td><?php echo $user_history[0]->user_mobile; ?></td>
                    </tr>
                    <tr>
                        <td>Ciudad</td>
                        <td><?php echo isset($user_city->city_name) ? $user_city->city_name : '-'; ?></td>
                    </tr>
                    <tr>
                        <th colspan="2">Dirección de envío</th>
                    </tr>
                    <tr>
                        <td>Nombre del perfil</td>
                        <td><?php echo $user_history[0]->address_name; ?></td>
                    </tr>
                    <tr>
                        <td>Número de teléfono</td>
                        <td><?php echo $user_history[0]->add_mobile; ?></td>
                    </tr>
                    <tr>
                        <td>Ciudad</td>
                        <td><?php echo isset($add_city->city_name) ? $add_city->city_name : '-'; ?></td>
                    </tr>
                    <tr>
                        <td>Dirección</td>
                        <td><?php echo $user_history[0]->house_no . " " . $user_history[0]->colony_street . " " . $user_history[0]->landmark . " " . $user_history[0]->pincode; ?></td>
                    </tr>
                    <tr>
                        <td>Punto de referencia</td>
                        <td><?php echo $user_history[0]->landmark; ?></td>
                    </tr>
                    <tr>
                        <td>Otros</td>
                        <td><?php echo $user_history[0]->other; ?></td>
                    </tr>
                    <tr>
                        <td>Empresa</td>
                        <td><?php echo $user_history[0]->company; ?></td>
                    </tr>
                    <tr>
                        <th>Otros</th>
                    </tr>
                    <tr>
                        <td>Otras notas</td>
                        <td><?php echo $user_history[0]->order_notes; ?></td>
                    </tr>
                    <?php
                }
                ?>

            </table>
            <h3 class="box-title">Lista de pedidos</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

    </div>
    <!-- /.box -->
</section>

<section class="content">
    <div class="box box-primary">
        <div class="table-responsive">
            <table id="example" class="table table-hover data-table-export">
                <thead>
                <th>#</th>
                <th>Nombre del producto</th>
                <th>Método de pago</th>
                <th>Estatus del pedido</th>
                <th>Detalles extras</th>
                <th>Supplies</th>
                <th>Cantidad</th>
                <th>Precio del producto</th>
                <!--                <th>Precio de envío</th>-->
                <th>Imagen</th>
                <th>Precio total</th>
                </thead>
                <tbody>
                <?php
                $i = "1";
                $tot_quan = 0;
                $tot_pro_prc = 0;
                $del_chrg = 0;
                $total = 0;


                if (isset($history) && !empty($history)) {
                    foreach ($history as $data) {

                        $ci = &get_instance();
                        $sql = $ci->db->query("select pm.name, o.payment_status, o.delievery_price from `orders` as o inner join `payment_methods` as pm on o.payment_method_id = pm.id where o.id = '" . $data->order_id . "'");
                        //echo "select pm.name, o.payment_status from `orders` as o inner join `payment_methods` as pm on o.payment_method_id = pm.id where o.id = '".$data->order_id."'";
                        $pay_name = $sql->result();


                        //Supplies price if supply selected by user
                        $suppliesStr = $data->supplies;
                        $suppliesAmount = 0;
                        if (!empty($suppliesStr)) {
                            $supplies = json_decode($suppliesStr);
                            foreach ($supplies as $supply) {
                                $suppliesAmount = $suppliesAmount + $supply->supplies_price;
                            }
                        }


                        $appliedProductPrice = $data->promotional_price ? $data->promotional_price : $data->product_price;
                        $total_price = ($data->quantity * $appliedProductPrice) + ($suppliesAmount * $data->quantity);
                        $tot_quan = $tot_quan + $data->quantity;
                        $tot_pro_prc = $tot_pro_prc + $appliedProductPrice;
                        $del_chrg = $pay_name[0]->delievery_price;
                        $total = $total + $total_price;
                        ?>

                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data->product_name; ?></td>
                            <td><?php echo $pay_name[0]->name; ?></td>
                            <td><?php echo $pay_name[0]->payment_status; ?></td>
                            <td><?php echo $data->extra_detail; ?></td>
                            <td>
                                <?php
                                $suppliesStr = $data->supplies;
                                if (!empty($suppliesStr)) {
                                    $supplies = json_decode($suppliesStr);
                                    foreach ($supplies as $supply) {
                                        echo "<div>" . $supply->supplies_name . " - $" . $supply->supplies_price . "</div>";
                                    }
                                }
                                ?>
                            </td>
                            <td><?php echo $data->quantity; ?></td>
                            <td><?php echo($data->promotional_price ? $data->promotional_price : $data->product_price); ?></td>
                            <!--                            <td>-->
                            <?php //echo $pay_name[0]->delievery_price; ?><!--</td>-->
                            <td><img src="<?php echo base_url() . $data->product_img; ?>" width="50" height="50"></td>
                            <td><?php echo $total_price; ?></td>
                        </tr>
                        <?php
                    }
                }

                $total = $total + $del_chrg;

                ?>
                <tr>
                    <td>Delivery</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <!--                    <td></td>-->
                    <td></td>
                    <td><?php echo $del_chrg; ?></td>
                </tr>

                <tr>
                    <td>Total</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><?php echo $tot_quan; ?></td>
                    <td><?php echo $tot_pro_prc; ?></td>
                    <!--                    <td>--><?php //echo $del_chrg; ?><!--</td>-->
                    <td></td>
                    <td><?php echo $total; ?></td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>

</section>
