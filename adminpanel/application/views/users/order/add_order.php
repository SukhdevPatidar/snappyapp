<section class="conteant">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Lista de pedidos</li>
    </ol>
    <?php echo msg_alert_backend(); ?>
    <!-- general form elements -->
    <div class="box box-primary">

        <div class="box-header with-border form-heading ">
            <h3 class="box-title">Lista de pedidos</h3>
            <form action="<?php echo htmlspecialchars(current_url()) ?>" method="post">
                <div class="form-group">
                    <div class="col-md-4">
                        <label>Usuario</label>
                        <select class="form-control" name="user_id">
                            <option value="">Seleccionar usuario</option>
                            <?php
                            if (isset($all_user) && !empty($all_user)) {
                                foreach ($all_user as $list) {
                                    $uname = get_user_name($list->user_Id);
                                    ?>
                                    <option value="<?php echo $uname->id; ?>"><?php echo $uname->name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label>Estatus de pedido</label>
                        <select class="form-control" name="order_sta">
                            <option value="">Seleccionar estatus</option>
                            <?php
                            if (isset($order_status) && !empty($order_status)) {
                                foreach ($order_status as $st_list) {
                                    ?>
                                    <option value="<?php echo $st_list->order_status; ?>"><?php echo $st_list->order_status; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label></label>
                        <button type="submit" name="search" id="search" class="btn btn-primary">Buscar</button>
                    </div>

                </div>
            </form>

        </div>
        <!-- /.box-header -->
        <!-- form start -->

    </div>
    <!-- /.box -->
</section>

<section class="content">
    <div class="box box-primary">
        <div class="table-responsive">
            <table id="example2" class="table table-hover data-table-export">
                <thead>
                <th>#</th>
                <th>Usuario</th>
                <th>Número de orden</th>
                <th>Metodo de pago</th>
                <th>Total</th>
                <th>Estatus del pago</th>
                <th>Fecha</th>
                <th>Ver</th>
                <th>Estatus de pedido</th>
                <th>Pedido a entregar</th>
                <th>Personal de envío</th>
                </thead>
                <tbody>
                <?php
                $i = "1";
                if (isset($order_list) && !empty($order_list)) {
                    foreach ($order_list as $data) {
                        $ci = &get_instance();
                        $sql = $ci->db->query("select u.name,u.l_name from delivery_boy as db inner join `users` as u on u.id = db.delivery_boy_id where db.order_id = '" . $data->id . "'");
                        $uname = $sql->result();
                        if (isset($uname) && !empty($uname)) {
                            $username = $uname[0]->name . " " . $uname[0]->l_name;
                            $boy_name = isset($username) ? $username : '';
                        } else {
                            $boy_name = "-";
                        }
                        $admin = get_user_name($data->user_Id);
                        if (!empty($admin)) {
                            $a_name = isset($admin->name) ? $admin->name : '';
                            $al_name = isset($admin->l_name) ? $admin->l_name : '';
                            $name = $a_name . $al_name;
                        } else {
                            $name = "-";
                        }

                        $tot_order_prc = 0;
                        // $admin = $this->city_model->get_row('users',array('id'=>$data->user_Id));
                        $order_prc = $this->city_model->get_result('order_products', array('order_id' => $data->id), array("*", "price as product_price"));

                        foreach ($order_prc as $orderProduct) {
                            $suppliesStr = $orderProduct->supplies;
                            $suppliesAmount = 0;
                            if (!empty($suppliesStr)) {
                                $supplies = json_decode($suppliesStr);
                                foreach ($supplies as $supply) {
                                    $suppliesAmount = $suppliesAmount + $supply->supplies_price;
                                }
                            }
                            $appliedProductPrice = $orderProduct->promotional_price ? $orderProduct->promotional_price : $orderProduct->product_price;
                            $tot_order_prc = $tot_order_prc + (intval($orderProduct->quantity) * floatval($appliedProductPrice)) + ($suppliesAmount * intval($orderProduct->quantity));
                        }

                        $total = $tot_order_prc + $data->delievery_price;
                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $name; ?></td>
                            <td><?php echo $data->id; ?></td>
                            <td><?php echo $data->name; ?></td>
                            <td><?php echo $total; ?></td>
                            <td><?php echo $data->payment_status; ?></td>
                            <td><?php echo datetimeFormat($data->created_at) ?></td>
                            <td>
                                <a href="<?php echo base_url('users/order/product_view/' . $data->id . '/' . $data->delievery_price) ?>"
                                   class="btn btn_success">Ver</a></td>
                            <td>
                                <?php
                                if ($data->order_status == "create") {
                                    ?>
                                    <a href="<?php echo base_url('users/order/change_order_status/' . $data->id) ?>"
                                       class="btn cancel_btn">Crear</a>
                                    <?php
                                } else {
                                    echo "Completo";
                                }
                                ?>
                            </td>
                            <td><a href="<?php echo base_url('users/order/order_to_deliveryboy/' . $data->id) ?>"
                                   class="btn">+</a></td>
                            <td><?php echo $boy_name; ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    if (isset($search_list) && !empty($search_list)) {
                        foreach ($search_list as $data) {
                            $ci = &get_instance();
                            $sql = $ci->db->query("select u.name,u.l_name from delivery_boy as db inner join `users` as u on u.id = db.delivery_boy_id where db.order_id = '" . $data->id . "'");
                            $uname = $sql->result();
                            if (isset($uname) && !empty($uname)) {
                                $username = $uname[0]->name . " " . $uname[0]->l_name;
                                $boy_name = isset($username) ? $username : '';
                            } else {
                                $boy_name = "-";
                            }
                            $admin = get_user_name($data->user_Id);
                            if (!empty($admin)) {
                                $a_name = isset($admin->name) ? $admin->name : '';
                                $al_name = isset($admin->l_name) ? $admin->l_name : '';
                                $name = $a_name . $al_name;
                            } else {
                                $name = "-";
                            }
                            $tot_order_prc = '';

                            $order_prc = $this->city_model->get_result('order_products', array('order_id' => $data->id), array("*", "price as product_price"));

                            foreach ($order_prc as $price) {
                                $tot_order_prc = $tot_order_prc + intval($price->quantity) * floatval($price->product_price);
                            }

                            $total = $tot_order_prc + floatval($data->delievery_price);
                            ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $name; ?></td>
                                <td><?php echo $data->id; ?></td>
                                <td><?php echo $data->name; ?></td>
                                <td><?php echo $total; ?></td>
                                <td><?php echo $data->payment_status; ?></td>
                                <td><?php echo datetimeFormat($data->created_at) ?></td>

                                <td>
                                    <a href="<?php echo base_url('users/order/product_view/' . $data->id . '/' . $data->delievery_price) ?>"
                                       class="btn btn_success">Ver</a></td>
                                <td>
                                    <?php
                                    if ($data->order_status == "create") {
                                        ?>
                                        <a href="<?php echo base_url('users/order/change_order_status/' . $data->id) ?>"
                                           class="btn cancel_btn">Crear</a>
                                        <?php
                                    } else {
                                        echo "Complete";
                                    }
                                    ?>
                                </td>
                                <td><a href="<?php echo base_url('users/order/order_to_deliveryboy/' . $data->id) ?>"
                                       class="btn">+</a></td>
                                <td><?php echo $boy_name; ?></td>
                            </tr>
                            <?php
                        }
                    }
                }
                ?>
                </tbody>
            </table>

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('.edit_event').click(function () {
            var row = $(this).val();
            $('div.form-heading').html('<h3 class="box-title">Add City</h3>');
            $('#f_name').val("");
            $('#l_name').val("");
            $('#email').val("");
            $('#mob').val("");
            $('#city_name').val("");
            $("div.form-actions").html('<button type="submit" name="add_admin" class="btn btn-primary">Guardar</button>');
            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('backend/admin/add_admin')); ?>");
            $("input#row").remove();
            if (row != '') {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('backend/admin/get_edit_admin'); ?>",
                    dataType: "JSON",
                    data: {'row': row, 'edit_form': '1'},
                    success: function (data) {
                        if (data != false) {
                            $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Editar administrador');
                            $('#event_name').val(data.event_name);
                            $('#ttl_days').val(data.total_days);

                            $('#f_name').val(data.name);
                            $('#l_name').val(data.l_name);
                            $('#email').val(data.email);
                            $('#mob').val(data.mobile);
                            $('#city_name').val(data.city_name);
                            $("#password").removeAttr("required");
                            $('div.form-actions').html('<button type="submit" class="btn submit_btn">Actualizar</button>');
                            $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('backend/admin/edit_admin')); ?>");
                            $('form#form').append('<input type="hidden" name="row" id="row" value="' + data.id + '" />');


                        }

                    }
                })
            }
        });
    });
</script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<!--script src="<?php //echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php //echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script-->
<script>
    $(document).ready(function () {
        $('#example2').DataTable();
    });
</script>


