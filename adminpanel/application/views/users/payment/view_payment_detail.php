<section class="content1">
<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Assign</li>
      </ol>
       <?php echo msg_alert_backend(); ?>
       <div id="alert"></div>
 <!-- general form elements -->
  <form role="form" id="form" action="" method="post"  enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label style='text-transform: capitalize;' for="inputtext"><span id ="pay_fld">Payment value</span></label>
                  <input type="text" required="" class="form-control" readonly="" id="pay_val" name="pay_val" placeholder="payment value ">
                <span id="chck"> </span>
                </div>
               
              </div>
              <!-- /.box-body -->

              <div class=" form-actions">
                <button type="submit" name="add_city" id="edit_pay" class="btn btn-primary">Submit</button>
                <a class="btn btn-primary" href="<?php echo base_url('users/payment/add_paypal_info') ?>">Back</a>
              </div>
            </form>
        
          <!-- /.box -->
</section>  

 <section class="content">
 <div class="table-responsive">
      <?php
      $method_name = $this->city_model->get_row('payment_methods' , array('id'=>$pay_met_id));
      echo "<span style='font-size:18px;padding:10px 0;'><b>Payment Method</b>:   ".ucfirst($method_name->name)."</span>";
      ?>
<div class="box box-primary">

          <table class="table table-hover data-table-export">
            <thead>
              <th>Title</th>
              <th>Value</th>                                            
              <th>Action</th>                                            
            </thead>
            	<tbody id="data">
              <?php 
                if(isset($show_detail) && !empty($show_detail)){
                  foreach ($show_detail as $data) {
                    ?>
            		
            		<tr>
            			<td style='text-transform: capitalize;'> <strong><?php echo $data->field_name ?></strong></td>
            			<td> <?php
                   if($data->field_value == 'true'){
                    ?>
<!--                     <input type="checkbox" checked="checked">  -->
                    <?php
                   }else if($data->field_value == 'false'){
                    ?>
<!--                     <input type="checkbox" >  -->
                    <?php
                   }
                   else{
//                    echo $data->field_value;
                   }
                    
                                       echo $data->field_value;

                    ?></td>
                  <td><button type="button" value="<?php echo $data->id; ?>" class="btn btn_edit get_paypal_info">Edit</button></td>
            		</tr>
            		
            		
            		  <?php
                  }
                }
              ?>
            		
            	</tbody>
          </table>

            </div>
            </div>
</section>
<script type="text/javascript">
      $("#form").hide();
  
  $(document).on("click",".get_paypal_info",function(){
    
    $('#pay_val').removeAttr("");
    var row = $(this).val();
    $('#pay_val').val("");
    $("input#row").remove();
    $("input#pay_id").remove();
     $("#pay_val").removeAttr("readonly");
    if(row != ''){
      $.ajax({
        type : "POST",
        url  : "<?php echo base_url('users/payment/get_edit_info'); ?>",
        dataType:"JSON",
        data : {'row' : row , 'edit_form' : '1'},
        success : function(data){     
              $("#form").show();

          if(data != false){
            if(data.field_value == 'true'){
              $('#chck').show();
                $("#chck").html('<input type="checkbox" name="pay_chck_val" value="true" id="pay_chck_val" checked="checked">');
                $('#pay_val').hide();
            }else if(data.field_value == 'false'){
              $('#chck').show();
                $("#chck").html('<input type="checkbox" value="false" name="pay_chck_val" id="pay_chck_val">');
                $('#pay_val').hide();
            }else{
              $('#pay_val').show();
                $('#pay_val').val(data.field_value);
                $('#chck').hide();
            }
            $('#pay_fld').html(data.field_name);
            $('form#form').append('<input type="hidden" name="row" id="row" value="'+data.id+'" />');
            $('form#form').append('<input type="hidden" name="pay_id" id="pay_id" value="'+data.payment_method_id+'" />');

          } 

        }
      })
    }
  });
</script>
<script type="text/javascript">
  $(document).on("click","#edit_pay",function(){
   var isCheckBox =  $('#chck').is(":visible"); 
    
    var pay_val;
    if(isCheckBox) {
      if($("#pay_chck_val").is(":checked")) {
          pay_val = 'true';        
      } else {
           pay_val = 'false';
      }
    } else {
      pay_val = $("#pay_val").val();
    }
    var pay_id = $("#pay_id").val();
    var c = 1;
    var id = $("#row").val();
    $("#alert").html('');
    $("#alert").fadeIn();
    if(pay_val == ''){
      alert("Please Fill Information");
    }
    else{
      $.ajax({
        type : "POST",
        url  : "<?php echo base_url('users/payment/update_payment_info'); ?>",
        data : {'pay_val' : pay_val, 'id':id, 'edit_form' : '1'},
        success : function(data){
                $("#form").hide();
          
          if(data != false){
            $("#pay_val").val('');
            $("input#row").remove();
            $("#alert").html(data);
            $("#alert").fadeOut(8000);
            $('span#pay_fld').html('<h3 class="box-title">Add Payment value</h3>');
            setInterval (loadLog(c,pay_id), 7500);
          }
        } 
      });
      return false;
    }
    function loadLog(y,x){
      if(c == 1){   
        var pay_val = document.getElementById('pay_val').value;
        console.log(x);
        $.ajax({
          url: "<?php echo base_url('users/payment/get_update_payment_info'); ?>",
          data : {'pay_id' : x},
          success: function(html){    
        $("#data").html(html); //Insert chat log into the #chatbox div        

      },
    });
      }
      return 2;
    }
  });
</script>
