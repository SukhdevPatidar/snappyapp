<section class="content">
<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Payment</li>
      </ol>
       <?php echo msg_alert_backend(); ?>
 <!-- general form elements -->
          <div class="box box-primary">

            <div class="box-header with-border form-heading ">
              <h3 class="box-title">Assign Payment </h3>
            </div>
            <div id="alert"></div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="table-responsive" id="refresh_sec">
          <table class="table table-hover data-table-export" id="coderefresh">
            <thead>
              <th>#</th>
              <th>Payment Method</th>                        
              <th>Status</th>                        
            </thead>
            <tbody id="data">
            <?php
            $i="1";
            if(isset($payment) && !empty($payment)){
            foreach($payment as $data){              
             
            ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $data->p_name; ?></td>
                <td> 
                <?php
                if($data->status == 1){
                  ?>
                    <button type="button" value="<?php echo $data->id.",".$data->status; ?>" id="change_status" class="btn btn_edit ">Active</button>
                  <?php
                }else{
                  ?>
                    <button type="button" value="<?php echo $data->id.",".$data->status; ?>" id="change_status" class="btn btn_edit ">Deactive</button>
                   <?php
                }
                ?>
                 
              </td>
                </tr>
              <?php
          }
        }
              ?>
                </tbody>
            </table>

            </div>

     </div>
          <!-- /.box -->
</section>  

 <section class="content">
<div class="box box-primary">
      
            </div>
</section>
<script type="text/javascript">
  $(document).on("click","#assign_payment",function(){
    var admin = $("#admin").val();
    var pay_method = $("#pay_method").val();
    $("#alert").html('');
    $("#alert").fadeIn();
    if(admin == '' || pay_method == ''){
        alert("Please Fill All Information");
    }
    else{
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/payment/assign_admin_payment'); ?>",
          data : {'admin' : admin ,'pay_method' : pay_method , 'admin_payment' : '1'},
          success : function(data){
            if(data != false){
              $("#admin").val('');
              $("#pay_method").val('');
              $("#alert").html(data);
              $("#alert").fadeOut(8000);
            }
          } 
        });
        return false;
    }
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#edit_payment').click(function(){
      var row = $(this).val();
      $('div.form-heading').html('<h3 class="box-title">Assign Payment</h3>');
      $('#admin').val("");
      $('#pay_method').val("");
      $("div.form-actions").html('<button type="submit" name="assign_payment" id="assign_payment" class="btn btn-primary">Submit</button>');
      $("form#form").attr('action',"<?php echo htmlspecialchars(base_url('backend/payment/assign_payment')); ?>");
      $("input#row").remove();
      if(row != ''){
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/payment/get_assign_payment'); ?>",
          dataType:"JSON",
          data : {'row' : row , 'edit_form' : '1'},
          success : function(data){         
            if(data != false){
              $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Edit Assign Payment');
             
              $('#admin').val(data.usesrid);
              $('#pay_method').val(data.paymentmethod_id);
              $('div.form-actions').html('<button type="submit" name="edit_assign_payment" id="edit_assign_payment" class="btn btn-primary">Update</button>');
              $("form#form").attr("");
              $('form#form').append('<input type="hidden" name="row" id="row" value="'+data.id+'" />');
              

            } 
            
          }
        })
      }
    });
  });

</script>

<script type="text/javascript">
  $(document).on("click","#edit_assign_payment",function(){
    var admin = $("#admin").val();
    var pay_method = $("#pay_method").val();
    var id = $("#row").val();
    $("#alert").html('');
    $("#alert").fadeIn();
    if(admin == '' || id == '' || pay_method == ''){
        alert("Please Fill All Information");
    }
    else{
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('backend/payment/update_assign_payment_info'); ?>",
          data : {'admin' : admin ,'pay_method' : pay_method ,'id' : id , 'edit_form' : '1'},
          success : function(data){
            if(data != false){
              $("#admin").val('');
              $("#pay_method").val('');
              $("input#row").remove();
              $("#alert").html(data);
              $("#alert").fadeOut(8000);
              $("div.form-actions").html('<button type="submit" name="assign_payment" id="assign_payment" class="btn btn-primary">Submit</button>');
              $('div.form-heading').html('<h3 class="box-title">Assign Payment</h3>');
            }
          } 
        });
        return false;
    }
});
</script>

<script type="text/javascript">
  $(document).on("click","#change_status",function(){
    var str = $(this).val();
    var id = str.split(",");
    var c = 1;
    
    $("#alert").html('');
    $("#alert").fadeIn();
    if(id == ''){
        alert("id is blank");
    }
    else{
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('users/payment/admin_change_status'); ?>",
          data : {'id' : id[0] ,'status' : id[1] , 'change' : '1'},
          success : function(data){
            if(data != false){
              $("#alert").html(data);
              $("#alert").fadeOut(8000);
              
              setInterval (loadLog(c), 7500);

            }
          } 
        });
        return false;
    }
    //Load the file containing the chat log
	function loadLog(){
	  if(c == 1){		
		$.ajax({
			url: "<?php echo base_url('users/payment/get_update_value'); ?>",
			cache: false,
			success: function(html){		
				$("#data").html(html); //Insert chat log into the #chatbox div				
					
		  	},
		});
	}
	return 2;
	}
});
</script>       
