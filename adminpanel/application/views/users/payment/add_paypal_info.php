<style type="text/css">
    #show_method, #show_method_mercadopago, #show_method_todopago, #delr_msg, #add_payment_info, #show_method_payu {
        display: none;
    }
</style>
<section class="content">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Payment</li>
    </ol>
    <?php echo msg_alert_backend(); ?>
    <div id="alert"></div>
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border form-heading">
            <h3 class="box-title">Add Payment Info</h3>
        </div>
        <div id="alert"></div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" id="form" action="" method="post" enctype="multipart/form-data">
            <div class="box-body">


                <div class="form-group">
                    <div class="paymentformss">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <label for="inputtext">Payment Method</label>
                                <select class="form-control" name="pay_method" class="show_pay_meth" id="pay_method">
                                    <option value="">Select Method</option>
                                    <?php
                                    if (isset($payment_mehtd) && !empty($payment_mehtd)) {
                                        foreach ($payment_mehtd as $method) {
                                            ?>
                                            <option value="<?php echo $method->paymentmethod_id . "," . $method->p_code; ?>"><?php echo $method->p_name ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <?php echo form_error('pay_method'); ?>
                            </div>
                        </div>
                        <div id="delr_msg">No Need To Configure</div>

                        <!--             Add Paypal Details -->

                        <div id="show_method">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">PayPal EnvironmentProduction</label>
                                    <input type="text" required="" class="form-control" id="pay_env_prod"
                                           name="pay_env_prod" placeholder="PayPal EnvironmentProduction" novalidate>
                                    <?php echo form_error('pay_env_prod'); ?>
                                </div>

                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">PayPal EnvironmentSandbox</label>
                                    <input type="text" required="" class="form-control" id="pay_env_snb"
                                           name="pay_env_snb" placeholder="PayPal EnvironmentSandbox" novalidate>
                                    <?php echo form_error('pay_env_snb'); ?>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">MerchantName</label>
                                    <input type="text" required="" class="form-control" id="merchantname"
                                           name="merchantname" placeholder="MerchantName" novalidate>
                                    <?php echo form_error('merchantname'); ?>
                                </div>

                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">Merchant PrivacyPolicy URL</label>
                                    <input type="text" required="" class="form-control" id="mer_pri_polcy"
                                           name="mer_pri_polcy" placeholder="Merchant PrivacyPolicy URL" novalidate>
                                    <?php echo form_error('mer_pri_polcy'); ?>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">Merchant UserAgreement URL</label>
                                    <input type="text" required="" class="form-control" id="mer_user_agr_url"
                                           name="mer_user_agr_url" placeholder="Merchant UserAgreement URL" novalidate>
                                    <?php echo form_error('mer_user_agr_url'); ?>
                                </div>

                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">&nbsp;</label>
                                    <div class="form-check">
                                        <input type="checkbox" class="filled-in form-check-input" value="true" id="card"
                                               name="card" novalidate>
                                        <label class="form-check-label" for="checkbox105">Accept CreditCards</label>
                                    </div>
                                    <?php echo form_error('card'); ?>
                                </div>
                            </div>
                        </div>
                        <!--             Add Paypal Details --- End -->


                        <!--Add Mercado Pago Details -->

                        <div id="show_method_mercadopago">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">Client ID</label>
                                    <input type="text" required="" class="form-control" id="mercadopago_client_id"
                                           name="mercadopago_client_id" placeholder="Client ID" novalidate>
                                    <?php echo form_error('mercadopago_client_id'); ?>
                                </div>

                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">Client Secret</label>
                                    <input type="text" required="" class="form-control" id="mercadopago_client_secret"
                                           name="mercadopago_client_secret" placeholder="Client secret" novalidate>
                                    <?php echo form_error('mercadopago_client_secret'); ?>
                                </div>

                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">Access Token</label>
                                    <input type="text" required="" class="form-control" id="mercadopago_access_token"
                                           name="mercadopago_access_token" placeholder="Access Token" novalidate>
                                    <?php echo form_error('mercadopago_access_token'); ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="is_mercadopago_sandbox"
                                               name="is_mercadopago_sandbox" novalidate>
                                        <?php echo "Sandbox Mode"; ?>
                                    </label>
                                </div>
                            </div>

                        </div>

                        <!--Add Mercado Pago Details --- End  -->

                        <!--Add Todo Pago Details -->

                        <div id="show_method_todopago">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">Merchant ID</label>
                                    <input type="text" required="" class="form-control" id="todopago_merchant_id"
                                           name="todopago_merchant_id" placeholder="Merchant ID" novalidate>
                                    <?php echo form_error('todopago_merchant_id'); ?>
                                </div>

                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">Api Key</label>
                                    <input type="text" required="" class="form-control" id="todopago_api_key"
                                           name="todopago_api_key" placeholder="Client secret" novalidate>
                                    <?php echo form_error('todopago_api_key'); ?>
                                </div>
                            </div>

                        </div>

                        <!--Add Mercado Pago Details --- End  -->


                        <!--Add Mercado Pago Details --- End  -->

                        <!--Add Todo Pago Details -->

                        <div id="show_method_payu">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">Public Key</label>
                                    <input type="text" required="" class="form-control" id="payu_public_key"
                                           name="payu_public_key" placeholder="Public Key" novalidate>
                                    <?php echo form_error('payu_public_key'); ?>
                                </div>

                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">Api Key</label>
                                    <input type="text" required="" class="form-control" id="payu_api_key"
                                           name="payu_api_key" placeholder="Api Key" novalidate>
                                    <?php echo form_error('payu_api_key'); ?>
                                </div>

                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">Api Login</label>
                                    <input type="text" required="" class="form-control" id="payu_api_login"
                                           name="payu_api_login" placeholder="Api Login" novalidate>
                                    <?php echo form_error('payu_api_login'); ?>
                                </div>

                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">Merchant Id</label>
                                    <input type="text" required="" class="form-control" id="payu_merchant_id"
                                           name="payu_merchant_id" placeholder="Merchant Id" novalidate>
                                    <?php echo form_error('payu_merchant_id'); ?>
                                </div>

                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="inputtext">Account Id</label>
                                    <input type="text" required="" class="form-control" id="payu_account_id"
                                           name="payu_account_id" placeholder="Account Id" novalidate>
                                    <?php echo form_error('payu_account_id'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="is_payu_sandbox"
                                               name="is_payu_sandbox" novalidate>
                                        <?php echo "Sandbox Mode"; ?>
                                    </label>
                                </div>
                            </div>

                        </div>

                        <!--Add Mercado Pago Details --- End  -->


                    </div>
                </div>


            </div>
            <!-- /.box-body -->

            <div class="box-footer form-actions">
                <button type="submit" name="add_payment_info" id="add_payment_info" class="btn btn-primary">Submit
                </button>
            </div>
        </form>
    </div>
    <!-- /.box -->
</section>

<section class="content">
    <div class="box box-primary">
        <div class="table-responsive">
            <table class="table table-hover data-table-export">
                <thead>
                <th>#</th>
                <th>Payment Method</th>
                <!--th>Code</th>
                  <th>value</th-->
                <th>Action</th>
                </thead>
                <tbody id="data">
                <?php
                $i = "1";
                if (isset($paypat_info) && !empty($paypat_info)) {
                    foreach ($paypat_info as $data) {
                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data->p_name; ?></td>
                            <!--td><?php //echo $data->field_name;
                            ?></td>
              <td><?php //echo $data->field_value;
                            ?></td-->
                            <td>
                                <a href="<?php echo base_url('users/payment/view_payment_detail/' . $data->payment_method_id) ?>"
                                   class="btn btn_edit">View</a>
                            <td><!--button type="button" value="<?php //echo $data->id;
                                ?>" class="btn btn_edit get_paypal_info">Edit</button-->
                                <button type="button" value="<?php echo $data->payment_method_id; ?>"
                                        id="del_paypal_det" class="btn btn_edit">Remove
                                </button>

                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>

        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).on("change", "#pay_method", function () {
        //$("#show_method").show();
        var val = $(this).val();
        var pay_type = val.split(",")[1];

        if (pay_type != '') {
            $("#delr_msg").hide();
            $("#show_method_mercadopago").hide();
            $("#show_method_todopago").hide();
            $("#show_method").hide();
            $("#show_method_payu").hide();
            $("#add_payment_info").hide();

            if (pay_type == "paypal") {
                $("#show_method").show();
                $("#add_payment_info").show();
            } else if (pay_type == "mercadopago") {
                $("#show_method_mercadopago").show();
                $("#add_payment_info").show();
            } else if (pay_type == "todopago") {
                $("#show_method_todopago").show();
                $("#add_payment_info").show();
            } else if (pay_type == "payu") {
                $("#show_method_payu").show();
                $("#add_payment_info").show();
            } else if (pay_type == "cod") {
                $("#delr_msg").show();
            }
        }
    });
</script>
<script type="text/javascript">
    $(document).on("click", "#add_payment_info", function () {
        debugger;

        var type = $("#pay_method").val();
        var pay_type = type.split(",")[1];
        var pay_method = type.split(",")[0];
        var c = 1;

        if (pay_type == "paypal") {
            var pay_env_prod = $("#pay_env_prod").val();
            var pay_env_snb = $("#pay_env_snb").val();
            var merchantname = $("#merchantname").val();
            var mer_pri_polcy = $("#mer_pri_polcy").val();
            var mer_user_agr_url = $("#mer_user_agr_url").val();
            var card = $("#card").val();
            $("#alert").html('');
            $("#alert").fadeIn();
            if (pay_method == '' || pay_env_prod == '' || pay_env_snb == '' || merchantname == '' || mer_pri_polcy == '' || mer_user_agr_url == '' || card == '') {
                alert("Please Fill All Information");
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('users/payment/insert_payment_detail'); ?>",
                    data: {
                        'pay_method_type': pay_type,
                        'pay_method': pay_method,
                        'pay_env_prod': pay_env_prod,
                        'pay_env_snb': pay_env_snb,
                        'merchantname': merchantname,
                        'mer_pri_polcy': mer_pri_polcy,
                        'mer_user_agr_url': mer_user_agr_url,
                        'card': card,
                        'add_payment_det': '1'
                    },
                    success: function (data) {
                        if (data != false) {
                            $("#pay_method").val('');
                            $("#pay_env_prod").val('');
                            $("#pay_env_snb").val('');
                            $("#merchantname").val('');
                            $("#mer_pri_polcy").val('');
                            $("#mer_user_agr_url").val('');
                            $("#alert").html(data);
                            $("#alert").fadeOut(8000);
                            setInterval(loadLog(c), 7500);
                        }
                    }
                });
                return false;
            }
        } else if (pay_type == "mercadopago") {
            var mercadopago_client_id = $("#mercadopago_client_id").val();
            var mercadopago_client_secret = $("#mercadopago_client_secret").val();
            var mercadopago_access_token = $("#mercadopago_access_token").val();
            var is_mercadopago_sandbox = $("#is_mercadopago_sandbox").is(":checked");
            $("#alert").html('');
            $("#alert").fadeIn();
            if (mercadopago_client_id == '' || mercadopago_client_secret == '' || mercadopago_access_token == '') {
                alert("Please Fill All Information");
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('users/payment/insert_payment_detail'); ?>",
                    data: {
                        'pay_method_type': pay_type,
                        'pay_method': pay_method,
                        'client_id': mercadopago_client_id,
                        'client_secret': mercadopago_client_secret,
                        'access_token': mercadopago_access_token,
                        'is_sandbox': is_mercadopago_sandbox,
                        'add_payment_det': '1'
                    },
                    success: function (data) {
                        if (data != false) {
                            $("#pay_method").val('');
                            $("#mercadopago_client_id").val('');
                            $("#mercadopago_client_secret").val('');
                            $("#mercadopago_access_token").val('');
                            $("#alert").html(data);
                            $("#alert").fadeOut(8000);
                            setInterval(loadLog(c), 7500);
                        }
                    }
                });
                return false;
            }
        } else if (pay_type == "todopago") {
            var todopago_merchant_id = $("#todopago_merchant_id").val();
            var todopago_api_key = $("#todopago_api_key").val();
            $("#alert").html('');
            $("#alert").fadeIn();
            if (todopago_merchant_id == '' || todopago_api_key == '') {
                alert("Please Fill All Information");
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('users/payment/insert_payment_detail'); ?>",
                    data: {
                        'pay_method_type': pay_type,
                        'pay_method': pay_method,
                        'merchant_id': todopago_merchant_id,
                        'api_key': todopago_api_key,
                        'add_payment_det': '1'
                    },
                    success: function (data) {
                        if (data != false) {
                            $("#pay_method").val('');
                            $("#todopago_merchant_id").val('');
                            $("#todopago_api_key").val('');
                            $("#alert").html(data);
                            $("#alert").fadeOut(8000);
                            setInterval(loadLog(c), 7500);
                        }
                    }
                });
                return false;
            }
        } else if (pay_type == "payu") {
            var payu_public_key = $("#payu_public_key").val();
            var payu_api_key = $("#payu_api_key").val();
            var payu_api_login = $("#payu_api_login").val();
            var payu_merchant_id = $("#payu_merchant_id").val();
            var payu_account_id = $("#payu_account_id").val();
            var is_payu_sandbox = $("#is_payu_sandbox").is(":checked");
            $("#alert").html('');
            $("#alert").fadeIn();
            if (todopago_merchant_id == '' || todopago_api_key == '') {
                alert("Please Fill All Information");
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('users/payment/insert_payment_detail'); ?>",
                    data: {
                        'pay_method_type': pay_type,
                        'pay_method': pay_method,
                        'payu_public_key': payu_public_key,
                        'payu_api_key': payu_api_key,
                        'payu_api_login': payu_api_login,
                        'payu_merchant_id': payu_merchant_id,
                        'payu_account_id': payu_account_id,
                        'is_payu_sandbox': is_payu_sandbox,
                        'add_payment_det': '1'
                    },
                    success: function (data) {
                        if (data != false) {
                            $("#pay_method").val('');
                            $("#payu_public_key").val('');
                            $("#payu_api_key").val('');
                            $("#payu_api_login").val('');
                            $("#payu_merchant_id").val('');
                            $("#payu_account_id").val('');
                            $("#alert").html(data);
                            $("#alert").fadeOut(8000);
                            setInterval(loadLog(c), 7500);
                        }
                    }
                });
                return false;
            }
        }


        function loadLog() {
            if (c == 1) {
                $.ajax({
                    url: "<?php echo base_url('users/payment/get_update_paypal_detail'); ?>",
                    cache: false,
                    success: function (html) {
                        $("#data").html(html); //Insert chat log into the #chatbox div

                    },
                });
            }
            return 2;
        }
    });
</script>
<script type="text/javascript">
    $(document).on("click", ".get_paypal_info", function () {
        var row = $(this).val();
        $('div.form-heading').html('<h3 class="box-title">Add Paypal Info</h3>');
        $('#city_name').val("");
        $("div.form-actions").html('<button type="submit" name="add_payment_info" id="add_payment_info" class="btn btn-primary">Submit</button>');
        $("form#form").attr('action', "<?php echo htmlspecialchars(base_url('users/payment/add_paypal_info')); ?>");
        $("input#row").remove();
        if (row != '') {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('users/payment/get_edit_info'); ?>",
                dataType: "JSON",
                data: {'row': row, 'edit_form': '1'},
                success: function (data) {
                    if (data != false) {
                        $('div.form-heading').html('<i class="fa fa-edit" aria-hidden="true"></i>Edit Payment Info');

                        $('#pay_method').val(data.payment_method_id);
                        $('#field_name').val(data.field_name);
                        $('#field_value').val(data.field_value);
                        $('div.form-actions').html('<button type="submit" name="edit_pay" id="edit_pay" class="btn btn-primary">Update</button>');
                        $("form#form").attr('action', "");
                        $('form#form').append('<input type="hidden" name="row" id="row" value="' + data.id + '" />');


                    }

                }
            })
        }
    });
</script>
<script type="text/javascript">
    $(document).on("click", "#edit_pay", function () {
        var pay_method = $("#pay_method").val();
        var field_name = $("#field_name").val();
        var field_value = $("#field_value").val();
        var c = 1;
        var id = $("#row").val();
        $("#alert").html('');
        $("#alert").fadeIn();
        if (pay_method == '' || field_name == '' || field_value == '' || id == '') {
            alert("Please Fill All Information");
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('users/payment/update_payment_info'); ?>",
                data: {
                    'pay_method': pay_method,
                    'field_name': field_name,
                    'field_value': field_value,
                    'id': id,
                    'edit_form': '1'
                },
                success: function (data) {
                    if (data != false) {
                        $("#pay_method").val('');
                        $("#field_name").val('');
                        $("#field_value").val('');
                        $("input#row").remove();
                        $("#alert").html(data);
                        $("#alert").fadeOut(8000);
                        $("div.form-actions").html('<button type="submit" name="add_payment_info" id="add_payment_info" class="btn btn-primary">Submit</button>');
                        $('div.form-heading').html('<h3 class="box-title">Add Paypal Info</h3>');
                        setInterval(loadLog(c), 7500);
                    }
                }
            });
            return false;
        }

        function loadLog() {
            if (c == 1) {
                $.ajax({
                    url: "<?php echo base_url('users/payment/get_update_paypal_detail'); ?>",
                    cache: false,
                    success: function (html) {
                        $("#data").html(html); //Insert chat log into the #chatbox div

                    },
                });
            }
            return 2;
        }
    });
</script>
<script type="text/javascript">
    $(document).on("click", "#del_paypal_det", function () {
        var id = $(this).val();
        var c = 1;
        $("#alert").html('');
        $("#alert").fadeIn();
        if (id == '') {
            alert("id is blank");
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('users/payment/delete_payment_met'); ?>",
                data: {'id': id, 'delete_paypal_met': '1'},
                success: function (data) {
                    if (data != false) {
                        $("#alert").html(data);
                        $("#alert").fadeOut(8000);
                        setInterval(loadLog(c), 7500);
                    }
                }
            });
            return false;
        }

        function loadLog() {
            if (c == 1) {
                $.ajax({
                    url: "<?php echo base_url('users/payment/get_update_paypal_detail'); ?>",
                    cache: false,
                    success: function (html) {
                        $("#data").html(html); //Insert chat log into the #chatbox div

                    },
                });
            }
            return 2;
        }
    });
</script>

