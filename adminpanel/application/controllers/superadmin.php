<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class superadmin extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('query_model');
    }

    public function dashboard()
    {
        _check_user_login();  //check User login authentication

        $data["users"] = $this->getUsers();
        $data["admins"] = $this->getAdmins();
        $data["categories"] = $this->getCategories();
        $data["products"] = $this->getProducts();
        $data["orders"] = $this->getOrders();

        $data['user_data'] = $this->session->userdata('user_info');
        $data['template'] = 'superadmin/dashboard';
        $this->load->view('templates/superadmin/layout', $data);
    }

    private function getOrders()
    {
        $user = $this->session->userdata('user_info');
        $userId = $user['id'];
        $conditions = array("users.added_by" => $userId);
        $columns = array("orders.*", "pm.name");
        $joins = array(array("payment_methods as pm", "pm.id=orders.payment_method_id", "inner"), array("users", "users.id=orders.provider_id", "inner"));
        $orderBy = array(array("orders.created_at", "desc"));
        $list = $this->query_model->getRows('orders', $conditions, $columns, $orderBy, $joins, null);
        return $list;
    }

    private function getUsers()
    {
        $user = $this->session->userdata('user_info');
        $conditions = array('users.role ' => 'user', "sadmin.id" => $user['id']);
        $coulumns = array("users.*");
        $joins = array(array("users as sadmin", "users.country_id=sadmin.country_id", "inner"));
        $list = $this->query_model->getRows('users', $conditions, $coulumns, null, $joins);
        return $list;
    }

    private function getAdmins()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows('users', array("added_by" => $user['id'], "role" => 'admin'), null, array(array('id', 'desc')));
        return $list;
    }

    private function getProducts()
    {
        $user = $this->session->userdata('user_info');
        $userId = $user['id'];
        $conditions = array("users.added_by" => $userId);
        $columns = array("p.*", "c.category_name");
        $joins = array(array("category as c", "c.id = p.category_id", "inner"), array("users", "users.id=p.user_id", "inner"));
        $orderBy = array(array("p.create_date", "desc"));
        $list = $this->query_model->getRows('product as p', $conditions, $columns, $orderBy, $joins, null);
        return $list;
    }

    private function getCategories()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows('category', array("added_by" => $user['id']), null, array(array('id', 'desc')));
        return $list;
    }

}
