<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $user = $this->session->userdata('user_info');
        if (isset($user) && !empty($user)) {
            if ($user['role'] == 'superadmin') {
                redirect('superadmin/dashboard');
            } else {
                redirect('admin/dashboard');
            }
        } else {

            $this->load->view('login');
        }
    }

    public function login()
    {
        $data = array(
            'email' => $this->input->post('username'),
            'password' => $this->input->post('password'),
        );
        $result = $this->user_model->auth_login('users', $data);
        if ($result == true) {
            $conditions = array('email' => $this->input->post('username'));
            $columns = array("users.*", "country.timezone");
            $joins = array(
                array("country", "country.id=users.country_id", "left")
            );
            $get_data = $this->query_model->getRow('users', $conditions, $columns, null, $joins);

            if ($get_data) {
                $user_data = array(
                    'id' => $get_data->id,
                    'name' => $get_data->name,
                    'l_name' => $get_data->l_name,
                    'email' => $get_data->email,
                    'mobile' => $get_data->mobile,
                    'role' => $get_data->role,
                    'timezone' => $get_data->timezone,
                    'country_id' => $get_data->country_id,
                    'is_user' => TRUE,
                    'logged_in' => TRUE
                );
                $this->session->set_userdata('user_info', $user_data);
                $user = $this->session->userdata('user_info');

                if ($get_data->role == 'superadmin') {

                    redirect('superadmin/dashboard');

                } else {
                    redirect('admin/dashboard');
                }
            }

        } else {
            $this->session->set_flashdata('msg_error', "Wrong Creadinction");
            redirect('user/index');
        }
    }

    public function logout()
    {
        _check_user_login();  //check  login authentication
        $this->session->unset_userdata('user_info');
        $this->session->sess_destroy();
        redirect('user/index');
    }

    public function dashboard()
    {

        $data['template'] = 'superadmin/welcome_message';
        $this->load->view('templates/superadmin/layout', $data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
