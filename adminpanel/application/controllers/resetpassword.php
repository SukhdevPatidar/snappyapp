<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Resetpassword extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('api/auth_model');
        $this->load->helper('URL');
        $this->load->library('session');
    }

    public function index()
    {
        $resetId = $this->input->get('resetid');
        if ($resetId) {
            if ($this->auth_model->checkResetPasswordCode($resetId)) {
                $data = array("resetId" => $resetId);
                $this->load->view('resetpassword', $data);
            } else {
                $data = array("error" => "Invalid Request");
                $this->load->view('resetpassword', $data);
            }
        }
    }

    public function updatedPassword()
    {
        $password = $this->input->post('password');
        $resetId = $this->input->post('resetId');

        $data = array();
        if ($this->auth_model->updatePasswordByResetCode($resetId, array("password" => $password, "reset_password_code" => ""))) {
            $data['updateSuccess'] = 'Password successfully updated';
        } else {
            $data['updateError'] = 'Password not updated';
        }
        $this->load->view('resetpassword', $data);
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
