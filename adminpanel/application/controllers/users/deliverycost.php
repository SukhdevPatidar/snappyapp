<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deliverycost extends CI_Controller {

	public function __construct()
  {
    parent::__construct();
        //clear_cache();
    $this->load->helper('form');
    $this->load->model('data_model');
  }

  public function index()
  {
    $session_data = $this->session->userdata('user_info');
    $userId = $session_data['id'];
		
    $deliveryCost = $this->data_model->get_result('delivery_cost', array("user_id" => $userId), array("delivery_cost.*", "city.city_name"), NULL, array(array("city", "city.id=delivery_cost.city_id", "inner")));;
    $assignedCity = $this->data_model->get_result('assign_city', array('assign_city.admin_name'=>$userId), array('city.*'), NULL, array(array('city', 'assign_city.city=city.id', 'inner')));
		
    $data['data'] = $deliveryCost[0];    
		$data['userId'] = $userId;    
    $data['assignedCity'] = $assignedCity;    
    $data['template'] = 'users/delivery_cost';
    $this->load->view('templates/users/layout',$data);
  }

  public function getDelieveryCost() {
		$session_data = $this->session->userdata('user_info');
    $userId = $session_data['id'];
		
    $deliveryCost = $this->data_model->get_result('delivery_cost', array("user_id" => $userId), array("delivery_cost.*", "city.city_name"), NULL, array(array("city", "city.id=delivery_cost.city_id", "inner")));;

    if ($deliveryCost == false) {
      echo json_encode(array("status"=>false));
    } else {
      echo json_encode($deliveryCost);
    }
  }

  public function addDeliveryCost() {
    $cost = $this->input->post("cost");
    $cityId = $this->input->post("city_id");
    $userId = $this->input->post("user_id");

	  $costForCity = $this->data_model->get_row('delivery_cost', array('user_id'=>$userId, 'city_id'=>$cityId));
		if($costForCity) {
      echo json_encode(array("status"=>false, "error" => "Delivery cost already added for this city"));
		} else {
			 $data = array(
      'cost' => $cost,
      'user_id' => $userId,
      'city_id' => $cityId
    );

    $insertedId = $this->data_model->insert('delivery_cost', $data);

    if ($insertedId) {
      echo json_encode(array("status"=>true, "data"=>$insertedId));
		} else { 
      echo json_encode(array("status"=>false));
    }
		}
   
  }

  public function updateDeliveryCost() {
    $id = $this->input->post("delievery_id");
    $cost = $this->input->post("cost");
		$cityId = $this->input->post("city_id");
    $userId = $this->input->post("user_id");

    $data = array(
      'cost' => $cost,
			'user_id' => $userId,
      'city_id' => $cityId
    );

    $result = $this->data_model->update('delivery_cost', $data, array('id'=>$id));
    if ($result == false) {
      echo json_encode(array("status"=>false));
    } else {
      $data = $this->data_model->get_result('delivery_cost', array('id'=>$id));
      echo json_encode(array("status"=>true, "data"=>$data));
    }
  }

  public function deleteDeliveryCost() {
    $id = $this->input->get('id', TRUE);
    $result = $this->data_model->delete('delivery_cost', array('id'=>$id));
    if ($result == false) {
      echo json_encode(array("status"=>false));
    } else {
      echo json_encode(array("status"=>true));
    }

  }
}
