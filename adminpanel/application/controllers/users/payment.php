<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class payment extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('city_model');
    }

    public function show_ass_pay()
    {
        _check_user_login();  //check User login authentication
        $session_data = $this->session->userdata('user_info');
        $ci = &get_instance();

        $sql = $ci->db->query("select apm.*, pm.code as p_code, pm.name as p_name, u.name as admin_name from `admin_paymentsmethods` as apm inner join `payment_methods` as pm on pm.id = apm.paymentmethod_id inner join `users` as u on u.id = apm.usesrid where apm.usesrid = '" . $session_data['id'] . "' order by apm.id desc");
        $data['payment'] = $sql->result();
        $data['template'] = 'users/payment/show_ass_pay';
        $this->load->view('templates/users/layout', $data);
    }

    public function admin_change_status()
    {
        if (isset($_POST['change']) && $_POST['change'] == 1) {
            $where = array('id' => $_POST['id']);
            if ($_POST['status'] == 1) {
                $update = array('status' => 0);
                $result = $this->city_model->update('admin_paymentsmethods', $update, $where);
                if ($result == true) {
                    echo "Status Successfully Deactivate.";
                } else {
                    echo "Not Deactivate.";
                }
            } else {
                $update = array('status' => 1);
                $result = $this->city_model->update('admin_paymentsmethods', $update, $where);
                if ($result == true) {
                    echo "Status Successfully Activate.";
                } else {
                    echo "Not Activate.";
                }
            }

        }
    }

    public function get_update_value()
    {

        $session_data = $this->session->userdata('user_info');
        $ci = &get_instance();

        $sql = $ci->db->query("select apm.*, pm.code as p_code, pm.name as p_name, u.name as admin_name from `admin_paymentsmethods` as apm inner join `payment_methods` as pm on pm.id = apm.paymentmethod_id inner join `users` as u on u.id = apm.usesrid where apm.usesrid = '" . $session_data['id'] . "' order by apm.id desc");
        $payment = $sql->result();
        $html = '';
        $i = 1;
        foreach ($payment as $data) {
            $html .= "<tr>";
            $html .= "<td>" . $i++ . "</td>";
            $html .= "<td>" . $data->p_name . "</td>";
            if ($data->status == 1) {
                $html .= "<td><button type='button' value='" . $data->id . "," . $data->status . "' id='change_status' class='btn btn_edit'>Active</button></td>";
            } else {
                $html .= "<td><button type='button' value='" . $data->id . "," . $data->status . "' id='change_status' class='btn btn_edit'>Deactive</button></td>";
                $html .= "</tr>";
            }
        }
        echo $html;
    }

    public function add_paypal_info()
    {
        _check_user_login();  //check User login authentication
        $session_data = $this->session->userdata('user_info');
        $ci = &get_instance();

        $sql = $ci->db->query("select apm.*, pm.code as p_code, pm.name as p_name from `admin_paymentsmethods` as apm inner join `payment_methods` as pm on pm.id = apm.paymentmethod_id where apm.usesrid = '" . $session_data['id'] . "' ");
        $data['payment_mehtd'] = $sql->result();

        $sql = $ci->db->query("select pc.*, pm.code as p_code, pm.name as p_name from `payment_config` as pc inner join `payment_methods` as pm on pm.id = pc.payment_method_id where pc.user_id = '" . $session_data['id'] . "' group by pc.payment_method_id ");
        $data['paypat_info'] = $sql->result();
        $data['template'] = 'users/payment/add_paypal_info';
        $this->load->view('templates/users/layout', $data);
    }

    public function insert_payment_detail()
    {
        _check_user_login();  //check User login authentication
        $session_data = $this->session->userdata('user_info');
        $ci = &get_instance();


        if (isset($_POST['add_payment_det']) && $_POST['add_payment_det'] == 1) {


            $fet_data = array(
                'payment_method_id' => $_POST['pay_method'],
                'user_id' => $session_data['id']
            );
            $dup = $this->city_model->get_row('payment_config', $fet_data);
            if (empty($dup)) {
                $paymentMethodType = $_POST['pay_method_type'];
                $pay = $_POST['pay_method'];
                if ($paymentMethodType == "paypal") {
                    $pay_env_prod = $_POST['pay_env_prod'];
                    $pay_env_snb = $_POST['pay_env_snb'];
                    $merchantname = $_POST['merchantname'];
                    $mer_pri_polcy = $_POST['mer_pri_polcy'];
                    $mer_user_agr_url = $_POST['mer_user_agr_url'];
                    $card1 = $_POST['card'];
                    if ($card1 == "true") {
                        $card = "true";
                    } else {
                        $card = "false";
                    }
                    $u_id = $session_data['id'];
                    $data1 = "(NULL, '" . $u_id . "', '" . $pay . "', 'PayPalEnvironmentProduction', '" . $pay_env_prod . "')";
                    $data2 = "(NULL, '" . $u_id . "', '" . $pay . "', 'PayPalEnvironmentSandbox', '" . $pay_env_snb . "')";
                    $data3 = "(NULL, '" . $u_id . "', '" . $pay . "', 'merchantName', '" . $merchantname . "')";
                    $data4 = "(NULL, '" . $u_id . "', '" . $pay . "', 'merchantPrivacyPolicyURL', '" . $mer_pri_polcy . "')";
                    $data5 = "(NULL, '" . $u_id . "', '" . $pay . "', 'MerchantUserAgreementURL', '" . $mer_user_agr_url . "')";
                    $data6 = "(NULL, '" . $u_id . "', '" . $pay . "', 'acceptCreditCards', '" . $card . "')";
                    $result = $ci->db->query("INSERT INTO `payment_config` (`id`, `user_id`, `payment_method_id`, `field_name`, `field_value`) VALUES " . $data1 . "," . $data2 . "," . $data3 . "," . $data4 . "," . $data5 . "," . $data6 . " ");
                } else if ($paymentMethodType == "mercadopago") {
                    $pay_env_prod = $_POST['client_id'];
                    $pay_env_snb = $_POST['client_secret'];
                    $access_token = $_POST['access_token'];
                    $is_mercadopago_sandbox = $_POST['is_sandbox'];
                    $u_id = $session_data['id'];
                    $data1 = "(NULL, '" . $u_id . "', '" . $pay . "', 'ClientId', '" . $pay_env_prod . "')";
                    $data2 = "(NULL, '" . $u_id . "', '" . $pay . "', 'ClientSecret', '" . $pay_env_snb . "')";
                    $data3 = "(NULL, '" . $u_id . "', '" . $pay . "', 'AccessToken', '" . $access_token . "')";
                    $data4 = "(NULL, '" . $u_id . "', '" . $pay . "', 'IsSandboxMode', '" . $is_mercadopago_sandbox . "')";
                    $result = $ci->db->query("INSERT INTO `payment_config` (`id`, `user_id`, `payment_method_id`, `field_name`, `field_value`) VALUES " . $data1 . "," . $data2 . "," . $data3 . "," . $data4 . " ");
                } else if ($paymentMethodType == "todopago") {
                    $pay_env_prod = $_POST['merchant_id'];
                    $pay_env_snb = $_POST['api_key'];
                    $u_id = $session_data['id'];
                    $data1 = "(NULL, '" . $u_id . "', '" . $pay . "', 'merchant_id', '" . $pay_env_prod . "')";
                    $data2 = "(NULL, '" . $u_id . "', '" . $pay . "', 'api_key', '" . $pay_env_snb . "')";
                    $result = $ci->db->query("INSERT INTO `payment_config` (`id`, `user_id`, `payment_method_id`, `field_name`, `field_value`) VALUES " . $data1 . "," . $data2 . " ");
                } else if ($paymentMethodType == "payu") {
                    $payu_public_key = $_POST['payu_public_key'];
                    $payu_api_key = $_POST['payu_api_key'];
                    $payu_api_login = $_POST['payu_api_login'];
                    $payu_merchant_id = $_POST['payu_merchant_id'];
                    $payu_account_id = $_POST['payu_account_id'];
                    $is_payu_sandbox = $_POST['is_payu_sandbox'];
                    $u_id = $session_data['id'];
                    $data1 = "(NULL, '" . $u_id . "', '" . $pay . "', 'payu_public_key', '" . $payu_public_key . "')";
                    $data2 = "(NULL, '" . $u_id . "', '" . $pay . "', 'payu_api_key', '" . $payu_api_key . "')";
                    $data3 = "(NULL, '" . $u_id . "', '" . $pay . "', 'payu_api_login', '" . $payu_api_login . "')";
                    $data4 = "(NULL, '" . $u_id . "', '" . $pay . "', 'payu_merchant_id', '" . $payu_merchant_id . "')";
                    $data5 = "(NULL, '" . $u_id . "', '" . $pay . "', 'payu_account_id', '" . $payu_account_id . "')";
                    $data6 = "(NULL, '" . $u_id . "', '" . $pay . "', 'IsSandboxMode', '" . $is_payu_sandbox . "')";
                    $result = $ci->db->query("INSERT INTO `payment_config` (`id`, `user_id`, `payment_method_id`, `field_name`, `field_value`) VALUES " . $data1 . "," . $data2 . "," . $data3 . "," . $data4 . "," . $data5 . "," . $data6 . " ");
                }


//echo $ci->db->last_query();

                if ($result == true) {
                    echo "Payment Detail Successfully Insert";
                } else {
                    echo "Not Insert";
                }
            } else {
                echo "Duplicate method Not Insert";
            }
        }
    }

    public function update_payment_info()
    { ///echo "sdds";die;
        if (isset($_POST['edit_form']) && $_POST['edit_form'] == 1) {

            $where = array('id' => $_POST['id']);
            $update_data = array(
                'field_value' => $_POST['pay_val']
            );
            $result = $this->city_model->update('payment_config', $update_data, $where);
            if ($result == true) {
                echo "Paypal Detail Successfully Update";
            } else {
                echo "Not Update";
            }
        }

    }

    public function get_edit_info()
    {
        if (isset($_POST['edit_form']) && $_POST['edit_form'] == 1) {
            // getting data from database
            $page_row = $this->city_model->get_row('payment_config', array('id' => $_POST['row']));

            echo json_encode($page_row);

        }
    }

    public function delete_payment_met()
    {
        $session_data = $this->session->userdata('user_info');
        if (isset($_POST['delete_paypal_met']) && $_POST['delete_paypal_met'] == 1) {
            $result = $this->city_model->delete('payment_config', array('payment_method_id' => $_POST['id'], 'user_id' => $session_data['id']));
            if ($result == true) {
                echo "Payment Detail Successfully Deleted.";
            } else {
                echo "Payment Detail Not Deleted.";
            }
        }
    }

    public function get_update_paypal_detail()
    {
        _check_user_login();  //check User login authentication
        $session_data = $this->session->userdata('user_info');
        $ci = get_instance();
        $sql = $ci->db->query("select pc.*, pm.name as p_name from `payment_config` as pc inner join `payment_methods` as pm on pm.id = pc.payment_method_id where pc.user_id = '" . $session_data['id'] . "' group by pc.payment_method_id ");
        $paypal = $sql->result();
        $html = '';
        $i = 1;
        foreach ($paypal as $data) {
            $url = "users/payment/view_payment_detail/$data->payment_method_id";
            $html .= "<tr>";
            $html .= "<td>" . $i++ . "</td>";
            $html .= "<td>" . $data->p_name . "</td>";
            //$html .="<td>".$data->field_name."</td>";
            //$html .="<td>".$data->field_value."</td>";
            $html .= "<td><a href=" . base_url($url) . " class='btn cancle_btn'>View</a><button type='button' value='" . $data->id . "' class='btn btn_edit get_paypal_info'>Edit</button><button type='button' value='" . $data->id . "' id='del_paypal_det' class='btn btn_edit get_city'>Remove</button></td>";
            // $html .="<td></td>";
            $html .= "</tr>";
        }
        echo $html;
    }

    public function view_payment_detail($id)
    {
        _check_user_login();  //check User login authentication
        $session_data = $this->session->userdata('user_info');
        $ci = &get_instance();

        $data['pay_met_id'] = $id;
        $data['show_detail'] = $this->city_model->get_result('payment_config', array('payment_method_id' => $id, 'user_id' => $session_data['id']));
        $data['template'] = 'users/payment/view_payment_detail';
        $this->load->view('templates/users/layout', $data);
    }

    public function get_update_payment_info()
    {
        _check_user_login();  //check User login authentication
        $session_data = $this->session->userdata('user_info');
        $show_info = $this->city_model->get_result('payment_config', array('payment_method_id' => $_GET['pay_id'], 'user_id' => $session_data['id']));
        $html = '';
        $i = 1;
        foreach ($show_info as $info) {
            $html .= "<tr>";
            $html .= "<td style='text-transform: capitalize;'> <strong>" . $info->field_name . "</strong></td>";
            $html .= "<td>" . $info->field_value . "</td>";
            $html .= "<td>
     <button type='button' value='" . $info->id . "' class='btn btn_edit get_paypal_info'>Edit</button></td>";
            // $html .="<td></td>";
            $html .= "</tr>";
        }
        echo $html;
    }
}