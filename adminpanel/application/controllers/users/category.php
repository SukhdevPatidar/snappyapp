<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class category extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('category_model');
        $this->load->model('query_model');
    }

    public function add_category()
    {
        _check_user_login();  //check User login authentication
        $user_data = $this->session->userdata('user_info');
        $ci = &get_instance();
        if (isset($_POST['add_category'])) {
            $this->form_validation->set_rules('cat', 'category name', 'required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run()) {
                foreach ($this->input->post('cat') as $cat) {
                    $dup = $ci->db->query("select category from `assign_cat` where `category` = '" . $cat . "' and admin_name = '" . $user_data['id'] . "'");
                    $dup_cat = $dup->result();
                }

                if (!empty($dup_cat)) {
                    $this->session->set_flashdata('msg_error', 'Same category Not Assign .');
                    redirect('users/category/add_category');
                } else {
                    foreach ($this->input->post('cat') as $cat) {
                        $array = array(
                            'admin_name' => $user_data['id'],
                            'category' => $cat,
                            'create_date' => date('Y-m-d')
                        );
                        $result_cat = $this->category_model->insert('assign_cat', $array);
                    }
                    if ($result_cat == true) {
                        $this->session->set_flashdata('msg_success', 'Category Successfully Assign.');
                        redirect('users/category/add_category');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Something went wrong. Please try again.');
                        redirect('users/category/add_category');
                    }
                }
            }
        }
        $data['ass_category_list'] = $this->getAssignedCategories();
        $data['template'] = 'users/category/add_category';
        $this->load->view('templates/users/layout', $data);
    }

    private function getAssignedCategories()
    {
        $user = $this->session->userdata('user_info');

        $conditions = array('admin_name' => $user['id']);
        $columns = array(
            "assign_cat.*",
            "category.category_name",
            "businesstype.name as businessgroup"
        );
        $orderBys = array(array('id', 'desc'));
        $joins = array(
            array("category", "category.id=assign_cat.category", "inner"),
            array("businesstype", "businesstype.id=category.businesstype_id", "left"),
        );
        $groupBys = null;
        $list = $this->query_model->getRows("assign_cat", $conditions, $columns, $orderBys, $joins, $groupBys);
        return $list;
    }


    public function category_file($str)
    {
        if (isset($_FILES['c_image']['name']) && !empty($_FILES['c_image']['name'])) {
            if ($this->session->userdata('c_image')) {
                return TRUE;
            } else {
                $param = array(
                    'file_name' => 'c_image',
                    'upload_path' => './assets/uploads/category/',
                    'allowed_types' => 'jpeg|jpg|png',
                    'image_resize' => FALSE,
                    'source_image' => './assets/uploads/category/',
                    'new_image' => './assets/uploads/category/thumb/',
                    'encrypt_name' => TRUE,
                );


                $upload_file = upload_file($param);
                //thumbnail create
                if ($upload_file['STATUS']) {
                    $this->session->set_userdata('c_image', array('image' => $param['upload_path'] . $upload_file['UPLOAD_DATA']['file_name'], 'thumb_image' => $param['new_image'] . $upload_file['UPLOAD_DATA']['file_name']));
                    return TRUE;
                } else {
                    $this->form_validation->set_message('c_image', $upload_file['FILE_ERROR']);
                    return FALSE;
                }
            }
        }
    }


    public function openTimeSchedule($catId)
    {
        $user = $this->session->userdata('user_info');
        $category = $this->category_model->get_row('category', array('id' => $catId));
        $availibilty = $this->query_model->getRows('category_availability', array("category_id" => $catId, "admin_id" => $user['id']), null, array(array('day', 'asc')));

        $data['category'] = $category;
        $data['availability'] = $availibilty;
        $data['template'] = 'users/category/category_time_schedule';
        $this->load->view('templates/users/layout', $data);
    }

    public function save_timesetting()
    {
        $user = $this->session->userdata('user_info');
        $catId = $this->input->get('catId');

        foreach (weekDays() as $key => $obj) {
            $openingTime = $this->input->post('openingtime' . $key);
            $closingTime = $this->input->post('closingtime' . $key);
            $availablestatus = $this->input->post('availablestatus' . $key);

            $condition = array('category_id' => $catId, 'day' => $key);
            $data = array(
                'category_id' => $catId,
                'day' => $key,
                'opening_time' => $openingTime,
                'closing_time' => $closingTime,
                'open_status' => $availablestatus,
                'admin_id' => $user['id'],
            );
            $checkAlready = $this->query_model->getRows('category_availability', $condition);
            if ($checkAlready) {
                $this->query_model->updateRow('category_availability', $condition, $data);
            } else {
                $this->query_model->insertRow('category_availability', $data);
            }
        }

        $this->session->set_flashdata('msg_success', 'Settings saved successfully.');
        redirect('users/category/add_category');
    }

    public function get_edit_category()
    {
        if (isset($_POST['edit_form']) && $_POST['edit_form'] == 1) {
            // getting data from database
            $page_row = $this->category_model->get_row('category', array('id' => $_POST['row']));

            echo json_encode($page_row);

        }
    }


    public function delete_category($id)
    {

        $result = $this->category_model->delete('assign_cat', array('id' => $id));
        if ($result == true) {
            $this->session->set_flashdata('msg_success', 'Category Successfully Deleted.');
            redirect('users/category/add_category');
        } else {
            $this->session->set_flashdata('msg_error', 'Category Not Delete.');
            redirect('users/category/add_category');
        }


    }
}
