<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class order extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('city_model');
        $this->load->model('query_model');
    }


    public function add_order()
    {
        _check_user_login();  //check User login authentication
        $session_data = $this->session->userdata('user_info');
        $id = $session_data['id'];
        $ci = &get_instance();

        if (isset($_POST['search'])) {
            $where = '';
            $where1 = '';
            $u_name = $this->input->post('user_id');
            $order_sta = $this->input->post('order_sta');
            $where .= "orders.provider_id ='" . $id . "'";
            if (!empty($u_name)) {
                /*if (!empty($where)) {
               $where .= " AND ";
           }*/
                $where .= " AND orders.user_id ='" . $u_name . "'";
            }
            if (!empty($order_sta)) {
                /*if (!empty($where)) {
                $where .= " AND ";
               }*/
                $where .= " AND orders.order_status ='" . $order_sta . "'";
            }


            $data['search_list'] = $this->city_model->fetch_join('orders.*', 'payment_methods.name', 'orders', 'payment_methods', 'orders.payment_method_id = payment_methods.id', 'inner', "$where", array("orders.created_at", "desc"));
            // echo $this->db->last_query();
        } else {
            $data['order_list'] = $this->city_model->fetch_join('orders.*', 'payment_methods.name', 'orders', 'payment_methods', 'orders.payment_method_id = payment_methods.id', 'inner', "orders.provider_id = $id", array("orders.created_at", "desc"));
        }

        $query = $ci->db->query("select * from orders group by order_status");
        $data['order_status'] = $query->result();

        $query2 = $ci->db->query("select * from orders where provider_id = '" . $session_data['id'] . "'group by user_Id");
        $data['all_user'] = $query2->result();

        //$data['all_user'] = $this->city_model->get_result('orders',array('provider_id'=>$session_data['id'] ));

        //$data['order_list'] = $this->city_model->get_result('order',array('user_id'=>$session_data['id']));
        $data['template'] = 'users/order/add_order';
        $this->load->view('templates/users/layout', $data);
    }

    public function product_view($order_id, $del_prc = '')
    {
        $productItems = $this->city_model->get_result('order_products', array("order_id" => $order_id), array("*", "price as product_price"), NULL);

        $data['history'] = $productItems;

        $ci = &get_instance();
        $sql = $ci->db->query("select u.*,u.name as user_name, u.mobile as user_mobile, a.*, a.name as address_name, a.mobile as add_mobile, o.order_notes from `orders` as o inner join `users` as u on u.id = o.user_id inner join `addresses` as a on a.id = o.address_id where o.id = '" . $order_id . "' ");
        $userHistory = $sql->result();
        if (!$userHistory) {
            $sql = $ci->db->query("select u.*,u.name as user_name, u.mobile as user_mobile, a.*, a.name as address_name, a.mobile as add_mobile, o.order_notes from `orders` as o inner join `users` as u on u.id = o.user_id inner join `order_addresses` as a on a.order_id = o.id where o.id = '" . $order_id . "' ");
            $userHistory = $sql->result();
        }
        $data['user_history'] = $userHistory;

        $data['delievery'] = $del_prc;
        $data['template'] = 'users/order/view_product';
        $this->load->view('templates/users/layout', $data);
    }

    public function change_order_status($id)
    {
        $where = array('id' => $id);
        $update = array('order_status' => 'completed');
        $result = $this->city_model->update('orders', $update, $where);
        if ($result == true) {
            $this->session->set_flashdata('msg_success', 'Your Order Is Successfully Completed.');
            redirect('users/order/add_order');
        } else {
            $this->session->set_flashdata('msg_error', 'Somthing Is wrong.');
            redirect('users/order/add_order');
        }
    }


    public function change_status($id, $stat)
    {
        $where = array('id' => $id);
        if ($stat == 0) {
            $update = array('status' => 1);
            $result = $this->city_model->update('order', $update, $where);
            if ($result == true) {
                $this->session->set_flashdata('msg_success', 'Your Order Is Open.');
                redirect('users/order/add_order');
            } else {
                $this->session->set_flashdata('msg_error', 'Your Order Is pending.');
                redirect('users/order/add_order');
            }
        } else if ($stat == 1) {
            $update = array('status' => 2);
            $result = $this->city_model->update('order', $update, $where);
            if ($result == true) {
                $this->session->set_flashdata('msg_success', 'Your Order Is Close.');
                redirect('users/order/add_order');
            } else {
                $this->session->set_flashdata('msg_error', 'Your Order Is Open.');
                redirect('users/order/add_order');
            }
        }
    }

    public function order_to_deliveryboy($id)
    {


        $data['order_id'] = $id;
        $data['boylist'] = $this->getBoyList();
        $data['template'] = 'users/order/boy_list';
        $this->load->view('templates/users/layout', $data);
    }

    public function getBoyList()
    {
        $session_data = $this->session->userdata('user_info');

        $coulumns = array("users.*");
        $conditions = array(
            "dbs.status" => 1,
            'role' => 'delivery_boy',
            'added_by' => $session_data['id']
        );
        $joins = array(
            array("delivery_boy_status as dbs", "dbs.user_id=users.id", "inner")
        );
        $boyList = $this->query_model->getRows('users', $conditions, $coulumns, null, $joins);

        foreach ($boyList as $boy) {
            $conditions = array(
                "db.delivery_boy_id" => $boy->id
            );
            $columns = array("db.order_id");
            $joins = array(
                array("orders", "orders.id=db.order_id AND orders.order_status='create'", "inner"),
            );
            $checkPendingOrder = $this->query_model->getRow("delivery_boy as db", $conditions, $columns, null, $joins);
            if ($checkPendingOrder) {
                $boy->is_pending_order = 1;
            } else {
                $boy->is_pending_order = 0;
            }
        }
        return $boyList;
    }

    public function fetchDeliveryBoyList()
    {
        $list = $this->getBoyList();
        if ($list) {
            $resposne = array(
                "status" => true,
                "data" => $list
            );
        } else {
            $resposne = array(
                "status" => false,
                "message" => "No delivery boy available"
            );
        }
        echo json_encode($resposne);
    }

    public function insert_delivery_boy()
    {
        $ci = &get_instance();
        $isRedirect = $this->input->get('redirect');
        $orderId = $this->input->post('orderId');
        $boy_id = $this->input->post('boy_id');

        $deliveryBoyAssigned = $this->city_model->get_row('delivery_boy', array("order_id" => $orderId), NULL);

        if ($deliveryBoyAssigned) {
            $insert_data = array(
                'order_id' => $orderId,
                'delivery_boy_id' => $boy_id
            );
            $result = $this->city_model->update('delivery_boy', $insert_data, array("id" => $deliveryBoyAssigned->id));
        } else {
            $insert_data = array(
                'order_id' => $orderId,
                'delivery_boy_id' => $boy_id
            );
            $result = $this->city_model->insert('delivery_boy', $insert_data);
        }

        //Get business name
        $condition = array("order_id" => $orderId);
        $columns = array(
            "category.category_name as business_name"
        );
        $joins = array(
            array("category", "category.id=order_products.category_id", "inner"),
            array("businesstype", "businesstype.id=category.businesstype_id", "left")
        );

        $orderProducts = $this->query_model->getRows('order_products', $condition, $columns, NULL, $joins);
        $businessName = "";
        if ($orderProducts) {
            foreach ($orderProducts as $product) {
                $businessName = $product->business_name;
                break;
            }
        }


        $user = $this->city_model->get_row('users', array("id" => $boy_id), NULL);
        // send push notifiation to delivery boy
        $session_data = $this->session->userdata('user_info');
        $providerName = $session_data['name'] . " " . $session_data['l_name'];

        $title = "Snappy Go te ha asignado una nueva orden para entrega.";
        $message = "El número es: " . $orderId . " Negocio: " . $businessName;

        $msgData = [
            "title" => $title,
            "body" => $message,
            "sound" => "default",
            "icon" => "notification_icon"
        ];
        $extraData = [
            "action" => "order_assigned",
            "message" => $msgData,
            "title" => $title,
            "body" => $message
        ];

        $res = $this->sendPushToSnappyGo($user->push_token, $msgData, $extraData);

        if ($result == true) {
            $response = array(
                "status" => true,
                "message" => 'Record Successfully insert.'
            );
            $this->session->set_flashdata('msg_success', 'Record Successfully insert.');
        } else {
            $response = array(
                "status" => false,
                "message" => 'something went wrong please try again.'
            );
            $this->session->set_flashdata('msg_error', 'something went wrong please try again.');
        }
        if (intval($isRedirect) == 1) {
            redirect('users/order/add_order');
        } else {
            echo json_encode($response);
        }
    }

    private function sendPushToSnappyGo($userToken, $msgData, $extraData)
    {
        define('API_ACCESS_KEY', 'AAAAdyid1lE:APA91bFnTXRl8zBFM5NplpYkbKlEiiPmvqG-pXWDYchOH7LFvvyLTDJ4Yj6rHOehUfWhhiWl4N8JScaVzaUZcpDdbMJhZc4mUsmMt96yQZJg72eg-UGItoAR00kI9DpQNy8ZGHeLWjFD');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token = 'cv8GV7iW3KI:APA91bESkfZc-JrgqD6w76i73gV2V455BeQL4RSIyfEPPDV1C38M0EpZKkQogA4vzpGhd6U57EfIX6w8W3mLA5BrPGUWClldSmRtlTap3BvJVkx1vzhlYrI8PSspOvBx-LIHN8VqqtR6';

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to' => $userToken, //single token
            'notification' => $msgData,
            'data' => $extraData
        ];

        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }
}
