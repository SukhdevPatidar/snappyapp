<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class product extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('city_model');
        $this->load->model('query_model');
    }


    public function index()
    {
        _check_user_login();  //check User login authentication
        redirect("users/product/add_product");
    }

    private function defaultViewData($data = array())
    {
        $session_data = $this->session->userdata('user_info');

        $data['cat_list'] = $this->city_model->fetch_join('category.category_name,category.id', 'assign_cat.*', 'assign_cat', 'category', 'category.id = assign_cat.category', 'inner', array('assign_cat.admin_name' => $session_data['id']));
        //echo $this->db->last_query();die;
        $data['paymentOptions'] = $this->city_model->fetch_join('apm.usesrid', 'pm.name,pm.code,pm.id', 'admin_paymentsmethods as apm', 'payment_methods as pm', 'apm.paymentmethod_id = pm.id', 'inner', array('apm.usesrid' => $session_data['id'], 'status' => 1));

        $orderBy = array(
            array("id", "desc")
        );
        $conditions = array(
            "product.user_id" => $session_data['id']
        );
        $columns = array("product.*", "category.category_name", "subcategory.name as subcategory_name");
        $joins = array(
            array("category", "category.id=product.category_id", "inner"),
            array("subcategory", "subcategory.id=product.subcategory_id", "left"),
        );
        $productList = $this->query_model->getRows("product", $conditions, $columns, $orderBy, $joins);


        $data['product_list'] = $productList;
        //print_r($data['product_list']);die;
        $data['template'] = 'users/product/add_product';
        $this->load->view('templates/users/layout', $data);
    }

    public function add_product()
    {
        _check_user_login();  //check User login authentication

        $session_data = $this->session->userdata('user_info');

        if (isset($_POST['add_product'])) {
            $this->form_validation->set_rules('cat_name', 'category name', 'required');
            $this->form_validation->set_rules('subcategory', 'subcategory', 'required');
            $this->form_validation->set_rules('p_name', 'product name', 'required');
            $this->form_validation->set_rules('p_img', 'product Image', 'callback_category_file');
            $this->form_validation->set_rules('price', 'Price', 'required');
            $this->form_validation->set_rules('delivery_time', 'delivery time', 'required');
            $this->form_validation->set_rules('shortDescription', 'Short Description', 'required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');


            $payOptionsForProduct = $this->input->post('product_payment_options');

            if ($this->form_validation->run()) {
                if ($payOptionsForProduct && sizeof($payOptionsForProduct) > 0) {
                    $fet_data = array(
                        'user_id' => $session_data['id'],
                        'category_id' => $this->input->post('cat_name'),
                        'product_name' => $this->input->post('p_name')
                    );
                    $dup = $this->city_model->get_row('product', $fet_data);
                    if (empty($dup)) {
                        $array = array(
                            'user_id' => $session_data['id'],
                            'category_id' => $this->input->post('cat_name'),
                            'subcategory_id' => $this->input->post('subcategory'),
                            'product_name' => $this->input->post('p_name'),
                            'max_supplies_select' => $this->input->post('noOfSuppliesAllowed'),
                            'short_description' => $this->input->post('shortDescription'),
                            'long_description' => $this->input->post('longDescription'),
                            'price' => $this->input->post('price'),
                            'promotional_price' => $this->input->post('promo_price'),
                            'delivery_time' => $this->input->post('delivery_time'),
                            'outofstock' => ($this->input->post('outofstock') == 1) ? 1 : 0,
                            'create_date' => date('Y-m-d')
                        );
                        if ($this->session->userdata('p_img')):
                            $p_img = $this->session->userdata('p_img');
                            $array['product_img'] = $p_img['image'];
                            $this->session->unset_userdata('p_img');
                        endif;

                        $insertedId = $this->city_model->insert('product', $array);
                        if ($insertedId) {
                            //Supplies
                            if (isset($_POST["supplies_name"])) {
                                $suppliesCount = count($_POST["supplies_name"]);
                                if ($suppliesCount && $suppliesCount > 0) {
                                    $suppliesData = array();
                                    for ($i = 0; $i < $suppliesCount; $i++) {
                                        if (!empty($_POST["supplies_name"][$i]) && !empty($_POST["supplies_price"][$i])) {
                                            $suppliesData[] = array(
                                                "product_id" => $insertedId,
                                                "supplies_name" => $_POST["supplies_name"][$i],
                                                "supplies_price" => $_POST["supplies_price"][$i]
                                            );
                                        }
                                    }
                                    $this->query_model->insertMultipleRow('product_supplies', $suppliesData);
                                }
                            }

                            //Supplies End


                            $payOptionsForProductData = array();
                            if (isset($payOptionsForProduct) && !empty($payOptionsForProduct)) {
                                foreach ($payOptionsForProduct as $value) {
                                    array_push($payOptionsForProductData, array(
                                        "product_id" => $insertedId,
                                        "paymentmethod_id" => $value
                                    ));
                                }
                                $insertedPayId = $this->city_model->insertBatch('product_payoptions', $payOptionsForProductData);
                            }

                            $this->session->set_flashdata('msg_success', 'Product Added Successfully.');
                            redirect('users/product/add_product');
                        } else {
                            $this->session->set_flashdata('msg_error', 'Something went wrong. Please try again.');
                            redirect('users/product/add_product');
                        }
                    } else {
                        $this->session->set_flashdata('msg_error', 'duplicate Product Not Insert.');
                        redirect('users/product/add_product');
                    }
                } else {
                    $this->session->set_flashdata('msg_error', 'Please select payment method.');
                    redirect('users/product/add_product');
                }


            }
        }

        $this->defaultViewData();
    }

    public function edit_product()
    {

        $this->form_validation->set_rules('cat_name', 'category name', 'required');
        $this->form_validation->set_rules('subcategory', 'subcategory', 'required');
        $this->form_validation->set_rules('p_name', 'product name', 'required');
        $this->form_validation->set_rules('p_img', 'product Image', 'callback_category_file');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('delivery_time', 'delivery time', 'required');
        $this->form_validation->set_rules('shortDescription', 'delivery time', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run()) {
            $productId = $this->input->post('row');
            $where = array('id' => $productId);
            $update_data = array(
                'category_id' => $_POST['cat_name'],
                'subcategory_id' => $this->input->post('subcategory'),
                'product_name' => $_POST['p_name'],
                'price' => $_POST['price'],
                'promotional_price' => $this->input->post('promo_price'),
                'max_supplies_select' => $this->input->post('noOfSuppliesAllowed'),
                'delivery_time' => $_POST['delivery_time'],
                'short_description' => $_POST['shortDescription'],
                'long_description' => $_POST['longDescription'],
                'outofstock' => ($this->input->post('outofstock') == 1) ? 1 : 0,
            );
            if ($this->session->userdata('p_img')):
                $p_img = $this->session->userdata('p_img');
                $update_data['product_img'] = $p_img['image'];
                $this->session->unset_userdata('p_img');
            endif;

            $result = $this->city_model->update('product', $update_data, $where);
            if ($result == true) {
                //Supplies
                if (isset($_POST["supplies_name"])) {
                    $suppliesCount = count($_POST["supplies_name"]);
                    if ($suppliesCount && $suppliesCount > 0) {
                        for ($i = 0; $i < $suppliesCount; $i++) {
                            $suppliesData = array(
                                "product_id" => $productId,
                                "supplies_name" => $_POST["supplies_name"][$i],
                                "supplies_price" => $_POST["supplies_price"][$i]
                            );

                            if (!empty($_POST["supplies_id"][$i])) {
                                $result = $this->query_model->updateRow('product_supplies', array("id" => $_POST["supplies_id"][$i]), $suppliesData);
                            } else {
                                $this->query_model->insertRow('product_supplies', $suppliesData);
                            }
                        }
                    }
                }
                //Supplies End

                //Removed Supplies
                if (isset($_POST["removed_supplies_id"])) {
                    $removedSuppliesCount = count($_POST["removed_supplies_id"]);
                    if ($removedSuppliesCount && $removedSuppliesCount > 0) {
                        for ($i = 0; $i < $removedSuppliesCount; $i++) {
                            $this->query_model->deleteRow('product_supplies', array("id" => $_POST["removed_supplies_id"][$i]));
                        }
                    }
                }
                //Removed Supplies End


                // $where = array('product_id'=>$this->input->post('row'));
                $this->city_model->delete('product_payoptions', array('product_id' => $this->input->post('row')));
                $payOptionsForProduct = $this->input->post('product_payment_options');
                $payOptionsForid = $this->input->post('p_id');
                //print_r($payOptionsForProduct);die;
                $payOptionsForProductData = array();
                if (isset($payOptionsForProduct) && !empty($payOptionsForProduct)) {
                    foreach ($payOptionsForProduct as $x => $value) {
                        //echo $x."-".$value."- ".$payOptionsForProduct[$value];
                        // $where = array('id'=>$value);
                        $product_id = isset($payOptionsForProduct[$value]) ? $payOptionsForProduct[$value] : 0;
                        $payOptionsForProductData = array(
                            "product_id" => $this->input->post('row'),
                            'paymentmethod_id' => $value
                        );

                        $insertedPayId = $this->city_model->insert('product_payoptions', $payOptionsForProductData);
                        //echo $this->db->last_query();die;
                    }
                }
                if (isset($insertedPayId) && $insertedPayId == true || $result == true) {
                    $this->session->set_flashdata('msg_success', 'Product Is Successfully Update.');
                    redirect('users/product/add_product');
                } else {
                    $this->session->set_flashdata('msg_success', 'Product Not Update.');
                    redirect('users/product/add_product');
                }
            } else {
                $this->session->set_flashdata('msg_error', 'Product Is Not Update.');
                redirect('users/product/add_product');
            }
        }
        $this->defaultViewData();
    }

    public function category_file($str)
    {
        if (isset($_FILES['p_img']['name']) && !empty($_FILES['p_img']['name'])) {
            if ($this->session->userdata('p_img')) {
                return TRUE;
            } else {
                $param = array(
                    'file_name' => 'p_img',
                    'upload_path' => './assets/uploads/product/',
                    'allowed_types' => 'jpeg|jpg|png',
                    'image_resize' => FALSE,
                    'source_image' => './assets/uploads/product/',
                    'new_image' => './assets/uploads/product/thumb/',
                    'encrypt_name' => TRUE,
                );


                $upload_file = upload_file($param);
                //thumbnail create
                if ($upload_file['STATUS']) {
                    $this->session->set_userdata('p_img', array('image' => $param['upload_path'] . $upload_file['UPLOAD_DATA']['file_name'], 'thumb_image' => $param['new_image'] . $upload_file['UPLOAD_DATA']['file_name']));
                    return TRUE;
                } else {
                    $this->form_validation->set_message('p_img', $upload_file['FILE_ERROR']);
                    return FALSE;
                }
            }
        }
    }

    public function insert_product()
    {
        _check_user_login();  //check User login authentication
        $session_data = $this->session->userdata('user_info');
        if (isset($_POST['add_form']) && $_POST['add_form'] == 1) {
            $fet_data = array(
                'user_id' => $session_data['id'],
                'category_id' => $_POST['cat_name'],
                'product_name' => $_POST['p_name']
            );
            $dup = $this->city_model->get_row('product', $fet_data);
            if (empty($dup)) {
                $add_data = array(
                    'user_id' => $session_data['id'],
                    'category_id' => $_POST['cat_name'],
                    'product_name' => $_POST['p_name'],
                    'create_date' => date('Y-m-d')
                );
                $result = $this->city_model->insert('product', $add_data);
                echo "Data successfully insert";
            } else {
                echo "Duplicate Product Not Insert";
            }
        }
    }

    public function get_edit_product()
    {
        if (isset($_POST['edit_form']) && $_POST['edit_form'] == 1) {
            // getting data from database
            $productData = $this->query_model->getRow("product", array('id' => $_POST['row']));
            if ($productData) {
                $subcategories = $this->subcategories($productData->category_id);
                echo json_encode(array(
                    "status" => true,
                    "data" => $productData,
                    "subcategories" => $subcategories
                ));
            } else {
                echo json_encode(array(
                    "status" => false,
                    "message" => "No product data found"
                ));
            }

        }
    }

    public function get_edit_payment_opt()
    {
        if (isset($_POST['edit_pay']) && $_POST['edit_pay'] == 1) {
            // getting data from database
            $page_row = $this->city_model->get_result('product_payoptions', array('product_id' => $_POST['row']));

            echo json_encode($page_row);

        }
    }


    public function delete_product($id)
    {
        $where = array('id' => $id);

        $result = $this->city_model->delete('product', $where);
        if ($result == true) {
            $this->session->set_flashdata('msg_success', 'Product Is Successfully delete.');
            redirect('users/product/add_product');
        } else {
            $this->session->set_flashdata('msg_error', 'Product Is Not Delete.');
            redirect('users/product/add_product');
        }
    }

    public function product_list()
    {
        _check_user_login();  //check User login authentication
        $ci = &get_instance();
        $session_data = $this->session->userdata('user_info');
        $session_id = $session_data['id'];
        if (isset($_POST['search_product'])) {
            $where = '';
            $where1 = '';
            $cat_id = $this->input->post('cat_id');
            $pro_id = $this->input->post('pro_id');
            $where .= "p.user_id ='" . $session_id . "'";
            if (!empty($cat_id)) {
                /*if (!empty($where)) {
               $where .= " AND ";
           }*/
                $where .= " AND p.category_id ='" . $cat_id . "'";
            }
            if (!empty($pro_id)) {
                /*if (!empty($where)) {
                $where .= " AND ";
               }*/
                $where .= " AND p.product_name ='" . $pro_id . "'";
            }
            //echo $where;
            //echo "select p.*, c.category_name from `product` as p inner join `category` as c on c.id = p.category_id where $where order by id desc ";
            $sql = $ci->db->query("select p.*, c.category_name from `product` as p inner join `category` as c on c.id = p.category_id where $where order by id desc ");
            $data['srch_product_list'] = $sql->result();


        } else {
            $sql = $ci->db->query("select p.*, c.category_name from `product` as p inner join `category` as c on c.id = p.category_id where p.user_id = '" . $session_id . "' order by id desc ");
            $data['product_list'] = $sql->result();

        }


        $data['category'] = $this->city_model->get_result('category');
        $query = $ci->db->query("select id, product_name from product where user_id = '" . $session_id . "' group by product_name");
        $data['prd_list'] = $query->result();
        $data['template'] = 'users/product/product_list';
        $this->load->view('templates/users/layout', $data);
    }

    public function suppliesItem()
    {
        if (!empty($_GET)) {
            $productId = $_GET['product_id'];
            $supplies = $this->query_model->getRows('product_supplies', array("product_id" => $productId));
            if ($supplies) {
                foreach ($supplies as $supply) {
                    $data = array();
                    $data['data'] = $supply;
                    $this->load->view('users/product/supplies_input', $data);
                }
            }
        } else {
            $this->load->view('users/product/supplies_input');
        }
    }

    public function getSubcategories()
    {
        $category_id = $this->input->post('category_id');

        $list = $this->subcategories($category_id);

        if ($list) {
            $response = array(
                "status" => true,
                "data" => $list
            );
        } else {
            $response = array(
                "status" => false,
                "message" => "No Subcategories found"
            );
        }

        echo json_encode($response);
    }

    private function subcategories($categoryId)
    {
        $columns = array(
            "subcategory.*"
        );

        $list = $this->query_model->getRows('subcategory', array("subcategory.category_id" => $categoryId), $columns, array(array('id', 'desc')));
        return $list;
    }
}
