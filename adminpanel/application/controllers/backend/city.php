<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class city extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('city_model');
        $this->load->model('query_model');
    }

   public function add_city(){
   	_check_user_login();  //check User login authentication

   	if(isset($_POST['add_city'])){
		$this->form_validation->set_rules('city_name','city name','required');

   			if($this->form_validation->run()){
   			$dup = $this->city_model->get_row('city',array('city_name'=>$this->input->post('city_name')));
				if(empty($dup)){
				$user = $this->session->userdata('user_info');
        $array = array(
								'city_name'	=> $this->input->post('city_name'),
								'added_by' => $user['id']
							);
			
				if($this->city_model->insert('city',$array)){
					$this->session->set_flashdata('msg_success','city Added Successfully.');
					redirect('backend/city/add_city');
				}else{
					$this->session->set_flashdata('msg_error','Something went wrong. Please try again.');
					redirect('backend/city/add_city');
				}
      }
      else{
        $this->session->set_flashdata('msg_error','Same City Not Insert again.');
        redirect('backend/city/add_city');
      }
			}
   	}
		
   	$data['city'] = $this->getCities();
   	$data['template'] = 'superadmin/city/add_city';
   	$this->load->view('templates/superadmin/layout',$data);
   }

	private function getCities() {
		$user = $this->session->userdata('user_info');
		$cities = $this->query_model->getRows('city', array("added_by" => $user['id']), null, array(array('id','desc')));
		return $cities;
	}
	
   public function insert_city() { 
    _check_user_login();  //check User login authentication
      if(isset($_POST['add_form']) && $_POST['add_form'] == 1){
        $dup = $this->city_model->get_row('city',array('city_name'=>$_POST['city_name']));
        if(empty($dup)){
					$user = $this->session->userdata('user_info');
          $array = array(
                'city_name' => $_POST['city_name'],
								'added_by' => $user['id']
              );
					
          $result = $this->query_model->insertRow('city',$array);
          if($result){
            echo "<span class='success_msg'>City Successfully Insert</span>";
          }
          else{
            echo "<span class='error_msg'>Not Insert".$_POST['user_id']."</span>";
          }
        }
        else{
          echo "<span class='error_msg'>Duplicate City Not Insert</span>";
        }
      }
   }

    public function update_city(){ 
    _check_user_login();  //check User login authentication
      if(isset($_POST['edit_form']) && $_POST['edit_form'] == 1){
        $where = array('id'=>$_POST['id']);
          $update_data = array(
                'city_name' => $_POST['city_name']
              );
          $result = $this->city_model->update('city',$update_data,$where);
          if($result == true){
            echo "<span class='success_msg'>City Successfully Update</span>";
          }
          else{
            echo "<span class='error_msg'>Not Update</span>";
          }
        }
      }
   

   public function get_edit_city(){
    if(isset($_POST['edit_form']) && $_POST['edit_form'] == 1){
      // getting data from database
      $page_row = $this->city_model->get_row('city' , array('id'=>$_POST['row']));
      
      echo json_encode($page_row);
      
    }
  }

  public function edit_city(){
    $where = array('id'=>$this->input->post('row'));
    $dup = $this->city_model->get_row('city',array('city_name' => $this->input->post('city_name')));
      if(empty($dup)){
        $update_data = array(
                    'city_name' => $this->input->post('city_name')
                  );
        $result = $this->city_model->update('city',$update_data,$where);
        if($result == true){
            $this->session->set_flashdata('msg_success','City Successfully Update.');
            redirect('backend/city/add_city');
        }
        else{
            $this->session->set_flashdata('msg_error','City Not Update.');
            redirect('backend/city/add_city');
        }
      }else{
        $this->session->set_flashdata('msg_error','City Already Exist.');
        redirect('backend/city/add_city');
      }
  }

   public function delete_city(){
    if(isset($_POST['delete_city']) && $_POST['delete_city'] == 1){
      $result = $this->city_model->delete('city',array('id'=>$_POST['id']));
      if($result == true){
        $this->city_model->delete('assign_city',array('city'=>$_POST['id']));
        echo "<span class='success_msg'>City Successfully Deleted.</span>";
      }
      else{
       echo "<span class='error_msg'>City Not Deleted.</span>";
      }
    }
   }

   public function get_update_value(){
    $city = $this->getCities();

    $html = '';
    $i = 1;
    foreach ($city as $data){
       $html .= "<tr>";
       $html .="<td>".$i++."</td>";
       $html .="<td>".$data->city_name."</td>";
       $html .="<td><button type='button' value='".$data->id."' id='get_city' class='btn btn_edit get_city'>Edit</button><button type='button' value='".$data->id."' id='del_city' class='btn btn_edit'>Remove</button></td>";
       // $html .="<td></td>";
        $html .="</tr>";
    }
    echo $html;
   }
}
