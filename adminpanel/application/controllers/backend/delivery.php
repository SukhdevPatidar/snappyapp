<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class delivery extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('city_model');
    }

    public function add_boy()
    {
        _check_user_login();  //check User login authentication

        $data['list'] = $this->city_model->get_result('users', array('role' => 'delivery_boy'));
        $data['template'] = 'superadmin/deliveryboy/add_boy';
        $this->load->view('templates/superadmin/layout', $data);
    }

    public function insert_boy()
    {
        _check_user_login();  //check User login authentication
        if (isset($_POST['add_boy']) && $_POST['add_boy'] == 1) {
            $dup = $this->city_model->get_row('users', array('email' => $_POST['email']));
            if (empty($dup)) {
                $array = array(
                    'name' => $_POST['f_name'],
                    'l_name' => $_POST['l_name'],
                    'email' => $_POST['email'],
                    'password' => $_POST['password'],
                    'mobile' => $_POST['mob'],
                    'role' => 'delivery_boy',
                    'create_date' => date('Y-m-d'),
                );
                $result = $this->city_model->insert('users', $array);
                if ($result == true) {
                    echo "<span class='success_msg'>Delivery Boy Successfully Insert</span>";
                } else {
                    echo "<span class='error_msg'>Not Insert</span>";
                }
            } else {
                echo "<span class='error_msg'>Same Email can't be used</span>";
            }
        }
    }

    public function update_boy()
    {
        _check_user_login();  //check User login authentication
        if (isset($_POST['edit_boy']) && $_POST['edit_boy'] == 1) {
            $where = array('id' => $_POST['id']);
            $update_data = array(
                'name' => $_POST['f_name'],
                'l_name' => $_POST['l_name'],
                'email' => $_POST['email'],
                'password' => $_POST['password'],
                'mobile' => $_POST['mob']
            );
            $result = $this->city_model->update('users', $update_data, $where);
            if ($result == true) {
                echo "<span class='success_msg'>Delivery Boy Successfully Update</span>";
            } else {
                echo "<span class='error_msg'>Not Update</span>";
            }
        }
    }


    public function delete_boy()
    {
        if (isset($_POST['delete_boy']) && $_POST['delete_boy'] == 1) {
            $result = $this->city_model->delete('users', array('id' => $_POST['id']));
            if ($result == true) {
                echo "<span class='success_msg'>Delivery Boy Successfully Deleted.</span>";
            } else {
                echo "<span class='error_msg'> Not Deleted.</span>";
            }
        }
    }


    public function get_edit_boy()
    {
        if (isset($_POST['edit_form']) && $_POST['edit_form'] == 1) {
            // getting data from database
            $page_row = $this->city_model->get_row('users', array('id' => $_POST['row']));

            echo json_encode($page_row);

        }
    }

    public function edit_boy()
    {
        $where = array('id' => $this->input->post('row'));
        $update_data = array(
            'name' => $this->input->post('f_name'),
            'l_name' => $this->input->post('l_name'),
            'email' => $this->input->post('email'),
            'mobile' => $this->input->post('mob'),
        );
        $result = $this->city_model->update('users', $update_data, $where);
        if ($result == true) {
            $this->session->set_flashdata('msg_success', 'Admin Data Successfully Update.');
            redirect('backend/admin/add_admin');
        } else {
            $this->session->set_flashdata('msg_error', 'Admin Data Not Update.');
            redirect('backend/admin/add_admin');
        }
    }

    public function assign()
    {
        _check_user_login();  //check User login authentication
        $ci = &get_instance();
        if (isset($_POST['assign_role'])) {
            $dup_cat = array();
            $dup_city = array();
            $this->form_validation->set_rules('admin', 'admin', 'required');
            $this->form_validation->set_rules('cat', 'cat', 'required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run()) {
                foreach ($this->input->post('cat') as $cat) {
                    $dup = $ci->db->query("select category from `assign_cat` where `category` = '" . $cat . "' and admin_name = '" . $this->input->post('admin') . "';");
                    $dup_cat = $dup->result();
                }

                if (!empty($dup_cat)) {
                    $this->session->set_flashdata('msg_error', 'Same category Not Assign .');
                    redirect('backend/admin/assign');
                } else {
                    foreach ($this->input->post('cat') as $cat) {
                        $array = array(
                            'admin_name' => $this->input->post('admin'),
                            'category' => $cat,
                        );
                        $result_cat = $this->city_model->insert('assign_cat', $array);
                    }
                    if ($result_cat == true) {
                        $this->session->set_flashdata('msg_success', 'Category Successfully Assign.');
                        redirect('backend/admin/assign');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Something went wrong. Please try again.');
                        redirect('backend/admin/assign');
                    }
                }

            }
        }
        $query = $ci->db->query("select * from `assign_cat` group by admin_name ");
        $data['role_list'] = $query->result();
        $data['admin_list'] = $this->city_model->get_result('users', array('role' => 'admin'));
        $data['category_list'] = $this->city_model->get_result('category');
        $data['city_list'] = $this->city_model->get_result('city');
        $data['template'] = 'superadmin/assign/assign_cat';
        $this->load->view('templates/superadmin/layout', $data);
    }

    public function insert_role()
    {
        _check_user_login();  //check User login authentication
        if (isset($_POST['assign_role']) && $_POST['assign_role'] == 1) {
            $dup = $this->city_model->get_row('assign_cat', array('category' => $_POST['cat'], 'admin_name' => $_POST['admin']));
            if (empty($dup)) {
                $array = array(
                    'admin_name' => $_POST['admin'],
                    'category' => $_POST['cat'],
                    'create_date' => date('Y-m-d')
                );
                $result_cat = $this->city_model->insert('assign_cat', $array);
                if ($result_cat == true) {
                    echo "<span class='success_msg'>Category Successfully Assign</span>";
                } else {
                    echo "<span class='error_msg'>Not Insert<span>";
                }
            } else {
                echo "<span class='error_msg'>Same Category can't be used</span>";
            }
        }
    }

    public function insert_city_role()
    {
        _check_user_login();  //check User login authentication
        if (isset($_POST['assign_role_city']) && $_POST['assign_role_city'] == 1) {
            $dup = $this->city_model->get_row('assign_city', array('city' => $_POST['city'], 'admin_name' => $_POST['admin']));
            if (empty($dup)) {
                $array = array(
                    'admin_name' => $_POST['admin'],
                    'city' => $_POST['city'],
                    'create_date' => date('Y-m-d')
                );
                $result_cat = $this->city_model->insert('assign_city', $array);
                if ($result_cat == true) {
                    echo "<span class='success_msg'>city Successfully Assign</span>";
                } else {
                    echo "<span class='error_msg'>Not Insert</span>";
                }
            } else {
                echo "<span class='error_msg'>Same City can't be used</span>";
            }
        }
    }


    public function get_asign_cat($id)
    {

        $data['cat_list'] = $this->city_model->get_result('assign_cat', array('admin_name' => $id));
        $data['admin_id'] = $id;
        $data['template'] = 'superadmin/assign/user_asn_cat';
        $this->load->view('templates/superadmin/layout', $data);
    }

    public function view_assign_city($id)
    {

        $data['city_list'] = $this->city_model->get_result('assign_city', array('admin_name' => $id));
        $data['admin_id'] = $id;
        $data['template'] = 'superadmin/assign/view_assign_city';
        $this->load->view('templates/superadmin/layout', $data);
    }

    public function assign_city()
    {
        _check_user_login();  //check User login authentication
        $ci = &get_instance();
        if (isset($_POST['assign_role'])) {

            $dup_city = array();
            $this->form_validation->set_rules('admin', 'admin', 'required');
            $this->form_validation->set_rules('city', 'city', 'required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run()) {
                foreach ($this->input->post('city') as $cty) {
                    $dup1 = $ci->db->query("select city from `assign_city` where `city` = '" . $cty . "' and admin_name = '" . $this->input->post('admin') . "';");
                    $dup_city = $dup1->result();
                }

                if (empty($dup_city)) {
                    $result_cat = array();
                    foreach ($this->input->post('city') as $cty) {
                        $array = array(
                            'admin_name' => $this->input->post('admin'),
                            'city' => $cty,
                        );
                        $result_cat = $this->city_model->insert('assign_city', $array);
                    }

                    if ($result_cat == true) {
                        $this->session->set_flashdata('msg_success', 'City Successfully Assign.');
                        redirect('backend/admin/assign_city');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Something went wrong. Please try again.');
                        redirect('backend/admin/assign_city');
                    }


                } else {
                    $this->session->set_flashdata('msg_error', 'Same city Not Assign .');
                    redirect('backend/admin/assign_city');
                }

            }
        }
        $query = $ci->db->query("select * from `assign_city` group by admin_name ");
        $data['role_list'] = $query->result();
        $data['admin_list'] = $this->city_model->get_result('users', array('role' => 'admin'));
        $data['category_list'] = $this->city_model->get_result('category');
        $data['city_list'] = $this->city_model->get_result('city');
        $data['template'] = 'superadmin/assign/assign_city';
        $this->load->view('templates/superadmin/layout', $data);
    }

    public function get_update_value()
    {

        $session_data = $this->session->userdata('user_info');
        $boy_list = $this->city_model->get_result('users', array('role' => 'delivery_boy'));

        $html = '';
        $i = 1;
        foreach ($boy_list as $data) {
            $html .= "<tr>";
            $html .= "<td>" . $i++ . "</td>";
            $html .= "<td>" . $data->name . "</td>";
            $html .= "<td>" . $data->email . "</td>";
            $html .= "<td>" . $data->mobile . "</td>";
            $html .= "<td><button type='button' value='" . $data->id . "' id='get_city' class='btn btn_edit edit_boy'>Edit</button><button type='button' value='" . $data->id . "' id='del_boy' class='btn btn_edit'>Remove</button></td>";
            // $html .="<td></td>";
            $html .= "</tr>";
        }
        echo $html;
    }

    public function get_update_assign_cat()
    {

        $session_data = $this->session->userdata('user_info');
        $ci = &get_instance();
        $catlist = $ci->db->query("select * from `assign_cat` group by admin_name ");
        $list = $catlist->result();
        $html = '';
        $i = 1;
        foreach ($list as $data) {
            $url = "backend/admin/get_asign_cat/$data->admin_name";
            $cate = get_cat_name($data->category);
            $name = get_user_name($data->admin_name);
            $f_name = isset($name->name) ? $name->name : '';
            $l_name = isset($name->l_name) ? $name->l_name : '';
            $c_name = isset($cate->category_name) ? $cate->category_name : '';
            $html .= "<tr>";
            $html .= "<td>" . $i++ . "</td>";
            $html .= "<td>" . $f_name . " " . $l_name . "</td>";
            // $html .="<td>".$c_name."</td>";
            $html .= "<td><a href=" . base_url($url) . " class='btn cancle_btn'>View</a><button type='button' value='" . $data->id . "' id='del_cat' class='btn btn_edit'>Remove</button></td>";
            // $html .="<td></td>";
            $html .= "</tr>";
        }
        echo $html;
    }

    public function get_update_assign_city()
    {

        $session_data = $this->session->userdata('user_info');
        $ci = &get_instance();
        $catlist = $ci->db->query("select * from `assign_city` group by admin_name ");
        $list = $catlist->result();
        $html = '';
        $i = 1;
        foreach ($list as $data) {
            $url = "backend/admin/view_assign_city/$data->admin_name";
            $cty = get_city_name($data->city);
            //echo $cty->city_name;
            $ct = isset($cty->city_name) ? $cty->city_name : '';
            $name = get_user_name($data->admin_name);
            $f_name = isset($name->name) ? $name->name : '';
            $l_name = isset($name->l_name) ? $name->l_name : '';

            $html .= "<tr>";
            $html .= "<td>" . $i++ . "</td>";
            $html .= "<td>" . $f_name . " " . $l_name . "</td>";
            // $html .="<td>".$ct."</td>";
            $html .= "<td><a href=" . base_url($url) . " class='btn cancle_btn'>View</a><button type='button' value='" . $data->id . "' id='del_city' class='btn btn_edit get_city'>Remove</button></td>";
            // $html .="<td></td>";
            $html .= "</tr>";
        }
        echo $html;
    }

}
