<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deliverycost extends CI_Controller {

	public function __construct()
  {
    parent::__construct();
        //clear_cache();
    $this->load->helper('form');
    $this->load->model('data_model');
  }

  public function index()
  {
    $deliveryCost = $this->data_model->get_result('delivery_cost');

    $data['data'] = $deliveryCost[0];    
    $data['template'] = 'superadmin/delivery_cost';
    $this->load->view('templates/superadmin/layout',$data);
  }

  public function getDelieveryCost() {
    $deliveryCost = $this->data_model->get_result('delivery_cost');

    if ($deliveryCost == false) {
      echo json_encode(array("status"=>false));
    } else {
      echo json_encode($deliveryCost);
    }
  }

  public function addDeliveryCost() {
    $activeStatus = $this->input->post("active_status");
    $cost = $this->input->post("cost");

    if($activeStatus=="on") {
      $deactiveAll = array("status"=> 0);
      $this->data_model->update('delivery_cost', $deactiveAll);
    }

    $data = array(
      'cost' => $cost,
      'status' => ($activeStatus=="on")?1:0,
    );

    $insertedId = $this->data_model->insert('delivery_cost', $data);

    if ($result == false) {
      echo json_encode(array("status"=>false));
    } else {
      $data = $this->data_model->get_result('delivery_cost', array('id'=>$insertedId));
      echo json_encode(array("status"=>true, "data"=>$data));
    }
  }

  public function updateDeliveryCost() {
    $activeStatus = $this->input->post("active_status");
    $id = $this->input->post("delievery_id");
    $cost = $this->input->post("cost");

    if($activeStatus=="on") {
      $deactiveAll = array("status"=> 0);
      $this->data_model->update('delivery_cost', $deactiveAll);
    }

    $data = array(
      'cost' => $cost,
      'status' => ($activeStatus=="on")?1:0,
    );

    $result = $this->data_model->update('delivery_cost', $data, array('id'=>$id));
    if ($result == false) {
      echo json_encode(array("status"=>false));
    } else {
      $data = $this->data_model->get_result('delivery_cost', array('id'=>$id));
      echo json_encode(array("status"=>true, "data"=>$data));
    }
  }

  public function deleteDeliveryCost() {
    $id = $this->input->get('id', TRUE);
    $result = $this->data_model->delete('delivery_cost', array('id'=>$id));
    if ($result == false) {
      echo json_encode(array("status"=>false));
    } else {
      echo json_encode(array("status"=>true));
    }

  }
}
