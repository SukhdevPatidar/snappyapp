<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class payment extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('city_model');
        $this->load->model('query_model');
    }

    public function add_payment()
    {
        _check_user_login();  //check User login authentication


        $data['payment'] = $this->city_model->get_result('payment_methods');
        $data['template'] = 'superadmin/payment/add_payment';
        $this->load->view('templates/superadmin/layout', $data);
    }

    public function insert_pay()
    {
        _check_user_login();  //check User login authentication
        if (isset($_POST['add_pay']) && $_POST['add_pay'] == 1) {
            $dup = $this->query_model->getRows('payment_methods', array('code' => $_POST['code']));
            if (empty($dup)) {
                $array = array(
                    'name' => $_POST['name'],
                    'code' => $_POST['code'],
                );
                $result = $this->city_model->insert('payment_methods', $array);
                if ($result == true) {
                    echo "<span class='success_msg'>Payment method successfully added.</span>";
                } else {
                    echo "<span class='error_msg'>Not Insert</span>";
                }
            } else {
                echo "<span class='error_msg'>Can't add duplicate payment method</span>";
            }
        }
    }

    public function get_edit_info()
    {
        if (isset($_POST['edit_form']) && $_POST['edit_form'] == 1) {
            // getting data from database
            $page_row = $this->city_model->get_row('payment_methods', array('id' => $_POST['row']));

            echo json_encode($page_row);

        }
    }

    public function update_payment_info()
    {
        _check_user_login();  //check User login authentication
        if (isset($_POST['edit_form']) && $_POST['edit_form'] == 1) {
            $dup = $this->query_model->getRows('payment_methods', array('id !=' => $_POST['id'], 'code' => $_POST['code']));
            if (empty($dup)) {
                $where = array('id' => $_POST['id']);
                $update_data = array(
                    'name' => $_POST['name'],
                    'code' => $_POST['code'],
                );
                $result = $this->city_model->update('payment_methods', $update_data, $where);
                if ($result == true) {
                    echo "<span class='success_msg'>Payment Method successfully updated</span>";
                } else {
                    echo "<span class='error_msg'>Not Update</span>";
                }
            } else {
                echo "<span class='error_msg'>This code is already added with another payment methods</span>";
            }

        }

    }


    public function delete_payment_met()
    {
        if (isset($_POST['delete_pay_met']) && $_POST['delete_pay_met'] == 1) {
            $result = $this->city_model->delete('payment_methods', array('id' => $_POST['id']));
            if ($result == true) {
                echo "<span class='success_msg'>Payment Detail Successfully Deleted.</span>";
            } else {
                echo "<span class='error_msg'>Payment Detail Not Deleted.</span>";
            }
        }
    }

    public function assign_payment()
    {
        _check_user_login();  //check User login authentication

        $data['admin_list'] = $this->getAdmins();
        $data['payment_list'] = $this->getPaymentMethods();
        $data['payment'] = $this->getPayments();
        $data['template'] = 'superadmin/payment/assign_payment';
        $this->load->view('templates/superadmin/layout', $data);
    }

    private function getAdmins()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows('users', array("added_by" => $user['id'], "role" => 'admin'), null, array(array('id', 'desc')));
        return $list;
    }

    private function getPaymentMethods()
    {
        $list = $this->query_model->getRows('payment_methods');
        return $list;
    }

    private function getPayments()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows("admin_paymentsmethods as apm", array("apm.added_by" => $user['id']), array("apm.*", "pm.name as p_name", "users.name as admin_name", "users.l_name as admin_lname"), array(array("apm.id", "desc")), array(array("payment_methods as pm", "pm.id=apm.paymentmethod_id", "inner"), array("users", "users.id=apm.usesrid", "inner")));
        return $list;
    }

    public function assign_admin_payment()
    {
        _check_user_login();  //check User login authentication
        if (isset($_POST['admin_payment']) && $_POST['admin_payment'] == 1) {
            $dup = $this->city_model->get_row('admin_paymentsmethods', array('usesrid' => $_POST['admin'], 'paymentmethod_id' => $_POST['pay_method']));
            if (empty($dup)) {
                $user = $this->session->userdata('user_info');
                $array = array(
                    'usesrid' => $_POST['admin'],
                    'paymentmethod_id' => $_POST['pay_method'],
                    'status' => 1,
                    'added_by' => $user['id']
                );
                $result = $this->city_model->insert('admin_paymentsmethods', $array);
                if ($result == true) {
                    echo "<span class='success_msg'>Payment Successfully Assign To Admin</span>";
                } else {
                    echo "<span class='error_msg'>Not Insert</span>";
                }
            } else {
                echo "<span class='error_msg'>Payment method already assigned</span> ";
            }
        }
    }

    public function get_assign_payment()
    {
        if (isset($_POST['edit_form']) && $_POST['edit_form'] == 1) {
            // getting data from database
            $page_row = $this->city_model->get_row('admin_paymentsmethods', array('id' => $_POST['row']));

            echo json_encode($page_row);

        }
    }

    public function update_assign_payment_info()
    {
        _check_user_login();  //check User login authentication
        if (isset($_POST['edit_form']) && $_POST['edit_form'] == 1) {
            $where = array('id' => $_POST['id']);
            $update_data = array(
                'usesrid' => $_POST['admin'],
                'paymentmethod_id' => $_POST['pay_method'],
            );
            $result = $this->city_model->update('admin_paymentsmethods', $update_data, $where);
            if ($result == true) {
                echo "<span class='success_msg'>Assign Payment Method Successfully Update</span>";
            } else {
                echo "<span class='error_msg'>Not Update</span>";
            }
        }

    }

    public function delete_assign_admin()
    {
        if (isset($_POST['delete_assignin']) && $_POST['delete_assignin'] == 1) {
            $result = $this->city_model->delete('admin_paymentsmethods', array('id' => $_POST['id']));
            if ($result == true) {
                echo "<span class='success_msg'>Assign Payment Method Successfully Deleted.</span>";
            } else {
                echo "<span class='error_msg'>Method Not Deleted.</span>";
            }
        }
    }

    public function get_update_payment_assign()
    {
        $pay_ass = $this->getPayments();
        $html = '';
        $i = 1;
        foreach ($pay_ass as $data) {
            $html .= "<tr>";
            $html .= "<td>" . $i++ . "</td>";
            $html .= "<td>" . $data->admin_name . " " . $data->admin_lname . "</td>";
            $html .= "<td>" . $data->p_name . "</td>";
            $html .= "<td><button type='button' value='" . $data->id . "' id='edit_payment' class='btn btn_edit'>Edit</button><button type='button' value='" . $data->id . "' id='del_assign' class='btn btn_edit get_city'>Remove</button></td>";
            // $html .="<td></td>";
            $html .= "</tr>";
        }
        echo $html;
    }

    public function get_update_payment_detail()
    {

        $payment_det = $this->getPaymentMethods();
        $html = '';
        $i = 1;
        foreach ($payment_det as $data) {
            $html .= "<tr>";
            $html .= "<td>" . $i++ . "</td>";
            $html .= "<td>" . $data->name . "</td>";
            $html .= "<td>" . $data->code . "</td>";
            $html .= "<td><button type='button' value='" . $data->id . "' class='btn btn_edit get_pay_info'>Edit</button><button type='button' value='" . $data->id . "' id='del_pay_method' class='btn btn_edit get_city'>Remove</button></td>";
            // $html .="<td></td>";
            $html .= "</tr>";
        }
        echo $html;
    }

}
