<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class subcategory extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('category_model');
        $this->load->model('query_model');

    }

    public function index()
    {
        _check_user_login();  //check User login authentication
        $this->defaultViewData();
    }

    private function defaultViewData($data = array())
    {
        $data['businessTypes'] = $this->getBusinessTypes();
        $data['category_list'] = $this->getSubCategories();
        $data['template'] = 'superadmin/category/add_subcategory';
        $this->load->view('templates/superadmin/layout', $data);
    }

    public function add_category()
    {
        _check_user_login();  //check User login authentication

        $user = $this->session->userdata('user_info');


        $this->form_validation->set_rules('businesstype', 'Business Type', 'required');
        $this->form_validation->set_rules('businesssubcat', 'Business Subcategory', 'required');
        $this->form_validation->set_rules('category', 'Business', 'required');
        $this->form_validation->set_rules('subcat_name', 'category name', 'required');
//        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run()) {
            $catCond = array('name' => $this->input->post('subcat_name'));
            $subcategory_id = $this->input->post('subcategory_id');
            if ($subcategory_id) {
                $catCond['id != '] = $subcategory_id;
            }

            $dup = $this->category_model->get_row('subcategory', $catCond);
            if (empty($dup)) {
                $array = array(
                    'businesstype_id' => $this->input->post('businesstype'),
                    'businesssubcat_id' => $this->input->post('businesssubcat'),
                    'category_id' => $this->input->post('category'),
                    'name' => $this->input->post('subcat_name'),
                    'description' => $this->input->post('description'),
                    'added_by' => $user['id']
                );


                if (!empty($subcategory_id)) {
                    if ($this->query_model->updateRow('subcategory', array("id" => $subcategory_id), $array)) {
                        $this->session->set_flashdata('msg_success', 'Sub category updated success.');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Something went wrong. Please try again.');
                    }
                } else {
                    if ($this->category_model->insert('subcategory', $array)) {
                        $this->session->set_flashdata('msg_success', 'Sub category added success.');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Something went wrong. Please try again.');
                    }
                }

            } else {
                $this->session->set_flashdata('msg_error', 'Cannot add duplicate category.');
            }
            redirect('backend/subcategory');
        } else {
            $this->defaultViewData();
        }
    }

    private function getBusinessTypes()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows('businesstype', array("added_by" => $user['id']), null, array(array('id', 'desc')));
        return $list;
    }


    private function getSubCategories()
    {
        $user = $this->session->userdata('user_info');
        $joins = array(
            array("businesstype", "businesstype.id=subcategory.businesstype_id", "inner"),
            array("category", "category.id=subcategory.category_id", "left"),
        );
        $columns = array(
            "subcategory.*",
            "businesstype.name as business_type",
            "category.category_name as category",
        );

        $list = $this->query_model->getRows('subcategory', array("subcategory.added_by" => $user['id']), $columns, array(array('id', 'desc')), $joins);
        return $list;
    }

    public function getCategories()
    {
        $businesssubcat_id = $this->input->post('businesssubcat_id');

        $list = $this->categories($businesssubcat_id);

        if ($list) {
            $response = array(
                "status" => true,
                "data" => $list
            );
        } else {
            $response = array(
                "status" => false,
                "message" => "No categories found"
            );
        }

        echo json_encode($response);
    }

    private function categories($businesssubcat_id)
    {
        $columns = array(
            "category.*"
        );

        $list = $this->query_model->getRows('category', array("category.businesssubcat_id" => $businesssubcat_id), $columns, array(array('id', 'desc')));
        return $list;
    }

    public function getBusinessSubcategories()
    {
        $businesstype_id = $this->input->post('businesstype_id');

        $list = $this->businessSubcategories($businesstype_id);

        if ($list) {
            $response = array(
                "status" => true,
                "data" => $list
            );
        } else {
            $response = array(
                "status" => false,
                "message" => "No categories found"
            );
        }

        echo json_encode($response);
    }

    private function businessSubcategories($businessTypeId)
    {
        $columns = array(
            "businesssubcat.*"
        );

        $list = $this->query_model->getRows('businesssubcat', array("businesssubcat.businesstype_id" => $businessTypeId), $columns, array(array('id', 'desc')));
        return $list;
    }

    public function edit($id)
    {
        _check_user_login();  //check User login authentication
        $subcategory = $this->category_model->get_row('subcategory', array('id' => $id));
        if ($subcategory) {
            $businessSubcatList = $this->businessSubcategories($subcategory->businesstype_id);
            $categoriesList = $this->categories($subcategory->businesssubcat_id);
            $this->defaultViewData(array(
                "editCategoryData" => $subcategory,
                "categoriesList" => $categoriesList,
                "businessSubcatList" => $businessSubcatList
            ));
        } else {
            $this->defaultViewData();
        }
    }

    public function delete($id)
    {
        $result = $this->category_model->delete('subcategory', array('id' => $id));
//        $this->category_model->delete('assign_cat', array('category' => $id));
        $this->category_model->delete('product', array('subcategory_id' => $id));
        if ($result == true) {
            $this->session->set_flashdata('msg_success', 'Subcategory Successfully Deleted.');
            redirect('backend/subcategory');
        } else {
            $this->session->set_flashdata('msg_error', 'Subcategory not deleted.');
            redirect('backend/subcategory');
        }
    }
}
