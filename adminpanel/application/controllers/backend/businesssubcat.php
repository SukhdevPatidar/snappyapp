<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class businesssubcat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('category_model');
        $this->load->model('query_model');

    }

    public function index()
    {
        _check_user_login();  //check User login authentication
        $this->defaultViewData();
    }

    private function defaultViewData($data = array())
    {
        $data['businessTypes'] = $this->getBusinessTypes();
        $data['category_list'] = $this->getBusinessSubCategories();
        $data['template'] = 'superadmin/businesstype/add_businesssubcat';
        $this->load->view('templates/superadmin/layout', $data);
    }

    public function add_category()
    {
        _check_user_login();  //check User login authentication

        $user = $this->session->userdata('user_info');


        $this->form_validation->set_rules('businesstype', 'Business Type', 'required');
        $this->form_validation->set_rules('businesssubcat_name', 'Business subcategory name', 'required');
//        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run()) {
            $catCond = array('name' => $this->input->post('businesssubcat_name'));
            $businesssubcat_id = $this->input->post('businesssubcat_id');
            if ($businesssubcat_id) {
                $catCond['id != '] = $businesssubcat_id;
            }

            $dup = $this->category_model->get_row('businesssubcat', $catCond);
            if (empty($dup)) {
                $array = array(
                    'businesstype_id' => $this->input->post('businesstype'),
                    'name' => $this->input->post('businesssubcat_name'),
                    'description' => $this->input->post('description'),
                    'added_by' => $user['id']
                );


                if (!empty($businesssubcat_id)) {
                    if ($this->query_model->updateRow('businesssubcat', array("id" => $businesssubcat_id), $array)) {
                        $this->session->set_flashdata('msg_success', 'Business Subcategory updated success.');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Something went wrong. Please try again.');
                    }
                } else {
                    if ($this->category_model->insert('businesssubcat', $array)) {
                        $this->session->set_flashdata('msg_success', 'Business Subcategory added success.');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Something went wrong. Please try again.');
                    }
                }

            } else {
                $this->session->set_flashdata('msg_error', 'Cannot add duplicate category.');
            }
            redirect('backend/businesssubcat');
        } else {
            $this->defaultViewData();
        }
    }

    private function getBusinessTypes()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows('businesstype', array("added_by" => $user['id']), null, array(array('id', 'desc')));
        return $list;
    }


    private function getBusinessSubCategories()
    {
        $user = $this->session->userdata('user_info');
        $joins = array(
            array("businesstype", "businesstype.id=businesssubcat.businesstype_id", "inner")
        );
        $columns = array(
            "businesssubcat.*",
            "businesstype.name as business_type"
        );

        $list = $this->query_model->getRows('businesssubcat', array("businesssubcat.added_by" => $user['id']), $columns, array(array('id', 'desc')), $joins);
        return $list;
    }


    public function edit($id)
    {
        _check_user_login();  //check User login authentication
        $subcategory = $this->category_model->get_row('businesssubcat', array('id' => $id));
        if ($subcategory) {
            $this->defaultViewData(array(
                "editCategoryData" => $subcategory,
            ));
        } else {
            $this->defaultViewData();
        }
    }

    public function delete($id)
    {
        $result = $this->category_model->delete('businesssubcat', array('id' => $id));
//        $this->category_model->delete('assign_cat', array('category' => $id));
//        $this->category_model->delete('product', array('subcategory_id' => $id));
        if ($result == true) {
            $this->session->set_flashdata('msg_success', 'Subcategory Successfully Deleted.');
            redirect('backend/businesssubcat');
        } else {
            $this->session->set_flashdata('msg_error', 'Subcategory not deleted.');
            redirect('backend/businesssubcat');
        }
    }

}
