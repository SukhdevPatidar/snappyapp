<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class order extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('city_model');
        $this->load->model('query_model');
    }


    public function order_list()
    {
        _check_user_login();  //check User login authentication
        if (isset($_POST['search'])) {
            $admin_id = $this->input->post('admin_id');
            $order_sta = $this->input->post('order_sta');
            $data['search_list'] = $this->getOrders($admin_id, $order_sta);
        } else {
            $data['order_list'] = $this->getOrders();
        }

        $data['order_status'] = $this->getOrderStatuses();
        $data['admins_list'] = $this->getAdmins();
        $data['template'] = 'superadmin/order/add_order';
        $this->load->view('templates/superadmin/layout', $data);
    }

    private function getOrderStatuses()
    {
        $list = $this->query_model->getRows("orders", null, null, null, null, array("order_status"));
        return $list;
    }

    private function getOrders($admin_id = "", $order_sta = "")
    {
        $user = $this->session->userdata('user_info');
        $userId = $user['id'];

        $conditions = array("users.added_by" => $userId);
        if (!empty($admin_id)) {
            $conditions['orders.provider_id'] = $admin_id;
        }
        if (!empty($order_sta)) {
            $conditions['orders.order_status'] = $order_sta;
        }
        $columns = array("orders.*", "pm.name");
        $joins = array(array("payment_methods as pm", "pm.id=orders.payment_method_id", "inner"), array("users", "users.id=orders.provider_id", "inner"));
        $orderBy = array(array("orders.created_at", "desc"));
        $list = $this->query_model->getRows('orders', $conditions, $columns, $orderBy, $joins, null);
        return $list;
    }

    private function getCategories()
    {
        $user = $this->session->userdata('user_info');
        $cities = $this->query_model->getRows('category', array("added_by" => $user['id']), null, array(array('id', 'desc')));
        return $cities;
    }

    private function getAdmins()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows('users', array("added_by" => $user['id'], "role" => 'admin'), null, array(array('id', 'asc')));
        return $list;
    }

    private function getProducts($cat_id = "", $admin_id = "")
    {
        $user = $this->session->userdata('user_info');
        $userId = $user['id'];

        $conditions = array("users.added_by" => $userId);
        if (!empty($cat_id)) {
            $conditions['p.category_id'] = $cat_id;
        }
        if (!empty($admin_id)) {
            $conditions['p.user_id'] = $admin_id;
        }
        $columns = array("p.*", "c.category_name");
        $joins = array(array("category as c", "c.id = p.category_id", "inner"), array("users", "users.id=p.user_id", "inner"));
        $orderBy = array(array("p.create_date", "desc"));
        $list = $this->query_model->getRows('product as p', $conditions, $columns, $orderBy, $joins, null);
        return $list;
    }

    public function product_view($order_id, $del_prc = '')
    {

        $productItems = $this->city_model->get_result('order_products', array("order_id" => $order_id), array("*", "price as product_price"), NULL);

        $data['history'] = $productItems;

        $ci = &get_instance();
        $sql = $ci->db->query("select u.*,u.name as user_name, u.mobile as user_mobile, a.*, a.name as address_name, a.mobile as add_mobile, o.order_notes from `orders` as o inner join `users` as u on u.id = o.user_id inner join `addresses` as a on a.id = o.address_id where o.id = '" . $order_id . "' ");
        $userHistory = $sql->result();
        if (!$userHistory) {
            $sql = $ci->db->query("select u.*,u.name as user_name, u.mobile as user_mobile, a.*, a.name as address_name, a.mobile as add_mobile, o.order_notes from `orders` as o inner join `users` as u on u.id = o.user_id inner join `order_addresses` as a on a.order_id = o.id where o.id = '" . $order_id . "' ");
            $userHistory = $sql->result();
        }
        $data['user_history'] = $userHistory;

        $data['delievery'] = $del_prc;
        $data['template'] = 'superadmin/order/view_product';
        $this->load->view('templates/superadmin/layout', $data);
    }

    public function change_status($id, $stat)
    {
        $where = array('id' => $id);
        if ($stat == 0) {
            $update = array('status' => 1);
            $result = $this->city_model->update('order', $update, $where);
            if ($result == true) {
                $this->session->set_flashdata('msg_success', 'Your Order Is Open.');
                redirect('backend/order/order_list');
            } else {
                $this->session->set_flashdata('msg_error', 'Your Order Is pending.');
                redirect('backend/order/order_list');
            }
        } else if ($stat == 1) {
            $update = array('status' => 2);
            $result = $this->city_model->update('order', $update, $where);
            if ($result == true) {
                $this->session->set_flashdata('msg_success', 'Your Order Is Close.');
                redirect('backend/order/order_list');
            } else {
                $this->session->set_flashdata('msg_error', 'Your Order Is active.');
                redirect('backend/order/order_list');
            }
        } else {

        }


    }

    public function product_list()
    {
        _check_user_login();  //check User login authentication
        $session_data = $this->session->userdata('user_info');
        $ci = &get_instance();

        if (isset($_POST['search_product'])) {
            $cat_id = $this->input->post('cat_id');
            $admin_id = $this->input->post('admin_id');
            $data['srch_product_list'] = $this->getProducts($cat_id, $admin_id);
        } else {
            $data['product_list'] = $this->getProducts();
        }


        $data['category'] = $this->getCategories();
        $data['admins_list'] = $this->getAdmins();
        $data['template'] = 'superadmin/product/product_list';
        $this->load->view('templates/superadmin/layout', $data);
    }
}
