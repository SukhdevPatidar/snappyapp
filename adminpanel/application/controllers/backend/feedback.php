<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class feedback extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('query_model');
    }
	
	public function show(){
		_check_user_login();  //check User login authentication
		$user = $this->session->userdata('user_info');
		$data['all_feedback'] = $this->query_model->getRows('feedback', array("sadmin.id"=>$user['id']), array("feedback.*"), null, array(array("users as appuser", "appuser.id=feedback.user_id", "inner"),array("users as sadmin", "appuser.country_id=sadmin.country_id", "inner")));
    	
    	$data['template'] = 'superadmin/feedback/show';
   	    $this->load->view('templates/superadmin/layout',$data);
	}

    public function view_detail($id){
        _check_user_login();  //check User login authentication
        
        $data['view_user_det'] = $this->query_model->getRows("feedback", array("feedback.id"=>$id), array("feedback.*", "users.*"), null, array(array("users", "users.id = feedback.user_id", "inner")));
        $data['template'] = 'superadmin/feedback/view_detail';
        $this->load->view('templates/superadmin/layout',$data);
    }
}
?>