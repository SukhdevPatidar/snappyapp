<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_list extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('query_model');
    }
	
	public function user_history(){
		_check_user_login();  //check User login authentication
		$data['all_user'] = $this->getUsers();
    	
   	$data['template'] = 'superadmin/user_history/users_list';
 		 $this->load->view('templates/superadmin/layout',$data);
	}
	
	private function getUsers() {
		$user = $this->session->userdata('user_info');
		$conditions = array('users.role '=> 'user', "sadmin.id"=>$user['id']);
		$coulumns = array("users.*");
		$joins = array(array("users as sadmin", "users.country_id=sadmin.country_id", "inner"));
		$list = $this->query_model->getRows('users', $conditions, $coulumns, null, $joins);
		return $list;
	}
}
?>