<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class businesstype extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('category_model');
        $this->load->model('query_model');

    }

    public function add_businesstype()
    {
        _check_user_login();  //check User login authentication

        $user = $this->session->userdata('user_info');

        if (isset($_POST['add_businesstype'])) {
            $this->form_validation->set_rules('group_name', 'Business Group Name', 'required');
            $this->form_validation->set_rules('c_image', 'Business Type Image', 'callback_category_file');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run()) {
                $dup = $this->category_model->get_row('businesstype', array('name' => $this->input->post('group_name')));
                if (empty($dup)) {
                    $array = array(
                        'name' => $this->input->post('group_name'),
                        'added_by' => $user['id'],
                        'show_big' => ($this->input->post('show_big') == 1) ? 1 : 0,
                    );
                    if ($this->session->userdata('c_image')):
                        $c_image = $this->session->userdata('c_image');
                        $array['imagepath'] = $c_image['image'];
                        $this->session->unset_userdata('c_image');
                    endif;
                    if ($this->category_model->insert('businesstype', $array)) {
                        $this->session->set_flashdata('msg_success', 'Business type Added Successfully.');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Something went wrong. Please try again.');
                    }
                } else {
                    $this->session->set_flashdata('msg_error', 'Duplicate business type not allowed');
                }
                redirect('backend/businesstype/add_businesstype');
            }
        }
        $data['businesstype_list'] = $this->getBusinessTypes();
        $data['template'] = 'superadmin/businesstype/add_businesstype';
        $this->load->view('templates/superadmin/layout', $data);
    }

    private function getBusinessTypes()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows('businesstype', array("added_by" => $user['id']), null, array(array('id', 'desc')));
        return $list;
    }

    public function get_edit_businesstype()
    {
        if (isset($_POST['edit_form']) && $_POST['edit_form'] == 1) {
            // getting data from database
            $page_row = $this->category_model->get_row('businesstype', array('id' => $_POST['row']));

            echo json_encode($page_row);

        }
    }

    public function edit_businesstype()
    {
        $this->form_validation->set_rules('group_name', 'Business Group Name', 'required');
        $this->form_validation->set_rules('c_image', 'Business Type Image', 'callback_category_file');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run()) {
            $where = array('id' => $this->input->post('row'));
            $update_data = array(
                'name' => $this->input->post('group_name'),
                'show_big' => ($this->input->post('show_big') == 1) ? 1 : 0,
            );

            if ($this->session->userdata('c_image')):
                $c_image = $this->session->userdata('c_image');
                $update_data['imagepath'] = $c_image['image'];
                $this->session->unset_userdata('c_image');
            endif;

            $result = $this->category_model->update('businesstype', $update_data, $where);
            //echo $this->db->last_query();die;
            if ($result == true) {
                $this->session->set_flashdata('msg_success', 'Successfully updated.');
            } else {
                $this->session->set_flashdata('msg_error', 'Not updated.');
            }
            redirect('backend/businesstype/add_businesstype');
        }
    }

    public function category_file($str)
    {
        if (isset($_FILES['c_image']['name']) && !empty($_FILES['c_image']['name'])) {
            if ($this->session->userdata('c_image')) {
                return TRUE;
            } else {
                $param = array(
                    'file_name' => 'c_image',
                    'upload_path' => './assets/uploads/businessgroup/',
                    'allowed_types' => 'jpeg|jpg|png',
                    'image_resize' => FALSE,
                    'source_image' => './assets/uploads/businessgroup/',
                    'new_image' => './assets/uploads/businessgroup/thumb/',
                    'encrypt_name' => TRUE,
                );

                $upload_file = upload_file($param);
                //thumbnail create
                if ($upload_file['STATUS']) {
                    $this->session->set_userdata('c_image', array('image' => $param['upload_path'] . $upload_file['UPLOAD_DATA']['file_name'], 'thumb_image' => $param['new_image'] . $upload_file['UPLOAD_DATA']['file_name']));
                    return TRUE;
                } else {
                    $this->form_validation->set_message('c_image', $upload_file['FILE_ERROR']);
                    return FALSE;
                }
            }
        }
    }

    public function delete_businesstype($id)
    {
        $img_path = $this->category_model->get_row('businesstype', array('id' => $id));
        if (unlink($img_path->imagepath)) {
            $result = $this->category_model->delete('businesstype', array('id' => $id));
//            $this->category_model->delete('assign_cat', array('category' => $id));
//            $this->category_model->delete('product', array('category_id' => $id));
            if ($result == true) {
                $this->session->set_flashdata('msg_success', 'Successfully Deleted.');
                redirect('backend/businesstype/add_businesstype');
            } else {
                $this->session->set_flashdata('msg_error', 'Category Not Delete.');
                redirect('backend/businesstype/add_businesstype');
            }
        } else {
            $this->session->set_flashdata('msg_error', 'Image Not Delete.');
            redirect('backend/businesstype/add_businesstype');
        }
    }

}
