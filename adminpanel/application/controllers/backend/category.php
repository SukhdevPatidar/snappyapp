<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class category extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('category_model');
        $this->load->model('query_model');

    }

    public function index()
    {
        _check_user_login();  //check User login authentication
        $this->defaultViewData();
    }

    private function defaultViewData($data = array())
    {
        $data['businessTypes'] = $this->getBusinessTypes();
        $data['category_list'] = $this->getCategories();
        $data['template'] = 'superadmin/category/add_category';
        $this->load->view('templates/superadmin/layout', $data);
    }

    public function add_category()
    {
        _check_user_login();  //check User login authentication

        $user = $this->session->userdata('user_info');

        if (isset($_POST['add_category'])) {
            $categoryAvailable = $this->input->post('categoryAvailable');
            $onlyCalltaxi = $this->input->post('onlycalltaxi');
            $onlyopenurl = $this->input->post('onlyopenurl');
            if ($onlyCalltaxi) {
                $this->form_validation->set_rules('phonenumber', 'phone number', 'required');
            }
            $this->form_validation->set_rules('cat_name', 'category name', 'required');
            $this->form_validation->set_rules('businesstype', 'Business Type', 'required');
            $this->form_validation->set_rules('businesssubcat', 'Business Subcategory', 'required');
            $this->form_validation->set_rules('email', 'email', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_rules('c_image', 'category Image', 'callback_category_file');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run()) {
                $dup = $this->category_model->get_row('category', array('category_name' => $this->input->post('cat_name')));
                if (empty($dup)) {
                    $array = array(
                        'category_name' => $this->input->post('cat_name'),
                        'notification_email' => $this->input->post('email'),
                        'create_date' => date('Y-m-d'),
                        'businesstype_id' => $this->input->post('businesstype'),
                        'businesssubcat_id' => $this->input->post('businesssubcat'),
                        'description' => $this->input->post('description'),
                        'onlycalltaxi' => ($onlyCalltaxi == 1) ? 1 : 0,
                        'contact_number' => ($onlyCalltaxi == 1) ? $this->input->post('phonenumber') : '',
                        'onlyopenurl' => ($onlyopenurl == 1) ? 1 : 0,
                        'category_available' => ($categoryAvailable == 1) ? 1 : 0,
                        'url' => ($onlyopenurl == 1) ? $this->input->post('url') : '',
                        'added_by' => $user['id']
                    );
                    if ($this->session->userdata('c_image')):
                        $c_image = $this->session->userdata('c_image');
                        $array['category_img'] = $c_image['image'];
                        $this->session->unset_userdata('c_image');
                    endif;
                    if ($this->category_model->insert('category', $array)) {
                        $this->session->set_flashdata('msg_success', 'category Added Successfully.');
                        redirect('backend/category/add_category');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Something went wrong. Please try again.');
                        redirect('backend/category/add_category');
                    }
                } else {
                    $this->session->set_flashdata('msg_error', 'duplicate Category Not Insert.');
                    redirect('backend/category/add_category');
                }
            }
        }

        $this->defaultViewData();
    }

    private function getBusinessTypes()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows('businesstype', array("added_by" => $user['id']), null, array(array('id', 'desc')));
        return $list;
    }


    private function getCategories()
    {
        $user = $this->session->userdata('user_info');
        $joins = array(
            array("businesstype", "businesstype.id=category.businesstype_id", "left")
        );
        $columns = array(
            "category.*",
            "businesstype.name as business_type",
        );

        $list = $this->query_model->getRows('category', array("category.added_by" => $user['id']), $columns, array(array('id', 'desc')), $joins);
        return $list;
    }

    public function getBusinessSubcategories()
    {
        $businesstype_id = $this->input->post('businesstype_id');

        $list = $this->businessSubcategories($businesstype_id);

        if ($list) {
            $response = array(
                "status" => true,
                "data" => $list
            );
        } else {
            $response = array(
                "status" => false,
                "message" => "No categories found"
            );
        }

        echo json_encode($response);
    }

    private function businessSubcategories($businessTypeId)
    {
        $columns = array(
            "businesssubcat.*"
        );

        $list = $this->query_model->getRows('businesssubcat', array("businesssubcat.businesstype_id" => $businessTypeId), $columns, array(array('id', 'desc')));
        return $list;
    }

    public function get_edit_category()
    {
        if (isset($_POST['edit_form']) && $_POST['edit_form'] == 1) {
            // getting data from database
            $categoryData = $this->query_model->getRow("category", array('id' => $_POST['row']));

            if ($categoryData) {
                $subcategories = $this->businessSubcategories($categoryData->businesstype_id);
                echo json_encode(array(
                    "status" => true,
                    "data" => $categoryData,
                    "businesssubcat" => $subcategories
                ));
            } else {
                echo json_encode(array(
                    "status" => false,
                    "message" => "No product data found"
                ));
            }
        }
    }

    public function edit_category()
    {
        $categoryAvailable = $this->input->post('categoryAvailable');
        $onlyCalltaxi = $this->input->post('onlycalltaxi');
        $onlyopenurl = $this->input->post('onlyopenurl');
        if ($onlyCalltaxi) {
            $this->form_validation->set_rules('phonenumber', 'phone number', 'required');
        }
        $this->form_validation->set_rules('businesstype', 'Business Type', 'required');
        $this->form_validation->set_rules('businesssubcat', 'Business Subcategory', 'required');
        $this->form_validation->set_rules('cat_name', 'category name', 'required');
        $this->form_validation->set_rules('c_image', 'category Image', 'callback_category_file');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run()) {
            $where = array('id' => $this->input->post('row'));
            $update_data = array(
                'category_name' => $this->input->post('cat_name'),
                'businesstype_id' => $this->input->post('businesstype'),
                'businesssubcat_id' => $this->input->post('businesssubcat'),
                'description' => $this->input->post('description'),
                'notification_email' => $this->input->post('email'),
                'onlycalltaxi' => ($onlyCalltaxi == 1) ? 1 : 0,
                'category_available' => ($categoryAvailable == 1) ? 1 : 0,
                'contact_number' => ($onlyCalltaxi == 1) ? $this->input->post('phonenumber') : '',
                'onlyopenurl' => ($onlyopenurl == 1) ? 1 : 0,
                'url' => ($onlyopenurl == 1) ? $this->input->post('url') : '',
            );

            if ($this->session->userdata('c_image')):
                $c_image = $this->session->userdata('c_image');
                $update_data['category_img'] = $c_image['image'];
                $this->session->unset_userdata('c_image');
            endif;

            $result = $this->category_model->update('category', $update_data, $where);
            //echo $this->db->last_query();die;
            if ($result == true) {
                $this->session->set_flashdata('msg_success', 'Category Successfully update.');
                redirect('backend/category/add_category');
            } else {
                $this->session->set_flashdata('msg_error', 'Category Not update.');
                redirect('backend/category/add_category');
            }
        }
    }


    public function category_file($str)
    {
        if (isset($_FILES['c_image']['name']) && !empty($_FILES['c_image']['name'])) {
            if ($this->session->userdata('c_image')) {
                return TRUE;
            } else {
                $param = array(
                    'file_name' => 'c_image',
                    'upload_path' => './assets/uploads/category/',
                    'allowed_types' => 'jpeg|jpg|png',
                    'image_resize' => FALSE,
                    'source_image' => './assets/uploads/category/',
                    'new_image' => './assets/uploads/category/thumb/',
                    'encrypt_name' => TRUE,
                );


                $upload_file = upload_file($param);
                //thumbnail create
                if ($upload_file['STATUS']) {
                    $this->session->set_userdata('c_image', array('image' => $param['upload_path'] . $upload_file['UPLOAD_DATA']['file_name'], 'thumb_image' => $param['new_image'] . $upload_file['UPLOAD_DATA']['file_name']));
                    return TRUE;
                } else {
                    $this->form_validation->set_message('c_image', $upload_file['FILE_ERROR']);
                    return FALSE;
                }
            }
        }
    }

    public function delete_category($id)
    {
        $img_path = $this->category_model->get_row('category', array('id' => $id));
        if (unlink($img_path->category_img)) {
            $result = $this->category_model->delete('category', array('id' => $id));
            $this->category_model->delete('assign_cat', array('category' => $id));
            $this->category_model->delete('product', array('category_id' => $id));
            if ($result == true) {
                $this->session->set_flashdata('msg_success', 'Category Successfully Deleted.');
                redirect('backend/category/add_category');
            } else {
                $this->session->set_flashdata('msg_error', 'Category Not Delete.');
                redirect('backend/category/add_category');
            }
        } else {
            $this->session->set_flashdata('msg_error', 'Image Not Delete.');
            redirect('backend/category/add_category');
        }
    }

    public function image()
    {
        _check_user_login();  //check User login authentication
        $data['template'] = 'superadmin/category/image';
        $this->load->view('templates/superadmin/layout', $data);

    }
}
