<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notifications extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('query_model');
    }


    public function addNotifications()
    {
        _check_user_login();  //check User login authentication

        $data['city_list'] = $this->getCities();
        $data['template'] = 'superadmin/notifications/send_notifications';
        $this->load->view('templates/superadmin/layout', $data);
    }

    private function getCities()
    {
        $user = $this->session->userdata('user_info');
        $cities = $this->query_model->getRows('city', array("added_by" => $user['id']), null, array(array('id', 'desc')));
        return $cities;
    }

    public function sendNotifications()
    {
        _check_user_login();  //check User login authentication

        $session_data = $this->session->userdata('user_info');
        $data = array(
            'sender_id' => $session_data['id'],
            'title' => $this->input->post('title'),
            'description' => $this->input->post('description'),
            'city_id' => $this->input->post('city_id')
        );
        $insertedId = $this->query_model->insertRow('notifications', $data);
        $insertedId = true;

        $title = $this->input->post('title');
        $message = $this->input->post('description');
        $msgData = [
            "title" => $title,
            "body" => $message,
            "sound" => "default",
            "icon" => "notification_icon"
        ];
        $extraData = [
            "action" => "offer",
            "title" => $title,
            "body" => $message
        ];

        $users = $this->query_model->getRows('users', array("city_id" => $this->input->post('city_id')), array("*"));
        $userTokens = array();
        if ($users) {
            foreach ($users as $user) {
                if ($user->push_token) {
                    $userTokens[] = $user->push_token;
                }
            }
        }

        $pushTokens = array_chunk($userTokens, 1000);

        foreach ($pushTokens as $tokens) {
            $insertedId = $this->sendPushToSnappyDelivery($tokens, $msgData, $extraData);
        }

        if (isset($insertedId) && $insertedId) {
            echo "Notification sent successfully";
        } else {
            echo "Notification not sent";
        }
    }

    private function sendPushToSnappyDelivery($userTokens, $msgData, $extraData)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $fcmNotification = [
            'registration_ids' => $userTokens, //multple token array
//             'to'        => $userToken, //single token
            'notification' => $msgData,
            'data' => $extraData
        ];

        $headers = [
            'Authorization: key=' . API_ACCESS_KEY_SNAPPY,
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }
}

?>
