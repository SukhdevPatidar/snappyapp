<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sliders extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('category_model');
        $this->load->model('query_model');

    }

    public function add_slider()
    {
        _check_user_login();  //check User login authentication

        $slideIndex = $this->uri->segment('4');

        $user = $this->session->userdata('user_info');

        if (isset($_POST['add_slider'])) {
// 			$this->form_validation->set_rules('slider_title','slider title','required');
            $this->form_validation->set_rules('slider_image', 'slider Image', 'callback_category_file');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            if ($this->form_validation->run()) {
                $slider = $this->query_model->getRow("app_sliders", array("slide_no" => $slideIndex, "added_by" => $user['id']));
                if (empty($slider)) {
                    $array = array(
                        'slide_no' => $slideIndex,
                        'url' => $_POST['banner_link'],
                        'added_by' => $user['id'],
                        'status' => 1
                    );
                    if ($this->session->userdata('slider_image')):
                        $c_image = $this->session->userdata('slider_image');
                        $array['image'] = $c_image['image'];
                        $this->session->unset_userdata('slider_image');
                    endif;
                    if ($this->query_model->insertRow('app_sliders', $array)) {
                        $this->session->set_flashdata('msg_success', 'Slide added Successfully.');
                        redirect('backend/sliders/add_slider/' . $slideIndex);
                    } else {
                        $this->session->set_flashdata('msg_error', 'Something went wrong. Please try again.');
                        redirect('backend/sliders/add_slider/' . $slideIndex);
                    }
                } else {
                    $sliderimage = null;
                    if ($this->session->userdata('slider_image')):
                        $sliderimageData = $this->session->userdata('slider_image');
                        $sliderimage = $sliderimageData['image'];
                        $this->session->unset_userdata('slider_image');
                        $thumbpath = str_replace("sliders", "sliders/thumb", $slider->image);
                        unlink($slider->image);
                        unlink($thumbpath);
                    endif;

                    $data = array();
                    if ($sliderimage) {
                        $data['image'] = $sliderimage;
                    }
                    if (isset($_POST['banner_link'])) {
                        $data['url'] = $_POST['banner_link'];
                    }
                    if ($this->query_model->updateRow("app_sliders", array("id" => $slider->id), $data)) {
                        $this->session->set_flashdata('msg_success', 'Slide updated Successfully.');
                        redirect('backend/sliders/add_slider/' . $slideIndex);
                    }
                }
            }
        }
        $data['slideIndex'] = $slideIndex;
        $data['slider'] = $this->getSlider($slideIndex);
        $data['template'] = 'superadmin/sliders/add_slider';
        $this->load->view('templates/superadmin/layout', $data);
    }

    private function getSliders()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows('app_sliders', array("added_by" => $user['id']), null, array(array('id', 'desc')));
        return $list;
    }

    private function getSlider($slideIndex)
    {
        $user = $this->session->userdata('user_info');
        $item = $this->query_model->getRow('app_sliders', array("added_by" => $user['id'], "slide_no" => $slideIndex));
        return $item;
    }

    public function get_edit_category()
    {
        if (isset($_POST['edit_form']) && $_POST['edit_form'] == 1) {
            // getting data from database
            $page_row = $this->category_model->get_row('category', array('id' => $_POST['row']));

            echo json_encode($page_row);

        }
    }

    public function edit_category()
    {


        $this->form_validation->set_rules('cat_name', 'category name', 'required');
        $this->form_validation->set_rules('c_image', 'category Image', 'callback_category_file');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run()) {
            $where = array('id' => $this->input->post('row'));
            $update_data = array('category_name' => $this->input->post('cat_name'));

            if ($this->session->userdata('c_image')):
                $c_image = $this->session->userdata('c_image');
                $update_data['category_img'] = $c_image['image'];
                $this->session->unset_userdata('c_image');
            endif;

            $result = $this->category_model->update('category', $update_data, $where);
            //echo $this->db->last_query();die;
            if ($result == true) {
                $this->session->set_flashdata('msg_success', 'Category Successfully update.');
                redirect('backend/category/add_category');
            } else {
                $this->session->set_flashdata('msg_error', 'Category Not update.');
                redirect('backend/category/add_category');
            }
        }
    }

    public function category_file($str)
    {
        if (isset($_FILES['slider_image']['name']) && !empty($_FILES['slider_image']['name'])) {
            if ($this->session->userdata('slider_image')) {
                return TRUE;
            } else {
                $param = array(
                    'file_name' => 'slider_image',
                    'upload_path' => './assets/uploads/sliders/',
                    'allowed_types' => 'jpeg|jpg|png',
                    'image_resize' => FALSE,
                    'source_image' => './assets/uploads/sliders/',
                    'new_image' => './assets/uploads/sliders/thumb/',
                    'encrypt_name' => TRUE,
                );


                $upload_file = upload_file($param);
                //thumbnail create
                if ($upload_file['STATUS']) {
                    $this->session->set_userdata('slider_image', array('image' => $param['upload_path'] . $upload_file['UPLOAD_DATA']['file_name'], 'thumb_image' => $param['new_image'] . $upload_file['UPLOAD_DATA']['file_name']));
                    return TRUE;
                } else {
                    $this->form_validation->set_message('slider_image', $upload_file['FILE_ERROR']);
                    return FALSE;
                }
            }
        }
    }

    public function delete_category($id)
    {
        $img_path = $this->category_model->get_row('category', array('id' => $id));
        if (unlink($img_path->category_img)) {
            $result = $this->category_model->delete('category', array('id' => $id));
            $this->category_model->delete('assign_cat', array('category' => $id));
            $this->category_model->delete('product', array('category_id' => $id));
            if ($result == true) {
                $this->session->set_flashdata('msg_success', 'Category Successfully Deleted.');
                redirect('backend/category/add_category');
            } else {
                $this->session->set_flashdata('msg_error', 'Category Not Delete.');
                redirect('backend/category/add_category');
            }
        } else {
            $this->session->set_flashdata('msg_error', 'Image Not Delete.');
            redirect('backend/category/add_category');
        }
    }

    public function image()
    {
        _check_user_login();  //check User login authentication
        $data['template'] = 'superadmin/category/image';
        $this->load->view('templates/superadmin/layout', $data);

    }
}
