<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //clear_cache();
        $this->load->model('city_model');
        $this->load->model('query_model');
    }

    public function add_admin()
    {
        _check_user_login();  //check User login authentication


        if (isset($_POST['add_admin'])) {
            $this->form_validation->set_rules('f_name', 'First name', 'required');
            $this->form_validation->set_rules('l_name', 'Last name', 'required');
            $this->form_validation->set_rules('email', 'email', 'required');
            $this->form_validation->set_rules('mob', 'mobile', 'required');
            $this->form_validation->set_rules('password', 'password', 'required');
            $this->form_validation->set_rules('city_name', 'city name', 'required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run()) {
                $dup = $this->city_model->get_row('users', array('email' => $this->input->post('email')));
                if (empty($dup)) {
                    $user = $this->session->userdata('user_info');

                    $array = array(
                        'name' => $this->input->post('f_name'),
                        'l_name' => $this->input->post('l_name'),
                        'email' => $this->input->post('email'),
                        'password' => $this->input->post('password'),
                        'mobile' => $this->input->post('mob'),
                        'city_name' => $this->input->post('city_name'),
                        'added_by' => $user['id'],
                        'country_id' => $user['country_id'],
                        'create_date' => date('Y-m-d'),
                        'role' => 'admin'
                    );

                    if ($this->city_model->insert('users', $array)) {
                        $this->session->set_flashdata('msg_success', 'New Admin Added Successfully.');
                        redirect('backend/admin/add_admin');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Something went wrong. Please try again.');
                        redirect('backend/admin/add_admin');
                    }
                } else {
                    $this->session->set_flashdata('msg_error', 'Email id Already Exist.');
                    redirect('backend/admin/add_admin');
                }
            }
        }
        $data['admin_list'] = $this->getAdmins();
        $data['template'] = 'superadmin/admin/add_admin';
        $this->load->view('templates/superadmin/layout', $data);
    }

    private function getAdmins()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows('users', array("added_by" => $user['id'], "role" => 'admin'), null, array(array('id', 'desc')));
        return $list;
    }

    private function getCategories()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows('category', array("added_by" => $user['id']));
        return $list;
    }

    private function getCities()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows('city', array("added_by" => $user['id']));
        return $list;
    }

    private function getAssignedCat()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows('assign_cat', array("added_by" => $user['id']), null, null, null, array("admin_name"));
        return $list;
    }

    private function getAssignedCity()
    {
        $user = $this->session->userdata('user_info');
        $list = $this->query_model->getRows('assign_city', array("added_by" => $user['id']), null, null, null, array("admin_name"));
        return $list;
    }


    public function insert_admin()
    {
        _check_user_login();  //check User login authentication
        if (isset($_POST['add_admin']) && $_POST['add_admin'] == 1) {
            $user = $this->session->userdata('user_info');
            $dup = $this->city_model->get_row('users', array('email' => $_POST['email']));
            if (empty($dup)) {
                $array = array(
                    'name' => $_POST['f_name'],
                    'l_name' => $_POST['l_name'],
                    'email' => $_POST['email'],
                    'password' => $_POST['password'],
                    'mobile' => $_POST['mob'],
                    'city_name' => $_POST['city_name'],
                    'create_date' => date('Y-m-d'),
                    'added_by' => $user['id'],
                    'country_id' => $user['country_id'],
                    'role' => 'admin'
                );
                $result = $this->city_model->insert('users', $array);
                if ($result == true) {
                    echo "<span class='success_msg'>Admin Successfully Insert</span>";
                } else {
                    echo "<span class='error_msg'>Not Insert</span>";
                }
            } else {
                echo "<span class='error_msg'>Same Email can't be used</span>";
            }
        }
    }

    public function update_admin()
    {
        _check_user_login();  //check User login authentication
        if (isset($_POST['edit_admin']) && $_POST['edit_admin'] == 1) {
            $user = $this->session->userdata('user_info');
            $where = array('id' => $_POST['id']);
            $update_data = array(
                'name' => $_POST['f_name'],
                'l_name' => $_POST['l_name'],
                'email' => $_POST['email'],
                'password' => $_POST['password'],
                'mobile' => $_POST['mob'],
                'country_id' => $user['country_id'],
                'city_name' => $_POST['city_name'],
            );
            $result = $this->city_model->update('users', $update_data, $where);
            if ($result == true) {
                echo "<span class='success_msg'>Admin Successfully Update</span>";
            } else {
                echo "<span class='error_msg'>Not Update</span>";
            }
        }
    }


    public function delete_admin()
    {
        if (isset($_POST['delete_admin']) && $_POST['delete_admin'] == 1) {
            $result = $this->city_model->delete('users', array('id' => $_POST['id']));
            if ($result == true) {
                $this->city_model->delete('assign_cat', array('admin_name' => $_POST['id']));
                $this->city_model->delete('assign_city', array('admin_name' => $_POST['id']));
                $this->city_model->delete('product', array('user_id' => $_POST['id']));
                echo "Admin Successfully Deleted.";
            } else {
                echo "Admin Not Deleted.";
            }
        }
    }

    public function delete_assign()
    {
        if (isset($_POST['delete_cat']) && $_POST['delete_cat'] == 1) {
            $result = $this->city_model->delete('assign_cat', array('id' => $_POST['id']));
            if ($result == true) {
                echo "<span class='success_msg'>Role Successfully Deleted.</span>";
            } else {
                echo "<span class='error_msg'>Role Not Deleted.</span>";
            }
        }
    }

    public function delete_assign_city()
    {
        if (isset($_POST['delete_city']) && $_POST['delete_city'] == 1) {
            $result = $this->city_model->delete('assign_city', array('id' => $_POST['id']));
            if ($result == true) {
                echo "<span class='success_msg'>Role Successfully Deleted.</span>";
            } else {
                echo "<span class='error_msg'>Role Not Deleted.</span>";
            }
        }
    }

    public function get_edit_admin()
    {
        if (isset($_POST['edit_form']) && $_POST['edit_form'] == 1) {
            // getting data from database
            $page_row = $this->city_model->get_row('users', array('id' => $_POST['row']));

            echo json_encode($page_row);

        }
    }

    public function edit_admin()
    {
        $where = array('id' => $this->input->post('row'));
        $user = $this->session->userdata('user_info');
        $update_data = array(
            'name' => $this->input->post('f_name'),
            'l_name' => $this->input->post('l_name'),
            'email' => $this->input->post('email'),
            'mobile' => $this->input->post('mob'),
            'country_id' => $user['country_id'],
            'city_name' => $this->input->post('city_name')
        );
        $result = $this->city_model->update('users', $update_data, $where);
        if ($result == true) {
            $this->session->set_flashdata('msg_success', 'Admin Data Successfully Update.');
            redirect('backend/admin/add_admin');
        } else {
            $this->session->set_flashdata('msg_error', 'Admin Data Not Update.');
            redirect('backend/admin/add_admin');
        }
    }

    public function assign()
    {
        _check_user_login();  //check User login authentication
        $ci = &get_instance();
        if (isset($_POST['assign_role'])) {
            $dup_cat = array();
            $dup_city = array();
            $this->form_validation->set_rules('admin', 'admin', 'required');
            $this->form_validation->set_rules('cat', 'cat', 'required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run()) {
                foreach ($this->input->post('cat') as $cat) {
                    $dup = $ci->db->query("select category from `assign_cat` where `category` = '" . $cat . "' and admin_name = '" . $this->input->post('admin') . "';");
                    $dup_cat = $dup->result();
                }

                if (!empty($dup_cat)) {
                    $this->session->set_flashdata('msg_error', 'Same category Not Assign .');
                    redirect('backend/admin/assign');
                } else {
                    $user = $this->session->userdata('user_info');
                    foreach ($this->input->post('cat') as $cat) {
                        $array = array(
                            'admin_name' => $this->input->post('admin'),
                            'category' => $cat,
                            'added_by' => $user['id']
                        );
                        $result_cat = $this->city_model->insert('assign_cat', $array);
                    }
                    if ($result_cat == true) {
                        $this->session->set_flashdata('msg_success', 'Category Successfully Assign.');
                        redirect('backend/admin/assign');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Something went wrong. Please try again.');
                        redirect('backend/admin/assign');
                    }
                }

            }
        }
        $data['role_list'] = $this->getAssignedCat();
        $data['admin_list'] = $this->getAdmins();
        $data['category_list'] = $this->getCategories();
        $data['city_list'] = $this->getCities();
        $data['template'] = 'superadmin/assign/assign_cat';
        $this->load->view('templates/superadmin/layout', $data);
    }

    public function insert_role()
    {
        _check_user_login();  //check User login authentication
        if (isset($_POST['assign_role']) && $_POST['assign_role'] == 1) {
            $dup = $this->city_model->get_row('assign_cat', array('category' => $_POST['cat'], 'admin_name' => $_POST['admin']));
            if (empty($dup)) {
                $user = $this->session->userdata('user_info');
                $array = array(
                    'admin_name' => $_POST['admin'],
                    'category' => $_POST['cat'],
                    'create_date' => date('Y-m-d'),
                    'added_by' => $user['id']
                );
                $result_cat = $this->city_model->insert('assign_cat', $array);
                if ($result_cat == true) {
                    echo "<span class='success_msg'>Category Successfully Assign</span>";
                } else {
                    echo "<span class='error_msg'>Not Insert<span>";
                }
            } else {
                echo "<span class='error_msg'>Same Category can't be used</span>";
            }
        }
    }

    public function insert_city_role()
    {
        _check_user_login();  //check User login authentication
        if (isset($_POST['assign_role_city']) && $_POST['assign_role_city'] == 1) {
            $dup = $this->city_model->get_row('assign_city', array('city' => $_POST['city'], 'admin_name' => $_POST['admin']));
            if (empty($dup)) {
                $user = $this->session->userdata('user_info');
                $array = array(
                    'admin_name' => $_POST['admin'],
                    'city' => $_POST['city'],
                    'create_date' => date('Y-m-d'),
                    'added_by' => $user['id']
                );
                $result_cat = $this->city_model->insert('assign_city', $array);
                if ($result_cat == true) {
                    echo "<span class='success_msg'>city Successfully Assign</span>";
                } else {
                    echo "<span class='error_msg'>Not Insert</span>";
                }
            } else {
                echo "<span class='error_msg'>Same City can't be used</span>";
            }
        }
    }


    public function get_asign_cat($id)
    {
        $data['cat_list'] = $this->city_model->get_result('assign_cat', array('admin_name' => $id));
        $data['admin_id'] = $id;
        $data['template'] = 'superadmin/assign/user_asn_cat';
        $this->load->view('templates/superadmin/layout', $data);
    }

    public function view_assign_city($id)
    {

        $data['city_list'] = $this->city_model->get_result('assign_city', array('admin_name' => $id));
        $data['admin_id'] = $id;
        $data['template'] = 'superadmin/assign/view_assign_city';
        $this->load->view('templates/superadmin/layout', $data);
    }

    public function assign_city()
    {
        _check_user_login();  //check User login authentication
        $ci = &get_instance();
        if (isset($_POST['assign_role'])) {

            $dup_city = array();
            $this->form_validation->set_rules('admin', 'admin', 'required');
            $this->form_validation->set_rules('city', 'city', 'required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run()) {
                foreach ($this->input->post('city') as $cty) {
                    $dup1 = $ci->db->query("select city from `assign_city` where `city` = '" . $cty . "' and admin_name = '" . $this->input->post('admin') . "';");
                    $dup_city = $dup1->result();
                }

                if (empty($dup_city)) {
                    $user = $this->session->userdata('user_info');
                    $result_cat = array();
                    foreach ($this->input->post('city') as $cty) {
                        $array = array(
                            'admin_name' => $this->input->post('admin'),
                            'city' => $cty,
                            'added_by' => $user['id']
                        );
                        $result_cat = $this->city_model->insert('assign_city', $array);
                    }

                    if ($result_cat == true) {
                        $this->session->set_flashdata('msg_success', 'City Successfully Assign.');
                        redirect('backend/admin/assign_city');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Something went wrong. Please try again.');
                        redirect('backend/admin/assign_city');
                    }
                } else {
                    $this->session->set_flashdata('msg_error', 'Same city Not Assign .');
                    redirect('backend/admin/assign_city');
                }

            }
        }

        $data['role_list'] = $this->getAssignedCity();
        $data['admin_list'] = $this->getAdmins();
        $data['category_list'] = $this->getCategories();
        $data['city_list'] = $this->getCities();
        $data['template'] = 'superadmin/assign/assign_city';
        $this->load->view('templates/superadmin/layout', $data);
    }

    public function get_update_admin_value()
    {

        $admin_list = $this->getAdmins();

        $html = '';
        $i = 1;
        foreach ($admin_list as $data) {
            $html .= "<tr>";
            $html .= "<td>" . $i++ . "</td>";
            $html .= "<td>" . $data->name . "</td>";
            $html .= "<td>" . $data->email . "</td>";
            $html .= "<td>" . $data->mobile . "</td>";
            $html .= "<td><button type='button' value='" . $data->id . "' id='get_city' class='btn btn_edit edit_admin'>Edit</button><button type='button' value='" . $data->id . "' id='del_admin' class='btn btn_edit'>Remove</button></td>";
            // $html .="<td></td>";
            $html .= "</tr>";
        }
        echo $html;
    }

    public function get_update_assign_cat()
    {

        $list = $this->getAssignedCat();
        $html = '';
        $i = 1;
        foreach ($list as $data) {
            $url = "backend/admin/get_asign_cat/$data->admin_name";
            $cate = get_cat_name($data->category);
            $name = get_user_name($data->admin_name);
            $f_name = isset($name->name) ? $name->name : '';
            $l_name = isset($name->l_name) ? $name->l_name : '';
            $c_name = isset($cate->category_name) ? $cate->category_name : '';
            $html .= "<tr>";
            $html .= "<td>" . $i++ . "</td>";
            $html .= "<td>" . $f_name . " " . $l_name . "</td>";
            // $html .="<td>".$c_name."</td>";
            $html .= "<td><a href=" . base_url($url) . " class='btn cancle_btn'>View</a><button type='button' value='" . $data->id . "' id='del_cat' class='btn btn_edit'>Remove</button></td>";
            // $html .="<td></td>";
            $html .= "</tr>";
        }
        echo $html;
    }

    public function get_update_assign_city()
    {

        $list = $this->getAssignedCity();
        $html = '';
        $i = 1;
        foreach ($list as $data) {
            $url = "backend/admin/view_assign_city/$data->admin_name";
            $cty = get_city_name($data->city);
            //echo $cty->city_name;
            $ct = isset($cty->city_name) ? $cty->city_name : '';
            $name = get_user_name($data->admin_name);
            $f_name = isset($name->name) ? $name->name : '';
            $l_name = isset($name->l_name) ? $name->l_name : '';

            $html .= "<tr>";
            $html .= "<td>" . $i++ . "</td>";
            $html .= "<td>" . $f_name . " " . $l_name . "</td>";
            // $html .="<td>".$ct."</td>";
            $html .= "<td><a href=" . base_url($url) . " class='btn cancle_btn'>View</a><button type='button' value='" . $data->id . "' id='del_city' class='btn btn_edit get_city'>Remove</button></td>";
            // $html .="<td></td>";
            $html .= "</tr>";
        }
        echo $html;
    }

}
