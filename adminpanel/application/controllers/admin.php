<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

		public function __construct()
    {
        parent::__construct();
				$this->load->model('query_model');
    }
	
   public function dashboard(){
   	_check_user_login();  //check User login authentication

		 
		 
		$data["deliveryboys"] = $this->getMyDeliveryBoys();
		$data["categories"] = $this->getCategories();
		$data["products"] = $this->getProducts();
		$data["orders"] = $this->getOrders();
		 
   	$data['user_data'] = $this->session->userdata('user_info');
   	//print_r($data['user_data']);die;
   	$data['template'] = 'users/dashboard';
   	$this->load->view('templates/users/layout',$data);
   }
	
	
	private function getOrders() {
		$user = $this->session->userdata('user_info');
		$userId = $user['id'];
		$conditions = array("orders.provider_id"=>$userId);
		$columns = array("orders.*");
		$joins = array();
		$orderBy = array();
		$list = $this->query_model->getRows('orders', $conditions, $columns, $orderBy, $joins, null);
		return $list?$list:array();
	}
	
	private function getMyDeliveryBoys() {
		$session_data = $this->session->userdata('user_info');
	  $list= $this->query_model->getRows('users',array('role'=>'delivery_boy', 'added_by' => $session_data['id']));
		return $list?$list:array();
	}
	
	private function getProducts() {
		$user = $this->session->userdata('user_info');
		$userId = $user['id'];
		$conditions = array("p.user_id"=>$userId);
		$columns = array("p.*");
		$joins = array();
		$orderBy = array();
		$list = $this->query_model->getRows('product as p', $conditions, $columns, $orderBy, $joins, null);
		return $list?$list:array();
	}
	
	private function getCategories() {
		$user = $this->session->userdata('user_info');
		
		$conditions = array('admin_name'=>$user['id']);
		$columns = array();
		$orderBys = array();
		$joins = array();
		$groupBys = null;
		$list = $this->query_model->getRows("assign_cat", $conditions, $columns, $orderBys, $joins, $groupBys);
		return $list?$list:array();
	}
}
