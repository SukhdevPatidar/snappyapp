<?php
require(APPPATH . '/libraries/REST_Controller.php');

class Products extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api/products_model');
        $this->load->helper('URL');

    }

    public function index_get()
    {
        $this->response([
            'status' => FALSE,
            'message' => "unknown method"
        ], 200);
    }


    public function productList_get()
    {
        $catId = $this->get('cat_id');
        $cityId = $this->get('city_id');
        $subCategoryId = $this->get('subCategoryId');
        $searchTxt = $this->get('search_txt');

        $data = $this->products_model->getAllProducts($catId, $subCategoryId, $cityId, $searchTxt);
        if ($data) {
            $this->response([
                'status' => TRUE,
                'data' => $data
            ], 200);
        } else {
            $this->response([
                'status' => FALSE,
                'error_en' => 'No products were found',
                'error' => 'No se encontraron productos'
            ], 200);
        }
    }

    public function checkProductAvailability_get()
    {
        $productId = $this->get('product_id');
        $cityId = $this->get('city_id');

        $data = $this->products_model->checkProductAvailability($productId, $cityId);
        if ($data) {
            $condition = array('product.id' => $productId);
            $columns = array("dc.cost as delievery_price");
            $joins = array(
                array("delivery_cost as dc", "dc.user_id=product.user_id AND dc.city_id=" . $cityId, "inner")
            );
            $productDeliveryPriceForCity = $this->query_model->getRow("product", $condition, $columns, null, $joins);

            $this->response([
                'status' => TRUE,
                'data' => array(
                    "delievery_price" => $productDeliveryPriceForCity->delievery_price
                )
            ], 200);
        } else {
            $this->response([
                'status' => FALSE,
                'error_en' => 'No products were found for this city',
                'error' => 'No se encontraron productos para esta ciudad'
            ], 200);
        }
    }
}
