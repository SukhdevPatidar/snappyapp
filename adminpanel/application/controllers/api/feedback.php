<?php
require(APPPATH . '/libraries/REST_Controller.php');

class Feedback extends  REST_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('api/feedback_model');
		$this->load->helper('URL');
	}
	
	public function index_get()
	{
		$this->response([
			'status' => FALSE,
			'message' => "unknown method"
		], 200);
	}
	
	
	public function addFeedback_post() {
		$userId = $this->post('user_id'); 
		$service = $this->post('service');
		$delieveryTime = $this->post('delievery_time');
		$staffBehaviour = $this->post('staff_behaviour');
		$comment = $this->post('comment');


		if(!$userId) {
			$this->response([
				'status' => FALSE,
				'error_en' => 'user id is required',
				'error' => 'la identificación del usuario es requerida'
			], 201);
		} else if(!$service) {
			$this->response([
				'status' => FALSE,
				'error_en' => 'service is required',
				'error' => 'se requiere servicio'
			], 201);
		}  else if(!$delieveryTime) {
			$this->response([
				'status' => FALSE,
				'error_en' => 'delievery time is required',
				'error' => 'se requiere tiempo de entrega'
			], 201);
		}  else if(!$staffBehaviour) {
			$this->response([
				'status' => FALSE,
				'error_en' => 'staff behaviour is required',
				'error' => 'el comportamiento del personal es requerido'
			], 201);
		}  else if(!$comment) {
			$this->response([
				'status' => FALSE,
				'error_en' => 'comment is required',
				'error' => 'se requiere comentario'
			], 201);
		}   else {

			$data = array(
				"user_id" => $userId,
				"service" => $service,
				"delievery_time" => $delieveryTime,
				"staff_behaviour" => $staffBehaviour,
				"comment" => $comment
			);
			$feedbackid = $this->feedback_model->addFeedback($data);

			if ($feedbackid)	{
				$this->response([
					'status' => TRUE,
					'message_en' => "Feedback posted",
					'message' => "Comentarios publicados",
					'feedback_id' => $feedbackid
				], 200);
			}
			else {
				$this->response([
					'status' => FALSE,
					'error_en' => 'Something went wrong!',
					'error' => 'Algo salió mal!'
				], 200);
			}
		}
	}

}