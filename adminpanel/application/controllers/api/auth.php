<?php
require(APPPATH . '/libraries/REST_Controller.php');

class Auth extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api/auth_model');
        $this->load->model('api/insertquery_model');
        $this->load->helper('URL');
    }

    public function index_get()
    {
        $this->response([
            'status' => FALSE,
            'message' => "unknown method"
        ], 200);
    }


    public function login_post()
    {
        $email = $this->post('email');
        $password = $this->post('password');
        $role = $this->post('role');

        if (!$email) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'email is required',
                'error' => 'correo electronico es requerido'
            ], 201);
        } else if (!$password) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'password is required',
                'error' => 'se requiere contraseña'
            ], 201);
        } else {
            $data = array(
                'email' => $email,
                'password' => $password
            );

            if ($role) {
                $data['role'] = $role;
            }

            $checkAlreadyExist = $this->auth_model->checkAlreadyExist($email);
            if ($checkAlreadyExist) {
                $loginStatus = $this->auth_model->login($data);
                if ($loginStatus) {
                    $this->response([
                        'status' => TRUE,
                        'data' => $this->auth_model->getUser(array("users.email" => $email))
                    ], 200);
                } else {
                    $this->response([
                        'status' => FALSE,
                        'error_en' => 'Username or password wrong.',
                        'error' => 'Nombre de usuario o contraseña incorrectos.'
                    ], 200);
                }
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'User not exists',
                    'error' => 'El usuario no existe'
                ], 200);
            }

        }
    }


    public function signup_post()
    {
        $email = $this->post('email');
        $fname = $this->post('fname');
        $lname = $this->post('lname');
        $mobile = $this->post('mobile');
        $password = $this->post('password');
        $cityId = $this->post('city');
        $countryId = $this->post('country_id');

        if (!$email) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'email is required',
                'error' => 'correo electronico es requerido'
            ], 201);
        } else if (preg_match('/[A-Z]+/', $email)) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'email must be in lowercase',
                'error' => 'correo electrónico debe estar en minúsculas'
            ], 201);
        } else if (!$fname) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'first name is required',
                'error' => 'Se requiere el primer nombre'
            ], 201);
        } else if (!$lname) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'last name is required',
                'error' => 'apellido es requerido'
            ], 201);
        } else if (!$mobile) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'mobile is required',
                'error' => 'se requiere un móvil'
            ], 201);
        } else if (!$password) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'password is required',
                'error' => 'se requiere contraseña'
            ], 201);
        } else if (!$cityId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'city is required',
                'error' => 'ciudad es requerida'
            ], 201);
        } else {


            $data = array(
                'email' => $email,
                'password' => $password,
                'name' => $fname,
                'l_name' => $lname,
                'email' => $email,
                'mobile' => $mobile,
                'city_id' => $cityId,
                'country_id' => $countryId,
                'role' => 'user'
            );

            $checkAlreadyExist = $this->auth_model->checkAlreadyExist($email);
            if (!$checkAlreadyExist) {
                $signupStatus = $this->auth_model->signup($data);
                if ($signupStatus) {
                    $this->response([
                        'status' => TRUE,
                        'message' => "User registered successfully",
                        'data' => $this->auth_model->getUser(array("users.email" => $email))
                    ], 200);
                } else {
                    $this->response([
                        'status' => FALSE,
                        'error_en' => 'Username or password wrong.',
                        'error' => 'Nombre de usuario o contraseña incorrectos.'
                    ], 400);
                }
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'User already exists',
                    'error' => 'El usuario ya existe'
                ], 200);
            }

        }
    }


    public function updateToken_post()
    {
        $pushToken = $this->post('push_token');
        $userId = $this->post('user_id');

        if (!$userId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'user id is required',
                'error' => 'la identificación del usuario es requerida'
            ], 201);
        } else if (!$pushToken) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'Push token is required',
                'error' => 'Se requiere un token de inserción'
            ], 201);
        } else {
            $data = array(
                'id' => $userId,
                'push_token' => $pushToken
            );

            $updated = $this->auth_model->updateUser($data);
            if ($updated) {
                $this->response([
                    'status' => TRUE,
                    'message_en' => "User updated successfully",
                    'message' => "Usuario actualizado con éxito",
                    'data' => $this->auth_model->getUser(array("users.id" => $userId))
                ], 200);
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'Somethig went wrong..',
                    'error' => 'Algo salió mal..'
                ], 400);
            }

        }
    }


    public function updateProfile_post()
    {
        $fname = $this->post('fname');
        $lname = $this->post('lname');
        $mobile = $this->post('mobile');
        $cityId = $this->post('city_id');
        $userId = $this->post('user_id');

        if (!$userId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'user id is required',
                'error' => 'la identificación del usuario es requerida'
            ], 201);
        } else if (!$fname) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'first name is required',
                'error' => 'Se requiere el primer nombre'
            ], 201);
        } else if (!$lname) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'last name is required',
                'error' => 'apellido es requerido'
            ], 201);
        } else if (!$mobile) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'mobile is required',
                'error' => 'se requiere un móvil'
            ], 201);
        } else if (!$cityId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'city is required',
                'error' => 'ciudad es requerida'
            ], 201);
        } else {
            $data = array(
                'id' => $userId,
                'name' => $fname,
                'l_name' => $lname,
                'mobile' => $mobile,
                'city_id' => $cityId
            );

            $updated = $this->auth_model->updateUser($data);
            if ($updated) {
                $this->response([
                    'status' => TRUE,
                    'message_en' => "User updated successfully",
                    'message' => "Usuario actualizado con éxito",
                    'data' => $this->auth_model->getUser(array("users.id" => $userId))
                ], 200);
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'Somethig went wrong..',
                    'error' => 'Algo salió mal..'
                ], 400);
            }

        }
    }


    public function updateUserCurrentLocation_post()
    {
        $userId = $this->post('user_id');
        $latitude = $this->post('latitude');
        $longitude = $this->post('longitude');

        if (!$userId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'latitude is required',
                'error' => 'la identificación del usuario es requerida'
            ], 201);
        } else if (!$latitude) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'latitude is required',
                'error' => 'Se requiere el latitude nombre'
            ], 201);
        } else if (!$longitude) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'longitude is required',
                'error' => 'longitude es requerido'
            ], 201);
        } else {
            $data = array(
                'id' => $userId,
                'latitude' => $latitude,
                'longitude' => $longitude
            );

            $updated = $this->auth_model->updateUser($data);
            if ($updated) {
                $this->response([
                    'status' => TRUE,
                    'message_en' => "User updated successfully",
                    'message' => "Usuario actualizado con éxito",
                    'data' => $this->auth_model->getUser(array("users.id" => $userId))
                ], 200);
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'Somethig went wrong..',
                    'error' => 'Algo salió mal..'
                ], 400);
            }

        }
    }


    public function updateProfilePhoto_post()
    {
        $base64Image = $this->post('photo');
        $userId = $this->post('user_id');

        if (!$userId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'user id is required',
                'error' => 'la identificación del usuario es requerida'
            ], 201);
        } else if (!$base64Image) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'photo is required',
                'error' => 'se requiere foto'
            ], 201);
        } else {
            $path = '/assets/uploads/profile';
            $dir = '.' . $path;

            if (!is_dir($dir)) {
                mkdir($dir, 0777, TRUE);
            }

            $user = $this->auth_model->getUser(array("users.id" => $userId));
            if ($user->profile_photo) {
                unlink('.' . $user->profile_photo);
            }

            $image_name = md5(uniqid(rand(), true));
            $decoded = base64_decode($base64Image);
            $fileName = $dir . '/' . $image_name . '.png';
            $res = file_put_contents($fileName, $decoded);

            $data = array(
                'id' => $userId,
                'profile_photo' => $path . '/' . $image_name . '.png'
            );

            $updated = $this->auth_model->updateUser($data);

            if ($updated) {
                $this->response([
                    'status' => TRUE,
                    'message_en' => "User updated successfully",
                    'message' => "Usuario actualizado con éxito",
                    'data' => $this->auth_model->getUser(array("users.id" => $userId))
                ], 200);
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'Somethig went wrong..',
                    'error' => 'Algo salió mal..'
                ], 400);
            }
        }
    }

    public function resetPassword_post()
    {
        $email = $this->post('email');

        if (!$email) {
            $this->response([
                'status' => FALSE,
                'message_en' => 'email is required',
                'message' => 'correo electronico es requerido'
            ], 201);
        } else {

            if ($this->auth_model->checkAlreadyExist($email)) {


                $resetCode = mt_rand(1111111111111111, 9999999999999999) . mt_rand(1111111111111111, 9999999999999999);

                $this->auth_model->resetPasswordCode($email, array("reset_password_code" => $resetCode));
                $message = "Click on this link to set new password <a href='" . base_url() . "resetpassword?resetid=" . $resetCode . "'>Reset Password</a>";
                if ($this->auth_model->sendEmail($email, 'Reset Password', $message)) {

                    $this->response([
                        'status' => TRUE,
                        'message_en' => "Reset password link sent successfully",
                        'message' => "Restablecer enlace de contraseña enviado con éxito",
                        'data' => $this->email->print_debugger()
                    ], 200);
                }
            } else {
                $this->response([
                    'status' => FALSE,
                    'message_en' => "This email is not register with us.",
                    'message' => "Este correo electrónico no está registrado con nosotros."
                ], 200);
            }
        }
    }


    public function logout_get()
    {
        $userId = $this->get('user_id');

        if (!$userId) {
            $this->response([
                'status' => FALSE,
                'message_en' => 'userid is required',
                'message' => 'userid es requerido'
            ], 201);
        } else {
            $logoutRes = $this->insertquery_model->updateRow('users', array('id' => $userId), array('push_token' => NULL));
            if ($logoutRes) {
                $this->response([
                    'status' => TRUE,
                    'message_en' => "User logout successfully",
                    'message' => "Usuario actualizado con éxito"
                ], 200);
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'Somethig went wrong..',
                    'error' => 'Algo salió mal..'
                ], 200);
            }
        }
    }


    public function sendEmail_post()
    {
        $email = $this->post('email');
        $message = $this->post('message');
        $subject = $this->post('subject');

        if (!$email) {
            $this->response([
                'status' => FALSE,
                'message_en' => 'email is required',
                'message' => 'email es requerido'
            ], 201);
        } else if (!$message) {
            $this->response([
                'status' => FALSE,
                'message_en' => 'message is required',
                'message' => 'message es requerido'
            ], 201);
        } else if (!$subject) {
            $this->response([
                'status' => FALSE,
                'message_en' => 'subject is required',
                'message' => 'subject es requerido'
            ], 201);
        } else {
            if ($this->auth_model->sendEmail($email, $subject, $message)) {
                $this->response([
                    'status' => TRUE,
                    'error_en' => 'Email Sent!',
                    'error' => '!'
                ], 200);
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'Email not Sent!',
                    'error' => '!'
                ], 200);
            }
        }
    }

    public function users_post()
    {
        $users = $this->post('users');

        $status = false;
        $resData = null;
        $message = "";

        $usersIds = explode(",", $users);

        $conditions = array(
            "id" => $usersIds
        );
        $userRes = $this->query_model->getRows("users", $conditions);

        if ($userRes) {
            $status = true;
            $resData = $userRes;
        } else {
            $message = "No users found";
        }

        $this->response([
            'data' => $resData,
            'status' => $status,
            'message' => $message
        ], 200);
    }

}
