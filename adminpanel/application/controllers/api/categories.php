<?php
require(APPPATH . '/libraries/REST_Controller.php');

class Categories extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api/categories_model');
        $this->load->helper('URL');
        $this->load->model('query_model');

    }

    public function index_get()
    {
        $this->response([
            'status' => FALSE,
            'message' => "unknown method"
        ], 200);
    }

    public function categoryList_get()
    {
        $cityId = $this->get('city_id');
        $countryId = $this->get('countryId');
        $businessGroupId = $this->get('businessGroupId');
        $businessGroupSubCatId = $this->get('businessGroupSubCatId');
        $searchTxt = $this->get('search_txt');

        $data = $this->categories_model->getAllCategories($cityId, $countryId, $businessGroupId, $businessGroupSubCatId, $searchTxt);
        if ($data) {
            if ($data) {
                $columns = array(
                    "*",
                );
                foreach ($data as $item) {
                    $availibilty = $this->query_model->getRows('category_availability', array("category_id" => $item->id), $columns, array(array('day', 'asc')));
                    $item->availibilty = $availibilty;
                }
            }

            $this->response([
                'status' => TRUE,
                'data' => $data
            ], 200);
        } else {
            $this->response([
                'status' => FALSE,
                'error_en' => 'No categories were found',
                'error' => 'No se encontraron categorías'
            ], 200);
        }
    }

    public function getBusinessGroups_get()
    {
        $countryId = $this->get('countryId');

        $data = $this->categories_model->getAllBusinessGroups($countryId);
        if ($data) {
            $this->response([
                'status' => TRUE,
                'data' => $data
            ], 200);
        } else {
            $this->response([
                'status' => FALSE,
                'error_en' => 'No business group were found',
                'error' => 'No se encontraron categorías'
            ], 200);
        }
    }

    public function businessGroupSubcat_get()
    {
        $businesstype = $this->get('businesstype');

        $columns = array(
            "businesssubcat.*",
        );

        $data = $this->query_model->getRows('businesssubcat', array("businesssubcat.businesstype_id" => $businesstype), $columns);

        if ($data) {
            $this->response([
                'status' => TRUE,
                'data' => $data
            ], 200);
        } else {
            $this->response([
                'status' => FALSE,
                'error_en' => 'No business subcategory were found',
                'error' => 'No se encontraron categorías'
            ], 200);
        }
    }

    public function productSubcat_get()
    {
        $categoryId = $this->get('categoryId');

        $data = $this->query_model->getRows('subcategory', array("subcategory.category_id" => $categoryId));
        if ($data) {
            $this->response([
                'status' => TRUE,
                'data' => $data
            ], 200);
        } else {
            $this->response([
                'status' => FALSE,
                'error_en' => 'No sub category were found',
                'error' => 'No se encontraron categorías'
            ], 200);
        }
    }

}
