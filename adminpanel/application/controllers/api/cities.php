<?php
require(APPPATH . '/libraries/REST_Controller.php');

class Cities extends  REST_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper('URL');
		$this->load->model('query_model');
	}
	
	public function index_get()
	{
		$this->response([
			'status' => FALSE,
			'message' => "unknown method"
		], 200);
	}
	
	
	public function citiesList_get() {
		$countryId = $this->get('countryId');
		if(!isset($countryId)) {
			$this->response([
				'status' => FALSE,
				'error_en' => 'countryId is missing',
				'error' => 'countryId is missing'
			], 200);
		} else {
		$data = $this->query_model->getRows("city", array("users.country_id" => $countryId, "users.role" => "superadmin"), array("city.*"), null, array(array("users", "users.id=city.added_by","inner")));
		if ($data)	{
			$this->response([
				'status' => TRUE,
				'data' => $data
			], 200);
		}
		else {
			$this->response([
				'status' => FALSE,
				'error_en' => 'No cities were found',
				'error' => 'No se encontraron ciudades'
			], 200);
		}
		}
		
	}
}