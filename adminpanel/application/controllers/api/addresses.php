<?php
require(APPPATH . '/libraries/REST_Controller.php');

class Addresses extends  REST_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('api/address_model');
		$this->load->helper('URL');

	}
	
	public function index_get()
	{
		$this->response([
			'status' => FALSE,
			'message' => "unknown method"
		], 200);
	}
	

	public function addressAdd_post() {
		$addressId = $this->post('address_id'); 
		$userId = $this->post('user_id'); 
		$fullName = $this->post('fullname');
		$townCity = $this->post('city'); 
		$mobile = $this->post('phone'); 
		$flatNo = $this->post('flat_or_house_no'); 
		$colonyStreet = $this->post('apartment_or_locality'); 
		$other = $this->post('address'); 
		$landMark = $this->post('landmark'); 
		$pincode = $this->post('postal_code'); 
		$company = $this->post('company'); 
		$geoLat = $this->post('geo_lat'); 
		$geoLong = $this->post('geo_long'); 
		$addressType = $this->post('type'); 
		$isDefault = $this->post('primary'); 

		if(!$fullName) {
			$this->response([
				'status' => FALSE,
				'error_en' => 'fullname is required',
				'error' => 'se requiere nombre completo'
			], 201);
		} else if(!$mobile) {
			$this->response([
				'status' => FALSE,
				'error_en' => 'mobile is required',
				'error' => 'se requiere un móvil'
			], 201);
		}  else if(!$townCity) {
			$this->response([
				'status' => FALSE,
				'error_en' => 'town city is required',
				'error' => 'ciudad de la ciudad es requerida'
			], 201);
		}  else if(!$addressType) {
			$this->response([
				'status' => FALSE,
				'error_en' => 'address type is required',
				'error' => 'se requiere el tipo de dirección'
			], 201);
		}  else {
			$data = array(
				'name' => $fullName,
				'mobile' => $mobile,
				'town_city' => $townCity,
				'address_type' => $addressType,
				'pincode' => $pincode,
				'house_no' => $flatNo,
				'colony_street' => $colonyStreet,
				'landmark' => $landMark,
				'other' => $other,
				'landmark' => $landMark,
				'company' => $company,
				'is_default' => $isDefault,
				'user_id' => $userId,
				'geo_lat' => $geoLat,
				'geo_long' => $geoLong
			);



			if($isDefault) {
				$this->address_model->resetDefaultAddress($userId);
			}

			if($addressId) {
				$data['id']	= $addressId;
				
				$status = $this->address_model->updateAddress($data);
				if ($status)	{
					$this->response([
						'status' => TRUE,
						'message_en' => "Address updated successfully",
						'message' => "Dirección actualizada con éxito",
						'data' => $this->address_model->getAddress($addressId)
					], 200);
				}
				else {
					$this->response([
						'status' => FALSE,
						'error_en' => 'Something went wrong try again.',
						'error' => 'Algo salió mal, intenta de nuevo.'
					], 200);
				}
			} else {
				$addressId = $this->address_model->addAddress($data);
				if ($addressId)	{
					$this->response([
						'status' => TRUE,
						'message_en' => "Address added successfully",
						'message' => "Dirección agregada con éxito",
						'data' => $this->address_model->getAddress($addressId)
					], 200);
				}
				else {
					$this->response([
						'status' => FALSE,
						'error_en' => 'Something went wrong try again.',
						'error' => 'Algo salió mal, intenta de nuevo.'
					], 200);
				}
			}
		}
	}

	public function addressList_get() {
		$userId = $this->get('userId');

		$data = $this->address_model->getAddresses($userId);
		if ($data)	{
			$this->response([
				'status' => TRUE,
				'data' => $data
			], 200);
		}
		else {
			$this->response([
				'status' => FALSE,
				'error_en' => 'No addresses were found',
				'error' => 'No se encontraron direcciones'
			], 200);
		}
	}


	public function deleteAddress_get() {
		$addressId = $this->get('id');

		if(!$addressId) {
			$this->response([
				'status' => FALSE,
				'error_en' => 'address id is required',
				'error' => 'se requiere identificación de dirección'
			], 201);
		} else {
			$status = $this->address_model->deleteAddress($addressId);
			if ($status)	{
				$this->response([
					'status' => TRUE,
					'message_en' => "Address deleted successfully",
					'message' => "Dirección borrada con éxito"
				], 200);
			}
			else {
				$this->response([
					'status' => FALSE,
					'error_en' => 'Something went wrong try again.',
					'error' => 'Algo salió mal, intenta de nuevo.'
				], 200);
			}
		}

	}
}