<?php
require(APPPATH . '/libraries/REST_Controller.php');

class Delivery extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('query_model');
        $this->load->model('api/auth_model');
        $this->load->model('api/selectquery_model');
        $this->load->model('api/insertquery_model');
        $this->load->helper('URL');
    }

    public function index_get()
    {
        $this->response([
            'status' => FALSE,
            'message' => "unknown method"
        ], 200);
    }

    public function orders_get()
    {
        $userId = $this->get('delivery_userid');
        if (!$userId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'delivery_userid is required',
                'error' => 'ID de usuario es requerido'
            ], 200);
        } else {
            $conditions = array('order_status' => 'create', 'delboy.delivery_boy_id' => $userId);
            $columns = array(
                "orders.*",
                "users.name as provider_fname",
                "users.l_name as provider_lname",
                "pm.name as payment_method_name"
            );
            $joins = array(
                array('delivery_boy as delboy', 'delboy.order_id=orders.id', 'inner'),
                array('users', 'users.id=orders.provider_id', 'inner'),
                array('payment_methods as pm', 'pm.id=orders.payment_method_id', 'inner')
            );
            $orderBy = array(array('orders.created_at', 'asc'));
            $ordersList = $this->selectquery_model->getResults('orders', $conditions, $columns, $orderBy, $joins);
            $resultArray = array();
            if ($ordersList && count($ordersList) > 0) {
                for ($x = 0; $x < count($ordersList); $x++) {
                    $data = $ordersList[$x];
                    $newData = array();
                    foreach ($data as $key => $value) {
                        $newData[$key] = $value;
                    }
                    $shippingAddress = $this->selectquery_model->get_row('order_addresses', array("order_id" => $data->id), array('order_addresses.*', 'city.city_name'), NULL, array(array('city', 'city.id=order_addresses.town_city', 'inner')));
                    if ($shippingAddress) {
                        $newData['shipping_address'] = $shippingAddress;
                    } else {
                        $newData['shipping_address'] = array();
                    }

                    $condition = array("order_products.order_id" => $data->id);
                    $columns = array(
                        "order_products.*",
                        "category.category_name as business_name"
                    );
                    $joins = array(
                        array("category", "category.id=order_products.category_id", "inner"),
                        array("businesstype", "businesstype.id=category.businesstype_id", "left")
                    );

                    $customer = $this->query_model->getRow("users", array("id" => $data->user_Id));
                    $newData['user'] = $customer;

                    $orderProducts = $this->query_model->getRows('order_products', $condition, $columns, NULL, $joins);

                    if ($orderProducts) {
                        foreach ($orderProducts as $product) {
                            $newData['business_name'] = $product->business_name;
                            if (empty($product->supplies)) {
                                $product->supplies = "{}";
                            }
                        }
                    }

                    if ($orderProducts) {
                        $newData['products'] = $orderProducts;
                    } else {
                        $newData['products'] = array();
                    }
                    $resultArray[] = $newData;
                }
            }

            if ($ordersList) {
                $this->response([
                    'status' => TRUE,
                    'data' => $resultArray
                ], 200);
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'No orders were found',
                    'error' => 'No se encontraron pedidos'
                ], 200);
            }
        }
    }

    public function orderStatusUpdate_post()
    {
        $orderId = $this->post('order_id');
        $deliveryUserId = $this->post('delivery_user');
        $orderStatus = $this->post('order_status');
        if (!$orderId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'order_id is required',
                'error' => 'Se requiere order_id'
            ], 200);
        } else if (!$deliveryUserId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'delivery_user is required',
                'error' => 'delivery_user es obligatorio'
            ], 200);
        } else if (!$orderStatus) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'order_status is required',
                'error' => 'order_status es obligatorio'
            ], 200);
        } else {
            $res = $this->insertquery_model->updateRow('orders', array('id' => $orderId), array('order_status' => $orderStatus));
            $this->sendEmailForProductComplete($orderId);
            if ($res) {
                $this->response([
                    'status' => TRUE,
                    'message_en' => "Order updated",
                    'message' => "Orden actualizada"
                ], 200);
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'Somethig went wrong..',
                    'error' => 'Algo salió mal..'
                ], 200);
            }
        }
    }


    private function sendEmailForProductComplete($orderId)
    {

        $conditions = array('orders.id' => $orderId);
        $coulmns = array(
            "orders.*",
            "dboyuser.name as delivery_boy_first_name",
            "dboyuser.l_name as delivery_boy_last_name",
            "users.email",
            "orderby.email as orderby_useremail",
            "orderby.name as orderedby_fname",
            "orderby.l_name as orderedby_lname",
            "pm.name as paymentmethod"
        );
        $joins = array(
            array("users", "users.id = orders.provider_id", "inner"),
            array("users as orderby", "orderby.id = orders.user_id", "inner"),
            array("delivery_boy as dboy", "dboy.order_id = orders.id", "inner"),
            array("users as dboyuser", "dboyuser.id = dboy.delivery_boy_id", "inner"),
            array("payment_methods as pm", "pm.id = orders.payment_method_id", "inner")
        );
        $order = $this->selectquery_model->get_row('orders', $conditions, $coulmns, NULL, $joins);
        $orderProducts = $this->selectquery_model->getResults('order_products',
            array("order_id" => $orderId),
            array("order_products.*", "price as product_price", "category.notification_email as superadmin_email", "businesstype.name as businesstype_name", "businesstype.imagepath as businesstype_logo", "category.category_name as business_name"),
            NULL,
            array(
                array("category", "category.id=order_products.category_id", "inner"),
                array("businesstype", "businesstype.id=category.businesstype_id", "left")
            ));

        //Get timezone for user
        $condition = array('users.id' => $order->user_Id);
        $columns = array("country.timezone");
        $joins = array(
            array("country", "country.id=users.country_id", "inner")
        );
        $user = $this->query_model->getRow('users', $condition, $columns, null, $joins);
        $timeZone = "America/Argentina/Buenos_Aires";
        if ($user) {
            $timeZone = $user->timezone;
        }
        $newTImeZone = new DateTimeZone($timeZone);
        $orderDatetime = new DateTime($order->created_at);
        $orderDatetime->setTimezone($newTImeZone);
        $formattedOrderDateTime = $orderDatetime->format("d-m-Y H:i:s");;
        //Get timezone for user--End
        $providerEmail = $order->email;

        $orderProductsHtml = "<table border='0' style='width:100%; border-spacing: 0;margin-top: 20px; border-color:#60269E;'>";
        $orderProductsHtml = $orderProductsHtml . "<tr style='padding:10px;text-align:center;'>";
        $orderProductsHtml = $orderProductsHtml . "<th style='padding:8px 10px;'>Imagen</th>";

        $orderProductsHtml = $orderProductsHtml . "<th style='padding:8px 10px;'>Nombre</th>";
        $orderProductsHtml = $orderProductsHtml . "<th style='padding:8px 10px;'>Precio</th>";
        $orderProductsHtml = $orderProductsHtml . "<th style='padding:8px 10px;'>Cantidad  </th>";
        $orderProductsHtml = $orderProductsHtml . "<th style='padding:8px 10px;'>Descripción</th>";
        $orderProductsHtml = $orderProductsHtml . "<th style='padding:8px 10px;'>Detalle</th>";
        $orderProductsHtml = $orderProductsHtml . "<th style='padding:8px 10px;'>Supplies</th>";
        $orderProductsHtml = $orderProductsHtml . "</tr>";


        $subTotal = 0;
        $delivery = $order->delievery_price;

        $businessGroupName = "";
        $businessGroupLogo = "";
        $businessName = "";

        foreach ($orderProducts as $product) {

            $price = (float)$product->promotional_price;
            $quantity = (int)$product->quantity;
            $businessGroupName = $product->businesstype_name;
            $businessGroupLogo = $product->businesstype_logo;
            $businessName = $product->business_name;

            if (!$price) {
                $price = (float)$product->price;
            }
            $subTotal = $subTotal + $price * $quantity;
            $orderProductsHtml = $orderProductsHtml . "<tr style='padding:10px;text-align:center;'>";
            $orderProductsHtml = $orderProductsHtml . "<td><img src='https://snappypanel.com/adminpanel/" . $product->product_img . "' style='width:60px;height:60px;'></td>";

            $orderProductsHtml = $orderProductsHtml . "<td style='padding:8px 10px;'>" . $product->product_name . "</td>";

            $orderProductsHtml = $orderProductsHtml . "<td style='padding:8px 10px;'>" . $price . "</td>";
            $orderProductsHtml = $orderProductsHtml . "<td style='padding:8px 10px;'>" . $quantity . "</td>";
            $orderProductsHtml = $orderProductsHtml . "<td style='padding:8px 10px;'>" . $product->short_description . "</td>";
            $orderProductsHtml = $orderProductsHtml . "<td style='padding:8px 10px;'>" . $product->extra_detail . "</td>";

            $suppliesStr = $product->supplies;
            $suppliesTotal = 0;
            if (!empty($suppliesStr)) {
                $supplies = json_decode($suppliesStr);
                $orderProductsHtml = $orderProductsHtml . "<td><table>";
                foreach ($supplies as $supply) {
                    $orderProductsHtml = $orderProductsHtml . "<tr style='padding:5px;text-align:center;'>";
                    $orderProductsHtml = $orderProductsHtml . "<th style='padding:3px 3px;'>" . $supply->supplies_name . "</th>";
                    $orderProductsHtml = $orderProductsHtml . "<td style='padding:3px 3px;'>" . $supply->supplies_price . "</td>";
                    $orderProductsHtml = $orderProductsHtml . "</tr>";

                    $suppliesTotal = $suppliesTotal + (float)$supply->supplies_price;
                }
                $orderProductsHtml = $orderProductsHtml . "</table></td>";

            }
            $subTotal = $subTotal + $suppliesTotal * $quantity;
            $orderProductsHtml = $orderProductsHtml . "</tr>";
        }

        $total = $subTotal + $delivery;


        $orderProductsHtml = $orderProductsHtml . '</table>';

        if ($order) {
            $messageHtml = "
        <html>
            <head>    
                <meta name='viewport' content='width=device-width' />
                <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    
                <title>Snappy Delivery</title>
                 
                 <style>
                                  /* -------------------------------------
                                      GLOBAL RESETS
                                  ------------------------------------- */
                                  
                                  /*All the styling goes here*/
                                  
                                  img {
                                    border: none;
                                    -ms-interpolation-mode: bicubic;
                                    max-width: 100%; 
                                  }
                            
                                  body {
                                    background-color: #f6f6f6;
                                    font-family: sans-serif;
                                    -webkit-font-smoothing: antialiased;
                                    font-size: 14px;
                                    line-height: 1.4;
                                    margin: 0;
                                    padding: 0;
                                    -ms-text-size-adjust: 100%;
                                    -webkit-text-size-adjust: 100%; 
                                  }
                            
                                  table {
                                    border-collapse: separate;
                                    mso-table-lspace: 0pt;
                                    mso-table-rspace: 0pt;
                                    width: 100%; }
                                    table td {
                                      font-family: sans-serif;
                                      font-size: 14px;
                                      vertical-align: top; 
                                  }
                            
                                  /* -------------------------------------
                                      BODY & CONTAINER
                                  ------------------------------------- */
                            
                                  .body {
                                    background-color: #f6f6f6;
                                    width: 100%; 
                                  }
                            
                                  /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
                                  .container {
                                    display: block;
                                    margin: 0 auto !important;
                                    /* makes it centered */
                                    max-width: 580px;
                                    padding: 10px;
                                    width: 580px; 
                                  }
                            
                                  /* This should also be a block element, so that it will fill 100% of the .container */
                                  .content {
                                    box-sizing: border-box;
                                    display: block;
                                    margin: 0 auto;
                                    max-width: 580px;
                                    padding: 10px; 
                                  }
                            
                                  /* -------------------------------------
                                      HEADER, FOOTER, MAIN
                                  ------------------------------------- */
                                  .main {
                                    background: #ffffff;
                                    border-radius: 3px;
                                    width: 100%; 
                                  }
                            
                                  .wrapper {
                                    box-sizing: border-box;
                                    padding: 20px; 
                                  }
                            
                                  .content-block {
                                    padding-bottom: 10px;
                                    padding-top: 10px;
                                  }
                            
                                  .footer {
                                    clear: both;
                                    margin-top: 10px;
                                    text-align: center;
                                    width: 100%; 
                                  }
                                    .footer td,
                                    .footer p,
                                    .footer span,
                                    .footer a {
                                      color: #999999;
                                      font-size: 12px;
                                      text-align: center; 
                                  }
                            
                                  /* -------------------------------------
                                      TYPOGRAPHY
                                  ------------------------------------- */
                                  h1,
                                  h2,
                                  h3,
                                  h4 {
                                    color: #000000;
                                    font-family: sans-serif;
                                    font-weight: 400;
                                    line-height: 1.4;
                                    margin: 0;
                                    margin-bottom: 30px; 
                                  }
                            
                                  h1 {
                                    font-size: 35px;
                                    font-weight: 300;
                                    text-align: center;
                                    text-transform: capitalize; 
                                  }
                            
                                  p,
                                  ul,
                                  ol {
                                    font-family: sans-serif;
                                    font-size: 14px;
                                    font-weight: normal;
                                    margin: 0;
                                    margin-bottom: 15px; 
                                  }
                                    p li,
                                    ul li,
                                    ol li {
                                      list-style-position: inside;
                                      margin-left: 5px; 
                                  }
                            
                                
                            
                                  /* -------------------------------------
                                      BUTTONS
                                  ------------------------------------- */
                                  .btn {
                                    box-sizing: border-box;
                                    width: 100%; }
                                    .btn > tbody > tr > td {
                                      padding-bottom: 15px; }
                                    .btn table {
                                      width: auto; 
                                  }
                                    .btn table td {
                                      background-color: #ffffff;
                                      border-radius: 5px;
                                      text-align: center; 
                                  }
                                    .btn a {
                                      background-color: #ffffff;
                                      border: solid 1px #3498db;
                                      border-radius: 5px;
                                      box-sizing: border-box;
                                      color: #3498db;
                                      cursor: pointer;
                                      display: inline-block;
                                      font-size: 14px;
                                      font-weight: bold;
                                      margin: 0;
                                      padding: 12px 25px;
                                      text-decoration: none;
                                      text-transform: capitalize; 
                                  }
                            
                                  .btn-primary table td {
                                    background-color: #3498db; 
                                  }
                            
                                  .btn-primary a {
                                    background-color: #3498db;
                                    border-color: #3498db;
                                    color: #ffffff; 
                                  }
                            
                                  /* -------------------------------------
                                      OTHER STYLES THAT MIGHT BE USEFUL
                                  ------------------------------------- */
                                  .last {
                                    margin-bottom: 0; 
                                  }
                            
                                  .first {
                                    margin-top: 0; 
                                  }
                            
                                  .align-center {
                                    text-align: center; 
                                  }
                            
                                  .align-right {
                                    text-align: right; 
                                  }
                            
                                  .align-left {
                                    text-align: left; 
                                  }
                            
                                  .clear {
                                    clear: both; 
                                  }
                            
                                  .mt0 {
                                    margin-top: 0; 
                                  }
                            
                                  .mb0 {
                                    margin-bottom: 0; 
                                  }
                            
                                  .preheader {
                                    color: transparent;
                                    display: none;
                                    height: 0;
                                    max-height: 0;
                                    max-width: 0;
                                    opacity: 0;
                                    overflow: hidden;
                                    mso-hide: all;
                                    visibility: hidden;
                                    width: 0; 
                                  }
                            
                                  .powered-by a {
                                    text-decoration: none; 
                                  }
                            
                                  hr {
                                    border: 0;
                                    border-bottom: 1px solid #f6f6f6;
                                    margin: 20px 0; 
                                  }
                            
                                  /* -------------------------------------
                                      RESPONSIVE AND MOBILE FRIENDLY STYLES
                                  ------------------------------------- */
                                  @media only screen and (max-width: 620px) {
                                    table[class=body] h1 {
                                      font-size: 28px !important;
                                      margin-bottom: 10px !important; 
                                    }
                                    table[class=body] p,
                                    table[class=body] ul,
                                    table[class=body] ol,
                                    table[class=body] td,
                                    table[class=body] span,
                                    table[class=body] a {
                                      font-size: 16px !important; 
                                    }
                                    table[class=body] .wrapper,
                                    table[class=body] .article {
                                      padding: 10px !important; 
                                    }
                                    table[class=body] .content {
                                      padding: 0 !important; 
                                    }
                                    table[class=body] .container {
                                      padding: 0 !important;
                                      width: 100% !important; 
                                    }
                                    table[class=body] .main {
                                      border-left-width: 0 !important;
                                      border-radius: 0 !important;
                                      border-right-width: 0 !important; 
                                    }
                                    table[class=body] .btn table {
                                      width: 100% !important; 
                                    }
                                    table[class=body] .btn a {
                                      width: 100% !important; 
                                    }
                                    table[class=body] .img-responsive {
                                      height: auto !important;
                                      max-width: 100% !important;
                                      width: auto !important; 
                                    }
                                  }
                            
                                  /* -------------------------------------
                                      PRESERVE THESE STYLES IN THE HEAD
                                  ------------------------------------- */
                                  @media all {
                                    .ExternalClass {
                                      width: 100%; 
                                    }
                                    .ExternalClass,
                                    .ExternalClass p,
                                    .ExternalClass span,
                                    .ExternalClass font,
                                    .ExternalClass td,
                                    .ExternalClass div {
                                      line-height: 100%; 
                                    }
                                    .apple-link a {
                                      color: inherit !important;
                                      font-family: inherit !important;
                                      font-size: inherit !important;
                                      font-weight: inherit !important;
                                      line-height: inherit !important;
                                      text-decoration: none !important; 
                                    }
                                    #MessageViewBody a {
                                      color: inherit;
                                      text-decoration: none;
                                      font-size: inherit;
                                      font-family: inherit;
                                      font-weight: inherit;
                                      line-height: inherit;
                                    }
                                    .btn-primary table td:hover {
                                      background-color: #34495e !important; 
                                    }
                                    .btn-primary a:hover {
                                      background-color: #34495e !important;
                                      border-color: #34495e !important; 
                                    } 
                                  }
                    
                 </style>
             </head>
             <body class=''>
             <span class='preheader'></span>
             <table role='presentation' border='0' cellpadding='0' cellspacing='0' class='body'>
              <tr>
                <td>&nbsp;</td>
                <td class='container'>
                  <div class='content'>
        
                    <!-- START CENTERED WHITE CONTAINER -->
                    <table role='presentation' class='main'>
        
                      <!-- START MAIN CONTENT AREA -->
                      <tr>
                        <td class='wrapper'>
                          <table role='presentation' border='0' cellpadding='0' cellspacing='0'>
                            <tr>
                              <td>
                                <img src='https://snappypanel.com/mailsnappy.png'>
                                <p>Muchas gracias por confiar en Snappy Delivery para atender tú pedido. Día con día nos esforzamos para llevar lo que más te gusta hasta donde te encuentres.</p>
                                <table role='presentation' border='0' cellpadding='0' cellspacing='0' class=''>
                                  <tbody>
                                    <tr>
                                      <td align='left'>
                                      
                                         
                                          
                                              <a href=''>
                                              <img src='https://snappypanel.com/Facebook100.png'>
                                              </a>
                                    
                                              <a href=''>
                                              <img src='https://snappypanel.com/Whatsapp100.png'>
                                              </a>
                                             
                                              <a href=''>
                                              <img src='https://snappypanel.com/Instagram100.png'>
                                              </a>
                                              
                                      
                                   
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                                <p>Puedes buscarnos en redes sociales, estaremos esperando tú visita.</p>
                                <p>Excelente día.</p>
                                
                                        <tr style='padding:10px;text-align:center;'>
                </tr>
                <tr>
                <td>
                <img src='https://snappypanel.com/adminpanel/" . $businessGroupLogo . "' style='width:60px;height:60px;'>
                <div><span style='font-size:18px;font-weight:bold;'>" . $businessGroupName . " -> " . "</span><span style='font-size:18px;'>" . $businessName . "</span></div>
                <div><span style='font-size:18px;font-weight:bold;'>Muchacho de entrega -> </span><span style='font-size:18px;'>" . $order->delivery_boy_first_name . " " . $order->delivery_boy_last_name . "</span></div>
                </td>
                </tr>
                <tr>
                <td>
                <table border='0' style='border-spacing: 0;width:100%; border-top:3px solid #ccc; border-color:#60269E;'>
                <tr style='padding:10px;text-align:center;'>
                <th style='padding:8px 10px;'>Número de orden</th>
                <th style='padding:8px 10px;'>Método de págo</th>
                <th style='padding:8px 10px;'>Notas</th>
                <th style='padding:8px 10px;'>Ordenó</th>
                <th style='padding:8px 10px;'>Fecha y hora</th>
                 <table border='0' style='border-spacing: 0;width:100%; border-top:3px solid #ccc; border-color:#60269E;'>
                </tr>
                
                <tr style='text-align:center;'>
                <td style='padding:8px 10px;'>  " . $orderId . "  </td>
                <td style='padding:8px 10px;'> " . $order->paymentmethod . " </td>
                <td style='padding:8px 10px;'> " . $order->order_notes . "</td>
                <td style='padding:8px 10px;'> " . $order->orderedby_fname . " " . $order->orderedby_lname . "</td>
                <td style='padding:8px 10px;'> " . $formattedOrderDateTime . "</td>
                </tr>
                </table>
                </td>
                </tr>
                <tr>
                <td>
                " . $orderProductsHtml . "
                </td>
                </tr>
                <tr>
                <td>
                <table border='0' style='border-spacing: 0;width:100%; border-top:3px solid #ccc;margin-top: 20px; border-color:#60269E;'>
                <tr>
                <th style='padding:8px 10px;'>Subtotal</th><td style='padding:8px 10px;'>" . $subTotal . "</td>
                </tr>
                <tr>
                <th style='padding:8px 10px;'>Envío</th><td style='padding:8px 10px;'>" . $delivery . "</td>
                </tr>
                <tr>
                <th style='padding:8px 10px;'>Total</th><td style='padding:8px 10px;'>" . $total . "</td>
                </tr>
                </table>
                </td>
                </tr>
                </table> 
                                
                                
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
        
                    <!-- END MAIN CONTENT AREA -->
                    </table>
                    <!-- END CENTERED WHITE CONTAINER -->
        
                    <!-- START FOOTER -->
                    <div class='footer'>
                      <table role='presentation' border='0' cellpadding='0' cellspacing='0'>
                        <tr>
                          <td class='content-block'>
                            <span class='apple-link'>Argentina</span>
                            <br> Correo informativo <a href='http://snappydelivery.com.ar/'>www.snappydelivery.com.ar</a>.
                          </td>
                        </tr>
                        <tr>
                          <td class='content-block powered-by'>
                            Powered by <a href='http://snappydelivery.com.ar/'>Snappy Delivery</a>.
                          </td>
                        </tr>
                      </table>
                    </div>
                    <!-- END FOOTER -->
        
                  </div>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>
          </body>
</html>

";

            $subject = "Order Delivered";


            if ($this->auth_model->sendEmail($providerEmail, $subject, $messageHtml)) {
                return true;
            }
            return false;
        }
        return true;


    }


    public function deliveryBoyStatus_post()
    {
        $userId = $this->post('user_id');
        if (!$userId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'user_id is required',
                'error' => 'Se requiere user_id'
            ], 200);
        } else {
            $status = $this->query_model->getRow("delivery_boy_status", array("user_id" => $userId));

            if ($status) {
                $this->response([
                    'status' => TRUE,
                    'data' => $status,
                ], 200);
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'status not found',
                    'error' => 'status not found'
                ], 200);
            }
        }
    }

    public function deliveryBoyStatusChange_post()
    {
        $userId = $this->post('user_id');
        $status = $this->post('status');
        if (!$userId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'user_id is required',
                'error' => 'Se requiere user_id'
            ], 200);
        } else if (!isset($status)) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'status is required',
                'error' => 'status es obligatorio'
            ], 200);
        } else {
            $data = array("user_id" => $userId, "status" => $status);
            $status = $this->query_model->getRows("delivery_boy_status", array("user_id" => $userId));
            if ($status) {
                $res = $this->query_model->updateRow("delivery_boy_status", array("user_id" => $userId), $data);
            } else {
                $res = $this->query_model->insertRow("delivery_boy_status", $data);
            }

            if ($res) {
                $this->response([
                    'status' => TRUE,
                    'message_en' => 'status is updated',
                    'message' => 'status is updated'
                ], 200);
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'status updated error',
                    'error' => 'status updated error'
                ], 200);
            }
        }
    }
}
