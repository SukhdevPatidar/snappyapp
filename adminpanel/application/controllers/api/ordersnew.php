<?php
require(APPPATH . '/libraries/REST_Controller.php');

class Ordersnew extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('api/auth_model');
        $this->load->model('api/orders_model');
        $this->load->model('api/selectquery_model');
        $this->load->model('api/insertquery_model');
        $this->load->helper('URL');
    }

    public function index_get()
    {
        $this->response([
            'status' => FALSE,
            'message' => "unknown method"
        ], 200);
    }


    public function createOrder_post()
    {
        $userId = $this->post('user_id');
        $paymentMethodId = $this->post('payment_method_id');
        $delieveryPrice = $this->post('delievery_price');
        $orderStatus = $this->post('order_status');
        $paymentStatus = $this->post('payment_status');
        $addressId = $this->post('address_id');
        $orderNotes = $this->post('order_notes');
        $items = $this->post('items');
        $providerId = $this->post('provider_id');


        if (!$userId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'user id is required',
                'error' => 'la identificación del usuario es requerida'
            ], 201);
        } else if (!$paymentMethodId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'payment method id is required',
                'error' => 'se requiere una identificación de método de pago'
            ], 201);
        } else if (!isset($delieveryPrice)) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'delivery price is required',
                'error' => 'se requiere el precio de entrega'
            ], 201);
        } else if (!$providerId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'provider id is required',
                'error' => 'se requiere identificación del proveedor'
            ], 201);
        } else if (!$orderStatus) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'order status is required',
                'error' => 'el estado de la orden es requerido'
            ], 201);
        } else if (!$paymentStatus) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'payment status is required',
                'error' => 'el estado de pago es obligatorio'
            ], 201);
        } else if (!$addressId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'address id is required',
                'error' => 'se requiere identificación de dirección'
            ], 201);
        } else if (!$items) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'items is required',
                'error' => 'artículos son requeridos'
            ], 201);
        } else {

            $data = array(
                "user_id" => $userId,
                "payment_method_id" => $paymentMethodId,
                "delievery_price" => $delieveryPrice,
                "order_status" => $orderStatus,
                "payment_status" => $paymentStatus,
                "order_notes" => $orderNotes,
                "provider_id" => $providerId
            );
            $orderId = $this->insertquery_model->insertRow('orders', $data);

            if ($orderId) {
                // prepare order products
                $products = array();
                foreach ($items as $item) {
                    $product = $this->selectquery_model->get_row('product', array("id" => $item['id']));
                    $supplies = $item['supplies'];
                    $suppliesJsonStr = "";
                    if ($supplies && count($supplies) > 0) {
                        $suppliesJsonStr = json_encode($supplies);
                    }

                    array_push($products, array(
                        "order_id" => $orderId,
                        "product_id" => $product->id,
                        "provider_id" => $product->user_id,
                        "category_id" => $product->category_id,
                        "product_name" => $product->product_name,
                        "short_description" => $product->short_description,
                        "long_description" => $product->long_description,
                        "price" => $product->price,
                        "quantity" => $item['quantity'],
                        "extra_detail" => $item['extra_detail'],
                        "supplies" => $suppliesJsonStr,
                        "promotional_price" => $product->promotional_price,
                        "product_img" => $product->product_img,
                        "delivery_time" => $product->delivery_time
                    ));
                }

                // prepare order address
                $address = $this->selectquery_model->get_row('addresses', array("id" => $addressId));
                $orderAddress = array(
                    "order_id" => $orderId,
                    "name" => $address->name,
                    "mobile" => $address->mobile,
                    "town_city" => $address->town_city,
                    "address_type" => $address->address_type,
                    "pincode" => $address->pincode,
                    "house_no" => $address->house_no,
                    "colony_street" => $address->colony_street,
                    "landmark" => $address->landmark,
                    "other" => $address->other,
                    "company" => $address->company,
                    "latitude" => $address->geo_lat,
                    "longitude" => $address->geo_long,
                );

                $orderProductsRes = $this->insertquery_model->insertMultipleRow('order_products', $products);
                $orderAddressId = $this->insertquery_model->insertRow('order_addresses', $orderAddress);

                if ($orderProductsRes && $orderAddressId) {
                    // check payment method
                    $paymentMethods = $this->selectquery_model->get_row('payment_methods', array('id' => $paymentMethodId), array('code'));
                    if (($paymentMethods && $paymentMethods->code == 'recovery') || ($paymentMethods && $paymentMethods->code == 'cod') || ($paymentMethods && ($paymentMethods->code == 'paypal' || $paymentMethods->code == 'payu') && $paymentStatus == 'complete')) {
                        // order completed
                        $this->sendEmailForProductComplete($orderId);
                    }

                    $this->response([
                        'status' => TRUE,
                        'message_en' => "Order created",
                        'message' => "Orden creado",
                        'order_id' => $orderId
                    ], 200);
                } else {
                    $this->response([
                        'status' => FALSE,
                        'error_en' => 'Something went wrong!',
                        'error' => 'Algo salió mal!'
                    ], 200);
                }
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'Something went wrong!',
                    'error' => 'Algo salió mal!'
                ], 200);
            }
        }
    }

    public function updateOrder_post()
    {
        $orderId = $this->post('order_id');
        $paymentMethodId = $this->post('payment_method_id');
        $delieveryPrice = $this->post('delievery_price');
        $orderStatus = $this->post('order_status');
        $paymentStatus = $this->post('payment_status');
        $addressId = $this->post('address_id');
        $orderNotes = $this->post('order_notes');
        $paymentResponse = $this->post('payment_response');


        if (!$orderId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'order id is required',
                'error' => 'se requiere identificación de pedido'
            ], 201);
        } else {

            $data = array(
                "id" => $orderId
            );
            if ($paymentMethodId) {
                $data['payment_method_id'] = $paymentMethodId;
            }
            if ($delieveryPrice) {
                $data['delievery_price'] = $delieveryPrice;
            }
            if ($orderStatus) {
                $data['order_status'] = $orderStatus;
            }
            if ($paymentStatus) {
                $data['payment_status'] = $paymentStatus;
            }
            if ($orderNotes) {
                $data['order_notes'] = $orderNotes;
            }
            if ($paymentResponse) {
                $data['payment_response'] = $paymentResponse;
            }

            $status = $this->orders_model->updateOrder($data);


            if ($paymentStatus == 'complete') {
                // order completed
                $this->sendEmailForProductComplete($orderId);
            }

            if ($status) {
                $this->response([
                    'status' => TRUE,
                    'message_en' => "Order updated",
                    'message' => "Orden actualizada"
                ], 200);
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'Something went wrong!',
                    'error' => 'Algo salió mal!'
                ], 200);
            }
        }
    }


    public function getOrders_get()
    {
        $userId = $this->get('userId');
        $status = $this->get('status');


        if (!$userId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'user id is required',
                'error' => 'la identificación del usuario es requerida'
            ], 201);
        } else {

            //Get timezone for user
            $condition = array('users.id' => $userId);
            $columns = array("country.timezone");
            $joins = array(
                array("country", "country.id=users.country_id", "inner")
            );
            $user = $this->query_model->getRow('users', $condition, $columns, null, $joins);
            $timeZone = "America/Argentina/Buenos_Aires";
            if ($user) {
                $timeZone = $user->timezone;
            }
            $newTImeZone = new DateTimeZone($timeZone);

            $ordersList = $this->selectquery_model->getResults('orders',
                array("user_Id" => $userId),
                array("orders.*", "pm.name as payment_method_name"),
                array(array('orders.created_at', 'desc')),
                array(array('payment_methods as pm', 'pm.id=orders.payment_method_id', 'inner')));

            $resultArray = array();
            if ($ordersList && count($ordersList) > 0) {
                for ($x = 0; $x < count($ordersList); $x++) {
                    $data = $ordersList[$x];
                    $newData = array();
                    foreach ($data as $key => $value) {
                        $newData[$key] = $value;
                    }
                    $orderProducts = $this->selectquery_model->getResults('order_products',
                        array("order_id" => $data->id),
                        array("order_products.*", "price as product_price"),
                        NULL,
                        NULL);
                    if ($orderProducts) {
                        $newData['items'] = $orderProducts;
                    } else {
                        $newData['items'] = array();
                    }

                    $datetime = new DateTime($newData['created_at']);
                    $datetime->setTimezone($newTImeZone);
                    $newData['created_at'] = $datetime->format("d-m-Y H:i:s");;

                    $resultArray[] = $newData;
                }
            }
            if ($resultArray) {
                $this->response([
                    'status' => TRUE,
                    'data' => $resultArray
                ], 200);
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'No orders were found',
                    'error' => 'No se encontraron pedidos'
                ], 200);
            }
        }
    }


    public function getPaymentInfoForProvider_get()
    {
        $providerId = $this->get('provider_id');
        $paymentMethodId = $this->get('payment_method_id');


        if (!$providerId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'provider id is required',
                'error' => 'se requiere identificación del proveedor'
            ], 201);
        } else if (!$paymentMethodId) {
            $this->response([
                'status' => FALSE,
                'error_en' => 'payment method id is required',
                'error' => 'se requiere una identificación de método de pago'
            ], 201);
        } else {
            $data = $this->orders_model->getPaymentInfoForProvider($providerId, $paymentMethodId);
            if ($data) {
                $config = array();
                foreach ($data as $key => $value) {
                    $config[$value->field_name] = $value->field_value;
                }

                $this->response([
                    'status' => TRUE,
                    'data' => $config
                ], 200);
            } else {
                $this->response([
                    'status' => FALSE,
                    'error_en' => 'No payment info were found',
                    'error' => 'No se encontraron datos de pago'
                ], 200);
            }
        }
    }


    private function sendEmailForProductComplete($orderId)
    {
        $conditions = array('orders.id' => $orderId);
        $joins = array(array("users", "users.id = orders.provider_id", "inner"),
            array("users as orderby", "orderby.id = orders.user_id", "inner"),
            array("payment_methods as pm", "pm.id = orders.payment_method_id", "inner"));
        $coulmns = array("orders.*", "users.email", "orderby.email as orderby_useremail", "orderby.name as orderedby_fname", "orderby.l_name as orderedby_lname", "pm.name as paymentmethod");
        $order = $this->selectquery_model->get_row('orders', $conditions, $coulmns, NULL, $joins);
        $orderProducts = $this->selectquery_model->getResults('order_products',
            array("order_id" => $orderId),
            array("order_products.*", "price as product_price", "category.notification_email as superadmin_email", "businesstype.name as businesstype_name", "businesstype.imagepath as businesstype_logo", "category.category_name as business_name"),
            NULL,
            array(
                array("category", "category.id=order_products.category_id", "inner"),
                array("businesstype", "businesstype.id=category.businesstype_id", "left")
            ));

        $orderProductsHtml = "<table border='0' style='width:100%; border-spacing: 0;margin-top: 20px; border-color:#60269E;'>";
        $orderProductsHtml = $orderProductsHtml . "<tr style='padding:10px;text-align:center;'>";
        $orderProductsHtml = $orderProductsHtml . "<th style='padding:8px 10px;'>Imagen</th>";

        $orderProductsHtml = $orderProductsHtml . "<th style='padding:8px 10px;'>Nombre</th>";
        $orderProductsHtml = $orderProductsHtml . "<th style='padding:8px 10px;'>Precio</th>";
        $orderProductsHtml = $orderProductsHtml . "<th style='padding:8px 10px;'>Cantidad  </th>";
        $orderProductsHtml = $orderProductsHtml . "<th style='padding:8px 10px;'>Descripción</th>";
        $orderProductsHtml = $orderProductsHtml . "<th style='padding:8px 10px;'>Detalle</th>";
        $orderProductsHtml = $orderProductsHtml . "<th style='padding:8px 10px;'>Supplies</th>";
        $orderProductsHtml = $orderProductsHtml . "</tr>";


        $subTotal = 0;
        $delivery = $order->delievery_price;

        $businessGroupName = "";
        $businessGroupLogo = "";
        $businessName = "";

        foreach ($orderProducts as $product) {

            $price = (float)$product->promotional_price;
            $quantity = (int)$product->quantity;
            $superadminEmail = $product->superadmin_email;
            $businessGroupName = $product->businesstype_name;
            $businessGroupLogo = $product->businesstype_logo;
            $businessName = $product->business_name;

            if (!$price) {
                $price = (float)$product->price;
            }
            $subTotal = $subTotal + $price * $quantity;
            $orderProductsHtml = $orderProductsHtml . "<tr style='padding:10px;text-align:center;'>";
            $orderProductsHtml = $orderProductsHtml . "<td><img src='https://snappypanel.com/adminpanel/" . $product->product_img . "' style='width:60px;height:60px;'></td>";

            $orderProductsHtml = $orderProductsHtml . "<td style='padding:8px 10px;'>" . $product->product_name . "</td>";

            $orderProductsHtml = $orderProductsHtml . "<td style='padding:8px 10px;'>" . $price . "</td>";
            $orderProductsHtml = $orderProductsHtml . "<td style='padding:8px 10px;'>" . $quantity . "</td>";
            $orderProductsHtml = $orderProductsHtml . "<td style='padding:8px 10px;'>" . $product->short_description . "</td>";
            $orderProductsHtml = $orderProductsHtml . "<td style='padding:8px 10px;'>" . $product->extra_detail . "</td>";


            $suppliesStr = $product->supplies;
            $suppliesTotal = 0;
            if (!empty($suppliesStr)) {
                $supplies = json_decode($suppliesStr);
                $orderProductsHtml = $orderProductsHtml . "<td><table>";
                foreach ($supplies as $supply) {
                    $orderProductsHtml = $orderProductsHtml . "<tr style='padding:5px;text-align:center;'>";
                    $orderProductsHtml = $orderProductsHtml . "<th style='padding:3px 3px;'>" . $supply->supplies_name . "</th>";
                    $orderProductsHtml = $orderProductsHtml . "<td style='padding:3px 3px;'>" . $supply->supplies_price . "</td>";
                    $orderProductsHtml = $orderProductsHtml . "</tr>";

                    $suppliesTotal = $suppliesTotal + (float)$supply->supplies_price;
                }
                $orderProductsHtml = $orderProductsHtml . "</table></td>";

            }
            $subTotal = $subTotal + $suppliesTotal * $quantity;
            $orderProductsHtml = $orderProductsHtml . "</tr>";
        }

        $total = $subTotal + $delivery;


        $orderProductsHtml = $orderProductsHtml . '</table>';

        if ($order) {
            $messageHtml = "
        <html>
            <head>    
                <meta name='viewport' content='width=device-width' />
                <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    
                <title>Snappy Delivery</title>
                 
                 <style>
                                  /* -------------------------------------
                                      GLOBAL RESETS
                                  ------------------------------------- */
                                  
                                  /*All the styling goes here*/
                                  
                                  img {
                                    border: none;
                                    -ms-interpolation-mode: bicubic;
                                    max-width: 100%; 
                                  }
                            
                                  body {
                                    background-color: #f6f6f6;
                                    font-family: sans-serif;
                                    -webkit-font-smoothing: antialiased;
                                    font-size: 14px;
                                    line-height: 1.4;
                                    margin: 0;
                                    padding: 0;
                                    -ms-text-size-adjust: 100%;
                                    -webkit-text-size-adjust: 100%; 
                                  }
                            
                                  table {
                                    border-collapse: separate;
                                    mso-table-lspace: 0pt;
                                    mso-table-rspace: 0pt;
                                    width: 100%; }
                                    table td {
                                      font-family: sans-serif;
                                      font-size: 14px;
                                      vertical-align: top; 
                                  }
                            
                                  /* -------------------------------------
                                      BODY & CONTAINER
                                  ------------------------------------- */
                            
                                  .body {
                                    background-color: #f6f6f6;
                                    width: 100%; 
                                  }
                            
                                  /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
                                  .container {
                                    display: block;
                                    margin: 0 auto !important;
                                    /* makes it centered */
                                    max-width: 580px;
                                    padding: 10px;
                                    width: 580px; 
                                  }
                            
                                  /* This should also be a block element, so that it will fill 100% of the .container */
                                  .content {
                                    box-sizing: border-box;
                                    display: block;
                                    margin: 0 auto;
                                    max-width: 580px;
                                    padding: 10px; 
                                  }
                            
                                  /* -------------------------------------
                                      HEADER, FOOTER, MAIN
                                  ------------------------------------- */
                                  .main {
                                    background: #ffffff;
                                    border-radius: 3px;
                                    width: 100%; 
                                  }
                            
                                  .wrapper {
                                    box-sizing: border-box;
                                    padding: 20px; 
                                  }
                            
                                  .content-block {
                                    padding-bottom: 10px;
                                    padding-top: 10px;
                                  }
                            
                                  .footer {
                                    clear: both;
                                    margin-top: 10px;
                                    text-align: center;
                                    width: 100%; 
                                  }
                                    .footer td,
                                    .footer p,
                                    .footer span,
                                    .footer a {
                                      color: #999999;
                                      font-size: 12px;
                                      text-align: center; 
                                  }
                            
                                  /* -------------------------------------
                                      TYPOGRAPHY
                                  ------------------------------------- */
                                  h1,
                                  h2,
                                  h3,
                                  h4 {
                                    color: #000000;
                                    font-family: sans-serif;
                                    font-weight: 400;
                                    line-height: 1.4;
                                    margin: 0;
                                    margin-bottom: 30px; 
                                  }
                            
                                  h1 {
                                    font-size: 35px;
                                    font-weight: 300;
                                    text-align: center;
                                    text-transform: capitalize; 
                                  }
                            
                                  p,
                                  ul,
                                  ol {
                                    font-family: sans-serif;
                                    font-size: 14px;
                                    font-weight: normal;
                                    margin: 0;
                                    margin-bottom: 15px; 
                                  }
                                    p li,
                                    ul li,
                                    ol li {
                                      list-style-position: inside;
                                      margin-left: 5px; 
                                  }
                            
                                
                            
                                  /* -------------------------------------
                                      BUTTONS
                                  ------------------------------------- */
                                  .btn {
                                    box-sizing: border-box;
                                    width: 100%; }
                                    .btn > tbody > tr > td {
                                      padding-bottom: 15px; }
                                    .btn table {
                                      width: auto; 
                                  }
                                    .btn table td {
                                      background-color: #ffffff;
                                      border-radius: 5px;
                                      text-align: center; 
                                  }
                                    .btn a {
                                      background-color: #ffffff;
                                      border: solid 1px #3498db;
                                      border-radius: 5px;
                                      box-sizing: border-box;
                                      color: #3498db;
                                      cursor: pointer;
                                      display: inline-block;
                                      font-size: 14px;
                                      font-weight: bold;
                                      margin: 0;
                                      padding: 12px 25px;
                                      text-decoration: none;
                                      text-transform: capitalize; 
                                  }
                            
                                  .btn-primary table td {
                                    background-color: #3498db; 
                                  }
                            
                                  .btn-primary a {
                                    background-color: #3498db;
                                    border-color: #3498db;
                                    color: #ffffff; 
                                  }
                            
                                  /* -------------------------------------
                                      OTHER STYLES THAT MIGHT BE USEFUL
                                  ------------------------------------- */
                                  .last {
                                    margin-bottom: 0; 
                                  }
                            
                                  .first {
                                    margin-top: 0; 
                                  }
                            
                                  .align-center {
                                    text-align: center; 
                                  }
                            
                                  .align-right {
                                    text-align: right; 
                                  }
                            
                                  .align-left {
                                    text-align: left; 
                                  }
                            
                                  .clear {
                                    clear: both; 
                                  }
                            
                                  .mt0 {
                                    margin-top: 0; 
                                  }
                            
                                  .mb0 {
                                    margin-bottom: 0; 
                                  }
                            
                                  .preheader {
                                    color: transparent;
                                    display: none;
                                    height: 0;
                                    max-height: 0;
                                    max-width: 0;
                                    opacity: 0;
                                    overflow: hidden;
                                    mso-hide: all;
                                    visibility: hidden;
                                    width: 0; 
                                  }
                            
                                  .powered-by a {
                                    text-decoration: none; 
                                  }
                            
                                  hr {
                                    border: 0;
                                    border-bottom: 1px solid #f6f6f6;
                                    margin: 20px 0; 
                                  }
                            
                                  /* -------------------------------------
                                      RESPONSIVE AND MOBILE FRIENDLY STYLES
                                  ------------------------------------- */
                                  @media only screen and (max-width: 620px) {
                                    table[class=body] h1 {
                                      font-size: 28px !important;
                                      margin-bottom: 10px !important; 
                                    }
                                    table[class=body] p,
                                    table[class=body] ul,
                                    table[class=body] ol,
                                    table[class=body] td,
                                    table[class=body] span,
                                    table[class=body] a {
                                      font-size: 16px !important; 
                                    }
                                    table[class=body] .wrapper,
                                    table[class=body] .article {
                                      padding: 10px !important; 
                                    }
                                    table[class=body] .content {
                                      padding: 0 !important; 
                                    }
                                    table[class=body] .container {
                                      padding: 0 !important;
                                      width: 100% !important; 
                                    }
                                    table[class=body] .main {
                                      border-left-width: 0 !important;
                                      border-radius: 0 !important;
                                      border-right-width: 0 !important; 
                                    }
                                    table[class=body] .btn table {
                                      width: 100% !important; 
                                    }
                                    table[class=body] .btn a {
                                      width: 100% !important; 
                                    }
                                    table[class=body] .img-responsive {
                                      height: auto !important;
                                      max-width: 100% !important;
                                      width: auto !important; 
                                    }
                                  }
                            
                                  /* -------------------------------------
                                      PRESERVE THESE STYLES IN THE HEAD
                                  ------------------------------------- */
                                  @media all {
                                    .ExternalClass {
                                      width: 100%; 
                                    }
                                    .ExternalClass,
                                    .ExternalClass p,
                                    .ExternalClass span,
                                    .ExternalClass font,
                                    .ExternalClass td,
                                    .ExternalClass div {
                                      line-height: 100%; 
                                    }
                                    .apple-link a {
                                      color: inherit !important;
                                      font-family: inherit !important;
                                      font-size: inherit !important;
                                      font-weight: inherit !important;
                                      line-height: inherit !important;
                                      text-decoration: none !important; 
                                    }
                                    #MessageViewBody a {
                                      color: inherit;
                                      text-decoration: none;
                                      font-size: inherit;
                                      font-family: inherit;
                                      font-weight: inherit;
                                      line-height: inherit;
                                    }
                                    .btn-primary table td:hover {
                                      background-color: #34495e !important; 
                                    }
                                    .btn-primary a:hover {
                                      background-color: #34495e !important;
                                      border-color: #34495e !important; 
                                    } 
                                  }
                    
                 </style>
             </head>
             <body class=''>
             <span class='preheader'>This is preheader text. Some clients will show this text as a preview.</span>
             <table role='presentation' border='0' cellpadding='0' cellspacing='0' class='body'>
              <tr>
                <td>&nbsp;</td>
                <td class='container'>
                  <div class='content'>
        
                    <!-- START CENTERED WHITE CONTAINER -->
                    <table role='presentation' class='main'>
        
                      <!-- START MAIN CONTENT AREA -->
                      <tr>
                        <td class='wrapper'>
                          <table role='presentation' border='0' cellpadding='0' cellspacing='0'>
                            <tr>
                              <td>
                                <img src='https://snappypanel.com/mailsnappy.png'>
                                <p>Muchas gracias por confiar en Snappy Delivery para atender tú pedido. Día con día nos esforzamos para llevar lo que más te gusta hasta donde te encuentres.</p>
                                <table role='presentation' border='0' cellpadding='0' cellspacing='0' class=''>
                                  <tbody>
                                    <tr>
                                      <td align='left'>
                                      
                                         
                                          
                                              <a href=''>
                                              <img src='https://snappypanel.com/Facebook100.png'>
                                              </a>
                                    
                                              <a href=''>
                                              <img src='https://snappypanel.com/Whatsapp100.png'>
                                              </a>
                                             
                                              <a href=''>
                                              <img src='https://snappypanel.com/Instagram100.png'>
                                              </a>
                                              
                                      
                                   
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                                <p>Puedes buscarnos en redes sociales, estaremos esperando tú visita.</p>
                                <p>Excelente día.</p>
                                
                                        <tr style='padding:10px;text-align:center;'>
                </tr>
                <tr>
                <td>
                <img src='https://snappypanel.com/adminpanel/" . $businessGroupLogo . "' style='width:60px;height:60px;'>
                <div><span style='font-size:18px;font-weight:bold;'>" . $businessGroupName . " -> " . "</span><span style='font-size:18px;'>" . $businessName . "</span></div>
                </td>
                </tr>
                <tr>
                <td>
                <table border='0' style='border-spacing: 0;width:100%; border-top:3px solid #ccc; border-color:#60269E;'>
                <tr style='padding:10px;text-align:center;'>
                <th style='padding:8px 10px;'>Número de orden</th>
                <th style='padding:8px 10px;'>Método de págo</th>
                <th style='padding:8px 10px;'>Notas</th>
                <th style='padding:8px 10px;'>Ordenó</th>
                 <table border='0' style='border-spacing: 0;width:100%; border-top:3px solid #ccc; border-color:#60269E;'>
                </tr>
                
                <tr style='text-align:center;'>
                <td style='padding:8px 10px;'>  " . $orderId . "  </td>
                <td style='padding:8px 10px;'> " . $order->paymentmethod . " </td>
                <td style='padding:8px 10px;'> " . $order->order_notes . "</td>
                <td style='padding:8px 10px;'> " . $order->orderedby_fname . " " . $order->orderedby_lname . "</td>
                </tr>
                </table>
                </td>
                </tr>
                <tr>
                <td>
                " . $orderProductsHtml . "
                </td>
                </tr>
                <tr>
                <td>
                <table border='0' style='border-spacing: 0;width:100%; border-top:3px solid #ccc;margin-top: 20px; border-color:#60269E;'>
                <tr>
                <th style='padding:8px 10px;'>Subtotal</th><td style='padding:8px 10px;'>" . $subTotal . "</td>
                </tr>
                <tr>
                <th style='padding:8px 10px;'>Envío</th><td style='padding:8px 10px;'>" . $delivery . "</td>
                </tr>
                <tr>
                <th style='padding:8px 10px;'>Total</th><td style='padding:8px 10px;'>" . $total . "</td>
                </tr>
                </table>
                </td>
                </tr>
                </table> 
                                
                                
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
        
                    <!-- END MAIN CONTENT AREA -->
                    </table>
                    <!-- END CENTERED WHITE CONTAINER -->
        
                    <!-- START FOOTER -->
                    <div class='footer'>
                      <table role='presentation' border='0' cellpadding='0' cellspacing='0'>
                        <tr>
                          <td class='content-block'>
                            <span class='apple-link'>Argentina</span>
                            <br> Correo informativo <a href='http://snappydelivery.com.ar/'>www.snappydelivery.com.ar</a>.
                          </td>
                        </tr>
                        <tr>
                          <td class='content-block powered-by'>
                            Powered by <a href='http://snappydelivery.com.ar/'>Snappy Delivery</a>.
                          </td>
                        </tr>
                      </table>
                    </div>
                    <!-- END FOOTER -->
        
                  </div>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>
          </body>
</html>

";

            $subject = "Order";

            $providerEmail = $order->email;
            $orderByEmail = $order->orderby_useremail;
            if (!empty($superadminEmail)) {
                $this->auth_model->sendEmail($superadminEmail, $subject, $messageHtml);
            }
            if ($this->auth_model->sendEmail($providerEmail, $subject, $messageHtml) && $this->auth_model->sendEmail($orderByEmail, $subject, $messageHtml)) {
                return true;
            }
            return false;
        }
        return true;


    }

}
