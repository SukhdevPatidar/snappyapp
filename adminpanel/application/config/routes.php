<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['sliders']                		        = 'api/api/getSliders'; // get
$route['appVersion']                		    = 'api/api/appVersion'; // get

$route['citiesList']                		    = 'api/cities/citiesList'; // get
$route['countries']                		        = 'api/api/getCountries'; // get

$route['addressAdd']                		    = 'api/addresses/addressAdd'; // post
$route['addressList']                           = 'api/addresses/addressList'; // get
$route['deleteAddress']                         = 'api/addresses/deleteAddress'; // get

$route['restaurantCategory']                    = 'api/categories/categoryList'; // get
$route['businessGroup']                         = 'api/categories/getBusinessGroups'; // get
$route['businessGroupSubcat']                   = 'api/categories/businessGroupSubcat'; // get
$route['productSubcat']                         = 'api/categories/productSubcat'; // get
$route['products']               	            = 'api/products/productList'; // get
$route['checkProductAvailability']              = 'api/products/checkProductAvailability'; // get

$route['login']               			        = 'api/auth/login'; // post
$route['signup']               			        = 'api/auth/signup'; // post
$route['updateProfile']               	        = 'api/auth/updateProfile'; // post
$route['updateUserCurrentLocation']             = 'api/auth/updateUserCurrentLocation'; // post
$route['updateProfilePhoto']                    = 'api/auth/updateProfilePhoto'; // post
$route['users']                                 = 'api/auth/users'; // post
$route['updateToken']                           = 'api/auth/updateToken'; // post
$route['resetPassword']                         = 'api/auth/resetPassword'; // post
$route['logoutUser']               			    = 'api/auth/logout'; // get
$route['sendEmail']               		        = 'api/auth/sendEmail'; // post

// order
$route['createOrder']               		    = 'api/ordersnew/createOrder'; // post
$route['updateOrder']               		    = 'api/ordersnew/updateOrder'; // post
$route['getOrders']               		        = 'api/ordersnew/getOrders'; // get
$route['providerPaymentInfo']				    = 'api/ordersnew/getPaymentInfoForProvider'; // get

$route['deliveryOrders']	                    = 'api/delivery/orders'; // get
$route['orderStatusUpdate']			            = 'api/delivery/orderStatusUpdate'; // post
$route['deliveryBoyStatusChange']	            = 'api/delivery/deliveryBoyStatusChange'; // post
$route['deliveryBoyStatus']			            = 'api/delivery/deliveryBoyStatus'; // post

$route['addFeedback']		   	                = 'api/feedback/addFeedback'; // post

$route['arPaymentRequest']		   	            = 'api/payment/arPaymentRequest'; // post
$route['payment/error']		   	                = 'api/payment/error'; // post
$route['payment/success']		   	            = 'api/payment/success'; // post

$route['postPayu']		   	                    = 'api/payment/postPayu'; // post
$route['payUReposne']		   	                = 'api/payment/payUReposne'; // post
$route['payUConfirmation']		   	            = 'api/payment/payUConfirmation'; // post



$route['default_controller']                    = "user";
//$route['default_controller'] = "welcome";
$route['404_override']                          = '';
$route['translate_uri_dashes']                  = FALSE;



/* End of file routes.php */
/* Location: ./application/config/routes.php */
