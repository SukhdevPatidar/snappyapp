var myLatLng = {
    lat: 24.931187,
    lng: -100.8887822
};
var map;
var geocoder;
var marker;
var placeSearchInput;
var infoWindowRef;
var addressCallback;
var deliveryBoyMarkers = [];

function initMap() {
    var mapElem = document.getElementById('locationMap');


    // Set location to map
    var lat = mapElem.getAttribute("latitude");
    var lng = mapElem.getAttribute("longitude");

    if (lat && lng) {
        lat = parseFloat(lat);
        lng = parseFloat(lng);
        myLatLng.lat = lat;
        myLatLng.lng = lng;
        reverseGeocode(lat, lng);
    }

    map = new google.maps.Map(mapElem, {
        center: myLatLng,
        zoom: 8,
        panControl: true,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        overviewMapControl: false,
        rotateControl: false,
        fullscreenControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    // Create the search box and link it to the UI element.
    placeSearchInput = document.createElement("input");
    placeSearchInput.style.position = "absolute";
    placeSearchInput.style.left = "8px";
    placeSearchInput.style.top = "8px";
    placeSearchInput.style.width = "300px";
    placeSearchInput.style.height = "40px";
    placeSearchInput.style.padding = "5px";
    placeSearchInput.placeholder = "Search Location";
    mapElem.appendChild(placeSearchInput);
    var autocomplete = new google.maps.places.Autocomplete(placeSearchInput);

    autocomplete.bindTo('bounds', this.map);

    autocomplete.addListener('place_changed', (event) => {
        var places = autocomplete.getPlace();
        if (!places.geometry) {
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (places.geometry.viewport) {
            this.map.fitBounds(places.geometry.viewport);
        } else {
            this.map.setCenter(places.geometry.location);
            this.map.setZoom(8); // Why 17? Because it looks good.
        }


        marker.setPosition(places.geometry.location);
        fireCallback(places.geometry.location.lat(), places.geometry.location.lng(), places.formatted_address);
        /* window.alert("Address: '" + place.formatted_address + "'");*/
    });

    // adMarker();
    google.maps.event.addListener(map, 'click', function (event) {
        marker.setPosition({
            lat: event.latLng.lat(),
            lng: event.latLng.lng()
        });
        reverseGeocode(event.latLng.lat(), event.latLng.lng());
    });
}

function fireCallback(lat, lng, address = "") {
    if (addressCallback) {
        addressCallback(lat, lng, address);
    }
}


function adMarker() {
    marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        draggable: true,
        title: ''
    });
    google.maps.event.addListener(marker, 'dragend', function (event) {
        reverseGeocode(event.latLng.lat(), event.latLng.lng());
    });
}

function animatedMove(marker, t, current, moveto) {
    var lat = current.lat();
    var lng = current.lng();

    var deltalat = (moveto.lat - current.lat()) / 100;
    var deltalng = (moveto.lng - current.lng()) / 100;

    var delay = 10 * t;
    for (var i = 0; i < 100; i++) {
        (function (ind) {
            setTimeout(
                function () {
                    var lat = marker.position.lat();
                    var lng = marker.position.lng();
                    lat += deltalat;
                    lng += deltalng;
                    latlng = new google.maps.LatLng(lat, lng);
                    marker.setPosition(latlng);
                }, delay * ind
            );
        })(i)
    }
}

function addDeliveryBoysToMap(boylist, baseUrl, orderId, needBound = false) {
    var bounds = new google.maps.LatLngBounds();

    var markerCount = 0;

    for (var i = 0; i < boylist.length; i++) {
        var boy = boylist[i];
        if (boy.latitude && boy.longitude) {
            const latLng = {
                lat: parseFloat(boy.latitude),
                lng: parseFloat(boy.longitude)
            };

            if (deliveryBoyMarkers[boy.id]) {
                animatedMove(deliveryBoyMarkers[boy.id], .5, deliveryBoyMarkers[boy.id].position, latLng);
            } else {
                var markerTitle = boy.name + " " + boy.l_name;
                var markerColor = (boy.is_pending_order == 1) ? 'red' : 'green';
                var infoWindowString = '<div id="content"><h3>' + markerTitle + '</h3>';
                if (boy.is_pending_order == 1) {
                    infoWindowString = infoWindowString + "<p style='color:red'>Delivey boy is busy with another order</p>";
                } else {
                    infoWindowString = infoWindowString + '<button style="background-color: #0b93d5;color:#fff" type="button" value="Assign Order" onclick="assignDeliveryBoy(\'' + baseUrl + '\',' + orderId + ',' + boy.id + ')">Assign Order</button>';
                }

                const infowindow = new google.maps.InfoWindow({
                    content: infoWindowString ? infoWindowString : "<div><h3>" + markerTitle + "</h3></h3></div>",
                    maxWidth: 200
                });

                const myMarker = new google.maps.Marker({
                    position: latLng,
                    map: this.map,
                    draggable: false,
                    title: markerTitle,
                    icon: 'http://maps.google.com/mapfiles/ms/icons/' + markerColor + '-dot.png'
                });
                google.maps.event.addListener(myMarker, 'click', function (event) {
                    closeOtherInfo();
                    infowindow.open(myMarker.get('map'), myMarker);
                    infoWindowRef = infowindow;
                });
                deliveryBoyMarkers[boy.id] = myMarker;
            }
            bounds.extend(latLng);
            markerCount++;
        }
    }
    if (markerCount > 0 && needBound) {
        this.map.fitBounds(bounds);
    }
}

function closeOtherInfo() {
    if (infoWindowRef) {
        infoWindowRef.set("marker", null);
        infoWindowRef.close();
    }
}

function reverseGeocode(Latitude, Longitude) {
    geocoder = new google.maps.Geocoder;
    var latlng = {
        lat: Latitude,
        lng: Longitude
    };
    geocoder.geocode({
        'location': latlng
    }, (results, status) => {
        if (status === 'OK') {
            if (results[0]) {
                placeSearchInput.value = results[0].formatted_address;
                fireCallback(Latitude, Longitude, results[0].formatted_address);
            } else {
                console.log('No results found');
            }
        } else {
            console.log('Geocoder failed due to: ' + status);
        }
    });
}


function assignDeliveryBoy(baseUrl, orderId, boyId) {
    $.ajax({
        type: "POST",
        url: baseUrl + "users/order/insert_delivery_boy",
        data: {'orderId': orderId, 'boy_id': boyId},
        success: function (data) {
            var response = JSON.parse(data);

            if (response.status) {
                window.location.href = baseUrl + 'users/order/add_order';
            } else {
                alert(response.message);
            }
        }
    });
}
