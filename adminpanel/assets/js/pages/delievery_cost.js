$(document).ready(function () {
    getDeliveryCost();
});


window.refresh = function () {
    getDeliveryCost();
}

function addDeliveryCost() {
    var fd = new FormData(document.getElementById("deliverycost"));
    var type = fd.get("addUpdateType");
    if(type == "update") {
       $.ajax({
        url: localStorage.getItem("base_url") + "backend/deliverycost/updateDeliveryCost",
        type: "POST",
        data: $("#deliverycost").serialize(),
        success: function (res) {
            resetFields();
            getDeliveryCost();
        },
        error: function (f) {
            console.log("Error "+JSON.stringify(f));
        }
    }).done(function (data) { });
   } else {
     $.ajax({
        url: localStorage.getItem("base_url") + "backend/deliverycost/addDeliveryCost",
        type: "POST",
        data: $("#deliverycost").serialize(),
        success: function (res) {
            resetFields();
            getDeliveryCost();
        },
        error: function (f) {
            console.log("Error "+JSON.stringify(f));
        }
    }).done(function (data) {
    });
}

return false;
}

function getDeliveryCost() {

    $("#delieveryCostTable").empty();
    $.ajax({
        url: localStorage.getItem("base_url") + "backend/deliverycost/getDelieveryCost",
        data: '',
        dataType: 'json',
        success: function (res) {
            appendDeliveryCost(res);
        },
        error: function (q) {
            alert("Something went wrong!! Try again.");
        }
    });
}


function appendDeliveryCost(response) {
    if (typeof response != "object") {
        response = JSON.parse(response);
    }

    for (var i = 0; i < response.length; i++) {
        var data = response[i];

            // + data.id +
            var row = '<tr  id="tr_' + data.id + '" data=\'' + JSON.stringify(data) + '\'  >\n' +
            '<td>' + (i + 1) + '</td>\n' +
            '<td>' + data.cost + '</td>\n' +
            '<td>' + ((data.status==1)?"Active":"Deactive") + '</td>\n' +
            '<td>\n' +
            '<div class="btn-group">\n' +
            '<button type="button" class="btn btn-light edit_' + data.id + '" id="' + data.id + '"  >Edit</button>\n' +
            '<button type="button" class="btn btn-light delete_' + data.id + '" id="' + data.id + '" >Delete</button>\n' +
            '</div>\n' +
            '</td>\n' +
            '</tr>';
            $("#delieveryCostTable").append(row);

            $(".edit_" + data.id).click(function () {
                var data = $('#tr_' + this.id).attr("data");
                updateCost(data);
            });

            $(".delete_" + data.id).click(function () {
                var data = $('#tr_' + this.id).attr("data");
                deleteCost(data);
            });
        }
    }

    function resetFields() {
        $("#addUpdateType").val("");
        $("#cost").val("");
        $("#delievery_id").val("");
        $("#active_status").removeAttr('checked');
    }

    function updateCost(data) {
        if (typeof data != 'object') {
            data = JSON.parse(data);
        }
        $("#delievery_id").val(data.id);
        $("#addUpdateType").val("update");
        $("#cost").val(data.cost);
        if(data.status == 1) {
            $("#active_status").attr ( "checked" ,"checked" );
        } else {
            $("#active_status").removeAttr('checked');
        }
    }

    function deleteCost(data) {
        if (typeof data != 'object') {
            data = JSON.parse(data);
        }

        var userselection = confirm("Are you sure you want to delete this Cost permanently?");
        if (userselection == true){
            if (data.length != 0) {
                $.ajax({
                    url: localStorage.getItem("base_url") +  "backend/deliverycost/deleteDeliveryCost?id="+data.id,
                    dataType: "json",
                    data:  "",
                    success: function(xi) {
                        debugger;
                        var row = $("#tr_"+data.id);
                        row.remove();
                    },
                    error: function (xi) {
                        alert("Something went wrong!! Try again.");
                    }
                });
            }

        }
    }


