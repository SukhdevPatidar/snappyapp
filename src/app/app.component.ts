import {Component, ViewChild} from '@angular/core';
import {Nav, Platform, AlertController, ActionSheetController, Events} from 'ionic-angular';
import {Camera, Splashscreen, StatusBar} from 'ionic-native';

import {Category} from '../pages/category/category';
import {Orders} from '../pages/orders/orders';
import {FeedBack} from '../pages/feed-back/feed-back';
import {Login} from '../pages/login/login';
import {Profile} from '../pages/profile/profile';
import {Address} from '../pages/address/address';
import {Wallet} from '../pages/wallet/wallet';
import {Settings} from '../pages/settings/settings';
import {CountrySelectionPage} from '../pages/country-selection/country-selection';


import {Storage} from '../providers/storage';
import {DataStore} from '../providers/data-store';
import {ApiService} from '../providers/api-service';

import {TranslateService} from 'ng2-translate';

import {Firebase} from '@ionic-native/firebase';
import {MainCategoryPagePage} from '../pages/main-category/main-category';

import {AppVersion} from '@ionic-native/app-version';
import {FirebaseProvider} from "../providers/firebase/firebase";
import {InboxPage} from "../pages/inbox/inbox";

const MessageMenuItem = {
    title: 'MESSAGES',
    component: InboxPage,
    icon: './assets/img/ico/perfil.png',
    visibleIfLogin: true
};

@Component({
    templateUrl: 'app.html',
    providers: [ApiService]
})

export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = CountrySelectionPage;
    // rootPage: any = MainCategoryPagePage;

    pages: Object;
    username = '';
    userImg = "";
    base64Img = "";

    private storage: Storage;
    private dataStore: DataStore;

    waitForAlertResponse = false;

    selectedMenuItem = 'Home';

    constructor(public platform: Platform,
                public alertCtrl: AlertController,
                public apiService: ApiService,
                public firebaseProvider: FirebaseProvider,
                public actionSheetCtrl: ActionSheetController,
                public events: Events,
                public translate: TranslateService,
                private firebase: Firebase,
                private appVersion: AppVersion
    ) {

        this.storage = Storage.getInstance();
        this.dataStore = DataStore.getInstance();

        translate.setDefaultLang(this.storage.getLanguage());

        if (this.storage.getCountryId()) {
            // this.rootPage = Category;
            this.rootPage = MainCategoryPagePage;
        }

        platform.ready().then(() => {
            if (platform.is('ios')) {
                StatusBar.hide();
            }

            this.initializeApp();

            this.hideSplashScreen();

            this.firebaseProvider.initializeFirebase();


            // this.pages = {
            //   'Book':[
            //   { title: 'Home', component: Category, icon: 'ios-home-outline' },
            //   // { title: 'ChooseTable', component: ChooseTable, icon: 'help-buoy' }
            //   ],
            //   'Orders':[
            //   { title: 'Orders', component: Orders, icon: 'ios-cloud-upload-outline' }
            //   ],
            //   'Account': [
            //   { title: 'Profile', component: Profile, icon: 'ios-people-outline', loginNeed: true },
            //   { title: 'Wallet', component: Wallet, icon: 'ios-briefcase-outline', visibleIfLogin: true },
            //   { title: 'Address', component: Address, icon: 'ios-map-outline', loginNeed: true }
            //   // { title: 'Notifications', component: Notifications, icon: 'ios-pulse-outline', loginNeed: true }
            //   ],
            //   'Others':[
            //   { title: 'Feedback', component: FeedBack, icon: 'ios-eye-outline', loginNeed: true },
            //   { title: 'Logout', component: Category , icon:'ios-log-out-outline', visibleIfLogin: true }
            //   ]
            // };

            this.pages = {
                'Book': [
                    {title: 'HOME', component: MainCategoryPagePage, icon: './assets/img/ico/inicio.png'},
                    {title: 'ORDERS', component: Orders, icon: './assets/img/ico/ordenes.png'},
                    {title: 'PROFILE', component: Profile, icon: './assets/img/ico/perfil.png', loginNeed: true},
                    MessageMenuItem,
                    {title: 'WALLET', component: Wallet, icon: './assets/img/ico/direccion.png', visibleIfLogin: true},
                    {title: 'ADDRESS', component: Address, icon: './assets/img/ico/inicio.png', loginNeed: true},
                    {title: 'FEEDBACK', component: FeedBack, icon: './assets/img/ico/config.png', loginNeed: true},
                    {title: 'SETTINGS', component: Settings, icon: './assets/img/ico/retro.png'},
                    {
                        title: 'LOGOUT',
                        component: MainCategoryPagePage,
                        icon: './assets/img/ico/inicio.png',
                        visibleIfLogin: true
                    }
                ]
            };
            events.subscribe(this.dataStore.eventsKey().LOGIN_SUCCESS, (userEventData) => {
                if (userEventData) {
                    let user = this.storage.getUser();
                    this.userImg = user['profile_photo'] ? user['profile_photo'] : "";
                } else {
                    this.userImg = '';
                }
            });
            this.setupFirebase();
        });
    }


    setupFirebase = () => {

        if (!DataStore.getInstance().isDevice) {
            return;
        }
        if (this.platform.is('ios')) {
            this.firebase.hasPermission().then(({isEnabled}) => {
                if (!isEnabled) {
                    this.firebase.grantPermission();
                }
            });
        }

        this.firebase.setBadgeNumber(0);

        this.firebase.getToken()
            .then(token => {
                console.log(`The token is ${token}`);
                this.updateFirebaseToken(token);
            }) // save the token server-side and use it to push notifications to this device
            .catch(error => {
                console.error('Error getting token', error)
            });

        this.firebase.onTokenRefresh()
            .subscribe((token: string) => {
                this.updateFirebaseToken(token);
                console.log(`Got a new token ${token}`)
            });

        try {
            this.firebase.onNotificationOpen().subscribe((notification) => {
                if (!notification.tap) {
                    this.apiService.showDirectAlert('Tienes un nuevo mensaje en el chat, da clic en OK para visualizarlo.', () => {
                        if (notification.action == 'order_assigned') {
                            this.events.publish("REFRESH_ORDER");
                        }
                        this.openPage(MessageMenuItem);
                    });
                } else {
                    this.apiService.showDirectAlert('Tienes un nuevo mensaje en el chat, da clic en OK para visualizarlo.', () => {
                        if (notification.action == 'order_assigned') {
                            this.events.publish("REFRESH_ORDER");
                        }
                        this.openPage(MessageMenuItem);
                    });
                }
            });
        } catch (e) {

        }
    }

    updateFirebaseToken(token) {
        if (token) {
            localStorage.setItem('firebase_token', token);
            if (this.storage.getUser()) {
                this.apiService.updateToken(this.storage.getUser().id, token).then((data) => {
                });
            }
        }
    }


    hideSplashScreen() {
        if (Splashscreen) {
            setTimeout(() => {
                Splashscreen.hide();
            }, 1000);
        }
    }

    initializeApp() {
        this.registerBackButtonListener();
        let user = this.storage.getUser();
        this.userImg = user['profile_photo'] ? user['profile_photo'] : "";


        //Check app version number
        this.appVersion.getVersionNumber().then((version) => {
            const platformName = this.platform.is('android') ? 'android' : 'ios';

            this.apiService.appVersion(platformName, version).then((response: any) => {
                if (response.status) {
                    const data = response.data;
                    if (parseInt(data.updateRequired) == 1) {
                        let alert = this.alertCtrl.create({
                            title: data.alert_title,
                            subTitle: data.alert_subtitle,
                            enableBackdropDismiss: false,
                            buttons: [{
                                text: data.btn_title,
                                role: 'ok',
                                handler: () => {
                                    const updateLink = data.redirect_link;
                                    window.open(updateLink, '_system', 'location=yes');
                                    return false;
                                }
                            }]
                        });
                        alert.present();
                    }
                }
            }).catch((error) => {

            });
        }).catch((error) => {

        });
    }

    registerBackButtonListener() {
        var ref = this;
        this.platform.registerBackButtonAction(function () {
            var nav = ref.nav;
            if (nav.canGoBack()) {
                if (!DataStore.getInstance().isLoadingShow && nav.getActive().component.name != "ThankYou") {
                    nav.pop();
                }
            } else {
                ref.confirmExitApp();
            }
        }, 500);
    }

    confirmExitApp() {
        let ref = this;

        if (!ref.waitForAlertResponse) {

            this.translate.get(['CONFIRM EXIT', 'REALLY EXIT APP?', 'CANCEL', 'EXIT']).subscribe((res: any) => {

                let confirm = this.alertCtrl.create({
                    title: res['CONFIRM EXIT'],
                    message: res['REALLY EXIT APP?'],
                    buttons: [
                        {
                            text: res['CANCEL'],
                            handler: () => {
                                console.log('Disagree clicked');
                                ref.waitForAlertResponse = false;
                            }
                        },
                        {
                            text: res['EXIT'],
                            handler: () => {
                                ref.waitForAlertResponse = false;
                                this.platform.exitApp();
                            }
                        }
                    ]
                });

                ref.waitForAlertResponse = true;
                confirm.present();
            });
        }
    }

    editProfilePic() {
        if (!this.storage.isLoggedIn()) {
            return;
        }

        this.translate.get(['CHOOSE OPTION', 'CAMERA', 'PHOTO LIBRARY', 'CANCEL']).subscribe((res: any) => {

            let actionSheet = this.actionSheetCtrl.create({
                title: res['CHOOSE OPTION'],
                buttons: [
                    {
                        text: res['CAMERA'],
                        handler: () => {
                            this.pickPhoto(Camera.PictureSourceType.CAMERA);
                            console.log('Camera clicked');
                        }
                    }, {
                        text: res['PHOTO LIBRARY'],
                        handler: () => {
                            this.pickPhoto(Camera.PictureSourceType.PHOTOLIBRARY);
                            console.log('PHOTO LIBRARY clicked');
                        }
                    }, {
                        text: res['CANCEL'],
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    }
                ]
            });
            actionSheet.present();
        });


    }


    pickPhoto(sourceType) {
        Camera.getPicture({
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: sourceType
        }).then((imageData) => {
            this.base64Img = 'data:image/jpeg;base64,' + imageData;
            this.savePictureToApi(imageData);
        }, (err) => {
            console.log("Get Picture Error " + JSON.stringify(err));
        });
    }

    savePictureToApi(base64Image) {
        this.apiService.showLoading();
        this.apiService.updateProfilePhoto(this.storage.getUser()['id'], base64Image).then(data => {
            this.apiService.hideLoading();
            console.log("Image Update Response " + JSON.stringify(data));
            this.apiService.hideLoading();
            if (data['status']) {
                this.apiService.showToast(data['message']);

                this.storage.setUser(data['data']);

                let user = this.storage.getUser();
                this.userImg = user['profile_photo'] ? user['profile_photo'] : "";

            } else {
                this.apiService.showAlert(data['message']);
            }
        });
    }

    getImgPath() {
        if (this.storage.isLoggedIn()) {
            let user = this.storage.getUser();
            this.userImg = user['profile_photo'] ? user['profile_photo'] : "";

            if (this.base64Img.length > 0) {
                return this.base64Img;
            } else if (this.userImg.length > 0) {
                return this.apiService.getBaseApiUrl() + this.userImg;
            }
        }
        return './assets/img/user-icon.png';
    }

    getBackgruondForBgImg() {
        if (this.storage.isLoggedIn()) {
            if (this.userImg.length > 0) {
                return 'url(' + this.getImgPath() + ')';
            }
        }
        return 'none';
    }

    openLoginPage() {
        this.nav.setRoot(Login);
    }


    logoutFromApi = () => {
        this.apiService.showLoading();
        this.apiService.logoutUser(this.storage.getUser().id).then(data => {
                this.apiService.hideLoading();
                this.logoutSuccess();
            },
            (error) => {
                this.apiService.hideLoading();
                this.logoutSuccess();
                console.log("Login Error " + JSON.stringify(error));
            });
    }

    logoutSuccess = () => {
        this.storage.removeLoggedIn();
        this.storage.removeLoginDetails();
        this.storage.removeUser();
        this.dataStore.emptyAddresses();
    }

    openPage(page) {
        this.selectedMenuItem = page.title;

        let pageCmp = page.component;
        let navParams = {};

        if (page.title == 'LOGOUT') {
            this.logoutFromApi();
        } else if (page.loginNeed) {
            if (!this.storage.isLoggedIn()) {
                pageCmp = Login;
            }

            navParams['PageType'] = page.title;
            navParams['PageComponents'] = page.component;
        }

        this.nav.setRoot(pageCmp, navParams);
    }

    isVisibleIfLogin(item) {
        if (item.visibleIfLogin) {
            if (item.title == 'WALLET' && this.storage.getUser().type != 'Wallet') {
                return false;
            }
            return this.storage.isLoggedIn();
        }
        return true;
    }
}
