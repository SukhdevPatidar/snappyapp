import {NgModule} from '@angular/core';
import {IonicApp, IonicModule, Platform, Events} from 'ionic-angular';
import {Http} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {Geolocation} from '@ionic-native/geolocation';

import {MyApp} from './app.component';

import {Category} from '../pages/category/category';
import {Items} from '../pages/items/items';
import {ItemDetails} from '../pages/itemdetails/itemdetails';
import {CartView} from '../pages/cartview/cartview';
import {Checkout} from '../pages/checkout/checkout';
import {Login} from '../pages/login/login';
import {ChooseTable} from '../pages/choose-table/choose-table';
import {ConfirmOrder} from '../pages/confirm-order/confirm-order';
import {Orders} from '../pages/orders/orders';
import {History} from '../pages/history/history';
import {FeedBack} from '../pages/feed-back/feed-back';
import {Profile} from '../pages/profile/profile';
import {Notifications} from '../pages/notifications/notifications';
import {Address} from '../pages/address/address';
import {AddressInfo} from '../pages/address-info/address-info';
import {ThankYou} from '../pages/thank-you/thank-you';
import {PaymentOptions} from '../pages/paymentoptions/paymentoptions';
import {Wallet} from '../pages/wallet/wallet';
import {WalletTransactions} from '../pages/wallet-transactions/wallet-transactions';
import {PaymentError} from '../pages/payment-error/payment-error';
import {Settings} from '../pages/settings/settings';
import {ForgotPasswordPage} from '../pages/forgot-password/forgot-password';
import {CountrySelectionPage} from '../pages/country-selection/country-selection';
import {DetailBoxModalPage} from '../pages/detail-box-modal/detail-box-modal';
import {MainCategoryPagePage} from '../pages/main-category/main-category';
import {BusinessListPagePage} from '../pages/business-list/business-list';
import {BusinessSubcatPage} from '../pages/business-subcat/business-subcat';
import {ProductsSubcatPage} from '../pages/products-subcat/products-subcat';
import {CallNumber} from '@ionic-native/call-number';

import {Stepper} from '../components/stepper/stepper';
import {AppHeader} from '../components/app-header/app-header';

import {NamePipe} from '../pipes/name-pipe';
import {KeysPipe} from '../pipes/keys-pipe';
import {PipesModule} from '../pipes/pipes.module';

import {DataStore} from '../providers/data-store'

// import {LoadingAnimateModule, LoadingAnimateService} from 'ng2-loading-animate';
import {TranslateModule, TranslateLoader, TranslateStaticLoader} from 'ng2-translate/ng2-translate';

import {Firebase} from '@ionic-native/firebase';
import {AppVersion} from '@ionic-native/app-version';
import {FirebaseProvider} from '../providers/firebase/firebase';
import {ApiService} from "../providers/api-service";
import {InboxPage} from "../pages/inbox/inbox";
import {ChatDetailPage} from "../pages/chat-detail/chat-detail";

@NgModule({
    declarations: [
        MyApp,
        // patchViaCapturingAllTheEvents()
        Category,
        Items,
        ItemDetails,
        CartView,
        Checkout,
        Login,
        ChooseTable,
        ConfirmOrder,
        Orders,
        History,
        FeedBack,
        Profile,
        Notifications,
        Address,
        AddressInfo,
        ThankYou,
        PaymentOptions,
        Wallet,
        WalletTransactions,
        PaymentError,
        Settings,
        ForgotPasswordPage,
        CountrySelectionPage,
        DetailBoxModalPage,
        MainCategoryPagePage,
        BusinessListPagePage,
        BusinessSubcatPage,
        ProductsSubcatPage,
        InboxPage,
        ChatDetailPage,

        // Custom Components
        Stepper,
        AppHeader,

        //Pipes
        NamePipe,
        KeysPipe
    ],

    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        PipesModule,
        IonicModule.forRoot(MyApp),
        // LoadingAnimateModule.forRoot(),
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: (createTranslateLoader),
            deps: [Http]
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,

        Category,
        Items,
        ItemDetails,
        CartView,
        Checkout,
        Login,
        ChooseTable,
        Orders,
        History,
        ConfirmOrder,
        FeedBack,
        Profile,
        Notifications,
        Address,
        AddressInfo,
        ThankYou,
        PaymentOptions,
        Wallet,
        WalletTransactions,
        PaymentError,
        Settings,
        ForgotPasswordPage,
        CountrySelectionPage,
        DetailBoxModalPage,
        MainCategoryPagePage,
        BusinessListPagePage,
        BusinessSubcatPage,
        ProductsSubcatPage,
        InboxPage,
        ChatDetailPage
    ],
    providers: [
        // LoadingAnimateService,
        Geolocation,
        Firebase,
        AppVersion,
        CallNumber,
        FirebaseProvider,
        ApiService
    ]
})
export class AppModule {
    dataStore: DataStore;

    constructor(private platform: Platform, public events: Events) {
        platform.ready().then(() => {
            this.dataStore = DataStore.getInstance();
            if (this.platform.is('core') || this.platform.is('mobileweb')) {
                this.dataStore.isDevice = false;
            } else {
                this.dataStore.isDevice = true;
            }
            this.dataStore.setEvents(events);
            this.dataStore.dbSetUp();
        });

    }
}

export function createTranslateLoader(http: Http) {
    return new TranslateStaticLoader(http, 'assets/i18n', '.json');
}
