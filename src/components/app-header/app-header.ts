import { Component } from '@angular/core';
import { CartView } from '../../pages/cartview/cartview';
import { DataStore } from '../../providers/data-store';
import { NavController , Events} from 'ionic-angular';

/*
  Generated class for the AppHeader component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
    */
  @Component({
    selector: 'app-header',
    templateUrl: 'app-header.html',
    inputs: ['title','showCart', 'hideBackButton']
  })
  export class AppHeader {

    public title = "";
    public showCart = true;
    public hideBackButton = false;
    dataStore:DataStore;
    public itemsCount:number = 0;

    constructor(public navCtrl: NavController,  public events: Events) {
      this.dataStore = DataStore.getInstance();

      console.log('Hello AppHeader Component'+this.dataStore.itemsCount);
      this.title = 'Restro App';

      this.itemsCount = this.dataStore.itemsCount;

      events.subscribe(this.dataStore.eventsKey().CART_MODIFY, (userEventData) => {
        this.itemsCount = userEventData;
      });
      
    }

    checkForCartItems() {

      console.log('Check For Items');
      
      this.dataStore.getItemsFromCart().then((response) => {
        if(response['success']) {
          let items = response['data'];
          if(items.length > 0) {
            this.navCtrl.push(CartView);
          } 
        }
      },
      (error) => {
        console.log("Error geting item "+JSON.stringify(error));
      });
    }

    getCountForItems() {
      console.log('Check For Items');
      
      this.dataStore.getItemsFromCart().then((response) => {
        if(response['success']) {
          let items = response['data'];
          this.itemsCount = items.length;
        }
      },
      (error) => {
        this.itemsCount = 0;
        console.log("Error geting item "+JSON.stringify(error));
      });
    }
    ngOnDestroy() {
      
    }
  }
