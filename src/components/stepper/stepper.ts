import { Component , EventEmitter } from '@angular/core';

/*
  Generated class for the Stepper component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'stepper',
  templateUrl: 'stepper.html',
  inputs: ['title','counterValue:stepperValue'],
  outputs: ['onChange']
})
export class Stepper {
  // @Input('init') counterValue = 0;
  public counterValue = 0;
  public title = "";
  onChange: EventEmitter<number>;

  constructor() {
    console.log('Hello Stepper Component');
    this.onChange = new EventEmitter<number>();
  }

  increment() {
    this.counterValue++;
    this.onChange.emit(this.counterValue);
  }
  decrement() {
    if(this.counterValue > 1) {
    this.counterValue--;
    this.onChange.emit(this.counterValue);
    }
  }
}
