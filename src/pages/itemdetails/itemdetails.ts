import {Component} from '@angular/core';

import {NavController, NavParams, ModalController, Events} from 'ionic-angular';
import {ApiService} from '../../providers/api-service';
import {DataStore} from '../../providers/data-store';
import {Storage} from '../../providers/storage';

import {Login} from '../login/login';
import {Category} from '../category/category';
import {CartView} from '../cartview/cartview';
import {DetailBoxModalPage} from '../detail-box-modal/detail-box-modal';
import {Firebase} from "@ionic-native/firebase";
import {MainCategoryPagePage} from "../main-category/main-category";

@Component({
    selector: 'page-itemdetails',
    templateUrl: 'itemdetails.html',
    providers: [ApiService, DataStore]
})
export class ItemDetails {
    item: any;
    dataStore: DataStore;
    storage: Storage;
    qty: any;

    selectedItem: any;

    itemExtraDetail = ""; //when add to cart

    constructor(
        public modalController: ModalController,
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiService: ApiService,
        private events: Events,
        private firebase: Firebase
    ) {
        this.qty = 1;
        this.dataStore = DataStore.getInstance();
        this.storage = Storage.getInstance();

        this.item = navParams.get('item');


        // this.getItemDetails();
    }


    ionViewDidLoad() {
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "ItemDetails"});

    }

    toList() {
        this.navCtrl.pop();
    }

    toCart() {
        if (!this.storage.isLoggedIn()) {
            var navParams = {};
            var pageCmp = Login;
            navParams['CallBack'] = (status) => {
                console.log("CallBack " + status);
                this.checkProductAvailability();
            };
            this.navCtrl.push(pageCmp, navParams);
            return
        } else {
            this.addToCart();
        }
    }


    checkProductAvailability = () => {
        let productId = this.item.id;
        let userCityId = this.storage.getUser()['city_id'];

        this.apiService.showLoading();
        this.apiService.checkProductAvailability(productId, userCityId).then((response) => {
                this.apiService.hideLoading();
                if (response['status']) {
                    this.item['delievery_price'] = response['data'].delievery_price;
                    this.addToCart();
                    this.events.publish("REFRESH_PRODUCTS_LIST", true);
                } else {
                    this.navCtrl.setRoot(MainCategoryPagePage);
                    this.apiService.showAlert("THIS PRODUCT DELIEVERY IS NOT AVAILABLE IN YOUR CITY");
                }
            },
            (error) => {
                this.apiService.hideLoading();
            });
    }

    addToCart = () => {
        if (this.item.supplies.filter(data => data.selected).length > parseInt(this.item.max_supplies_select)) {
            this.apiService.showToast("Puedes seleccionar un máximo de " + this.item.max_supplies_select + " suministros");
        } else {
            var currentDate = new Date();
            var currentDateStr = currentDate.toString();
            this.item["extra_detail"] = this.itemExtraDetail;

            var item = JSON.parse(JSON.stringify(this.item));
            this.checkIfDifferentCategoryItemAdded(item, (isDifferentCat) => {
                if (isDifferentCat) {
                    this.apiService.showAlertWithAccept("CART_SAME_CATEGORY_MSG", "ACCEPT", (status) => {
                        console.log("STTAUS " + status);
                        if (status) {
                            this.navCtrl.push(CartView);
                        }
                    });
                } else {

                    if (item.promotional_price) {
                        item.price = item.promotional_price;
                    }

                    this.dataStore.addItemToCart(item, this.qty, currentDateStr, item.category_name, item.category_id).then(data => {
                            this.apiService.showToast("ITEM SUCCESSFULLY ADDED");
                            this.toList();
                        },
                        (error) => {
                            console.log("Error adding item " + JSON.stringify(error));
                        });
                }
            })
        }
    }


    checkIfDifferentCategoryItemAdded = (item, callBack) => {
        this.dataStore.getItemsFromCart().then((response) => {
                var isDifferentCat = false;
                if (response['success']) {
                    var items = response['data'];
                    for (var i = 0; i < items.length; ++i) {
                        var product = items[i];
                        if (product.categoryId != item.category_id) {
                            isDifferentCat = true;
                            break;
                        }
                    }
                }

                if (isDifferentCat) {
                    callBack(true);
                } else {
                    callBack(false);
                }
            },
            (error) => {
                console.log("Error geting item " + JSON.stringify(error));
            });
    }


    showDetailInputBox() {
        const modal = this.modalController.create(DetailBoxModalPage, {
            callBack: (text) => {
                console.log("Detail Text " + text);
                this.itemExtraDetail = text;
                modal.dismiss();
            }
        }, {});
        return modal.present();
    }

    getImagePath(imgName) {
        return this.apiService.getBaseApiUrl() + imgName;
    }

    getItemDetails() {
        this.apiService.showLoading();
        this.apiService.itemDetails(this.selectedItem.item_id).then(data => {
            this.apiService.hideLoading();
            this.item = data['result'][0];
        });
    }

    ifMaxSelected() {
        return this.item.supplies.filter(data => data.selected).length === parseInt(this.item.max_supplies_select);
    }
}
