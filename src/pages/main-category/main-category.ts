import {Component, ViewChild, NgZone} from '@angular/core';
import {NavController, NavParams, Platform, Slides, ActionSheetController} from 'ionic-angular';
import {ApiService} from "../../providers/api-service";
import {DataStore} from "../../providers/data-store";
import {Storage} from "../../providers/storage";
import {InAppBrowser} from "ionic-native";
import {BusinessListPagePage} from "../business-list/business-list";
import {Category} from "../category/category";
import {Items} from "../items/items";
import {Firebase} from "@ionic-native/firebase";
import {BusinessSubcatPage} from "../business-subcat/business-subcat";
import {ProductsSubcatPage} from "../products-subcat/products-subcat";

/*
  Generated class for the MainCategoryPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-main-category',
    templateUrl: 'main-category.html'
})
export class MainCategoryPagePage {
    @ViewChild(Slides) slides: Slides;

    sliders: any;
    dataStore: DataStore;
    storage: Storage;
    refresher;

    itemsBig = [];
    filterItemsBig = [];

    items = [];
    filterItems = [];

    topCategory = [];
    allCategories = [];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiService: ApiService,
        public platform: Platform,
        public ngZone: NgZone,
        private firebase: Firebase,
        public actionSheetCtrl: ActionSheetController
    ) {
        this.dataStore = DataStore.getInstance();
        this.storage = Storage.getInstance();

        //temp data restore
        this.sliders = this.apiService.businessGroupPageStore.sliders;
        this.items = this.apiService.businessGroupPageStore.items;
        this.filterItems = this.apiService.businessGroupPageStore.filterItems;
        this.topCategory = this.apiService.businessGroupPageStore.topCategory;
        this.allCategories = this.apiService.businessGroupPageStore.allCategories;
        this.itemsBig = this.apiService.businessGroupPageStore.itemsBig;
        this.filterItemsBig = this.apiService.businessGroupPageStore.filterItemsBig;

        this.platform.ready().then((readySource) => {
            // this.getRestaurantCategory();
            this.getRestaurantCategoryFromApi(undefined);
            this.getSliders();
            this.getAllCategories(undefined);
            this.startAutoPlaySlide();
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad MainCategoryPagePage');
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "MainCategory"});

        setTimeout(() => {
            // this.payByMercadopago();
        }, 4000);
    }

    ionViewDidEnter() {
        console.log('ionViewDidEnter MainCategoryPagePage');
        if (this.allCategories && this.allCategories.length > 0) {
            this.getAllCategories(undefined);
        }
    }

    startAutoPlaySlide = () => {
        if (this.sliders) {
            var index = 0;
            setInterval(() => {
                index++;
                if (index >= this.sliders.length) {
                    index = 0
                }
                this.slides.slideTo(index);
            }, 3000);
        }
    }


    getSliders() {
        let userCityId = this.storage.getUser()['city_id'];
        this.apiService.sliders().then((data: any) => {
                this.ngZone.run(() => {
                    this.sliders = data.data;
                    this.apiService.businessGroupPageStore.sliders = data.data;
                    this.startAutoPlaySlide();
                });
            },
            err => {
            });
    }

    // For test only
    payByMercadopago = () => {
        var mercadopago = require("mercadopago");

        // let ClientId = info['ClientId'];
        // let ClientSecret = info['ClientSecret'];
        var oldAccessToken = mercadopago.configurations.getAccessToken();

        var payment = {
            description: 'Buying a PS4',
            transaction_amount: 10500,
            payer: {
                email: 'test_user_3931694@testuser.com',
                identification: {
                    type: 'DNI',
                    number: '34123123'
                }
            }
        };

        mercadopago.configure({
            access_token: 'APP_USR-6771131097828467-021312-fc8608b8f53de700935d6c59469437fc-191793538',
            client_id: '6771131097828467',
            client_secret: 'IFJR7K7V9RjdxIqBz5QMhBmHDzktzjCq',
        });
        var payItems = [];
        for (var i = 0; i < 1; ++i) {

            var payItem = {
                "title": "Test t",
                "quantity": 1,
                "currency_id": 'USD',
                "unit_price": 1
            }
            payItems.push(payItem);
        }

        var preference = {
            "items": payItems,
            "payer": {
                "name": "Test",
                "surname": "User",
                "email": "sukhdev.patidar99@gmail.com",
                "identification": {
                    'type': 'other',
                    'number': '8912893891'
                }
            },
            "back_urls": {
                "success": "https://snappypanel.com/adminpanel/mercadopago/success",
                "pending": "https://snappypanel.com/adminpanel/mercadopago/pending",
                "failure": "https://snappypanel.com/adminpanel/mercadopago/failure",
            },
            'auto_return': 'all'
        };

        mercadopago.createPreference(preference).then(function (data) {
            debugger;
        }).catch(function (error) {
            debugger;
        }).finally(function () {
            debugger;
        });
        // https://snappypanel.com/adminpanel/mercadopago/success?collection_id=17823425&collection_status=approved&preference_id=191793538-f4068307-1f2b-4abe-a85f-210e173b23b5&external_reference=null&payment_type=credit_card&merchant_order_id=969709166
        // mercadopago.sandboxMode(true);
        // mercadopago.configurations.setAccessToken('TEST-6771131097828467-021312-4b23c0c7a16d25446a73e3b3b8ff35bc-191793538');
        mercadopago.payment.create(payment).then(function (data) {
            debugger;
        }).catch(function (error) {
            debugger;
        }).finally(function () {
            mercadopago.configurations.setAccessToken(oldAccessToken);
            debugger;
        });
    }


    getRestaurantCategoryFromApi(refresher) {
        this.refresher = refresher;
        // let userCityId = this.storage.getUser()['city_id'];

        this.apiService.businessGroups().then(data => {
                // this.showRetryButton = false;
                let list = data['data'];
                var items = [];
                var itemsBig = [];
                if (list) {
                    list.map((item, key) => {
                        if (item.show_big == "1" || item.show_big == 1) {
                            itemsBig.push(item);
                        } else {
                            items.push(item);
                        }
                    });
                }

                this.itemsBig = itemsBig;
                this.filterItemsBig = itemsBig;

                this.items = items;
                this.filterItems = items;


                this.apiService.businessGroupPageStore.itemsBig = itemsBig;
                this.apiService.businessGroupPageStore.filterItemsBig = itemsBig;
                this.apiService.businessGroupPageStore.items = items;
                this.apiService.businessGroupPageStore.itemsBig = items;

                if (refresher) {
                    refresher.complete();
                }
            },
            err => {
                this.items = [];

                this.apiService.businessGroupPageStore.itemsBig = [];
                this.apiService.businessGroupPageStore.filterItemsBig = [];
                this.apiService.businessGroupPageStore.items = [];
                this.apiService.businessGroupPageStore.itemsBig = [];

                if (refresher) {
                    refresher.complete();
                }
                // this.showRetryButton = true;
            });
    }

    getAllCategories(refresher) {
        this.refresher = refresher;
        let userCityId = this.storage.getUser()['city_id'];

        this.apiService.restaurantCategory(userCityId).then((data: any) => {
                // this.showRetryButton = false;
                var list = [];
                if (data.status) {
                    const items = data['data'];
                    items.map((obj) => {
                        if (this.checkAvailability(obj).isAvailable) {
                            list.push(obj);
                        }
                    });
                }
                var allCategories = [];
                list.map((catData) => {
                    if (catData.category_available == 1 || catData.category_available == '1') {
                        allCategories.push(catData);
                    }
                });
                this.allCategories = allCategories;
                this.apiService.businessGroupPageStore.allCategories = allCategories;
                if (refresher) {
                    refresher.complete();
                }
            },
            err => {
                this.allCategories = [];
                this.apiService.businessGroupPageStore.allCategories = [];
                // this.showRetryButton = true;
                if (refresher) {
                    refresher.complete();
                }
            });
    }

    bannerClick(item) {
        console.log(JSON.stringify(item));
        if (item.url) {
            console.log(item.url);
            this.openInAppBrowserForPayU(item.url);
        }
    }

    checkAvailability(item) {
        let message = "";
        let status = false;
        let timerange = "";

        if (!item.availibilty) {
            message = "TODAY_CLOSED";
            status = false;
        } else {
            const availibilty = item.availibilty;
            var d = new Date();
            var weekday = d.getDay();
            if (weekday == 0) {
                weekday = 6;
            } else {
                weekday = weekday - 1;
            }
            const todayTimeSchedule = availibilty[weekday];
            if (todayTimeSchedule.open_status == 1) {
                message = "TODAY_CLOSED";
                status = false;
            } else if (todayTimeSchedule.open_status == 2) {
                message = "OPEN_FULL_DAY";
                status = true;
            } else {
                var today = new Date();
                var formattedtoday = today.getFullYear() + "/" + (today.getMonth() + 1) + '/' + today.getDate();


                //Abierto neste momento
                //Cerrado por el momento
                const openingTime = todayTimeSchedule.opening_time;
                const closingTime = todayTimeSchedule.closing_time;
                var openingTimeStr = formattedtoday + " " + openingTime;
                var closingTimeStr = formattedtoday + " " + closingTime;

                var openingTimeDate = new Date(openingTimeStr).getTime();
                var closingTimeDate = new Date(closingTimeStr).getTime();
                var nowDate = new Date().getTime();
                if (nowDate >= openingTimeDate && nowDate <= closingTimeDate) {
                    message = "OPEN_AT_THE_MOMENT";
                    status = true;
                } else {
                    message = "CLOSE_AT_THE_MOMENT";
                    status = false;
                }
                timerange = openingTime + " " + closingTime;
            }
        }

        return {
            message,
            isAvailable: status,
            timerange
        };
    }


    openInAppBrowserForPayU(url, loadStartCallback = null) {
        let browser = new InAppBrowser(url, '_self');
        browser.show();

        browser.on("loadstart").subscribe(event => {
            console.log("LOAD START " + JSON.stringify(event));
            if (loadStartCallback) {
                var url = event.url;
                loadStartCallback(url);
            }
        });
        browser.on("loadstop").subscribe(event => {
            console.log("LOAD STOP " + JSON.stringify(event));
        });
        browser.on("loaderror").subscribe(event => {
            console.log("LOAD ERROR " + JSON.stringify(event));
            browser.close();
        });
        return browser;
    }

    getImagePath(imgName) {
        return this.apiService.getBaseApiUrl() + imgName;
    }

    getItems(ev: any) {
        const val = ev.target.value;
        let items = this.items;
        let itemsBig = this.itemsBig;
        if (val && val.length > 0) {
            this.filterItems = items.filter((data) => {
                return data.name.toLowerCase().indexOf(val.toLowerCase()) != -1;
            });

            this.filterItemsBig = itemsBig.filter((data) => {
                return data.name.toLowerCase().indexOf(val.toLowerCase()) != -1;
            });

        } else {
            this.filterItems = items;
        }
    }

    openCategoryList(businessGroup) {
        // this.navCtrl.push(Category, {
        //     businessGroup: businessGroup
        // });

        this.navCtrl.push(BusinessSubcatPage, {
            businessGroup: businessGroup
        });
    }

    openCategoryProducts(category) {
        this.navCtrl.push(ProductsSubcatPage, {
            category: category
        });
    }

    doRefresh(refresher) {
        this.getRestaurantCategoryFromApi(refresher);
        this.getAllCategories(refresher);
    }

}
