import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams, Slides, Events} from 'ionic-angular';
import {ItemDetails} from '../itemdetails/itemdetails';

import {ApiService} from '../../providers/api-service';
import {DataStore} from '../../providers/data-store';
import {Storage} from '../../providers/storage';
import {Firebase} from "@ionic-native/firebase";

@Component({
    selector: 'page-items',
    templateUrl: 'items.html',
    providers: [ApiService, DataStore]
})

export class Items {
    storage: Storage;
    dataStore: DataStore;
    refresher;
    tagResponseCount = 0;

    @ViewChild('mySlider') slider: Slides;

    selectedItem: any;
    tabs: any;
    selectedTab = 0;

    items = [];

    searchInput = "";
    searchNeed = false;
    searchInProcess = false;


    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiService: ApiService,
        private firebase: Firebase,
        private events: Events,
    ) {
        this.dataStore = DataStore.getInstance();
        this.storage = Storage.getInstance();

        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');

        this.tabs = [];
        this.getProducts(true);
    }

    ionViewDidLoad() {
        this.events.subscribe('REFRESH_PRODUCTS_LIST', (res) => {
            this.getProducts(true);
        });
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Items"});
    }

    getProducts = (showLoading) => {
        let subCategory = this.navParams.get('subCategory');
        let userCityId = this.storage.getUser()['city_id'];

        if (showLoading) {
            this.apiService.showLoading();
        }

        this.apiService.restaurantProducts(subCategory.category_id, subCategory.id, userCityId, this.searchInput).then((data) => {
                if (showLoading) {
                    this.apiService.hideLoading();
                }
                if (data['status']) {
                    this.items = data['data'];
                } else {
                    this.items = [];
                }
                this.checkRefreshFinish();
                this.checkForSearch();
            },
            (error) => {
                if (showLoading) {
                    this.apiService.hideLoading();
                }
                this.checkRefreshFinish();
                this.items = [];
            });
    }

    doRefresh(refresher) {
        this.refresher = refresher;
        this.getProducts(false);
    }

    clicked(index) {
        console.log("Clicked Index" + index);
        this.slider.slideTo(index, 500);
        this.selectedTab = index;
    }

    onSlideChanged() {
        let currentIndex = this.slider.getActiveIndex();
        this.selectedTab = currentIndex;
    }


    itemSelect(item) {
        this.navCtrl.push(ItemDetails, {'item': item});
    }

    getImagePath(imgName) {
        return this.apiService.getBaseApiUrl() + imgName;
    }

    checkRefreshFinish() {
        if (this.refresher) {
            this.refresher.complete();
        }
    }


    checkForSearch() {
        this.searchInProcess = false;
        if (this.searchNeed) {
            this.searchNeed = false;
            this.getProducts(false);
        }
    }

    searchProducts(event) {
        this.searchNeed = true;
        if (!this.searchInProcess) {
            this.searchInProcess = true;
            this.searchNeed = false;
            this.getProducts(false);
        }
    }
}


