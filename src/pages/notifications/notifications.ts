import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Firebase} from "@ionic-native/firebase";

/*
  Generated class for the Notifications page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html'
})
export class Notifications {

  constructor(
      public navCtrl: NavController,
      private firebase: Firebase
  ) {}

  ionViewDidLoad() {
    console.log('Hello Notifications Page');
    this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Notifications"});

  }

}
