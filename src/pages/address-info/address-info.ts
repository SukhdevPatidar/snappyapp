import {Component, ViewChild, ElementRef, NgZone} from '@angular/core';
import {NavController, NavParams, Events, Platform} from 'ionic-angular';
import {Diagnostic, LocationAccuracy} from 'ionic-native';
import {Geolocation} from '@ionic-native/geolocation';

import {DataStore} from '../../providers/data-store';
import {ApiService} from '../../providers/api-service';
import {Storage} from '../../providers/storage';
import {Firebase} from "@ionic-native/firebase";

declare var google: any;

/*
  Generated class for the AddressInfo page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
@Component({
    selector: 'page-address-info',
    templateUrl: 'address-info.html',
    providers: [DataStore, ApiService, Storage]
})
export class AddressInfo {
    @ViewChild('map') mapElement: ElementRef;
    @ViewChild('search') searchElement: ElementRef;
    map: any;
    locationMarker: any;
    geocoder: any;


    placeName = "";
    placeLat = "";
    placeLng = "";

    title = "Address";
    dataStore: DataStore;
    storage: Storage;

    name = "";
    mobile = "";
    pincode = "";
    address1 = "";
    address2 = "";
    landmark = "";
    townCity = "";
    other = "";
    company = "";
    type = "home";

    lat = undefined;
    lng = undefined;

    defaultAddress = false;
    isReadOnly = false;

    extraFieldToggle = true;

    citiesList = [];

    userCountry;
    countryCode;


    constructor(
        public platform: Platform,
        public zone: NgZone,
        public navCtrl: NavController,
        public geolocation: Geolocation,
        public navParams: NavParams,
        public apiService: ApiService,
        public events: Events,
        private firebase: Firebase
    ) {
        this.dataStore = DataStore.getInstance();
        this.storage = Storage.getInstance();
        this.userCountry = this.storage.getCountry();
        this.countryCode = this.userCountry.country_code;
        this.mobile = this.countryCode;


        if (navParams.get("type") === 'add') {
            this.title = "NEW ADDRESS";

            this.name = this.storage.getUser().name + " " + this.storage.getUser().l_name;
            this.mobile = this.storage.getUser().mobile;

        } else if (navParams.get("type") == "edit") {
            let address = navParams.get("data");
            this.name = address.name;
            this.mobile = address.mobile;
            // this.pincode = address.pincode;
            this.address1 = address.house_no;
            this.address2 = address.colony_street;
            // this.landmark = address.landmark;
            this.townCity = address.town_city;
            // this.other = address.other;
            // this.company = address.company;
            this.type = address.address_type;
            this.lat = address.geo_lat;
            this.lng = address.geo_long;

            this.title = "EDIT ADDRESS";

            if (address.is_default == '1') {
                this.defaultAddress = true;
            }
        } else if (navParams.get("type") == "view") {
            this.isReadOnly = true;

            let address = navParams.get("data");
            this.name = address.name;
            this.mobile = address.mobile;
            // this.pincode = address.pincode;
            this.address1 = address.house_no;
            this.address2 = address.colony_street;
            // this.landmark = address.landmark;
            this.townCity = address.town_city;
            // this.other = address.other;
            // this.company = address.company;
            this.type = address.address_type;
            this.lat = address.geo_lat;
            this.lng = address.geo_long;

            this.title = "VIEW ADDRESS";

            if (address.is_default == '1') {
                this.defaultAddress = true;
            }
        } else if (navParams.get("type") === "add-currentlocation") {
            var ref = this;
            ref.title = "NEW ADDRESS";
            this.name = this.storage.getUser().name + " " + this.storage.getUser().l_name;
            this.mobile = this.storage.getUser().mobile;

            var locationSuccessCallBack = function (address) {
                ref.pincode = address.PIN;
                ref.address1 = address.Premise;
                ref.address2 = address.Address1;
                ref.landmark = "";
                ref.townCity = address.City;
                ref.other = address.Address2;
                ref.company = "";
            };

            Diagnostic.isLocationEnabled().then((isAvailable) => {
                console.log('Is Gps Enabled? ' + isAvailable);
                if (isAvailable) {
                    this.getCurrentLocation(locationSuccessCallBack);
                } else {
                    LocationAccuracy.canRequest().then((canRequest: boolean) => {

                        if (canRequest) {
                            // the accuracy option will be ignored by iOS
                            LocationAccuracy.request(LocationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                                () => {
                                    console.log('Request successful');
                                    this.getCurrentLocation(locationSuccessCallBack);
                                },
                                error => console.log('Error requesting location permissions', error)
                            );
                        }
                    });
                }
            }, (e) => console.error(e));
        }
    }

    ionViewDidLoad() {
        console.log('Hello AddressInfo Page');
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "AddressDetail"});

        this.getCitiesList();

        this.platform.ready().then(() => {
            this.loadMap();
        });
    }


    loadMap() {
        this.geocoder = new google.maps.Geocoder;
        var latLng;
        if (this.lat && this.lng) {
            latLng = new google.maps.LatLng(this.lat, this.lng);
        } else {
            latLng = new google.maps.LatLng(22.7196, 75.8577);
        }

        var geocoder = new google.maps.Geocoder;
        if (DataStore.getInstance().isDevice) {
            this.geolocation.getCurrentPosition().then((position) => {

                if (!this.lat && !this.lng) {
                    this.lat = position.coords.latitude;
                    this.lng = position.coords.longitude;
                }
                latLng = new google.maps.LatLng(this.lat, this.lng);
                this.map.setCenter(latLng);
                this.locationMarker.setPosition(latLng);
            }, (err) => {
                console.log(err);
            });
        }


        let mapOptions = {
            center: latLng,
            zoom: 15,
            panControl: true,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: false,
            rotateControl: false,
            fullscreenControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        google.maps.event.trigger(this.map, 'resize')

        var lat = latLng.lat();
        var lng = latLng.lng();
        this.addLocationMarker(lat, lng);

        google.maps.event.addListener(this.map, "click", (event) => {
            var myLatLng = event.latLng;
            var latlng = new google.maps.LatLng(myLatLng.lat(), myLatLng.lng());
            this.locationMarker.setPosition(latlng);
            this.geocodeLatLng(this.geocoder, myLatLng.lat(), myLatLng.lng());
            console.log('.......................' + myLatLng);
            this.lat = myLatLng.lat();
            this.lng = myLatLng.lng();
        });

        var input = document.querySelector('#search input');
        var searchBox = new google.maps.places.Autocomplete(input);
        //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);


        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.bindTo('bounds', this.map);

        autocomplete.addListener('place_changed', (event) => {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                this.map.fitBounds(place.geometry.viewport);
            } else {
                this.map.setCenter(place.geometry.location);
                this.map.setZoom(17);  // Why 17? Because it looks good.
            }
            this.locationMarker.setPosition(place.geometry.location);
            console.log('.......................' + place.geometry.location);
            this.geocodeLatLng(this.geocoder, place.geometry.location.lat(), place.geometry.location.lng());
            /* window.alert("Address: '" + place.formatted_address + "'");*/
            this.lat = place.geometry.location.lat();
            this.lng = place.geometry.location.lng();
        });


    }

    addLocationMarker(lat, lng) {
        this.geocodeLatLng(this.geocoder, lat, lng);

        if (!this.locationMarker) {
            this.locationMarker = new google.maps.Marker({
                map: this.map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                position: new google.maps.LatLng(lat, lng),
            });

            google.maps.event.addListener(this.locationMarker, 'dragend', (marker) => {
                var latLng = marker.latLng;
                this.geocodeLatLng(this.geocoder, latLng.lat(), latLng.lng());
                console.log('.......................' + latLng);

                this.lat = latLng.lat();
                this.lng = latLng.lng();
            });

        } else {
            this.locationMarker.setPosition(new google.maps.LatLng(lat, lng));
        }

    }

    geocodeLatLng(geocoder, lat, lng) {
        var latlng = {lat, lng};
        geocoder.geocode({'location': latlng}, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {


                    this.zone.run(() => {
                        this.placeName = results[0].formatted_address;
                        this.placeLat = lat;
                        this.placeLng = lng;
                    });

                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }

    addInfoWindow(marker, content) {
        let infoWindow = new google.maps.InfoWindow({
            content: content
        });

        google.maps.event.addListener(marker, 'click', () => {
            infoWindow.open(this.map, marker);
        });


    }

    getCitiesList = () => {
        this.apiService.citiesList().then(data => {
                this.citiesList = data['data'];
                if (this.citiesList && this.citiesList.length > 0) {
                    this.townCity = this.citiesList[0].id;
                }
            },
            err => {
            });
    }

    getCurrentLocation(callBack) {
        this.apiService.showLoading();

        var ref = this;
        this.geolocation.getCurrentPosition().then(pos => {
            var lat = pos.coords.latitude;
            var lng = pos.coords.longitude;
            console.log('lat: ' + lat + ', lon: ' + lng);
            ref.getReverseGeocodingData(lat, lng, function (address) {
                console.log('getReverseGeocodingData ' + JSON.stringify(address));

                ref.apiService.hideLoading();
                if (callBack) {
                    callBack(address);
                }
            });

        }, error => {
            console.log("Error in get current position " + JSON.stringify(error));
        });
    }

    getReverseGeocodingData(lat, lng, callBack) {
        var latlng = new google.maps.LatLng(lat, lng);
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng': latlng}, function (results, status) {

            // console.log("Response "+JSON.stringify(results)+" Status :"+status);

            if (status !== google.maps.GeocoderStatus.OK) {
                alert(status);
            }
            if (status == google.maps.GeocoderStatus.OK) {
                var zero = results[0];
                var address_components = zero["address_components"];

                var Premise = "", Address1 = "", Address2 = "", City = "", County = "", State = "", Country = "",
                    PIN = "";

                for (var i = 0; i < address_components.length; i++) {
                    var zero2 = address_components[i];
                    var long_name = zero2["long_name"];
                    var mtypes = zero2["types"];
                    var Type = mtypes[0];
                    if (long_name || !long_name.equals(null) || long_name.length > 0 || long_name != "") {
                        if (Type.toLowerCase() == "premise") {
                            Premise = long_name + " ";
                        } else if (Type.toLowerCase() == "street_number") {
                            Address1 = long_name + " ";
                        } else if (Type.toLowerCase() == "route") {
                            Address1 = Address1 + long_name;
                        } else if (Type.toLowerCase() == "sublocality") {
                            Address2 = long_name;
                        } else if (Type.toLowerCase() == "locality") {
                            // Address2 = Address2 + long_name + ", ";
                            City = long_name;
                        } else if (Type.toLowerCase() == "administrative_area_level_2") {
                            County = long_name;
                        } else if (Type.toLowerCase() == "administrative_area_level_1") {
                            State = long_name;
                        } else if (Type.toLowerCase() == "country") {
                            Country = long_name;
                        } else if (Type.toLowerCase() == "postal_code") {
                            PIN = long_name;
                        }
                    }
                }

                if (callBack) {
                    callBack({
                        Premise: Premise,
                        Address1: Address1,
                        Address2: Address2,
                        City: City,
                        County: County,
                        State: State,
                        Country: Country,
                        PIN: PIN
                    });
                }
            }
        });
    }

    onMobileChange() {
        if (this.mobile.length < 3) {
            this.mobile = this.countryCode;
        } else {
            this.mobile = this.mobile.replace(this.mobile.substring(0, 3), this.countryCode);
        }
    }

    saveAddress() {
        var msgArr = [];

        if (!this.lat || !this.lng) {
            this.apiService.showAlert("Selecciona tu ubicación en el mapa");
            return;
        }

        // Name
        if (this.name.length == 0) {
            this.apiService.showAlert("Introduzca su nombre");
            return;
        }

        // TownCity
        if (this.townCity.length == 0) {
            this.apiService.showAlert("Se requiere campo de pueblo / ciudad");
            return;
        }

        // Mobile
        if (this.mobile.length == 0) {
            msgArr.push("mobile number");
            this.apiService.showAlert("Ingrese el número de teléfono móvil");
            return;
        } else {
            var mobileRegx = /^\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*(\d{1,2})$/;
            if (!this.mobile.match(mobileRegx)) {
                msgArr.push("correct mobile number");
                this.apiService.showAlert("Ingrese el número de teléfono móvil correcto");
                return;
            }
        }

        // address1
        if (this.address1.length == 0) {
            msgArr.push("House number");
        }

        // address2
        if (this.address2.length == 0) {
            msgArr.push("Locality");
        }

        if (msgArr.length > 0) {
            // var finalErrorMsg = "";
            // var postFix = "";
            // if (msgArr.length > 1) {
            //     var lastError = msgArr[msgArr.length - 1];
            //     msgArr.pop();
            //     finalErrorMsg = msgArr.join(", ") + " and " + lastError;
            // } else {
            //     finalErrorMsg = msgArr[0];
            // }
            this.apiService.showAlert('Debes llenar el apartado "Otra información" para poder continuar.');
        } else {
            this.addUpdateAddressToApi(this.lat, this.lng);
        }
    }

    addUpdateAddressToApi(currLat, currLng) {
        let addressId = undefined;
        if (this.navParams.get("type") == "edit") {
            let address = this.navParams.get("data");
            addressId = address.id;
        }
        let callBack = this.navParams.get('callBack');
        var primary = "0";
        if (this.defaultAddress) {
            primary = "1";
        }
        this.apiService.showLoading();
        this.apiService.userAddress(
            this.storage.getUser()['id'],
            this.name,
            this.townCity,
            this.mobile,
            this.address1,
            this.address2,
            this.other,
            this.landmark,
            this.pincode,
            this.company,
            currLat,
            currLng,
            this.type,
            addressId,
            primary
        ).then(data => {
                this.apiService.hideLoading();
                console.log("Address add Response " + JSON.stringify(data));
                if (data['status']) {


                    if (this.navParams.get("type") == "add") {
                        var newAddId = data['id'];
                        this.saveAddressToDevice(newAddId, currLat, currLng);
                    } else {
                        this.saveAddressToDevice(addressId, currLat, currLng);
                    }
                    this.apiService.showToast(data['message']);
                    this.navCtrl.pop();
                    if (callBack) {
                        callBack();
                    }
                } else {
                    this.apiService.showAlert(data['message']);
                }
            },
            (error) => {
                this.apiService.hideLoading();
                console.log("Address add Error " + JSON.stringify(error));
            });
    }

    saveAddressToDevice(addressId, currLat, currLng) {
        if (this.navParams.get("type") == "add" || this.navParams.get("type") == "add-currentlocation") {
            if (this.defaultAddress) {
                this.storage.setDefaultAddress(addressId);
            }

            this.dataStore.addAddress(
                addressId,
                this.name,
                this.mobile,
                this.pincode,
                this.address1,
                this.address2,
                this.landmark,
                this.townCity,
                this.other,
                this.company,
                this.type,
                currLat,
                currLng
            ).then(response => {
                if (response['success']) {

                }
            });
        } else if (this.navParams.get("type") == "edit") {
            let address = this.navParams.get("data");

            if (this.defaultAddress) {
                this.storage.setDefaultAddress(address.address_id);
            }

            this.dataStore.updateAddress(
                address.address_id,
                address.address_id,
                this.name,
                this.mobile,
                this.pincode,
                this.address1,
                this.address2,
                this.landmark,
                this.townCity,
                this.other,
                this.company,
                this.type,
                currLat,
                currLng
            ).then(response => {
                if (response['success']) {

                }
            });
        }
    }
}
