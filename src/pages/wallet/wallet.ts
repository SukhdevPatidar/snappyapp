import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiService } from '../../providers/api-service';
import { DataStore } from '../../providers/data-store';
import { Storage } from '../../providers/storage';
import { Http, RequestOptions, Headers } from '@angular/http';
import { WalletTransactions }  from '../wallet-transactions/wallet-transactions';
import {Firebase} from "@ionic-native/firebase";

/*
  Generated class for the Wallet page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
  	selector: 'page-wallet',
  	templateUrl: 'wallet.html',
  	providers: [ApiService]
  })
  export class Wallet {
  	walletTransactions = WalletTransactions;

  	storage: Storage;
  	dataStore: DataStore;

  	walletInfo:any;
    userType = 'Normal';

    paytmWalletAdded = false;
    paytmWalletInfo = {};

    constructor(
        public http: Http,
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiService: ApiService,
        private firebase: Firebase
    ) {}

    ionViewDidLoad() {
      console.log('ionViewDidLoad WalletPage');
      this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Wallet"});

      this.storage = Storage.getInstance();
      this.dataStore = DataStore.getInstance();

      this.userType = this.storage.getUser().type;

      if(this.userType == 'Wallet') {
        this.getWallet();  
      }
      
    }

    getWallet() {
      var ref = this;
      let userId = this.storage.getUser().user_id;

      this.apiService.showLoading();
      this.apiService.wallet(userId).then((response) => {
        this.apiService.hideLoading();
        if(response) {
          ref.walletInfo = response;
        }
      },
      (error) => {
        this.apiService.hideLoading();
      });
    }


    addPaytmWallet() {
      this.sendOtpApi();
    }
    paytmWalletClick() {

    }

    /******************************************************************************************************************/
    /***************************************************  PAYTM   *****************************************************/
    /******************************************************************************************************************/

    payTmStagingApi = "https://accounts-uat.paytm.com/";

    sendOtpApi() {
      var ref = this;

      var baseUrl = "http://localhost:8100/paytmApi/";

      this.apiService.showLoading();

      this.postRequest(baseUrl+"signin/otp",{
        "email" : "sukhdev.patidar99@gmail.com",
        "phone" : "7777777777",
        "clientId" : "sdevco37289547463083",
        "scope" : "wallet",
        "responseType" : "token",
      }).then(res => {
        ref.apiService.hideLoading();

      });
    }
    postRequest(url,postData) {
      var ref = this;

      return new Promise((resolve, reject) => {

        let body = postData;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        this.http.post(url, body ,options)
        .map((res) => res.json())
        .subscribe(data => {

          console.log("Response "+JSON.stringify(data));

          resolve(data);
        },
        (error) => {
          console.log("Error "+JSON.stringify(error));
          ref.apiService.hideLoading();
          // ref.showToast("Network error");
          reject(error);
        }
        );
      });
    }
  }
