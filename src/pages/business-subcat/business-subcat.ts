import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {ApiService} from "../../providers/api-service";
import {DataStore} from "../../providers/data-store";
import {Storage} from "../../providers/storage";
import {Category} from "../category/category";

/**
 * Generated class for the BusinessSubcatPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
    selector: 'page-business-subcat',
    templateUrl: 'business-subcat.html',
    providers: [ApiService, DataStore]
})


export class BusinessSubcatPage {
    storage: Storage;

    businessGroup;
    businessGroupSubCatList;

    selectedSubCat;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiService: ApiService
    ) {
        this.businessGroup = this.navParams.get('businessGroup');
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad BusinessSubcatPage');
        this.getBusinessGroupSubcateogry();
    }

    getBusinessGroupSubcateogry() {
        this.apiService.showLoading();
        this.apiService.businessGroupSubcategory(this.businessGroup.id).then((data: any) => {
                this.apiService.hideLoading();
                if (data.status) {
                    const items = data['data'];
                    this.businessGroupSubCatList = items;
                }
            },
            err => {
                this.apiService.hideLoading();
            });
    }

    chooseSubCat(data) {
        this.selectedSubCat = data;

        if (this.selectedSubCat) {
            this.navCtrl.push(Category, {
                businessGroupSubCat: this.selectedSubCat
            });
        } else {
            this.apiService.showToast("Select business sub category");
        }
    }
}
