import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {ApiService} from '../../providers/api-service';
import Util from '../../providers/util';
import {Storage} from '../../providers/storage';
import {DataStore} from '../../providers/data-store';
import {Address} from '../address/address';
import {ThankYou} from '../thank-you/thank-you';
import {DateFormat} from '../../providers/date-format';
import {PaymentOptions} from '../paymentoptions/paymentoptions';
import {PaymentError} from "../payment-error/payment-error";
import {InAppBrowser} from "ionic-native";
import {Md5} from 'ts-md5/dist/md5'
import {Firebase} from "@ionic-native/firebase";

var mercadopago = require("mercadopago");

/*
  Generated class for the ConfirmOrder page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
@Component({
    selector: 'page-confirm-order',
    templateUrl: 'confirm-order.html',
    providers: [DataStore, ApiService, Storage, DateFormat]
})
export class ConfirmOrder {
    addressCmp = Address;
    notesBookTable: string = "";
    dataStore: DataStore;
    storage: Storage;
    selectedDineOption: string;
    address = {};
    selectedTables = [];

    totalAmount = 0;

    deliveryDate;
    deliveryTime;
    notesDelivery: string = "";

    takeawayDate;
    takeawayTime;
    notesTakeaway: string = "";

    addressChooseCallBack: any;

    minDateForPicker;
    maxYearForPicker;

    minTimeForPicker;
    maxTimeForPicker;


    byPayoption;
    addresses = [];
    defaultAddress = {};

    selectedPaymentMethod;


    createdOrderId = undefined;

    constructor(
        public navCtrl: NavController,
        public  navParams: NavParams,
        public  apiService: ApiService,
        private firebase: Firebase
    ) {

        this.byPayoption = this.navParams.get('byPayoption');
        this.selectedPaymentMethod = this.byPayoption.payOptions.filter((option) => {
            return option.code == this.byPayoption.selectedPaymentModeCode
        })[0];


        var currDate = DateFormat.shared().getCurrentTimezoneDate();

        this.minDateForPicker = currDate.toISOString();
        this.maxYearForPicker = currDate.getFullYear() + 1;
        this.deliveryDate = currDate.toISOString();
        this.takeawayDate = currDate.toISOString();

        // var dateForAfter1Hour = DateFormat.shared().getCurrentTimezoneDate();
        // dateForAfter1Hour.setHours(dateForAfter1Hour.getHours()+1);
        this.deliveryTime = currDate.toISOString();
        this.takeawayTime = currDate.toISOString();


        this.dataStore = DataStore.getInstance();
        this.storage = Storage.getInstance();
        this.selectedDineOption = navParams.get("DineOption");
        this.totalAmount = navParams.get('Amount');

        this.selectedTables = this.navParams.get("selectedTables");


        // this.dataStore.getAddresses().then(response => {
        //   console.log("Addresses "+JSON.stringify(response));

        //   if(response['success']) {
        //     var addresses= response['data'];

        //     for (var i = 0; i < addresses.length; i++) {
        //       var tmpAddress = addresses[i];
        //       if(tmpAddress.address_id == this.storage.getDefaultAddress()) {
        //         this.address = tmpAddress;
        //         break;
        //       }
        //     }
        //     var ref = this;
        //     this.addressChooseCallBack =  function(newAddress) {
        //       ref.address = newAddress;
        //     };
        //   }
        // });
    }

    ionViewDidLoad() {
        console.log('Hello ConfirmOrder Page');
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "ConfirmOrder"});

        this.getAddressesFromApi();
    }

    getAddressesFromApi() {
        this.addressChooseCallBack = (newAddress) => {
            this.defaultAddress = newAddress;
        };

        this.apiService.showLoading();
        this.apiService.getUserAddresses(this.storage.getUser()['id']).then(data => {
                this.apiService.hideLoading();
                if (data['status']) {
                    var user_address_details = data['data'];
                    if (user_address_details.length > 0) {
                        for (var i = 0; i < user_address_details.length; i++) {
                            var address = user_address_details[i];
                            if (address.is_default == '1') {
                                this.defaultAddress = address;
                                break;
                            }
                        }
                        this.addresses = user_address_details;
                    }

                }
            },
            (error) => {
                this.apiService.hideLoading();
            });
    }


    getImagePath(imgName) {
        return this.apiService.getBaseApiUrl() + imgName;
    }

    calculateSuppliesPrice(supplies) {
        var totalSuppliesPrice = 0;
        for (var i = 0; i < supplies.length; i++) {
            let supply = supplies[i];
            if (supply.selected) {
                totalSuppliesPrice = totalSuppliesPrice + parseFloat(supply.supplies_price);
            }
        }
        return totalSuppliesPrice;
    }

    getTotalPriceForItems(items) {
        var total = 0;
        for (let data of items) {
            let qty = data['quantity'];
            let price = data['item'].price;

            //supplies
            var suppliesPrice = 0;
            if (data['item'].supplies && data['item'].supplies.length > 0) {
                suppliesPrice = this.calculateSuppliesPrice(data['item'].supplies);
            }

            total = total + (qty * price) + (suppliesPrice * qty);
        }
        return total;
    }

    getDelieveryPriceForItems(items) {
        var delieveryPrice: any = 0;
        for (let data of items) {
            delieveryPrice = Number(data['item'].delievery_price);
        }
        return parseFloat(delieveryPrice);
    }

    confirmOrder = () => {
        if (!this.defaultAddress || Object.keys(this.defaultAddress).length == 0) {
            this.apiService.showAlert("SELECT_ADDRESS_MSG");
            return;
        }


        let userId = this.storage.getUser().id;
        let delievryPrice = this.getDelieveryPriceForItems(this.byPayoption.items);
        let items = [];
        var providerId;
        for (var i = 0; i < this.byPayoption.items.length; ++i) {
            var item = this.byPayoption.items[i];
            providerId = item.item.user_id;

            var selectedSupplies = [];
            const supplies = item.item.supplies;
            if (supplies && supplies.length > 0) {
                for (let supply of supplies) {
                    if (supply.selected) {
                        selectedSupplies.push({
                            id: supply.id,
                            supplies_name: supply.supplies_name,
                            supplies_price: supply.supplies_price
                        });
                    }
                }
            }

            items.push({
                id: item.item.id,
                quantity: item.quantity,
                price: item.item.price,
                supplies: selectedSupplies,
                extra_detail: item.item.extra_detail,
                product_name: item.item.product_name
            });
        }
        if (this.createdOrderId) {
            this.processPayment(userId, providerId, this.createdOrderId);
        } else {
            this.apiService.showLoading();
            this.apiService.createOrder(userId, providerId, this.selectedPaymentMethod.id, delievryPrice, 'create', 'pending', this.defaultAddress['id'], this.notesDelivery, items).then(data => {
                    this.apiService.hideLoading();

                    if (data['status']) {
                        var orderID = data['order_id'];
                        this.createdOrderId = orderID;
                        this.processPayment(userId, providerId, orderID);
                    } else {
                        this.apiService.showAlert(data['error']);
                    }
                },
                error => {
                    this.apiService.hideLoading();
                    this.apiService.showAlert("PROBLEM IN PLACE ORDER");
                    console.log("Error getRestaurantTable : " + JSON.stringify(error));
                });
        }

    }


    processPayment = (userId, providerId, orderID) => {
        if (this.selectedPaymentMethod.code == 'mercadopago') {
            this.getProvidePaymentInfoFormApi(providerId, this.selectedPaymentMethod.id, (info) => {
                this.payByMercadopago(orderID, info, (status) => {
                    this.orderPlacedSuccessfully(orderID);
                });
            });
        } else if (this.selectedPaymentMethod.code == 'todopago') {
            let totalPrice: number = this.getTotalPriceForItems(this.byPayoption.items) + this.getDelieveryPriceForItems(this.byPayoption.items);

            this.apiService.showLoading();
            this.apiService.arPaymentRequest({
                user_id: userId,
                provider_id: providerId,
                payment_method_id: this.selectedPaymentMethod.id,
                order_id: orderID,
                total_price: totalPrice,
                address_id: this.defaultAddress['id']
            }).then(payData => {
                    this.apiService.hideLoading();
                    if (payData['status']) {
                        var info = payData['data'];
                        if (info) {
                            let paymentUrl = info.payment_url;
                            let successUrl = info.success_url;
                            let errorUrl = info.error_url;

                            var browser = this.openInAppBrowser(paymentUrl, (loadingUrl) => {
                                if (loadingUrl.indexOf(successUrl) !== -1 || loadingUrl.indexOf(errorUrl) !== -1) {
                                    browser.close();

                                    let message = Util.getQueryStringValue(loadingUrl, "message");
                                    if (loadingUrl.indexOf(successUrl) !== -1) {
                                        console.log("PAYMENT SUCCESS:: " + message);
                                        this.orderPlacedSuccessfully(orderID);
                                    } else {
                                        console.log("PAYMENT Error:: " + message);
                                        this.apiService.showAlert(message);
                                    }
                                }
                            });
                        }
                    } else {
                        this.apiService.showAlert(payData['message']);
                    }
                },
                (error) => {
                    this.apiService.hideLoading();
                });
        } else if (this.selectedPaymentMethod.code == 'payu') {
            this.getProvidePaymentInfoFormApi(providerId, this.selectedPaymentMethod.id, (info) => {
                this.payByPayu(orderID, info, (status) => {
                    this.orderPlacedSuccessfully(orderID);
                });
            });
        } else {
            this.orderPlacedSuccessfully(orderID);
        }
    }


    /**
     * Open In App Browser for payment gateway
     * @param paymentUrl - url for payment gateway
     * @param loadStartCallback - callback for get event when start loading url
     * @returns  InAppBrowser - InAppBrowser reference object
     **/
    openInAppBrowser(paymentUrl, loadStartCallback) {
        let browser = new InAppBrowser(paymentUrl, '_blank', 'location=no,hideurlbar=yes,hidenavigationbuttons=yes,toolbar=yes');
        browser.show();

        browser.on("loadstart").subscribe(event => {
            console.log("LOAD START " + JSON.stringify(event));
            if (loadStartCallback) {
                var url = event.url;
                loadStartCallback(url);
            }
        });
        browser.on("loadstop").subscribe(event => {
            console.log("LOAD STOP " + JSON.stringify(event));
        });
        browser.on("loaderror").subscribe(event => {
            console.log("LOAD ERROR " + JSON.stringify(event));
            browser.close();
        });
        return browser;
    }

    orderPlacedSuccessfully = (order_id) => {
        var itemIds = [];
        for (let data of this.byPayoption.items) {
            itemIds.push(data.id);
        }

        this.dataStore.removeItemsFromCart(itemIds);

        let callBack = () => {
            this.navCtrl.pop({animate: false});
            let clbk = this.navParams.get('callBack');
            if (clbk) {
                clbk();
            }
        };

        this.navCtrl.push(ThankYou, {"message": "Order Id: #" + order_id, "callBack": callBack});
    }

    getProvidePaymentInfoFormApi = (providerId, paymentMethodId, callBack) => {
        this.apiService.showLoading();
        this.apiService.providerPaymentInfo(providerId, paymentMethodId).then(data => {
                this.apiService.hideLoading();
                if (data['status']) {
                    var info = data['data'];
                    if (info) {
                        if (callBack) {
                            callBack(info);
                        }
                    }
                }
            },
            (error) => {
                this.apiService.hideLoading();
            });
    }

    payByMercadopago = (orderID, info, callBack) => {

        let ClientId = info['ClientId'];
        let ClientSecret = info['ClientSecret'];
        let AccessToken = info['AccessToken'];
        let IsSandboxMode = info['IsSandboxMode'];
        var oldAccessToken = mercadopago.configurations.getAccessToken();

        let totalPrice: number = this.getTotalPriceForItems(this.byPayoption.items) + this.getDelieveryPriceForItems(this.byPayoption.items);
        let deliveryPrice = this.getDelieveryPriceForItems(this.byPayoption.items);

        try {
            mercadopago.configure({
                // access_token: AccessToken,
                client_id: ClientId,
                client_secret: ClientSecret,
            });
        } catch (e) {
            console.error(e);
        }
        const currency = "ARS";

        var payItems = [];
        for (var i = 0; i < this.byPayoption.items.length; ++i) {
            let item = this.byPayoption.items[i];
            let itemPrice = parseFloat(item.item.price);
            var suppliesPrice = 0;
            if (item.item.supplies && item.item.supplies.length > 0) {
                suppliesPrice = this.calculateSuppliesPrice(item.item.supplies);
            }
            let itemId = item.item.id;

            var payItem = {
                "id": itemId,
                "title": item.item.product_name,
                "quantity": item.quantity,
                "currency_id": currency,
                "unit_price": (itemPrice + suppliesPrice)
            }
            payItems.push(payItem);
        }

        let successUrl = this.apiService.getBaseApiUrl() + "/mercadopago/success";
        let errorUrl = this.apiService.getBaseApiUrl() + "/mercadopago/failure";
        let pendingUrl = this.apiService.getBaseApiUrl() + "/mercadopago/pending";
        let user = this.storage.getUser();

        var code = "+52";
        var mobile = user.mobile;
        if (user.mobile.indexOf("+") > -1) {
            code = user.mobile.substring(0, 3);
            mobile = user.mobile.substring(3, user.mobile.length);
            mobile = mobile.replace(" ", "");
        }

        //Payer
        var payer = {
            "name": user.name,
            "surname": user.l_name,
        };
        let email = user.email.replace(" ", "");
        if (user.email && Util.validateEmail(email)) {
            payer['email'] = email
        }
        if (mobile) {
            payer['phone'] = {
                number: parseInt(mobile),
                area_code: code
            }
        }

        var preference = {
            "items": payItems,
            "shipments": {
                "cost": deliveryPrice
            },
            "payer": payer,
            "back_urls": {
                "success": successUrl,
                "pending": pendingUrl,
                "failure": errorUrl,
            },
            'auto_return': 'all'
        };

        mercadopago.createPreference(preference).then((data) => {
            let response = data.response;
            if (response) {
                let sandbox_init_point = response.sandbox_init_point;
                let init_point = response.init_point;
                let client_id = response.client_id;
                var paymentUrl;
                if (IsSandboxMode == true || IsSandboxMode == "true") {
                    paymentUrl = sandbox_init_point;
                } else {
                    paymentUrl = init_point;
                }

                let browser = this.openInAppBrowser(paymentUrl, (loadingUrl) => {
                    if (loadingUrl.indexOf(successUrl) !== -1 || loadingUrl.indexOf(errorUrl) !== -1) {
                        browser.close();

                        let message = Util.getQueryStringValue(loadingUrl, "message");
                        if (loadingUrl.indexOf(successUrl) !== -1) {
                            console.log("PAYMENT SUCCESS:: " + message);
                            var data = {
                                payment_status: 'complete',
                                order_id: orderID,
                                payment_response: JSON.stringify({
                                    collection_id: Util.getQueryStringValue(loadingUrl, "collection_id"),
                                    preference_id: Util.getQueryStringValue(loadingUrl, "preference_id"),
                                    payment_type: Util.getQueryStringValue(loadingUrl, "payment_type"),
                                    merchant_order_id: Util.getQueryStringValue(loadingUrl, "merchant_order_id"),
                                })
                            };

                            this.apiService.showLoading();
                            this.apiService.updateOrder(data).then(data => {
                                this.apiService.hideLoading();
                                if (data['status']) {
                                    if (callBack) {
                                        callBack(true);
                                    }
                                }
                            });
                            this.orderPlacedSuccessfully(orderID);
                        } else {
                            console.log("PAYMENT Error:: " + message);
                            this.apiService.showAlert(message);
                        }
                    }
                });
            }
        }).catch((error) => {
            this.apiService.showAlert(error.message);
        }).finally(function () {
            mercadopago.configurations.setAccessToken(oldAccessToken);
            debugger;
        });

    }


    payByPayu = (orderID, info, callBack) => {
        var payu_public_key = info['payu_public_key'];
        var payu_api_key = info['payu_api_key'];
        var payu_api_login = info['payu_api_login'];
        var payu_merchant_id = info['payu_merchant_id'];
        var payu_account_id = info['payu_account_id'];
        let IsSandboxMode = info['IsSandboxMode'];

        var url = "https://checkout.payulatam.com/ppp-web-gateway-payu";
        var isTest = 0;
        if (IsSandboxMode == true || IsSandboxMode == "true") {
            url = "https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu";

            payu_api_key = "4Vj8eK4rloUd272L48hsrarnUA";
            payu_api_login = "pRRXKOl8ikMmt9u";
            payu_merchant_id = "508029";
            payu_account_id = "512322";
        }


        let totalPrice: number = this.getTotalPriceForItems(this.byPayoption.items) + this.getDelieveryPriceForItems(this.byPayoption.items);

        let user = this.storage.getUser();
        let email = user.email.replace(" ", "");
        var mobile = user.mobile;
        mobile = mobile.replace(" ", "");

        var currency = "ARS";
        var refId = orderID;
        var signatureFormat = payu_api_key + "~" + payu_merchant_id + "~" + refId + "~" + totalPrice + "~" + currency;
        var signature = Md5.hashStr(signatureFormat);

        var desciption = "";
        for (var i = 0; i < this.byPayoption.items.length; ++i) {
            var item = this.byPayoption.items[i];
            if (desciption.length > 0) {
                desciption = desciption + " | ";
            }
            desciption = desciption + item.item.product_name;
        }

        var responseUrl = this.apiService.getBaseApiUrl() + "payUReposne";
        var payData = {
            'payerFullName': user.name + " " + user.l_name,
            'merchantId': payu_merchant_id,
            'ApiKey': payu_api_key,
            'referenceCode': refId,
            'accountId': payu_account_id,
            'description': desciption,
            'amount': totalPrice,
            'tax': 0,
            'taxReturnBase': 0,
            'currency': currency,
            'test': isTest,
            'buyerEmail': email,
            'mobilePhone': mobile,
            'responseUrl': responseUrl,
            'confirmationUrl': this.apiService.getBaseApiUrl() + "payUConfirmation",
            'signature': signature,
            'lng': 'en'
        };

        var pageContent = '<html><head></head><body><form id="loginForm" action="' + url + '" method="post">';

        Object.keys(payData).map((fieldKey) => {
            pageContent = pageContent + '<input type="hidden" name="' + fieldKey + '" value="' + payData[fieldKey] + '">';
        });
        pageContent = pageContent + '</form> <script type="text/javascript">document.getElementById("loginForm").submit();</script></body></html>';

        var pageContentUrl = 'data:text/html;base64,' + btoa(pageContent);
        var browser = this.openInAppBrowser(pageContentUrl, (loadingUrl) => {
            if (loadingUrl.indexOf(responseUrl) !== -1) {
                browser.close();

                let message = Util.getQueryStringValue(loadingUrl, "message");
                let lapResponseCode = Util.getQueryStringValue(loadingUrl, "lapResponseCode");

                if (lapResponseCode == "APPROVED") {
                    let lapPaymentMethodType = Util.getQueryStringValue(loadingUrl, "lapPaymentMethodType");
                    let lapPaymentMethod = Util.getQueryStringValue(loadingUrl, "lapPaymentMethod");
                    let reference_pol = Util.getQueryStringValue(loadingUrl, "reference_pol");
                    let transactionId = Util.getQueryStringValue(loadingUrl, "transactionId");

                    var data = {
                        payment_status: 'complete',
                        order_id: orderID,
                        payment_response: JSON.stringify({
                            lapResponseCode: lapResponseCode,
                            lapPaymentMethodType: lapPaymentMethodType,
                            reference_pol: reference_pol,
                            lapPaymentMethod: lapPaymentMethod,
                            transactionId: transactionId,
                        })
                    };

                    console.log("PAYMENT SUCCESS:: " + message);
                    callBack(true);
                } else {
                    console.log("PAYMENT Error:: " + message);
                    this.apiService.showAlert(message);
                }
            }
        });
    }

    goToThankYouPage(msg) {
        this.navCtrl.push(ThankYou, {
            message: msg
        });
    }

    combineAddress(address) {
        let mobile = address.mobile;
        let pincode = address.pincode;
        let address1 = address.house_no;
        let address2 = address.colony_street;
        let landmark = address.landmark;
        let townCity = address.town_city;
        let state = address.other;
        let country = address.company;

        var addressString = "";
        if (address1 && address1.length > 0) {
            addressString += address1 + "<br />";
        }
        if (address2 && address2.length > 0) {
            addressString += address2 + "<br />";
        }
        if (landmark && landmark.length > 0) {
            addressString += landmark + "<br />";
        }
        if (townCity && townCity.length > 0) {
            addressString += townCity + "<br />";
        }
        if (state && state.length > 0) {
            addressString += state + " " + pincode + "<br />";
        }
        if (country && country.length > 0) {
            addressString += country + "<br />";
        }
        if (mobile && mobile.length > 0) {
            addressString += "Phone : " + mobile;
        }
        return addressString;
    }


    checkTimeWithCurrentTime(date, time, delay) {
        var currentDate = DateFormat.shared().getCurrentTimezoneDate();
        var tempCurrDate = DateFormat.shared().convertDateWithZeroTime(DateFormat.shared().getCurrentTimezoneDate());
        var otherDate = DateFormat.shared().convertDateWithZeroTime(new Date(date));

        if (otherDate.getTime() > tempCurrDate.getTime()) {
            return true;
        } else if (otherDate.getTime() == tempCurrDate.getTime()) {
            var timeDateArr = time.split("T");
            var timeArr = timeDateArr[1].split(":");
            var timeHours = parseInt(timeArr[0]);
            var timeMinutes = parseInt(timeArr[1]);

            var currentTime = (currentDate.getUTCHours() * 60) + currentDate.getUTCMinutes();
            var otherTime = (timeHours * 60) + timeMinutes;
            if (otherTime > (currentTime + delay)) {
                return true;
            }
            return false;

        }
        return false;
    }
}
