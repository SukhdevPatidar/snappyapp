import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ApiService } from '../../providers/api-service';
import { Storage } from '../../providers/storage';

import { ConfirmOrder } from '../confirm-order/confirm-order';
import {Firebase} from "@ionic-native/firebase";

/*
  Generated class for the ChooseTable page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
    selector: 'page-choose-table',
    templateUrl: 'choose-table.html',
    providers: [ApiService]
  })
  export class ChooseTable {
    tables:any;
    storage:Storage;
    selectedTable:any;

    constructor(
        public navCtrl: NavController,
        public  apiService: ApiService,
        private firebase: Firebase
    ) {
      this.storage = Storage.getInstance();

      this.getTables();
    }


    ionViewDidLoad() {
      this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "ChooseTable"});
      console.log('Hello ChooseTable Page');

    }

    getTables() {
      
      this.apiService.showLoading();     
      this.apiService.getRestaurantTable().then(data => {
        this.apiService.hideLoading();     

        let tablesArr = data['result'];

        if(tablesArr && tablesArr.length > 0) {
          this.tables = tablesArr;
          for(let table of this.tables) {
            table['book'] = false;
          }
        } else {
          this.tables = [];
        }
      },
      error => {
        this.apiService.hideLoading();
        this.tables = [];
        console.log("Error getRestaurantTable : "+JSON.stringify(error));
      });
    }

    tableSelect(tableData) {
      for(let table of this.tables) {
        if(table.t_id == tableData.t_id) {
          tableData.book = true;
          this.selectedTable = tableData;
        } else {
          table.book = false;
        }
      }
    }
    bookNow() {

      this.navCtrl.push(ConfirmOrder,{
        "selectedTables":[this.selectedTable],
        "DineOption":"RESERVE TABLE"
      });
    }


  }
