import { Component } from '@angular/core';
import { NavController , ActionSheetController } from 'ionic-angular';
import { Camera } from 'ionic-native';
import { Storage } from '../../providers/storage';
import { ApiService } from '../../providers/api-service';
import { DateFormat } from '../../providers/date-format';
import {Firebase} from "@ionic-native/firebase";
/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
  	selector: 'page-profile',
  	templateUrl: 'profile.html',
  	providers: [ApiService, DateFormat]

  })
  export class Profile {
  	storage: Storage
    userImg = "";
    base64Img = "";
    user = {};

    citiesList = [];

    constructor(
        public navCtrl: NavController,
        public actionSheetCtrl: ActionSheetController,
        public apiService: ApiService,
        private firebase: Firebase
    ) {
      this.storage = Storage.getInstance();

    }

    ionViewDidLoad() {
      console.log('Hello Profile Page');
      this.user = this.storage.getUser();
      this.userImg = this.user['profile_photo']?this.user['profile_photo']:"";
      this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Profile"});

      this.getCitiesList();

    }

    getCitiesList = () => {
      this.apiService.citiesList().then(data => {
        this.citiesList = data['data'];  
        if(this.citiesList && this.citiesList.length > 0) {
        }
      },
      err => {
      }); 
    }

    save() {
      if(this.user['name'].length > 0 && this.user['l_name'].length > 0 && this.user['mobile'].length > 0) {
        this.apiService.showLoading();
        this.apiService.updateProfile(
          this.user['id'],
          this.user['name'], 
          this.user['l_name'], 
          this.user['mobile'], 
          this.user['city_id'], 
          DateFormat.shared().dateToStrDDMMYYYY(new Date(this.user['user_birthday'])), 
          ).then(data => {
            this.apiService.hideLoading();
            console.log("Profile Update Response " +JSON.stringify(data));

            if(data['status']) {
              this.apiService.showToast(data['message']);
              this.storage.setUser(data['data']);

              this.user = this.storage.getUser();
              this.userImg = this.user['profile_photo']?this.user['profile_photo']:"";

            } else {
              this.apiService.showAlert(data['error']);
            }          
          },
          (error) => {
            this.apiService.hideLoading();
            console.log("Signup Error " +JSON.stringify(error));
          });
        } else if (this.user['name'].length == 0) {
          this.apiService.showAlert('PLEASE ENTER FIRSTNAME');
        } else if (this.user['l_name'].length == 0) {
          this.apiService.showAlert('PLEASE ENTER LASTNAME');
        } else if (this.user['mobile'].length == 0) {
          this.apiService.showAlert('PLEASE ENTER PHONENUMBER');
        } 
      }


      editProfilePic() {
        if(!this.storage.isLoggedIn()) {
          return ;
        }
        let actionSheet = this.actionSheetCtrl.create({
          title: 'Choose option',
          buttons: [
          {
            text: 'Camera',
            handler: () => {
              this.pickPhoto(Camera.PictureSourceType.CAMERA);
              console.log('Camera clicked');
            }
          },{
            text: 'Photo Library',
            handler: () => {
              this.pickPhoto(Camera.PictureSourceType.PHOTOLIBRARY);
              console.log('PHOTO LIBRARY clicked');
            }
          },{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
          ]
        });
        actionSheet.present();


      } 

      pickPhoto(sourceType) {
        Camera.getPicture({destinationType:Camera.DestinationType.DATA_URL, sourceType:sourceType}).then((imageData) => {
          this.base64Img = 'data:image/jpeg;base64,' + imageData;
          this.savePictureToApi(imageData);
        }, (err) => {
          console.log("Get Picture Error "+JSON.stringify(err));
        });
      }

      savePictureToApi(base64Image) {
        this.apiService.showLoading();
        this.apiService.updateProfilePhoto(this.storage.getUser()['id'], base64Image).then(data => {
          this.apiService.hideLoading();
          console.log("Image Update Response " +JSON.stringify(data));
          this.apiService.hideLoading();
          if(data['status']) {
            this.apiService.showToast(data['message']);

            this.storage.setUser(data['data']);

            this.user = this.storage.getUser();
            this.userImg = this.user['profile_photo']?this.user['profile_photo']:"";

          } else {
            this.apiService.showAlert(data['message']);
          }
        });
      }

      getPhotoFromApi() {
        if(this.storage.isLoggedIn()) {
          this.apiService.showLoading();
          this.apiService.getUser(this.storage.getUser()['user_id']).then(data => {
            this.apiService.hideLoading();
            console.log("Image Response " +JSON.stringify(data));
            this.apiService.hideLoading();
            if(data['user_details']) {
              var userData = data['user_details'][0];
              this.userImg = userData.u_icon;
            } else {
              this.userImg = '';
            }
          });
        }
      }

      getImgPath(){
        if(this.storage.isLoggedIn()) {
          let  user = this.storage.getUser();
          this.userImg = user['profile_photo']?user['profile_photo']:"";
          
          if(this.base64Img.length > 0) {
            return this.base64Img;
          } else if(this.userImg.length > 0) {
            return  this.apiService.getBaseApiUrl() + this.userImg;
          }
        } 
        return  './assets/img/user-icon.png';
      }
    }
