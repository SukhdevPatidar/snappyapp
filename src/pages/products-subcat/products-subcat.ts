import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {ApiService} from "../../providers/api-service";
import {DataStore} from "../../providers/data-store";
import {Storage} from "../../providers/storage";
import {Category} from "../category/category";
import {Items} from "../items/items";

/**
 * Generated class for the ProductsSubcatPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-products-subcat',
    templateUrl: 'products-subcat.html',
    providers: [ApiService, DataStore]
})
export class ProductsSubcatPage {

    storage: Storage;

    category;
    subCatList;

    selectedSubCategory;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiService: ApiService
    ) {
        this.category = this.navParams.get('category');
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ProductsSubcatPage');
        this.getSubcateogry();
    }

    getSubcateogry() {
        this.apiService.showLoading();
        this.apiService.productSubcategory(this.category.id).then((data: any) => {
                this.apiService.hideLoading();
                if (data.status) {
                    const items = data['data'];
                    this.subCatList = items;
                }
            },
            err => {
                this.apiService.hideLoading();
            });
    }

    chooseSubCat(data) {
        this.selectedSubCategory = data;

        if (this.selectedSubCategory) {
            this.navCtrl.push(Items,
                {
                    subCategory: this.selectedSubCategory
                });
        } else {
            this.apiService.showToast("Select Sub category");
        }
    }

}
