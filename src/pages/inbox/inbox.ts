import {Component} from '@angular/core';
import {NavController, Platform} from 'ionic-angular';

import {ApiService} from "../../providers/api-service";
import {FirebaseProvider} from "../../providers/firebase/firebase";
import {Storage} from "../../providers/storage";
import {DataStore} from "../../providers/data-store";
import {ChatDetailPage} from "../chat-detail/chat-detail";

@Component({
    selector: 'page-inbox',
    templateUrl: 'inbox.html',
    providers: [DataStore, ApiService, Storage]
})

export class InboxPage {
    conversations = {};
    rooms = [];
    loadingUsers = false;
    getConversationsListner;
    storage: Storage;

    constructor(
        public navCtrl: NavController,
        public api: ApiService,
        public firebaseProvider: FirebaseProvider,
        public platform: Platform,
    ) {
        this.storage = Storage.getInstance();
        //for fix websocket error
        localStorage.removeItem('firebase:previous_websocket_failure');
    }

    ionViewDidLoad() {
        this.platform.ready().then(() => {
            this.loadingUsers = true;
            this.getConversationsList(() => {
                this.loadingUsers = false;
            });
        });
    }

    ionViewWillUnload() {
        console.log('ionViewWillUnload ChatroomsPage');
        this.getConversationsListner.off()
    }

    doRefresh(refresher) {
        this.getConversationsListner.off();
        this.getConversationsList(() => {
            refresher.complete();
        });
    }

    getConversationsList = (callBack = null) => {
        let user = this.storage.getUser();

        this.getConversationsListner = this.firebaseProvider.getConversations(user.id);

        this.getConversationsListner.on("value", res => {

            if (callBack) {
                callBack();
            }
            let values = res.val();
            if (values) {
                this.conversations = values;
                let chats = Object.keys(values);

                var usersList = [];
                chats.map((uid) => {
                    var data = values[uid];
                    data["user_id"] = uid;
                    usersList.push(data);
                })

                usersList.sort(function (a, b) {
                    return b.updated_at - a.updated_at;
                })
                var usersIds = [];
                usersList.map((data) => {
                    usersIds.push(data.user_id);
                })

                console.log(usersIds);
                this.getUsers(usersIds);
            }
        });
    }

    getUsers(chats) {
        if (chats && chats.length > 0) {
            var data = {
                users: chats.join(),
            };
            this.api.postRequest(this.api.getBaseApiUrl() + this.api.apiMethods().users, data).then((res: any) => {
                if (res.status) {
                    this.rooms = res.data;
                } else {
                    this.rooms = [];
                }
            }).catch((error) => {
                this.rooms = [];
            });
        } else {
            this.rooms = [];
        }
    }

    openMessages(user) {
        const conversation = this.conversations[user.id];

        this.navCtrl.push(ChatDetailPage, {
            'receiver': JSON.stringify(user),
            'order_id': conversation.lastMessage.order_id
        });
    }

    getImagePath(path) {
        if (path) {
            return this.api.getBaseApiUrl() + path;
        } else {
            return "./assets/img/placeholder.png"
        }
    }

}
