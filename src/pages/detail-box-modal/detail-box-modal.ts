import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Firebase} from "@ionic-native/firebase";

/*
  Generated class for the DetailBoxModalPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-detail-box-modal',
    templateUrl: 'detail-box-modal.html'
})
export class DetailBoxModalPage {

    detailText = "";

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private firebase: Firebase
    ) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad DetailBoxModalPagePage');
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "DetailBoxModalPagePage"});
    }

    addDetail() {
        let callBack = this.navParams.get("callBack");
        if (callBack) {
            callBack(this.detailText);
        }
    }
}
