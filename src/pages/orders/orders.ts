import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';

import {ApiService} from '../../providers/api-service';
import {Storage} from '../../providers/storage';
import {DataStore} from '../../providers/data-store';

import {History} from '../history/history';
import {Category} from '../category/category';
import {Login} from '../login/login';
import {ThankYou} from '../thank-you/thank-you';
import {Firebase} from "@ionic-native/firebase";
import {MainCategoryPagePage} from "../main-category/main-category";

/*
  Generated class for the Orders page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
@Component({
    selector: 'page-orders',
    templateUrl: 'orders.html',
    providers: [DataStore, ApiService]
})
export class Orders {
    cart: DataStore;
    taxes = {};
    storage: Storage;
    ordersList = [];
    isParentOrder: boolean = false;
    orderStatus: number = -1;
    dataStore: DataStore;

    requestButtonText = "REQUEST FOR BILL !";

    constructor(
        public navCtrl: NavController,
        public  apiService: ApiService,
        private firebase: Firebase
    ) {
        this.dataStore = DataStore.getInstance();
        this.storage = Storage.getInstance();
    }

    ionViewDidLoad() {
        console.log('Hello Orders Page');
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Orders"});

        this.getOrderDetails(this.storage.getUser().id);
    }

    getOrderDetails(orderId) {

        this.apiService.showLoading();

        this.apiService.getOrders(orderId, 'create').then((response) => {
                this.apiService.hideLoading();
                if (response['status']) {
                    this.ordersList = response['data'];
                } else {
                    this.ordersList = [];
                }
            },
            (error) => {
                this.apiService.hideLoading();
                this.ordersList = [];
            });
    }


    continue() {
        this.navCtrl.setRoot(MainCategoryPagePage);
    }

    openHistory() {
        if (this.storage.isLoggedIn()) {
            this.navCtrl.push(History);
        } else {
            this.navCtrl.push(Login, {
                'PageType': 'History'
            })
        }
    }

    getImagePath(imgName) {
        return this.apiService.getBaseApiUrl() + imgName;
    }

    getProductPrice(product) {
        if (product.promotional_price) {
            return parseFloat(product.promotional_price);
        }
        return parseFloat(product.product_price);
    }

    getProductTotalPrice(product) {
        var totalPrice = this.getProductPrice(product) * parseInt(product.quantity);
        if (product.supplies && product.supplies.length > 0) {
            totalPrice = totalPrice + (this.calculateSuppliesPrice(product.supplies) * parseInt(product.quantity));
        }
        return totalPrice;
    }

    getTotalPriceForItems(items) {
        var total = 0;
        for (let data of items) {
            total = total + this.getProductTotalPrice(data);
        }
        return total;
    }

    getDelieveryPriceForOrder(order) {
        return parseFloat(order.delievery_price);
    }

    getTotalPriceForOrder(order) {
        var total = this.getTotalPriceForItems(order.items);
        return total + this.getDelieveryPriceForOrder(order);
    }

    calculateSuppliesPrice(suppliesStr) {
        let supplies = JSON.parse(suppliesStr);
        var totalSuppliesPrice = 0;
        for (var i = 0; i < supplies.length; i++) {
            let supply = supplies[i];
            totalSuppliesPrice = totalSuppliesPrice + parseFloat(supply.supplies_price);
        }
        return totalSuppliesPrice;
    }

}
