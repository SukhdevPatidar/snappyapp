import { Component } from '@angular/core';
import { NavController, NavParams , ModalController , AlertController } from 'ionic-angular';
import { Storage } from '../../providers/storage';
import { ApiService } from '../../providers/api-service';
import { ThankYou } from '../thank-you/thank-you';
import { Http, RequestOptions, Headers } from '@angular/http';
// import { Md5 } from 'ts-md5/dist/md5';
import {InAppBrowser} from 'ionic-native';
import { DataStore } from '../../providers/data-store';

import { PaymentError }  from '../payment-error/payment-error';
import {Firebase} from "@ionic-native/firebase";

/*
  Generated class for the Paymentoptions page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
  	selector: 'page-paymentoptions',
  	templateUrl: 'paymentoptions.html',
    providers: [ApiService, Storage, DataStore]
  })
  export class PaymentOptions {
  	remainingAmount = 0;
    storage:Storage;
    dataStore: DataStore;
    
    userType = 'Normal';

    //If user type Wallet
    walletInfo;
    walletOptionFlag = true;
    deductedWalletAmount = 0;
    walletAmount = 0;

    orderData = {};

    //PAY U
    payUtestUrl = "https://test.payu.in/_payment";
    // this.payUtestUrl = "http://localhost:8100/payUApi/_payment";

    payUSalt = "eCwWELxi";
    payUKey = "gtKFFx";
    payUSuccessUrl = "httpss://payu.herokuapp.com/ios_success";
    payUFailureUrl = "httpss://payu.herokuapp.com/ios_failure";
    payUCancelUrl = "httpss://payu.herokuapp.com/ios_cancel";
    payUAuthorization = "4sOSsCZXIopj4XvbddLX8kF7tmlTu2UZsjHVAwPt404=";

    isTemporaryDisableButton = false;

    constructor(
        public http: Http,
        public modalCtrl: ModalController,
        private alertCtrl: AlertController,
        public navCtrl: NavController,
        public navParams: NavParams,
        public  apiService: ApiService,
        private firebase: Firebase
    ) {

      this.orderData = navParams.get('order_data');

      this.storage = Storage.getInstance();
      this.dataStore = DataStore.getInstance();

      this.userType = this.storage.getUser().type;

      this.remainingAmount = this.orderData['amount'];

      if(this.userType == 'Wallet') {
        this.getWallet();
      } 
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad PaymentoptionsPage');
      this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Paymentoptions"});

    }

    temporaryDisableButton() {
      var ref = this;
      this.isTemporaryDisableButton = true;
      setTimeout(function() {
        ref.isTemporaryDisableButton = false;
      }, 3000);
    }

    getWallet() {
      var ref = this;
      let userId = this.storage.getUser().user_id;

      this.apiService.showLoading();
      this.apiService.wallet(userId).then((response) => {
        this.apiService.hideLoading();
        if(response) {
          ref.walletInfo = response;
          ref.walletAmount = ref.walletInfo['full'];
          // this.setWalletOption();
        }
      },
      (error) => {
        this.apiService.hideLoading();
      });
    } 

    paymentWithWallet() {
      if(this.isTemporaryDisableButton) {
        return;
      }
      this.temporaryDisableButton();

      let alert = this.alertCtrl.create({
        title: 'Enter login password',
        inputs: [      
        {
          name: 'password',
          placeholder: 'Password',
          type: 'password'
        }
        ],
        buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: data => {
            var user = this.storage.getUser();

            this.apiService.showLoading();
            this.apiService.payFromWallet(data.password, user['phonenumber'], user['user_id'], this.orderData['order_id']).then(response => {
              this.apiService.hideLoading();

              if(response['status']) {
                let t_id = response['transaction'];
                this.paymentSuccess("Transaction completed transaction id "+t_id);
              } else {
                this.apiService.showAlert(response['msg']);
              }
            },
            error => {
              this.apiService.hideLoading();
              this.apiService.showAlert("PAYMENT FAILED");
            });
          }
        }
        ]
      });
      alert.present();
    }




    payByBank() {
      if(this.isTemporaryDisableButton) {
        return;
      }
      this.temporaryDisableButton();

      var ref = this;

      var key = this.payUKey;
      var txnId = this.generateTxnId();
      var fname = this.storage.getUser().firstname;
      var lname = this.storage.getUser().lastname;
      var email = this.storage.getUser().email;
      var phonenumber = this.storage.getUser().phonenumber;
      var surl = this.payUSuccessUrl;
      var furl = this.payUFailureUrl;
      var curl = this.payUCancelUrl;

      var productinfo = JSON.stringify(this.orderData['product']);

      ref.apiService.showLoading();

      ref.apiService.orderBillPayment(ref.orderData['order_id'], "payu", "pending", txnId, ref.orderData['amount'], null).then(response => {

        ref.apiService.hideLoading();

        if(response['status'] == 'Success') {
          ref.generateHash(key, txnId, ref.remainingAmount, productinfo, fname, email, "", "", "", "", "", ref.payUSalt, 
            function(hash) {
              ref.postRequest(ref.payUtestUrl, { 
                key: key,
                txnid: txnId,
                firstname: fname,
                lastname: lname,
                email: email,
                phone: phonenumber,
                productinfo: productinfo,
                amount: ref.remainingAmount,
                surl: surl,
                furl: furl,
                curl:curl,
                hash: hash
              }).then(response => {
                var url = response['url'];
                if(url) {
                  var browser = ref.openInAppBrowserForPayU(url, function(loadingUrl) {
                    if(loadingUrl.indexOf(ref.payUSuccessUrl) !== -1 || loadingUrl.indexOf(ref.payUFailureUrl) !== -1) {
                      browser.close();

                      var status = "failed";
                      if(loadingUrl.indexOf(ref.payUSuccessUrl) !== -1) {
                        console.log("PAYMENT SUCCESS");                    
                        status = "success";
                      } else {
                        console.log("PAYMENT FAILED");
                        status = "failed";
                      }
                      ref.apiService.showLoading();

                      ref.apiService.orderBillPayment(ref.orderData['order_id'], "payu", status, txnId, ref.orderData['amount'], null).then(response => {
                        ref.apiService.hideLoading();
                        
                        if(status == 'success') {
                          ref.paymentSuccess("Payment success.<br/>Transaction id : "+txnId);
                        } else {
                          ref.navCtrl.push(PaymentError, null, {animate:false});
                        }
                      },
                      error => {
                        ref.apiService.hideLoading();
                      });
                    }                   
                  });
                }
              },
              error => {
                console.log("Pay U Error "+JSON.stringify(error));
              });
            });
        } else {
          ref.apiService.showAlert(response['msg']);
        }        
      },
      error => {
        ref.apiService.hideLoading();
        console.log("Error in orderBillPayment "+JSON.stringify(error));
      });
    }

    postRequest(url,postData) {
      var ref = this;

      return new Promise((resolve, reject) => {

        let body = postData;

        let headers = new Headers({ 'Content-Type': 'application/json', 
          'Authorization': this.payUAuthorization }
          );
        let options = new RequestOptions({ headers: headers });

        this.http.post(url, this.apiService.QueryStringBuilder(body) ,options)
        .map((res) => res)
        .subscribe(data => {
          console.log("Data  "+data);
          resolve(data);
        },
        (error) => {
          console.log("Error "+error);
          ref.apiService.hideLoading();
          reject(error);
        }
        );
      });
    }


    paymentSuccess(msg) {
      var ref = this;
      this.dataStore.emptyCart().then((response) => {
        if(response['success']) {
          ref.navCtrl.push(ThankYou,{
            message:msg
          });
        }
      },
      (error) => {
        console.log("Error in removing item "+JSON.stringify(error));
      });
    }

    generateTxnId()
    {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for( var i=0; i < 15; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      return text;
    }

    /**
    * Generate hash string
    **/
    generateHash(key, txnid, amount, productinfo, firstname, email, udf1, udf2, udf3, udf4, udf5, salt, callBack) {
      var hashStr = key+"|"+txnid+"|"+amount+"|"+productinfo+"|"+firstname+"|"+email+"|"+udf1+"|"+udf2+"|"+udf3+"|"+udf4+"|"+udf5+"||||||"+salt;
      var params = {data: hashStr, hash: "sha-512"};
      if( window['hashString']) {
        var hashStringFun = window['hashString'];
        hashStringFun(params, function(hash) {
          console.log(params.hash + ": " + hash);
          if(callBack) {
            callBack(hash.toLowerCase());
          }
        }); 
      }
    }

    /**
    * Open In App Browser for payment gateway
    * @param paymentUrl - url for payment gateway
    * @param loadStartCallback - callback for get event when start loading url 
    * @returns  InAppBrowser - InAppBrowser reference object
    **/
    openInAppBrowserForPayU(paymentUrl, loadStartCallback) {
      let browser = new InAppBrowser(paymentUrl, '_self');
      browser.show();

      browser.on("loadstart").subscribe(event => {
        console.log("LOAD START "+JSON.stringify(event));
        if(loadStartCallback) {
          var url = event.url;
          loadStartCallback(url);
        }
      });        
      browser.on("loadstop").subscribe(event => {
        console.log("LOAD STOP "+JSON.stringify(event));
      });   
      browser.on("loaderror").subscribe(event => {
        console.log("LOAD ERROR "+JSON.stringify(event));
        browser.close();
      });   
      return browser;
    }

    confirmOrderOnServer() {

      if(this.orderData['order_type'] == "TakeAway") {
        this.apiService.showLoading();     
        this.apiService.placeOrderForTakeaway(
          this.orderData['date'], 
          this.orderData['time'],
          this.storage.getUser().user_id, 
          this.orderData['items'], 
          this.orderData['notes']
          ).then(data => {
            this.apiService.hideLoading();     

            if(data['status'] == "Success") {
              if(data['order_id']) {
                this.storage.updateOrderId(data['order_id']);

                this.dataStore.emptyCart().then((response) => {
                  if(response['success']) {
                    this.paymentSuccess(data['msg']);
                  }
                },
                (error) => {
                  console.log("Error in removing item "+JSON.stringify(error));
                });
              }
            }
          },
          error => {  
            this.apiService.hideLoading();     
            this.apiService.showAlert("PROBLEM IN PLACE ORDER");      
            console.log("Error getRestaurantTable : "+JSON.stringify(error));
          });
        } else if(this.orderData['order_type'] == "GetDelivery") {
          this.apiService.showLoading();     

          this.apiService.placeOrderForDelivery(
            this.orderData['address_id'], 
            this.orderData['date'], 
            this.orderData['time'],
            this.storage.getUser().user_id, 
            this.orderData['items'], 
            this.orderData['notes']
            ).then(data => {
              this.apiService.hideLoading();     

              if(data['status'] == "Success") {
                if(data['order_id']) {
                  this.storage.updateOrderId(data['order_id']);

                  this.dataStore.emptyCart().then((response) => {
                    if(response['success']) {
                      this.paymentSuccess(data['msg']);
                    }
                  },
                  (error) => {
                    console.log("Error in removing item "+JSON.stringify(error));
                  });
                }
              }
            },
            error => {  
              this.apiService.hideLoading();     
              this.apiService.showAlert("PROBLEM IN PLACE ORDER");      
              console.log("Error getRestaurantTable : "+JSON.stringify(error));
            });
          }
        }
      }
