import { Component } from '@angular/core';
import { NavController, NavParams , Platform } from 'ionic-angular';
import { Category } from '../category/category';
import {Firebase} from "@ionic-native/firebase";
import {MainCategoryPagePage} from "../main-category/main-category";

/*
  Generated class for the ThankYou page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
  	selector: 'page-thank-you',
  	templateUrl: 'thank-you.html'
  })
  export class ThankYou {
    message:string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public platform:Platform,
        private firebase: Firebase

    ) {
      this.message = navParams.get('message');
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad ThankYouPage');
      this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Thankyou"});

    }

    continue() {
      let callBack = this.navParams.get('callBack');

      this.navCtrl.pop({animate:false});
      if(callBack) {
        callBack();
      } else {
        this.navCtrl.setRoot(MainCategoryPagePage);
      }
    }
  }
