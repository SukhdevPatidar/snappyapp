import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams, Platform, Slides} from 'ionic-angular';
import {Items} from '../items/items';
import {ApiService} from '../../providers/api-service';
import {DataStore} from '../../providers/data-store';
import {Storage} from '../../providers/storage';
import {PaymentError} from "../payment-error/payment-error";
import {InAppBrowser} from "ionic-native";
import {CallNumber} from '@ionic-native/call-number';
import {Firebase} from "@ionic-native/firebase";
import {ProductsSubcatPage} from "../products-subcat/products-subcat";


@Component({
    selector: 'page-category',
    templateUrl: 'category.html',
    providers: [ApiService, DataStore]
})

export class Category {
    @ViewChild(Slides) slides: Slides;


    dataStore: DataStore;
    storage: Storage;
    refresher;

    categories: any;
    sliders: any;
    showRetryButton = false;

    searchInput = "";
    searchNeed = false;
    searchInProcess = false;

    businessGroupSubCat;

    constructor(
        private callNumber: CallNumber,
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiService: ApiService,
        public platform: Platform,
        private firebase: Firebase
    ) {
        this.dataStore = DataStore.getInstance();
        this.storage = Storage.getInstance();
        this.businessGroupSubCat = this.navParams.get('businessGroupSubCat');

        this.platform.ready().then((readySource) => {
            this.getRestaurantCategoryFromApi(undefined);
        });
    }

    ionViewDidLoad() {
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Category"});
    }

    openCategoryProducts(category) {
        if (category.availabilityData.isAvailable) {
            if (category.onlycalltaxi == 1 || category.onlycalltaxi == '1') {
                this.callNumber.callNumber(category.contact_number, true)
                    .then(res => console.log('Launched dialer!', res))
                    .catch(err => console.log('Error launching dialer', err));
            } else if (category.onlyopenurl == 1 || category.onlyopenurl == '1') {
                this.openInAppBrowser(category.url);
            } else {
                this.navCtrl.push(ProductsSubcatPage,
                    {
                        category: category,
                    });
            }

        } else {
            this.apiService.showAlert('YOU_CANT_BUY_PRODUCT_MSG');
        }
    }

    bannerClick(item) {
        console.log(JSON.stringify(item));
        if (item.url) {
            console.log(item.url);
            this.openInAppBrowser(item.url);
        }
    }

    generateHash(key, txnid, amount, productinfo, firstname, email, udf1, udf2, udf3, udf4, udf5, salt, callBack) {
        var hashStr = key + "|" + txnid + "|" + amount + "|" + productinfo + "|" + firstname + "|" + email + "|" + udf1 + "|" + udf2 + "|" + udf3 + "|" + udf4 + "|" + udf5 + "||||||" + salt;

        // var SHA256 = require("crypto-js/sha256");
        //
        // let hash = SHA256(hashStr);
        // callBack(hash);
    }


    getRestaurantCategory() {
        this.dataStore.getCategories().then(response => {
                this.showRetryButton = false;
                console.log("getCategories success " + JSON.stringify(response));
                this.categories = response['data'];
            },
            error => {
                this.apiService.showLoading();

                this.getRestaurantCategoryFromApi(undefined);
                console.log("getCategories error " + JSON.stringify(error));
            });
    }

    getRestaurantCategoryFromApi(refresher) {
        this.refresher = refresher;
        let userCityId = this.storage.getUser()['city_id'];

        this.apiService.restaurantCategory(userCityId, this.businessGroupSubCat.businesstype_id, this.businessGroupSubCat.id, this.searchInput).then((data: any) => {
                this.showRetryButton = false;
                this.apiService.hideLoading();

                var list = [];
                if (data.status) {
                    const items = data['data'];
                    items.map((obj) => {
                        obj.availabilityData = this.checkAvailability(obj);
                        list.push(obj);
                    })
                    this.dataStore.emptyCategories().then(response => {
                            this.insertCategoriesInDB();
                        },
                        error => {
                            this.insertCategoriesInDB();
                        });
                }
                this.categories = list;
                this.checkRefreshFinish();
                this.checkForSearch();

            },
            err => {
                this.checkRefreshFinish();
                this.checkForSearch();
                this.categories = [];

                this.showRetryButton = true;
            });
    }

    checkRefreshFinish() {
        if (this.refresher) {
            this.refresher.complete();
        }
    }

    insertCategoriesInDB() {
        this.dataStore.insertAllCategories(this.categories).then(response => {
                console.log("insertAllCategories success " + JSON.stringify(response));
                if (this.refresher) {
                    this.refresher.complete();
                }
            },
            error => {
                console.log("insertAllCategories error " + JSON.stringify(error));
                if (this.refresher) {
                    this.refresher.complete();
                }
            });
    }

    getImagePath(imgName) {
        return this.apiService.getBaseApiUrl() + imgName;
    }

    doRefresh(refresher) {
        this.getRestaurantCategoryFromApi(refresher);
    }

    checkForSearch() {
        this.searchInProcess = false;
        if (this.searchNeed) {
            this.searchNeed = false;
            this.getRestaurantCategoryFromApi(false);
        }
    }

    searchProducts(event) {
        this.searchNeed = true;
        if (!this.searchInProcess) {
            this.searchInProcess = true;
            this.searchNeed = false;
            this.getRestaurantCategoryFromApi(false);
        }
    }

    checkAvailability(item) {
        let message = "";
        let status = false;
        let timerange = "";

        if (!item.availibilty) {
            message = "TODAY_CLOSED";
            status = false;
        } else {
            const availibilty = item.availibilty;
            var d = new Date();
            var weekday = d.getDay();
            if (weekday == 0) {
                weekday = 6;
            } else {
                weekday = weekday - 1;
            }
            const todayTimeSchedule = availibilty[weekday];
            if (todayTimeSchedule.open_status == 1) {
                message = "TODAY_CLOSED";
                status = false;
            } else if (todayTimeSchedule.open_status == 2) {
                message = "OPEN_FULL_DAY";
                status = true;
            } else {
                var today = new Date();
                var formattedtoday = today.getFullYear() + "/" + (today.getMonth() + 1) + '/' + today.getDate();


                //Abierto neste momento
                //Cerrado por el momento
                const openingTime = todayTimeSchedule.opening_time;
                const closingTime = todayTimeSchedule.closing_time;
                var openingTimeStr = formattedtoday + " " + openingTime;
                var closingTimeStr = formattedtoday + " " + closingTime;

                var openingTimeDate = new Date(openingTimeStr).getTime();
                var closingTimeDate = new Date(closingTimeStr).getTime();
                var nowDate = new Date().getTime();
                if (nowDate >= openingTimeDate && nowDate <= closingTimeDate) {
                    message = "OPEN_AT_THE_MOMENT";
                    status = true;
                } else {
                    message = "CLOSE_AT_THE_MOMENT";
                    status = false;
                }
                timerange = openingTime + " " + closingTime;
            }
        }

        return {
            message,
            isAvailable: status,
            timerange
        };
    }

    openInAppBrowser(url, loadStartCallback = null) {
        let browser = new InAppBrowser(url, '_self');
        if (browser) {
            browser.show();

            browser.on("loadstart").subscribe(event => {
                console.log("LOAD START " + JSON.stringify(event));
                if (loadStartCallback) {
                    var url = event.url;
                    loadStartCallback(url);
                }
            });
            browser.on("loadstop").subscribe(event => {
                console.log("LOAD STOP " + JSON.stringify(event));
            });
            browser.on("loaderror").subscribe(event => {
                console.log("LOAD ERROR " + JSON.stringify(event));
                browser.close();
            });
        }
        return browser;
    }

}
