import {Component} from '@angular/core';

import {NavController, NavParams} from 'ionic-angular';
import {Checkout} from '../checkout/checkout';
import {Login} from '../login/login';
import {ApiService} from '../../providers/api-service';
import {Storage} from '../../providers/storage';
import {DataStore} from '../../providers/data-store'
import {Firebase} from "@ionic-native/firebase";

@Component({
    selector: 'page-cartview',
    templateUrl: 'cartview.html',
    providers: [DataStore, ApiService]
})
export class CartView {
    icons: string[];
    items = [];
    dataStore: DataStore;
    taxes = {};
    storage: Storage;

    constructor(
        public  navCtrl: NavController,
        public  navParams: NavParams,
        public  apiService: ApiService,
        private firebase: Firebase
    ) {
        this.dataStore = DataStore.getInstance();
        this.storage = Storage.getInstance();

        // If we navigated to this page, we will have an item available as a nav param

        this.getItems();

    }

    ionViewDidLoad() {
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Cartview"});
    }

    toCheckout() {
        if (this.storage.isLoggedIn()) {
            this.navCtrl.push(Checkout);
        } else {
            this.navCtrl.push(Login, {
                'PageType': 'Checkout'
            });
        }
    }

    removeItem(item, index) {
        this.dataStore.removeItemFromCart(item.id).then((response) => {
                if (response['success']) {
                    this.apiService.showToast("ITEM SUCCESSFULLY REMOVED");
                    if (this.items.length > 0) {
                        this.items.splice(index, 1);

                        if (this.items.length > 0) {
                            console.log("Cart Updated");
                        } else {
                            console.log("Cart Empty");
                            this.navCtrl.pop();
                        }
                    }
                }
            },
            (error) => {
                console.log("Error removing item " + JSON.stringify(error));
            });
    }

    quantityChanged(item) {
        this.dataStore.updateItemQuantity(item.id, item.quantity, item.item).then((response) => {
                if (response['success']) {

                }
            },
            (error) => {
                console.log("Error geting item " + JSON.stringify(error));
            });
    }

    suppliesUpdate(item) {
        this.dataStore.updateItemSupplies(item.id, item.item).then((response) => {
                if (response['success']) {

                }
            },
            (error) => {
                console.log("Error geting item " + JSON.stringify(error));
            });
    }

    getItems = () => {
        this.items = [];

        this.dataStore.getItemsFromCart().then((response) => {
                if (response['success']) {
                    this.items = response['data'];
                    console.log(JSON.stringify(this.items));
                }
            },
            (error) => {
                console.log("Error geting item " + JSON.stringify(error));
            });
    }

    getImagePath(imgName) {
        return this.apiService.getBaseApiUrl() + imgName;
    }

    getTotalPrice() {
        var total = 0;
        var shoppingPrice = 0;
        for (var i = 0; i < this.items.length; ++i) {
            let data = this.items[i];
            let qty = data['quantity'];
            let price = data['item'].price;
            let supplies = data['item'].supplies;

            var suppliesPrice = 0;
            if (supplies && supplies.length > 0) {
                for (var j = 0; j < supplies.length; j++) {
                    let supply = supplies[j];
                    if (supply.selected) {
                        suppliesPrice = suppliesPrice + (parseFloat(supply.supplies_price) * qty);
                    }
                }
            }


            if (data['item'].delievery_price) {
                shoppingPrice = parseFloat(data['item'].delievery_price);
            }

            total = total + (qty * price) + suppliesPrice;


        }
        total = total;

        return total;
    }

    ifMaxSelected(item) {
        return item.supplies.filter(data => data.selected).length === parseInt(item.max_supplies_select);
    }
}
