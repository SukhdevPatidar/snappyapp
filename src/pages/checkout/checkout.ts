import {Component} from '@angular/core';

import {NavController, AlertController, NavParams} from 'ionic-angular';
import {DataStore} from '../../providers/data-store';
import {ApiService} from '../../providers/api-service';
import {Storage} from '../../providers/storage';

import {ChooseTable} from '../choose-table/choose-table';
import {ConfirmOrder} from '../confirm-order/confirm-order';

import {Category} from '../category/category';
import {Firebase} from "@ionic-native/firebase";
import {MainCategoryPagePage} from "../main-category/main-category";

@Component({
    selector: 'page-checkout',
    templateUrl: 'checkout.html',
    providers: [DataStore, ApiService]
})
export class Checkout {
    selectedItem: any;
    icons: string[];
    items: Array<{ title: string, note: string, icon: string }>;
    dataStore: DataStore;
    taxes = {};
    storage: Storage;
    selectedPaymentModeCode = 'TAKE AWAY';


    dataList = [];

    constructor(
        public  navCtrl: NavController,
        public  navParams: NavParams,
        public  apiService: ApiService,
        public alertCtrl: AlertController,
        private firebase: Firebase
    ) {
        this.dataStore = DataStore.getInstance();
        this.storage = Storage.getInstance();

        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');

        this.getItems();

    }

    ionViewDidLoad() {
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Checkout"});
    }

    proceedOrder(byPayoption) {
        this.navCtrl.push(ConfirmOrder, {
            'byPayoption': byPayoption, 'callBack': () => {
                this.getItems();
            }
        });
    }

    getItems() {
        this.items = [];

        this.dataStore.getItemsFromCart().then((response) => {

                if (response['success']) {
                    this.items = response['data'];
                    if (this.items && this.items.length > 0) {
                        var itemsByProvider = {};
                        for (var i = 0; i < this.items.length; ++i) {
                            let item = this.items[i];
                            var itemByPayoption = itemsByProvider[item['item']['user_id']];
                            if (!itemByPayoption) {
                                itemByPayoption = {};
                            }

                            let payoptions = item['item']['payment_options'];
                            var payopids = [];
                            for (var j = 0; j < payoptions.length; ++j) {
                                payopids.push(parseInt(payoptions[j].id));
                            }
                            payopids.sort(function (a, b) {
                                return a - b
                            });
                            let payOpUniqueId = payopids.join("_");

                            var dataList = itemByPayoption[payOpUniqueId];
                            if (!dataList) {
                                dataList = [];
                            }
                            dataList.push(item);
                            itemByPayoption[payOpUniqueId] = dataList;
                            itemsByProvider[item['item']['user_id']] = itemByPayoption;
                        }

                        var filteredArray = [];
                        for (var index in itemsByProvider) {
                            var byProvider = itemsByProvider[index];

                            var providerName;

                            var byPayOptList = [];
                            for (var key in byProvider) {
                                var byPayOption = byProvider[key];
                                providerName = byPayOption[0]['item']['sold_by'];

                                byPayOptList.push({
                                    payOptions: byPayOption[0]['item']['payment_options'],
                                    items: byPayOption,
                                    selectedPaymentModeCode: byPayOption[0]['item']['payment_options'][0].code
                                });
                            }

                            filteredArray.push({
                                providerName: providerName,
                                data: byPayOptList
                            });
                        }

                        this.dataList = filteredArray;

                    } else {
                        this.navCtrl.setRoot(MainCategoryPagePage);
                    }

                } else {
                    this.navCtrl.setRoot(MainCategoryPagePage);
                }
            },
            (error) => {
                console.log("Error geting item " + JSON.stringify(error));
            });
    }

    calculateSuppliesPrice(supplies) {
        var totalSuppliesPrice = 0;
        for (var i = 0; i < supplies.length; i++) {
            let supply = supplies[i];
            if (supply.selected) {
                totalSuppliesPrice = totalSuppliesPrice + parseFloat(supply.supplies_price);
            }
        }
        return totalSuppliesPrice;
    }

    getImagePath(imgName) {
        return this.apiService.getBaseApiUrl() + imgName;
    }

    getTotalPriceForItems(items) {
        var total = 0;
        for (let data of items) {
            let qty = data['quantity'];
            let price = data['item'].price;

            //supplies
            var suppliesPrice = 0;
            if (data['item'].supplies && data['item'].supplies.length > 0) {
                suppliesPrice = this.calculateSuppliesPrice(data['item'].supplies);
            }

            total = total + (qty * price) + (suppliesPrice * qty);
        }
        return total;
    }

    getDelieveryPriceForItems(items) {
        var delieveryPrice: any = 0;
        for (let data of items) {
            delieveryPrice = Number(data['item'].delievery_price);
        }
        return parseFloat(delieveryPrice);
    }

}
