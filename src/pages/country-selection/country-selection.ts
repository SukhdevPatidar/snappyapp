import {Component} from '@angular/core';
import {NavController, NavParams, Platform} from 'ionic-angular';

import {ApiService} from '../../providers/api-service';
import {DataStore} from '../../providers/data-store';
import {Storage} from '../../providers/storage';


import {Category} from '../category/category';
import {MainCategoryPagePage} from "../main-category/main-category";
import {Firebase} from "@ionic-native/firebase";

/*
  Generated class for the CountrySelectionPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
@Component({
    selector: 'page-country-selection',
    templateUrl: 'country-selection.html'
})
export class CountrySelectionPage {
    dataStore: DataStore;
    storage: Storage;

    countries = [];

    myCountry = 1;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiService: ApiService,
        public platform: Platform,
        private firebase: Firebase
    ) {
        this.dataStore = DataStore.getInstance();
        this.storage = Storage.getInstance();
        this.platform.ready().then((readySource) => {
            // this.getRestaurantCategory();
            this.getCountries();

        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CountrySelectionPage');
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "CountrySelectionPage"});

    }

    getCountries() {
        let userCityId = this.storage.getUser()['city_id'];
        this.apiService.showLoading();
        this.apiService.countriesList().then((data: any) => {
                this.apiService.hideLoading();
                this.countries = data.data;
            },
            err => {
                this.apiService.hideLoading();
            });
    }

    saveCountry() {
        for (var i = 0; i < this.countries.length; ++i) {
            if (this.countries[i].id == this.myCountry) {
                this.storage.setCountry(this.countries[i]);
                break;
            }
        }
        this.navCtrl.setRoot(MainCategoryPagePage);
    }

}
