import {Component} from '@angular/core';
import {NavController, NavParams, AlertController, Events, FabContainer} from 'ionic-angular';

import {AddressInfo} from '../address-info/address-info'

import {DataStore} from '../../providers/data-store'
import {Storage} from '../../providers/storage';
import {ApiService} from '../../providers/api-service';
import {Firebase} from '@ionic-native/firebase';

/*
  Generated class for the Address page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
@Component({
    selector: 'page-address',
    templateUrl: 'address.html',
    providers: [DataStore, Storage, ApiService]
})
export class Address {
    addressInfo = AddressInfo;
    dataStore: DataStore;
    addresses: Array<Object>;
    storage: Storage;
    options: string;

    selectedAddress = {}; // for choose address from  select_address

    constructor(
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public events: Events,
        public apiService: ApiService,
        public navParams: NavParams,
        private firebase: Firebase
    ) {
        this.dataStore = DataStore.getInstance();
        this.storage = Storage.getInstance();

        events.subscribe(this.dataStore.eventsKey().ADDRESS_MODIFY, (userEventData) => {
            // this.addresses = userEventData;
        });

        if (navParams.get("options") === "select_address") {
            this.options = navParams.get('options');
            this.selectedAddress = navParams.get('currentAddress');
        }
        // this.dataStore.getAddresses();

        this.getAddressesFromApi();
    }


    ionViewDidLoad() {
        console.log('Hello Address Page');
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Address"});
    }


    closeFabMenu(fab?: FabContainer) {
        console.log("Close Fab Menu Click");

        if (fab !== undefined) {
            fab.close();
        }
    }

    addressUpdateCallBack = () => {
        this.getAddressesFromApi();
    }

    getAddressesFromApi() {
        this.apiService.showLoading();
        this.apiService.getUserAddresses(this.storage.getUser()['id']).then(data => {
                this.apiService.hideLoading();

                if (data['status']) {

                    var user_address_details = data['data'];
                    if (user_address_details.length > 0) {

                        for (var i = 0; i < user_address_details.length; i++) {
                            var address = user_address_details[i];
                        }

                        this.addresses = user_address_details;

                        // this.dataStore.emptyAddresses().then((data) => {
                        //   if(data['success']) {
                        //     this.dataStore.insertAllAddresses(user_address_details).then((insertData) => {
                        //       if(insertData['success']) {
                        //         console.log(insertData['message']);
                        //       }
                        //     },
                        //     (error) => {
                        //       console.log("Error inserting addresses "+JSON.stringify(error));
                        //     });
                        //   }
                        // },
                        // (error) => {
                        //   console.log("Error removing addresses "+JSON.stringify(error));
                        // });

                    }
                }
            },
            (error) => {
                this.apiService.hideLoading();
            });
    }


    removeAddress(address) {
        let confirm = this.alertCtrl.create({
            title: 'Confirm !',
            message: 'Do you really want to remove this address?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('cancel clicked');
                    }
                },
                {
                    text: 'Remove',
                    handler: () => {
                        console.log('Agree clicked');
                        this.removeAddressFromApi(address.id);
                    }
                }
            ]
        });
        confirm.present();
    }

    removeAddressFromApi(addressId) {
        this.apiService.showLoading();
        this.apiService.deleteAddress(
            this.storage.getUser()['id'],
            addressId
        ).then(data => {
                this.apiService.hideLoading();
                console.log("Address delete Response " + JSON.stringify(data));
                if (data['status']) {


                    this.apiService.showToast(data['message']);

                    this.dataStore.removeAddress(addressId);
                    this.removeAddressFromArray(addressId);
                } else {
                    this.apiService.showAlert(data['message']);
                }
            },
            (error) => {
                this.apiService.hideLoading();
                console.log("Address delete error " + JSON.stringify(error));
            });
    }

    removeAddressFromArray = (addressId) => {
        for (var i = 0; i < this.addresses.length; ++i) {
            let address: any = this.addresses[i];
            if (address.id == addressId) {
                this.addresses.splice(i, 1);
                break;
            }
        }
    }

    addressChoose(address) {
        if (this.navParams.get("options") === "select_address") {
            this.selectedAddress = address;
            var callback = this.navParams.get('callback');
            if (callback) {
                setTimeout(() => {
                    callback(address);
                    this.navCtrl.pop();
                }, 500);
            }
        }
    }


    addressAddCallBack = () => {
        this.getAddressesFromApi();
    }

    combineAddress(address) {
        let mobile = address.phone;
        let pincode = address.postal_code;
        let address1 = address.flat_or_house_no;
        let address2 = address.apartment_or_locality;
        let landmark = address.landmark;
        let townCity = address.city;
        let state = address.address;
        let country = address.company;

        var addressString = "";
        if (address1 && address1.length > 0) {
            addressString += address1 + "<br />";
        }
        if (address2 && address2.length > 0) {
            addressString += address2 + "<br />";
        }
        if (landmark && landmark.length > 0) {
            addressString += landmark + "<br />";
        }
        if (townCity && townCity.length > 0) {
            addressString += townCity + "<br />";
        }
        if (state && state.length > 0) {
            addressString += state + " " + pincode + "<br />";
        }
        if (country && country.length > 0) {
            addressString += country + "<br />";
        }
        if (mobile && mobile.length > 0) {
            addressString += "Phone : " + mobile;
        }

        return addressString;
    }

    combineAddressInSingleLine(address) {
        let mobile = address.phone;
        let pincode = address.postal_code;
        let address1 = address.flat_or_house_no;
        let address2 = address.apartment_or_locality;
        let landmark = address.landmark;
        let townCity = address.city;
        let state = address.address;
        let country = address.company;

        var addressString = "";
        if (address1 && address1.length > 0) {
            addressString += address1 + " ";
        }
        if (address2 && address2.length > 0) {
            addressString += address2 + " ";
        }
        if (landmark && landmark.length > 0) {
            addressString += landmark + " ";
        }
        if (townCity && townCity.length > 0) {
            addressString += townCity + " ";
        }
        if (state && state.length > 0) {
            addressString += state + " " + pincode + " ";
        }
        if (country && country.length > 0) {
            addressString += country + " ";
        }
        if (mobile && mobile.length > 0) {
            addressString += "Phone : " + mobile;
        }

        return addressString;
    }
}
