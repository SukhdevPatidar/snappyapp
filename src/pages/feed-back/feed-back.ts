import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ApiService } from '../../providers/api-service';
import { Storage } from '../../providers/storage';
import { Category } from '../category/category'
import {Firebase} from "@ionic-native/firebase";
import {MainCategoryPagePage} from "../main-category/main-category";

/*
  Generated class for the FeedBack page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
  	selector: 'page-feed-back',
  	templateUrl: 'feed-back.html',
    providers: [ApiService]
  })
  export class FeedBack {
  	storage:Storage;

  	rateOptions:any;

  	hotelname:string = "";
  	restaurantLogo:string = "";

  	commentText = "";

  	constructor(
  		public navCtrl: NavController,
		public  apiService: ApiService,
		private firebase: Firebase
	) {
  		this.storage = Storage.getInstance();

  		this.rateOptions = {
  			'service'		:{name:'SERVICE',rating:1},
  			'deliveryTime'			:{name:'DELIVERY TIME',rating:1},
  			'staffBehave'	:{name:'STAFF BEHAVIOR',rating:1}
  		};

  	}

  	ionViewDidLoad() {
  		console.log('Hello FeedBack Page');
		this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "FeedBack"});
	}

  	restaurantDetails() {

  		this.apiService.showLoading();
  		this.apiService.restaurantDetails().then(data => {
  			this.apiService.hideLoading();

  			let restaurantDetails = data['restaurant_details'];
  			let restaurantLogo = data['restaurant_logo'];

  			if(restaurantDetails) {
  				let resDtl = restaurantDetails[0];
  				let name = resDtl['name'];
  				if(name) {
  					this.hotelname = name;
  				}
  			}
  			if(restaurantLogo) {
  				this.restaurantLogo = restaurantLogo;
  			}
  		},
  		error => {
  			this.apiService.hideLoading();
  			console.log("Error restaurantDetails : "+JSON.stringify(error));
  		});
  	}

  	submit() {
  		if(this.commentText.length < 10) {
  			this.apiService.showAlert("PLEASE ENTER COMMENT WITH MINIMUM 10 CHARS");
  			return;
  		}

  		let userId = this.storage.getUser().id;

  		this.apiService.showLoading();
  		this.apiService.addFeedback(userId, this.rateOptions.service.rating, this.rateOptions.deliveryTime.rating, this.rateOptions.staffBehave.rating,  this.commentText).then(data => {
  			this.apiService.hideLoading();

  			let status = data['status'];
        if(status) {
          this.goToHome();
          this.apiService.showAlert(data['message']);
        } else {
          this.apiService.showAlert(data['error']);
        }
      },
      error => {
        this.apiService.hideLoading();
        console.log("Error restaurantDetails : "+JSON.stringify(error));
      });
  	}

  	getImagePath(imgName) {
  		return this.apiService.getBaseApiUrl()+imgName;
  	}

  	goToHome() {
      this.navCtrl.setRoot(MainCategoryPagePage);
    }
  }
