import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {DateFormat} from '../../providers/date-format';
import {Firebase} from "@ionic-native/firebase";

/*
  Generated class for the WalletTransactions page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
@Component({
    selector: 'page-wallet-transactions',
    templateUrl: 'wallet-transactions.html',
    providers: [DateFormat]

})
export class WalletTransactions {
    lastTransactions;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private firebase: Firebase
    ) {
        this.lastTransactions = navParams.get('last-transaction');
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad WalletTransactionsPage');
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "WalletTransactions"});
    }

    dateStrToFormattedStr(dateSTr) {
        var dateObj = DateFormat.shared().ddmmyyyyStrToDate(dateSTr, "-");
        return DateFormat.shared().getMonthName(dateObj) + " " + dateObj.getDate() + ", " + dateObj.getFullYear();
    }
}
