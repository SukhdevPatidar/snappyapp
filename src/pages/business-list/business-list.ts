import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams, Slides} from 'ionic-angular';
import {Storage} from "../../providers/storage";
import {DataStore} from "../../providers/data-store";
import {ApiService} from "../../providers/api-service";
import {ItemDetails} from "../itemdetails/itemdetails";
import {Category} from "../category/category";
import {Firebase} from "@ionic-native/firebase";

/*
  Generated class for the BusinessListPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-business-list',
    templateUrl: 'business-list.html'
})
export class BusinessListPagePage {

    storage: Storage;
    dataStore: DataStore;
    refresher;
    tagResponseCount = 0;

    @ViewChild('mySlider') slider: Slides;


    items = [];

    searchInput = "";
    searchNeed = false;
    searchInProcess = false;

    businessGroup;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public apiService: ApiService,
        private firebase: Firebase
    ) {
        this.dataStore = DataStore.getInstance();
        this.storage = Storage.getInstance();

        // If we navigated to this page, we will have an item available as a nav param
        this.businessGroup = navParams.get('businessGroup');

        this.getBusiness(true);
    }


    getBusiness = (showLoading) => {
        let cateId = this.navParams.get('catId');
        let userCityId = this.storage.getUser()['city_id'];

        if (showLoading) {
            this.apiService.showLoading();
        }


    }

    ionViewDidLoad() {
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Business List"});
    }

    doRefresh(refresher) {

        this.refresher = refresher;
        this.getBusiness(false);
    }


    getImagePath(imgName) {
        if (!imgName) {
            return "./assets/img/bg3.jpeg";
        }
        return this.apiService.getBaseApiUrl() + imgName;
    }

    checkRefreshFinish() {
        if (this.refresher) {
            this.refresher.complete();
        }
    }


    checkForSearch() {
        this.searchInProcess = false;
        if (this.searchNeed) {
            this.searchNeed = false;
            this.getBusiness(false);
        }
    }

    searchProducts(event) {
        this.searchNeed = true;
        if (!this.searchInProcess) {
            this.searchInProcess = true;
            this.searchNeed = false;
            this.getBusiness(false);
        }
    }

    openBusinessProducts(business) {
        this.navCtrl.push(Category, {
            business: business
        });
    }

    checkAvailability(item) {
        let message = "";
        let status = false;
        let timerange = "";

        if (!item.availibilty) {
            message = "Today Closed";
            status = false;
        } else {
            const availibilty = item.availibilty;
            var d = new Date();
            var weekday = d.getDay();
            if (weekday == 0) {
                weekday = 6;
            } else {
                weekday = weekday - 1;
            }
            const todayTimeSchedule = availibilty[weekday];
            if (todayTimeSchedule.open_status == 1) {
                message = "Today Closed";
                status = false;
            } else if (todayTimeSchedule.open_status == 2) {
                message = "Open full day";
                status = true;
            } else {
                var today = new Date();
                var formattedtoday = today.getFullYear() + "/" + (today.getMonth() + 1) + '/' + today.getDate();


                //Abierto neste momento
                //Cerrado por el momento
                const openingTime = todayTimeSchedule.opening_time;
                const closingTime = todayTimeSchedule.closing_time;
                var openingTimeStr = formattedtoday + " " + openingTime;
                var closingTimeStr = formattedtoday + " " + closingTime;

                var openingTimeDate = new Date(openingTimeStr).getTime();
                var closingTimeDate = new Date(closingTimeStr).getTime();
                var nowDate = new Date().getTime();
                if (nowDate >= openingTimeDate && nowDate <= closingTimeDate) {
                    message = "Open at the moment";
                    status = true;
                } else {
                    message = "Close at the moment";
                    status = false;
                }
                timerange = openingTime + " " + closingTime;
            }
        }

        return {
            message,
            isAvailable: status,
            timerange
        };
    }
}
