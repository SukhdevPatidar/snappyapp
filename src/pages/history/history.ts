import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ApiService } from '../../providers/api-service';
import { Storage } from '../../providers/storage';
import {Firebase} from "@ionic-native/firebase";

/*
  Generated class for the History page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
  	selector: 'page-history',
  	templateUrl: 'history.html',
  	providers: [ApiService]
  })
  export class History {
  	storage:Storage;
  	ordersList:Array<any>;

  	constructor(
  	    public navCtrl: NavController,
        public  apiService: ApiService,
        private firebase: Firebase
    ) {
  		this.storage = Storage.getInstance();
  	}

  	ionViewDidLoad() {
  		console.log('Hello History Page');

      this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "History"});


      this.getOrderDetails(this.storage.getUser().id);
    }



    getOrderDetails(orderId) {

      this.apiService.showLoading();

      this.apiService.getOrders(orderId).then((response) => {
        this.apiService.hideLoading();    
        if(response['status']) {
          this.ordersList = response['data'];
        } else {
          this.ordersList = [];
        }
      },
      (error) => {
        this.apiService.hideLoading();    
        this.ordersList = []; 
      });
    }



    getImagePath(imgName) {
      return this.apiService.getBaseApiUrl()+imgName;
    }



    getProductPrice(product) {
      if(product.promotional_price) {
        return parseFloat(product.promotional_price);
      }
      return parseFloat(product.product_price);
    }

    getProductTotalPrice(product) {
      return this.getProductPrice(product)*parseInt(product.quantity);
    }

    getTotalPriceForItems(items) {
      var total = 0;
      for (let data of items) {
        total = total + this.getProductTotalPrice(data);
      }
      return total;
    }

    getDelieveryPriceForOrder(order) {
      return parseFloat(order.delievery_price);
    }

    getTotalPriceForOrder(order) {
      var total = this.getTotalPriceForItems(order.items);
      return total+this.getDelieveryPriceForOrder(order);
    }


    forrmatedDateTime(dateTimeStr) {
      var dateTimeStrArr = dateTimeStr.split(' ');
      if(dateTimeStrArr.length == 3) {
        return dateTimeStrArr[0]+"<br>"+dateTimeStrArr[1]+" "+dateTimeStrArr[2];
      }
      return dateTimeStr;
    }
  }
