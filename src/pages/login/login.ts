import {Component} from '@angular/core';
import {NavController, ViewController, NavParams, AlertController} from 'ionic-angular';
import {ApiService} from '../../providers/api-service';
import {DateFormat} from '../../providers/date-format';
import {Storage} from '../../providers/storage';
import {DataStore} from '../../providers/data-store';
import {TranslateService} from 'ng2-translate';

import {ForgotPasswordPage} from '../forgot-password/forgot-password';

import {Events} from 'ionic-angular';

import {Checkout} from '../checkout/checkout';
import Util from "../../providers/util";
import {Firebase} from "@ionic-native/firebase";

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
    providers: [ApiService, DateFormat]
})
export class Login {
    countryCode;

    forgotPasswordPage = ForgotPasswordPage;

    storage: Storage;
    dataStore: DataStore;

    loginsignup = "login";

    emailLogin = "";
    passwordLogin = "";

    nameSignup = "";
    lnameSignup = "";
    emailSignup = "";
    mobileSignup = "";
    passwordSignup = "";
    citySignup = "";
    dobSignup = "";
    anniversaryDateSignup = "";

    citiesList = [];

    callBack: Function;

    userCountry;

    constructor(
        public navCtrl: NavController,
        public translate: TranslateService,
        private viewCtrl: ViewController,
        public  navParams: NavParams,
        public apiService: ApiService,
        public events: Events,
        public alertCtrl: AlertController,
        private firebase: Firebase
    ) {
        this.storage = Storage.getInstance();
        this.dataStore = DataStore.getInstance();
        this.userCountry = this.storage.getCountry();
        this.countryCode = this.userCountry.country_code;
        this.mobileSignup = this.countryCode;
    }

    ionViewDidLoad() {
        console.log('Hello Login Page');
        this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Login"});

        this.getCitiesList();
    }

    getCitiesList = () => {
        this.apiService.citiesList().then(data => {
                this.citiesList = data['data'];
                if (this.citiesList && this.citiesList.length > 0) {
                    this.citySignup = this.citiesList[0].id;
                }
            },
            err => {
            });
    }

    onMobileChange() {
        if (this.mobileSignup.length < 3) {
            this.mobileSignup = this.countryCode;
        } else {
            this.mobileSignup = this.mobileSignup.replace(this.mobileSignup.substring(0, 3), this.countryCode);
        }
    }


    updateFirebaseToken(token) {
        if (token) {
            localStorage.setItem('firebase_token', token);
            if (this.storage.getUser()) {
                this.apiService.updateToken(this.storage.getUser().id, token).then((data) => {
                });
            }
        }
    }

    showPrompt() {
        this.translate.get(['RESET PASSWORD', 'RESET PASSWORD MESSAGE', 'EMAIL ADDRESS', 'SEND', 'CANCEL']).subscribe((res: any) => {
            let prompt = this.alertCtrl.create({
                title: res['RESET PASSWORD'],
                message: res['RESET PASSWORD MESSAGE'],
                inputs: [
                    {
                        name: 'email',
                        placeholder: res['EMAIL ADDRESS']
                    },
                ],
                buttons: [{
                    text: res['CANCEL'],
                    role: 'cancel',
                    handler: (data) => {
                        console.log('Cancel clicked');
                    }
                },
                    {
                        text: res['SEND'],
                        role: 'send',
                        handler: (data) => {
                            let email = data.email;
                            console.log('Saved clicked' + email);
                        }
                    }]
            });

            prompt.present();
        });
    }

    login() {
        if (this.emailLogin.length > 0 && this.passwordLogin.length > 0) {
            this.apiService.showLoading();
            this.apiService.checkUserLogin(this.emailLogin, this.passwordLogin).then(data => {
                    this.apiService.hideLoading();
                    console.log("Login Response " + JSON.stringify(data));

                    if (data['status']) {
                        let user = data['data'];
                        if (user) {
                            this.storage.setLoginDetails({email: this.emailLogin, password: this.passwordLogin});
                            this.storage.setUser(user);
                            this.storage.setLoggedIn();
                            this.updateFirebaseToken(localStorage.getItem("firebase_token"));

                            this.loginSuccess();
                        }

                    } else if (!data['status']) {
                        let msg = data['error'];
                        this.apiService.showAlert(msg);
                    }
                },
                (error) => {
                    this.apiService.hideLoading();
                    console.log("Login Error " + JSON.stringify(error));
                });
        } else if (this.emailLogin.length == 0) {
            this.apiService.showAlert('PLEASE ENTER EMAIL');
        } else if (this.passwordLogin.length == 0) {
            this.apiService.showAlert('PLEASE ENTER PASSWORD');
        }
    }

    signUp() {
        var msgArr = [];
        // Name
        if (this.nameSignup.length == 0) {
            msgArr.push("name");
        } else {
            // var nameRegx = /^[a-zA-Z ]+$/ ;
            // if(!this.nameSignup.match(nameRegx)) {
            //   msgArr.push("correct name");
            // }
        }
        if (this.lnameSignup.length == 0) {
            msgArr.push("last name");
        } else {
            // var nameRegx = /^[a-zA-Z ]+$/ ;
            // if(!this.lnameSignup.match(nameRegx)) {
            //   msgArr.push("correct name");
            // }
        }
        // Mobile
        if (this.mobileSignup.length == 0) {
            msgArr.push("mobile number");
        } else {
            // var mobileRegx = /^(\+91-|\+91|0)?\d{10}$/;
            var mobileRegx = /^\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*\d\W*(\d{1,2})$/;
            if (!this.mobileSignup.match(mobileRegx)) {
                msgArr.push("correct mobile number");
            }
        }
        // Password
        if (this.passwordSignup.length == 0) {
            msgArr.push("password");
        } else if (this.passwordSignup.length < 6) {
            msgArr.push("password should be atleast 6 character");
        }

        // email
        if (this.emailSignup.length > 0) {
            var emailRegex = /\S+@\S+\.\S+/;
            if (!Util.validateEmail(this.emailSignup)) {
                msgArr.push("correct email address");
            }
        }

        if (msgArr.length > 0) {
            var finalErrorMsg = "";
            if (msgArr.length > 1) {
                var lastError = msgArr[msgArr.length - 1];
                msgArr.pop();
                finalErrorMsg = msgArr.join(", ") + " and " + lastError;
            } else {
                finalErrorMsg = msgArr[0];
            }
            this.apiService.showAlert("Please enter " + finalErrorMsg + ".");
        } else {
            this.apiService.showLoading();
            this.apiService.addResUser(
                this.nameSignup,
                this.lnameSignup,
                this.mobileSignup,
                this.citySignup,
                this.emailSignup,
                this.passwordSignup,
                DateFormat.shared().dateToStrDDMMYYYY(new Date(this.dobSignup))
            ).then(data => {
                    this.apiService.hideLoading();
                    console.log("Signup Response " + JSON.stringify(data));

                    if (!data['status']) {
                        var msgStr = data['error'];
                        this.apiService.showAlert(msgStr);
                    } else {
                        this.emailLogin = this.mobileSignup;
                        this.passwordLogin = this.passwordSignup;
                        let user = data['data'];
                        if (user) {
                            this.storage.setLoginDetails({email: this.emailLogin, password: this.passwordLogin});
                            this.storage.setUser(user);
                            this.storage.setLoggedIn();
                            this.updateFirebaseToken(localStorage.getItem("firebase_token"));

                            this.loginSuccess();
                        }

                    }
                },
                (error) => {
                    this.apiService.hideLoading();
                    console.log("Signup Error " + JSON.stringify(error));
                });
        }
    }

    loginSuccess() {
        this.events.publish(this.dataStore.eventsKey().LOGIN_SUCCESS, true)
        var index = 0;
        var callBack = this.navParams.get('CallBack');
        if (callBack) {
            this.navCtrl.pop();
            callBack(true);
        } else {
            var pageCmp = this.navParams.get('PageComponents');
            var pageType = this.navParams.get('PageType');
            switch (pageType) {
                case "Checkout":
                    index = this.viewCtrl.index;
                    this.navCtrl.remove(index, 1, {animate: false}).then(() => {
                        this.navCtrl.push(Checkout);
                    });
                    break;
                case "History":
                    index = this.viewCtrl.index;
                    this.navCtrl.remove(index, 1, {animate: false}).then(() => {
                        this.navCtrl.push(History);
                    });
                    break;
                default:
                    this.navCtrl.setRoot(pageCmp);
                    break;
            }
        }

    }
}
