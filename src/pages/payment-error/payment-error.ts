import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Firebase} from "@ionic-native/firebase";

/*
  Generated class for the PaymentError page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
  	selector: 'page-payment-error',
  	templateUrl: 'payment-error.html'
  })
  export class PaymentError {

  	constructor(
  		public navCtrl: NavController,
		public navParams: NavParams,
		private firebase: Firebase
	) {}

  	ionViewDidLoad() {
  		console.log('ionViewDidLoad PaymentErrorPage');
		this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "PaymentError"});

	}
  	retry() {
  		// let data = { 'retry': '' };
  		this.navCtrl.pop();
  	}
  }
