import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiService } from '../../providers/api-service';
import {Firebase} from "@ionic-native/firebase";

/*
  Generated class for the ForgotPasswordPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
  	selector: 'page-forgot-password',
  	templateUrl: 'forgot-password.html'
  })
  export class ForgotPasswordPage {

  	email;

  	constructor(
  		public navCtrl: NavController,
		public navParams: NavParams,
		public apiService: ApiService,
		private firebase: Firebase
	) {}

  	ionViewDidLoad() {
  		console.log('ionViewDidLoad ForgotPasswordPagePage');

		this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "ForgotPassword"});

	}

  	forgotPassword() {
  		if (!this.email || this.email.length == 0) {
  			this.apiService.showAlert('PLEASE ENTER EMAIL');
  		} else {
  			this.apiService.showLoading();
  			this.apiService.resetPassword(this.email).then(data => {
  				this.apiService.hideLoading();
  				if(data['status']) {
  					this.apiService.showAlert(data['message']);
  					this.navCtrl.pop();
  				} else if(!data['status']) {
  					this.apiService.showAlert(data['message']);
  				}
  			},
  			(error) => {
  				this.apiService.hideLoading();
  				console.log("Login Error " +JSON.stringify(error));
  			});
  		}
  	}
  }
