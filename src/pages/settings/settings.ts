import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {TranslateService} from 'ng2-translate';

import { Storage } from '../../providers/storage';
import {Firebase} from "@ionic-native/firebase";

/*
  Generated class for the Settings page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
    selector: 'page-settings',
    templateUrl: 'settings.html'
  })
  export class Settings {
    storage:Storage;
    languagesArr = [];
    language;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public translate: TranslateService,
        private firebase: Firebase
    ) {
      this.storage = Storage.getInstance();

      this.language = this.storage.getLanguage();

      this.languagesArr.push({
        "name":"ENGLISH",
        "key": "en"		
      });
      // this.languagesArr.push({
      //   "name":"HINDI",
      //   "key": "hi"		
      // });

      this.languagesArr.push({
        "name":"SPANISH",
        "key": "es"    
      });
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad SettingsPage');

      this.firebase.logEvent("select_content", {content_type: "page_view", item_id: "Settings"});

    }

    languageChange() {
      this.translate.setDefaultLang(this.language);
      this.storage.setLanguage(this.language);
    }

    getLanguageNameForKey(key) {
      var result = this.languagesArr.filter(function(v) {
        return v.key === key; 
      }); 
      if(result.length > 0) {
        return result[0].name
      }
      return "";
    }
  }
