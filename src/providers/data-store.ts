import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {SQLite} from 'ionic-native';
import {Events} from 'ionic-angular';

declare var window: any;


/*
  Generated class for the DataStore provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
  	*/
@Injectable()
export class DataStore {
    isLoadingShow = false;

    public itemsCount = 0;

    static instance: DataStore;
    static isCreating: Boolean = false;
    db: SQLite = new SQLite();
    events: Events;
    isDevice: boolean = false;

    constructor() {
        if (!DataStore.isCreating) {
            throw new Error("no utilize getInstance()");
        }
    }

    static getInstance(): DataStore {
        if (DataStore.instance == null) {
            DataStore.isCreating = true;
            DataStore.instance = new DataStore();
            DataStore.isCreating = false;
        }
        return DataStore.instance;
    }

    setEvents(eventsRef) {
        this.events = eventsRef;
    }

    dbSetUp() {
        console.log('Hello DataStore Provider');

        if (this.isDevice) {
            this.db.openDatabase({
                name: 'restrodata.db',
                location: 'default'
            }).then(() => {
                this.tablesCreate();
            }, (err) => {
                console.error('Unable to open database: ', err);
            });
        } else {
            this.db = window.openDatabase("restrodata.db", '1.0', "My WebSQL Database", 2 * 1024 * 1024);
            this.tablesCreate();

            this.getItemsFromCart();
        }
    }


    dbQuery = (sql: any, values?: any) => {

        return new Promise((resolve, reject) => {
            if (this.isDevice) {
                this.db.executeSql(sql, values).then((response: any) => {
                    resolve(response);
                }, (err) => {
                    reject(err);
                });
            } else {
                this.db.transaction((tx) => {
                    tx.executeSql(sql, [], (tx, results) => {
                        resolve(results);
                    }, (err) => {
                        reject(err);
                    });
                });
            }
        });
    }

    tablesCreate() {
        // Cart Table
        let query = 'CREATE TABLE IF NOT EXISTS ' + this.keys().TABLE_CART + '(' + this.keys().COLUMN_ID_CART + ' INTEGER PRIMARY KEY AUTOINCREMENT, ' + this.keys().COLUMN_ITEMID_CART + ' INTEGER, ' + this.keys().COLUMN_ITEM_CART + ' TEXT, ' + this.keys().COLUMN_QUANTITY_CART + ' INTEGER, ' + this.keys().COLUMN_ADDEDDATE_CART + ' VARCHAR(32), ' + this.keys().COLUMN_CATEGORY_CART + ' VARCHAR(32), ' + this.keys().COLUMN_CATEGORYID_CART + ' VARCHAR(32))';

        this.dbQuery(query, {}).then((response: any) => {
            console.log("CREATE TABLE Cart" + JSON.stringify(response));

            this.getItemsFromCart();
        }, (err) => {
            console.error('Unable to execute sql: ', err);
        });


        // Address Table
        let addressTableQuery = 'CREATE TABLE IF NOT EXISTS ' + this.keys().TABLE_ADDRESS + '(' + this.keys().COLUMN_ID_ADDRESS + ' INTEGER PRIMARY KEY AUTOINCREMENT, ' + this.keys().COLUMN_ADDRESSID_ADDRESS + ' INTEGER, ' + this.keys().COLUMN_NAME_ADDRESS + ' VARCHAR(32), ' + this.keys().COLUMN_MOBILE_ADDRESS + ' VARCHAR(32), ' + this.keys().COLUMN_PINCODE_ADDRESS + ' VARCHAR(32), ' + this.keys().COLUMN_ADDRESS1_ADDRESS + ' TEXT, ' + this.keys().COLUMN_ADDRESS2_ADDRESS + ' TEXT, ' + this.keys().COLUMN_LANDMARK_ADDRESS + ' TEXT, ' + this.keys().COLUMN_TOWNCITY_ADDRESS + ' TEXT, ' + this.keys().COLUMN_STATE_ADDRESS + ' TEXT, ' + this.keys().COLUMN_COUNTRY_ADDRESS + ' TEXT, ' + this.keys().COLUMN_TYPE_ADDRESS + ' TEXT, ' + this.keys().COLUMN_GEO_LAT_ADDRESS + ' TEXT, ' + this.keys().COLUMN_GEO_LONG_ADDRESS + ' TEXT)';

        this.dbQuery(addressTableQuery, {}).then((response: any) => {
            console.log("CREATE TABLE Address" + JSON.stringify(response));

        }, (err) => {
            console.error('Unable to execute sql: ', err);
        });

        // Category Table
        let categoryTableQuery = 'CREATE TABLE IF NOT EXISTS ' + this.keys().TABLE_CATEGORIES + '(' + this.keys().COLUMN_ID_CATEGORIES + ' INTEGER PRIMARY KEY AUTOINCREMENT, ' + this.keys().COLUMN_CAT_ID_CATEGORIES + ' INTEGER, ' + this.keys().COLUMN_BG_IMAGE_CATEGORIES + ' VARCHAR(32), ' + this.keys().COLUMN_CATEGORY_CATEGORIES + ' VARCHAR(32), ' + this.keys().COLUMN_CATEGORY_CODE_CATEGORIES + ' VARCHAR(32), ' + this.keys().COLUMN_ICON_IMAGE_CATEGORIES + ' TEXT, ' + this.keys().COLUMN_ADDED_DATE_CATEGORIES + ' VARCHAR(32))';

        this.dbQuery(categoryTableQuery, {}).then((response: any) => {
            console.log("CREATE TABLE CATEGORIES" + JSON.stringify(response));

        }, (err) => {
            console.error('Unable to execute sql: ', err);
        });

        // CatTags Table
        let catTagsTableQuery = 'CREATE TABLE IF NOT EXISTS ' + this.keys().TABLE_CATTAGS + '(' + this.keys().COLUMN_ID_CATTAGS + ' INTEGER PRIMARY KEY AUTOINCREMENT, ' + this.keys().COLUMN_CAT_ID_CATTAGS + ' INTEGER, ' + this.keys().COLUMN_TAG_ID_CATTAGS + ' TEXT, ' + this.keys().COLUMN_TAG_NAME_CATTAGS + ' VARCHAR(32), ' + this.keys().COLUMN_ITEMS_CATTAGS + ' TEXT, ' + this.keys().COLUMN_ADDED_DATE_CATTAGS + ' VARCHAR(32))';

        this.dbQuery(catTagsTableQuery, {}).then((response: any) => {
            console.log("CREATE TABLE CATTAGS" + JSON.stringify(response));

        }, (err) => {
            console.error('Unable to execute sql: ', err);
        });
    }

    keys() {
        return {
            TABLE_CART: "CART",
            COLUMN_ID_CART: "id",
            COLUMN_ITEM_CART: "item",
            COLUMN_ITEMID_CART: "itemId",
            COLUMN_QUANTITY_CART: "quantity",
            COLUMN_ADDEDDATE_CART: "addedDate",
            COLUMN_CATEGORY_CART: "category",
            COLUMN_CATEGORYID_CART: "categoryId",

            TABLE_ADDRESS: "ADDRESS",
            COLUMN_ID_ADDRESS: "id",
            COLUMN_ADDRESSID_ADDRESS: "address_id",
            COLUMN_NAME_ADDRESS: "fullname",
            COLUMN_MOBILE_ADDRESS: "phone",
            COLUMN_PINCODE_ADDRESS: "postal_code",
            COLUMN_ADDRESS1_ADDRESS: "flat_or_house_no",
            COLUMN_ADDRESS2_ADDRESS: "apartment_or_locality",
            COLUMN_LANDMARK_ADDRESS: "landmark",
            COLUMN_TOWNCITY_ADDRESS: "city",
            COLUMN_STATE_ADDRESS: "address",
            COLUMN_COUNTRY_ADDRESS: "company",
            COLUMN_TYPE_ADDRESS: "type",
            COLUMN_GEO_LAT_ADDRESS: 'geo_lat',
            COLUMN_GEO_LONG_ADDRESS: 'geo_long',

            TABLE_CATEGORIES: "CATEGORIES",
            COLUMN_ID_CATEGORIES: "id",
            COLUMN_CAT_ID_CATEGORIES: "cat_id",
            COLUMN_BG_IMAGE_CATEGORIES: "background_image",
            COLUMN_CATEGORY_CATEGORIES: "category",
            COLUMN_CATEGORY_CODE_CATEGORIES: "category_code",
            COLUMN_ICON_IMAGE_CATEGORIES: "icon_image",
            COLUMN_ADDED_DATE_CATEGORIES: "added_timestamp",

            TABLE_CATTAGS: "CATTAGS",
            COLUMN_ID_CATTAGS: "id",
            COLUMN_CAT_ID_CATTAGS: "cat_id",
            COLUMN_TAG_ID_CATTAGS: "t_id",
            COLUMN_TAG_NAME_CATTAGS: "t_name",
            COLUMN_ITEMS_CATTAGS: "items",
            COLUMN_ADDED_DATE_CATTAGS: "added_timestamp"
        };
    }

    eventsKey() {
        return {
            CART_MODIFY: 'CartModify',
            ADDRESS_MODIFY: 'AddressModify',
            LOGIN_SUCCESS: 'LoginSuccess'
        };
    }

    /**********************************************************/
    /************************  Cart ***************************/

    /**********************************************************/

    addItemToCart(newItem, quantity, addedDate, category, categoryId) {
        return new Promise((resolve, reject) => {
            this.checkItemAdded(newItem.id).then(itemDetail => {
                if (itemDetail['cartItemId'] == -1) {
                    let query = 'INSERT INTO ' + this.keys().TABLE_CART + ' (' + this.keys().COLUMN_ITEMID_CART + ',  ' + this.keys().COLUMN_ITEM_CART + ',  ' + this.keys().COLUMN_QUANTITY_CART + ', ' + this.keys().COLUMN_ADDEDDATE_CART + ', ' + this.keys().COLUMN_CATEGORY_CART + ', ' + this.keys().COLUMN_CATEGORYID_CART + ') VALUES (' + newItem.id + ',\'' + JSON.stringify(newItem) + '\',' + quantity + ',\'' + addedDate + '\',\'' + category + '\',\'' + categoryId + '\')';
                    this.dbQuery(query, []).then((resultSet: any) => {

                        console.log('resultSet.rowsAffected: ' + resultSet.rowsAffected);

                        this.getItemsFromCart();

                        resolve({
                            'success': true,
                            'message': 'Item successfully added'
                        });
                    }, (error) => {
                        reject(error);
                        console.log('Item insert error : ' + error.message);
                    });
                } else {
                    let newQuantity = quantity + itemDetail['quantity'];
                    this.updateItemQuantity(itemDetail['cartItemId'], newQuantity, newItem).then((response: any) => {
                            resolve(response);
                        },
                        (error) => {
                            reject(error);
                        });
                }
            });
        });
    }

    checkItemAdded(itemId) {
        return new Promise((resolve) => {
            let query = 'SELECT ' + this.keys().COLUMN_ID_CART + ',' + this.keys().COLUMN_QUANTITY_CART + ' FROM ' + this.keys().TABLE_CART + ' WHERE ' + this.keys().COLUMN_ITEMID_CART + ' = ' + itemId;
            this.dbQuery(query, []).then((resultSet: any) => {

                console.log('resultSet.rowsAffected: ' + resultSet.rowsAffected);

                if (resultSet.rows.length > 0) {
                    let data = resultSet.rows.item(0);
                    let cartItemId = data[this.keys().COLUMN_ID_CART];
                    let quantity = data[this.keys().COLUMN_QUANTITY_CART];

                    resolve({
                        cartItemId: cartItemId,
                        quantity: quantity
                    });
                } else {
                    resolve({
                        cartItemId: -1,
                        quantity: 0
                    });
                }
                console.log('Item get success : ' + JSON.stringify(resultSet.rows.item(0)));

            }, (error) => {

                resolve({
                    cartItemId: -1,
                    quantity: 0
                });
                console.log('Item get error : ' + error.message);
            });
        });
    }

    getItemsFromCart() {
        return new Promise((resolve, reject) => {
            let query = 'SELECT * FROM ' + this.keys().TABLE_CART;
            this.dbQuery(query, []).then((getResult: any) => {
                var itemsArr = [];
                for (var i = 0; i < getResult.rows.length; i++) {
                    let data = getResult.rows.item(i);
                    data.item = JSON.parse(data.item);
                    itemsArr.push(data);
                }
                this.itemsCount = itemsArr.length;
                DataStore.getInstance().events.publish(this.eventsKey().CART_MODIFY, itemsArr.length);

                resolve({
                    'success': true,
                    'data': itemsArr
                });
            }, (error) => {
                reject(error);
                console.log('Item get error : ' + error.message);
            });
        });
    }

    updateItemQuantity(cartItemId, newQuantity, item) {
        return new Promise((resolve, reject) => {
            let query = 'UPDATE ' + this.keys().TABLE_CART + ' SET ' + this.keys().COLUMN_QUANTITY_CART + '=' + newQuantity + ', ' + this.keys().COLUMN_ITEM_CART + '=\'' + JSON.stringify(item) + '\' WHERE ' + this.keys().COLUMN_ID_CART + ' = ' + cartItemId;
            this.dbQuery(query, []).then((response: any) => {
                console.log('Update Response ' + response);

                if (response.rowsAffected) {
                    resolve({
                        'success': true,
                        'message': 'Item successfully updated'
                    });
                }
            }, (error) => {
                reject(error);
                console.log('Item update error : ' + error.message);
            });
        });
    }

    updateItemSupplies(cartItemId, item) {
        return new Promise((resolve, reject) => {
            let query = 'UPDATE ' + this.keys().TABLE_CART + ' SET ' + this.keys().COLUMN_ITEM_CART + '=\'' + JSON.stringify(item) + '\' WHERE ' + this.keys().COLUMN_ID_CART + ' = ' + cartItemId;
            this.dbQuery(query, []).then((response: any) => {
                console.log('Update Response ' + response);

                if (response.rowsAffected) {
                    resolve({
                        'success': true,
                        'message': 'Item successfully updated'
                    });
                }
            }, (error) => {
                reject(error);
                console.log('Item update error : ' + error.message);
            });
        });
    }

    removeItemFromCart(cartItemId) {
        return new Promise((resolve, reject) => {
            let query = 'DELETE FROM ' + this.keys().TABLE_CART + ' WHERE ' + this.keys().COLUMN_ID_CART + ' = ' + cartItemId;

            this.dbQuery(query, []).then((response: any) => {
                console.log('Item delete response ' + response);
                if (response.rowsAffected) {
                    this.getItemsFromCart();

                    resolve({
                        'success': true,
                        'message': 'Item successfully updated'
                    });
                }
            }, (error) => {
                reject(error);
                console.log('Item delete error : ' + error.message);
            });
        });
    }


    removeItemsFromCart(itemIds) {
        return new Promise((resolve, reject) => {
            let query = 'DELETE FROM ' + this.keys().TABLE_CART + ' WHERE ' + this.keys().COLUMN_ID_CART + ' in (' + itemIds.join(", ") + ')';

            this.dbQuery(query, []).then((response: any) => {
                console.log('All data delete response : ' + response);
                this.itemsCount = 0;
                DataStore.getInstance().events.publish(this.eventsKey().CART_MODIFY, 0);

                resolve({
                    'success': true,
                    'message': 'Cart successfully deleted.'
                });
            }, (error) => {
                reject(error);
                console.log('All data delete error : ' + error.message);
            });
        });
    }

    emptyCart() {
        return new Promise((resolve, reject) => {
            let query = 'DELETE FROM ' + this.keys().TABLE_CART;

            this.dbQuery(query, []).then((response: any) => {
                console.log('All data delete response : ' + response);
                this.itemsCount = 0;
                DataStore.getInstance().events.publish(this.eventsKey().CART_MODIFY, 0);

                resolve({
                    'success': true,
                    'message': 'Cart successfully deleted.'
                });
            }, (error) => {
                reject(error);
                console.log('All data delete error : ' + error.message);
            });
        });
    }

    /**********************************************************/
    /**********************  Addresses ************************/

    /**********************************************************/

    addAddress(addressId, name, mobile, pincode, address1, address2, landmark, townCity, state, country, type, lat, lng) {


        return new Promise((resolve, reject) => {
            let query = 'INSERT INTO ' + this.keys().TABLE_ADDRESS + ' (' + this.keys().COLUMN_ADDRESSID_ADDRESS + ',  ' + this.keys().COLUMN_NAME_ADDRESS + ',  ' + this.keys().COLUMN_MOBILE_ADDRESS + ', ' + this.keys().COLUMN_PINCODE_ADDRESS + ', ' + this.keys().COLUMN_ADDRESS1_ADDRESS + ', ' + this.keys().COLUMN_ADDRESS2_ADDRESS + ', ' + this.keys().COLUMN_LANDMARK_ADDRESS + ', ' + this.keys().COLUMN_TOWNCITY_ADDRESS + ', ' + this.keys().COLUMN_STATE_ADDRESS + ', ' + this.keys().COLUMN_COUNTRY_ADDRESS + ', ' + this.keys().COLUMN_TYPE_ADDRESS + ', ' + this.keys().COLUMN_GEO_LAT_ADDRESS + ', ' + this.keys().COLUMN_GEO_LONG_ADDRESS + ') VALUES (' + addressId + ',\'' + mobile + '\', \'' + pincode + '\', \'' + address1 + '\', \'' + address2 + '\', \'' + landmark + '\', \'' + townCity + '\', \'' + state + '\', \'' + country + ', \'' + type + '\', \'' + lat + '\', \'' + lng + '\')';
            this.dbQuery(query, []).then((resultSet: any) => {

                console.log('resultSet.rowsAffected: ' + resultSet.rowsAffected);

                resolve({
                    'success': true,
                    'message': 'Address successfully added'
                });

                this.getAddresses();
            }, (error) => {
                reject(error);
                console.log('address insert error : ' + error.message);
            });
        });
    }

    getAddresses() {
        return new Promise((resolve, reject) => {
            let query = 'SELECT * FROM ' + this.keys().TABLE_ADDRESS;
            this.dbQuery(query, []).then((getResult: any) => {
                var addressesArr = [];
                for (var i = 0; i < getResult.rows.length; i++) {
                    let data = getResult.rows.item(i);
                    addressesArr.push(data);
                }

                resolve({
                    'success': true,
                    'data': addressesArr
                });

                DataStore.getInstance().events.publish(this.eventsKey().ADDRESS_MODIFY, addressesArr);
            }, (error) => {
                reject(error);
                console.log('Address get error : ' + error.message);
            });
        });
    }

    updateAddress(id, addressId, name, mobile, pincode, address1, address2, landmark, townCity, state, country, type, lat, lng) {
        return new Promise((resolve, reject) => {
            let query = 'UPDATE ' + this.keys().TABLE_ADDRESS + ' SET ' + this.keys().COLUMN_ADDRESSID_ADDRESS + '=?,' + this.keys().COLUMN_NAME_ADDRESS + '=?,' + this.keys().COLUMN_MOBILE_ADDRESS + '=?,' + this.keys().COLUMN_PINCODE_ADDRESS + '=?,' + this.keys().COLUMN_ADDRESS1_ADDRESS + '=?,' + this.keys().COLUMN_ADDRESS2_ADDRESS + '=?,' + this.keys().COLUMN_LANDMARK_ADDRESS + '=?,' + this.keys().COLUMN_TOWNCITY_ADDRESS + '=?,' + this.keys().COLUMN_STATE_ADDRESS + '=?,' + this.keys().COLUMN_COUNTRY_ADDRESS + '=?,' + this.keys().COLUMN_TYPE_ADDRESS + '=?,' + this.keys().COLUMN_GEO_LAT_ADDRESS + '=?,' + this.keys().COLUMN_GEO_LONG_ADDRESS + '=? WHERE ' + this.keys().COLUMN_ADDRESSID_ADDRESS + ' = ?';
            this.dbQuery(query, [addressId, name, mobile, pincode, address1, address2, landmark, townCity, state, country, type, lat, lng, id]).then((response: any) => {
                console.log('Update Response ' + JSON.stringify(response));

                if (response.rowsAffected) {
                    resolve({
                        'success': true,
                        'message': 'Address successfully updated'
                    });

                    this.getAddresses();
                }
            }, (error) => {
                reject(error);
                console.log('Address update error : ' + error.message);
            });

        });
    }

    removeAddress(addressId) {
        return new Promise((resolve, reject) => {

            let query = 'DELETE FROM ' + this.keys().TABLE_ADDRESS + ' WHERE ' + this.keys().COLUMN_ADDRESSID_ADDRESS + ' = ?';

            this.dbQuery(query, [addressId]).then((response: any) => {
                console.log('Address delete response ' + JSON.stringify(response));
                if (response.rowsAffected) {
                    resolve({
                        'success': true,
                        'message': 'Address successfully deleted'
                    });

                    this.getAddresses();
                }
            }, (error) => {
                reject(error);
                console.log('Address delete error : ' + error.message);
            });

        });
    }

    emptyAddresses() {
        return new Promise((resolve, reject) => {
            let query = 'DELETE FROM ' + this.keys().TABLE_ADDRESS;

            this.dbQuery(query, []).then((response: any) => {
                console.log('All address delete response : ' + response);
                resolve({
                    'success': true,
                    'message': 'Address successfully deleted.'
                });
            }, (error) => {
                reject(error);
                console.log('All addresses delete error : ' + error.message);
            });
        });
    }


    insertAllAddresses(addresses) {
        return new Promise((resolve, reject) => {
            let query = 'INSERT INTO ' + this.keys().TABLE_ADDRESS + ' (' + this.keys().COLUMN_ADDRESSID_ADDRESS + ',  ' + this.keys().COLUMN_NAME_ADDRESS + ',  ' + this.keys().COLUMN_MOBILE_ADDRESS + ', ' + this.keys().COLUMN_PINCODE_ADDRESS + ', ' + this.keys().COLUMN_ADDRESS1_ADDRESS + ', ' + this.keys().COLUMN_ADDRESS2_ADDRESS + ', ' + this.keys().COLUMN_LANDMARK_ADDRESS + ', ' + this.keys().COLUMN_TOWNCITY_ADDRESS + ', ' + this.keys().COLUMN_STATE_ADDRESS + ', ' + this.keys().COLUMN_COUNTRY_ADDRESS + ', ' + this.keys().COLUMN_TYPE_ADDRESS + ', ' + this.keys().COLUMN_GEO_LAT_ADDRESS + ', ' + this.keys().COLUMN_GEO_LONG_ADDRESS + ') VALUES ';

            var data = [];
            var rowArgs = [];
            var ref = this;

            addresses.forEach(function (address) {
                rowArgs.push("(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                data.push(address[ref.keys().COLUMN_ADDRESSID_ADDRESS]);
                data.push(address[ref.keys().COLUMN_NAME_ADDRESS]);
                data.push(address[ref.keys().COLUMN_MOBILE_ADDRESS]);
                data.push(address[ref.keys().COLUMN_PINCODE_ADDRESS]);
                data.push(address[ref.keys().COLUMN_ADDRESS1_ADDRESS]);
                data.push(address[ref.keys().COLUMN_ADDRESS2_ADDRESS]);
                data.push(address[ref.keys().COLUMN_LANDMARK_ADDRESS]);
                data.push(address[ref.keys().COLUMN_TOWNCITY_ADDRESS]);
                data.push(address[ref.keys().COLUMN_STATE_ADDRESS]);
                data.push(address[ref.keys().COLUMN_COUNTRY_ADDRESS]);
                data.push(address[ref.keys().COLUMN_TYPE_ADDRESS]);
                data.push(address[ref.keys().COLUMN_GEO_LAT_ADDRESS]);
                data.push(address[ref.keys().COLUMN_GEO_LONG_ADDRESS]);
            });
            query += rowArgs.join(", ");

            this.dbQuery(query, data).then((resultSet: any) => {

                console.log('resultSet.rowsAffected: ' + resultSet.rowsAffected);

                resolve({
                    'success': true,
                    'message': 'Addressess successfully added'
                });

                this.getAddresses();
            }, (error) => {
                reject(error);
                console.log('address insert error : ' + error.message);
            });
        });
    }

    /**********************************************************/
    /**********************  Categories ************************/

    /**********************************************************/

    insertAllCategories(categories) {
        return new Promise((resolve, reject) => {
            let query = 'INSERT INTO ' + this.keys().TABLE_CATEGORIES + ' (' + this.keys().COLUMN_CAT_ID_CATEGORIES + ',  ' + this.keys().COLUMN_BG_IMAGE_CATEGORIES + ', ' + this.keys().COLUMN_CATEGORY_CATEGORIES + ', ' + this.keys().COLUMN_CATEGORY_CODE_CATEGORIES + ', ' + this.keys().COLUMN_ICON_IMAGE_CATEGORIES + ', ' + this.keys().COLUMN_ADDED_DATE_CATEGORIES + ') VALUES ';

            var data = [];
            var rowArgs = [];
            var ref = this;

            categories.forEach(function (address) {
                rowArgs.push("( ?, ?, ?, ?, ?, ?)");
                data.push(address[ref.keys().COLUMN_CAT_ID_CATEGORIES]);
                data.push(address[ref.keys().COLUMN_BG_IMAGE_CATEGORIES]);
                data.push(address[ref.keys().COLUMN_CATEGORY_CATEGORIES]);
                data.push(address[ref.keys().COLUMN_CATEGORY_CODE_CATEGORIES]);
                data.push(address[ref.keys().COLUMN_ICON_IMAGE_CATEGORIES]);
                data.push(Date.now());
            });
            query += rowArgs.join(", ");

            this.dbQuery(query, data).then((resultSet: any) => {

                console.log('resultSet.rowsAffected: ' + resultSet.rowsAffected);

                resolve({
                    'success': true,
                    'message': 'All Categories successfully added'
                });

            }, (error) => {
                reject(error);
                console.log('All Categories insert error : ' + error.message);
            });

        });
    }


    getCategories() {
        return new Promise((resolve, reject) => {
            let query = 'SELECT * FROM ' + this.keys().TABLE_CATEGORIES;
            this.dbQuery(query, []).then((getResult: any) => {
                var categoriesArr = [];
                for (var i = 0; i < getResult.rows.length; i++) {
                    let data = getResult.rows.item(i);
                    categoriesArr.push(data);
                }

                resolve({
                    'success': true,
                    'data': categoriesArr
                });

                // DataStore.getInstance().events.publish(this.eventsKey().ADDRESS_MODIFY, categoriesArr);
            }, (error) => {
                reject(error);
                console.log('Categories get error : ' + error.message);
            });

        });
    }

    emptyCategories() {
        return new Promise((resolve, reject) => {
            let query = 'DELETE FROM ' + this.keys().TABLE_CATEGORIES;

            this.dbQuery(query, []).then((response: any) => {
                console.log('All categories delete response : ' + response);
                resolve({
                    'success': true,
                    'message': 'categories successfully deleted.'
                });
            }, (error) => {
                reject(error);
                console.log('All categories delete error : ' + error.message);
            });
        });
    }


    /**********************************************************/
    /**********************  Category Items ************************/

    /**********************************************************/

    insertTag(tag) {

        var ref = this;
        return new Promise((resolve, reject) => {
            var insertNow = function () {
                let query = 'INSERT INTO ' + ref.keys().TABLE_CATTAGS + ' (' + ref.keys().COLUMN_CAT_ID_CATTAGS + ',  ' + ref.keys().COLUMN_TAG_ID_CATTAGS + ', ' + ref.keys().COLUMN_TAG_NAME_CATTAGS + ', ' + ref.keys().COLUMN_ITEMS_CATTAGS + ', ' + ref.keys().COLUMN_ADDED_DATE_CATTAGS + ') VALUES (?,?,?,?,?)';

                var dataArr = [];
                dataArr.push(tag[ref.keys().COLUMN_CAT_ID_CATTAGS]);
                dataArr.push(tag[ref.keys().COLUMN_TAG_ID_CATTAGS]);
                dataArr.push(tag[ref.keys().COLUMN_TAG_NAME_CATTAGS]);
                dataArr.push(JSON.stringify(tag[ref.keys().COLUMN_ITEMS_CATTAGS]));
                dataArr.push(Date.now());

                ref.db.executeSql(query, dataArr).then((resultSet: any) => {

                    console.log('resultSet.rowsAffected: ' + resultSet.rowsAffected);

                    resolve({
                        'success': true,
                        'message': 'Tag successfully added'
                    });

                }, (error) => {
                    reject(error);
                    console.log('Tag insert error : ' + error.message);
                });
            };

            var updateNow = function () {
                let query = 'UPDATE ' + ref.keys().TABLE_CATTAGS + ' SET ' + ref.keys().COLUMN_ITEMS_CATTAGS + '=?' + ref.keys().COLUMN_ADDED_DATE_CATTAGS + '=? WHERE ' + ref.keys().COLUMN_CAT_ID_CATTAGS + ' = ? AND ' + ref.keys().COLUMN_TAG_ID_CATTAGS + ' = ?';

                var dataArr = [];
                dataArr.push(JSON.stringify(tag[ref.keys().COLUMN_ITEMS_CATTAGS]));
                dataArr.push(Date.now());
                dataArr.push(tag[ref.keys().COLUMN_CAT_ID_CATTAGS]);
                dataArr.push(tag[ref.keys().COLUMN_TAG_ID_CATTAGS]);


                ref.db.executeSql(query, dataArr).then((response: any) => {
                    console.log('Update Response ' + response);

                    if (response.rowsAffected) {
                        resolve({
                            'success': true,
                            'message': 'Tag successfully updated'
                        });
                    }
                }, (error) => {
                    reject(error);
                    console.log('Tag update error : ' + error.message);
                });
            };

            ref.getTagsForCategoryId(tag[ref.keys().COLUMN_CAT_ID_CATTAGS]).then(response => {
                    if (response['success']) {
                        updateNow();
                    } else {
                        insertNow();
                    }
                },
                error => {
                    insertNow();
                });
        });
    }


    getTagsForCategoryId(cat_id) {
        return new Promise((resolve, reject) => {
            let query = 'SELECT * FROM ' + this.keys().TABLE_CATTAGS + ' WHERE ' + this.keys().COLUMN_CAT_ID_CATTAGS + '=' + cat_id;
            this.dbQuery(query, []).then((getResult: any) => {
                var tagsArr = [];
                for (var i = 0; i < getResult.rows.length; i++) {
                    let data = getResult.rows.item(i);
                    data[this.keys().COLUMN_ITEMS_CATTAGS] = JSON.parse(data[this.keys().COLUMN_ITEMS_CATTAGS]);
                    tagsArr.push(data);
                }

                if (tagsArr.length > 0) {
                    resolve({
                        'success': true,
                        'data': tagsArr
                    });
                } else {
                    reject(new Error('No tags stored in database for these category'));
                }
                // DataStore.getInstance().events.publish(this.eventsKey().ADDRESS_MODIFY, tagsArr);
            }, (error) => {
                reject(error);
                console.log('tags get error : ' + error.message);
            });
        });
    }
}
