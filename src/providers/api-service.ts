import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {Platform, LoadingController, AlertController, ToastController} from 'ionic-angular';
import {Network} from 'ionic-native';
// import {LoadingAnimateService} from 'ng2-loading-animate';
import {DataStore} from '../providers/data-store'
import 'rxjs/add/operator/timeout';

import {TranslateService} from 'ng2-translate';

import {Storage} from './storage';

declare var navigator: any;
declare var Connection: any;

/*
  Generated class for the ApiService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
  	*/
@Injectable()
export class ApiService {
    loading;
    isNetworkDisconnected = false;
    requestTimeout = 30; //seconds


    businessGroupPageStore = {
        sliders: undefined,
        items: [],
        filterItems: [],
        topCategory: [],
        allCategories: [],
        itemsBig: [],
        filterItemsBig: []
    };

    storage: Storage;

    constructor(
        public http: Http,
        public translate: TranslateService,
        private platform: Platform,
        public loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        private toastCtrl: ToastController,
        // private _loadingSvc: LoadingAnimateService
    ) {
        console.log('This is a ApiService Provider');

        this.storage = Storage.getInstance();

        var ref = this;
        // let disconnectSubscription =
        Network.onDisconnect().subscribe(() => {
            ref.hideLoading();

            if (!this.isNetworkDisconnected) {
                ref.isNetworkDisconnected = true;
                ref.showToast("network disconnected");
                console.log('network disconnected!');
            }
        });

        // let connectSubscription =
        Network.onConnect().subscribe(() => {
            console.log('network connected!');
            ref.isNetworkDisconnected = false;
        });

        // this.checkNetwork();
    }

    // checkNetwork() {
    //     this.platform.ready().then(() => {

    //         var networkState = navigator.connection.type;
    //         var states = {};
    //         states[Connection.UNKNOWN]  = 'Unknown connection';
    //         states[Connection.ETHERNET] = 'Ethernet connection';
    //         states[Connection.WIFI]     = 'WiFi connection';
    //         states[Connection.CELL_2G]  = 'Cell 2G connection';
    //         states[Connection.CELL_3G]  = 'Cell 3G connection';
    //         states[Connection.CELL_4G]  = 'Cell 4G connection';
    //         states[Connection.CELL]     = 'Cell generic connection';
    //         states[Connection.NONE]     = 'No network connection';

    //         console.log("Connection Status" + states[networkState]);

    //       });
    // }

    showLoading() {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();

        DataStore.getInstance().isLoadingShow = true;
        // this._loadingSvc.setValue(true);

    }

    hideLoading() {
        if (this.loading && DataStore.getInstance().isLoadingShow) {
            this.loading.dismiss();
        }
        DataStore.getInstance().isLoadingShow = false;
        // this._loadingSvc.setValue(false);

    }

    showAlert(msg, callBack?: Function) {
        this.translate.get(['ALERT!', msg, 'OK']).subscribe((res: any) => {

            let alert = this.alertCtrl.create({
                title: res['ALERT!'],
                subTitle: res[msg],
                buttons: [{
                    text: res['OK'],
                    role: 'ok',
                    handler: () => {
                        if (callBack) {
                            callBack();
                        }
                    }
                }]
            });
            alert.present();
        });
    }

    showAlertWithAccept(msg, acceptBtnTitle, callBack?: Function) {
        this.translate.get(['ALERT!', msg, acceptBtnTitle, 'CANCEL']).subscribe((res: any) => {

            let alert = this.alertCtrl.create({
                title: res['ALERT!'],
                subTitle: res[msg],
                buttons: [{
                    text: res['CANCEL'],
                    role: 'cancel',
                    handler: () => {
                        if (callBack) {
                            callBack(false);
                        }
                    }
                },
                    {
                        text: res[acceptBtnTitle],
                        role: 'ok',
                        handler: () => {
                            if (callBack) {
                                callBack(true);
                            }
                        }
                    }]
            });
            alert.present();
        });
    }

    showDirectAlert(msg, callBack?: Function) {
        let alert = this.alertCtrl.create({
            title: 'ALERT!',
            subTitle: msg,
            buttons: [{
                text: 'Ok',
                role: 'ok',
                handler: () => {
                    if (callBack) {
                        callBack();
                    }
                }
            }]
        });
        alert.present();
    }

    showToast(message) {
        this.translate.get([message]).subscribe((res: any) => {
            let toast = this.toastCtrl.create({
                message: res[message],
                duration: 2000,
                position: 'bottom'
            });

            toast.onDidDismiss(() => {
                console.log('Dismissed toast');
            });

            toast.present();
        });

    }

    getRestaurantId() {
        return 2;
    }


    getBaseApiUrl() {
        if (DataStore.getInstance().isDevice) {
            return "https://snappypanel.com/adminpanel/";
        }
        return "http://localhost:8100/apiUrl/";
        // return "http://localhost:8100/testApi/snappyadmin/";
    }

    getApiUrl() {
        return this.getBaseApiUrl();
    }

    apiMethods() {
        return {
            login: "login",
            signup: "signup",
            restaurantCategory: "restaurantCategory",
            businessGroup: "businessGroup",
            businessGroupSubcat: "businessGroupSubcat",
            productSubcat: "productSubcat",
            products: "products",
            users: "users",
            logoutUser: "logoutUser",

            addressAdd: "addressAdd",
            addressUpdate: "addressUpdate",
            addressList: "addressList",
            deleteAddress: "deleteAddress",
            checkProductAvailability: "checkProductAvailability",

            createOrder: "createOrder",
            updateOrder: "updateOrder",
            getOrders: "getOrders",
            providerPaymentInfo: "providerPaymentInfo",
            updateToken: "updateToken",
            arPaymentRequest: "arPaymentRequest",

            citiesList: "citiesList",
            countriesList: "countries",
            sliders: "sliders",
            appVersion: "appVersion",


            addFeedback: "addFeedback",
            updateProfile: "updateProfile",
            updateProfilePhoto: "updateProfilePhoto",
            resetPassword: "resetPassword",


            // categoryItems                : "categoryItems",
            getItemsByTagCat: "getItemsByTagCat",
            resCategoryTag: "resCategoryTag",
            items: "products",
            getOrder: "getOrder",
            addResUser: "addResUser",
            placeOrder: "placeOrder",
            checkUserLogin: "checkUserLogin",
            requestForBill: "requestForBill",
            getRestaurantTable: "getRestaurantTable",
            getRestroTaxes: "getRestroTaxes",
            getResUserOrderHistory: "getResUserOrderHistory",
            restaurantDetails: "restaurantDetails",
            userDetails: "userDetails",
            userIcon: "userIcon",
            userAddress: "userAddress",
            wallet: "wallet",
            payFromWallet: "payFromWallet",
            orderBillPayment: 'orderBillPayment'
        };
    }

    postRequest(url, postData, header = {}) {
        var ref = this;

        return new Promise((resolve, reject) => {
            //add restaurent id
            // if(postData) {
            //   postData['r_id'] = this.getRestaurantId();
            // } else {
            //   postData = {'r_id':this.getRestaurantId()};
            // }

            // let body = this.QueryStringBuilder(postData);
            postData = postData ? postData : {};

            let headers = new Headers(Object.assign({'Content-Type': 'application/json'}, header));
            let options = new RequestOptions({headers: headers});

            this.http.post(url, postData, options)
                .timeout(20000, new Error('Request Timeout'))
                .map((res) => res.json())
                .subscribe(data => {
                        console.log("Response " + JSON.stringify(data));
                        resolve(data);
                    },
                    (error) => {
                        console.log("Error " + JSON.stringify(error));
                        ref.hideLoading();
                        // ref.showToast("Network error");
                        reject(error);
                    }
                );
        });
    }

    getRequest(url) {
        var ref = this;

        return new Promise((resolve, reject) => {

            // console.log("Network State "+  Network.type());
            this.http.get(url)
                .timeout(20000, new Error('Request Timeout'))
                .map(res => res.json())
                .subscribe(data => {
                        console.log("Api Url response : " + JSON.stringify(data));
                        resolve(data);
                    },
                    error => {
                        ref.hideLoading();
                        // ref.showToast("Network error");
                        console.log("Error " + JSON.stringify(error));
                        reject(error);
                    });
        });
    }


    QueryStringBuilder(params) {
        var segments = [], value;
        for (var key in params) {
            value = params[key];
            segments.push(encodeURIComponent(key) + "=" + encodeURIComponent(value));
        }
        return segments.join("&");
    }

    // Get Requests

    restaurantCategory(userCityId?: string, businessGroupId = null, businessGroupSubCatId = null, searchInput = '') {
        var url = this.getApiUrl() + this.apiMethods().restaurantCategory;
        if (userCityId) {
            url = url + "?city_id=" + userCityId;
        } else {
            url = url + "?countryId=" + this.storage.getCountryId();
        }
        if (businessGroupId) {
            url = url + "&businessGroupId=" + businessGroupId;
        }

        if (businessGroupSubCatId) {
            url = url + "&businessGroupSubCatId=" + businessGroupSubCatId;
        }
        if (searchInput) {
            url = url + "&search_txt=" + searchInput;
        }
        return this.getRequest(url);
    }

    businessGroups() {
        var url = this.getApiUrl() + this.apiMethods().businessGroup;
        url = url + "?countryId=" + this.storage.getCountryId();
        return this.getRequest(url);
    }

    businessGroupSubcategory(businesstypeId) {
        var url = this.getApiUrl() + this.apiMethods().businessGroupSubcat;
        url = url + "?businesstype=" + businesstypeId;
        return this.getRequest(url);
    }

    productSubcategory(categoryId) {
        var url = this.getApiUrl() + this.apiMethods().productSubcat;
        url = url + "?categoryId=" + categoryId;
        return this.getRequest(url);
    }

    restaurantProducts(catId, subCategoryId, userCityId?: string, searchTxt?: string) {
        var url = this.getApiUrl() + this.apiMethods().products + "?cat_id=" + catId;
        if (userCityId) {
            url = url + "&city_id=" + userCityId;
        }
        if (subCategoryId) {
            url = url + "&subCategoryId=" + subCategoryId;
        }
        if (searchTxt) {
            url = url + "&search_txt=" + searchTxt;
        }

        return this.getRequest(url);
    }


    checkProductAvailability(productId, userCityId?: string) {
        let url = this.getApiUrl() + this.apiMethods().checkProductAvailability + "?city_id=" + userCityId + "&product_id=" + productId;
        return this.getRequest(url);
    }


    createOrder(userId, providerId, paymentMethodId, delieveryPrice, orderStatus, paymentStatus, addressId, notes, items) {
        var params = {
            'user_id': userId,
            'payment_method_id': paymentMethodId,
            'delievery_price': delieveryPrice,
            'order_status': orderStatus,
            'payment_status': paymentStatus,
            'address_id': addressId,
            'order_notes': notes,
            'items': items,
            'provider_id': providerId
        };

        let url = this.getApiUrl() + this.apiMethods().createOrder;
        return this.postRequest(url, params);
    }

    updateOrder(params) {
        let url = this.getApiUrl() + this.apiMethods().updateOrder;
        return this.postRequest(url, params);
    }


    updateToken(user_id, push_token) {
        var params = {
            'user_id': user_id,
            'push_token': push_token,
        };
        let url = this.getApiUrl() + this.apiMethods().updateToken;
        return this.postRequest(url, params);
    }


    logoutUser(userId) {
        var params = {
            'user_id': userId
        };
        let url = this.getApiUrl() + this.apiMethods().logoutUser + "?" + this.QueryStringBuilder(params);
        return this.getRequest(url);
    }

    getOrders(userId, status?: string) {
        var url = this.getApiUrl() + this.apiMethods().getOrders + "?userId=" + userId;
        if (status) {
            url = url + "&status=" + status;
        }
        return this.getRequest(url);
    }

    providerPaymentInfo(providerId, paymentMethodId) {
        let url = this.getApiUrl() + this.apiMethods().providerPaymentInfo + "?payment_method_id=" + paymentMethodId + "&provider_id=" + providerId;
        return this.getRequest(url);
    }

    resCategoryTag(catId) {
        let url = this.getApiUrl() + this.apiMethods().resCategoryTag + "/" + catId + "/.json";
        return this.getRequest(url);
    }

    itemDetails(itemId) {
        let url = this.getApiUrl() + this.apiMethods().items + "/" + itemId;
        return this.getRequest(url);
    }

    citiesList() {
        let url = this.getApiUrl() + this.apiMethods().citiesList + "?countryId=" + this.storage.getCountryId();
        return this.getRequest(url);
    }

    sliders() {
        let url = this.getApiUrl() + this.apiMethods().sliders + "?countryId=" + this.storage.getCountryId();
        return this.getRequest(url);
    }

    appVersion(platform, version) {
        let url = this.getApiUrl() + this.apiMethods().appVersion + "?app=snappy_delivery&platform=" + platform + "&app_version=" + version;
        return this.getRequest(url);
    }

    countriesList() {
        let url = this.getApiUrl() + this.apiMethods().countriesList;
        return this.getRequest(url);
    }

    getRestaurantTable() {
        let url = this.getApiUrl() + this.apiMethods().getRestaurantTable + "/" + this.getRestaurantId() + "/.json";
        return this.getRequest(url);
    }

    getOrder(orderId) {
        let url = this.getApiUrl() + this.apiMethods().getOrder + "/" + orderId + "/.json";
        return this.getRequest(url);
    }

    getUser(uid) {
        let url = this.getApiUrl() + this.apiMethods().userDetails + "/" + uid + "/.json";
        return this.getRequest(url);
    }


    getUserAddresses(uid) {
        let url = this.getApiUrl() + this.apiMethods().addressList + "?userId=" + uid;
        return this.getRequest(url);
    }

    restaurantDetails() {
        let url = this.getApiUrl() + this.apiMethods().restaurantDetails + "/" + this.getRestaurantId() + "/.json";
        return this.getRequest(url);
    }

    wallet(userId) {
        let url = this.getApiUrl() + this.apiMethods().wallet + "/" + userId + "/.json";
        return this.getRequest(url);
    }

    // Post Requests

    getItemsByTagCat(catId, tagId) {
        var params = {
            'res_id': this.getRestaurantId(),
            'category_id': catId,
            'tag_id': tagId
        };
        let url = this.getApiUrl() + this.apiMethods().getItemsByTagCat + ".json";
        return this.postRequest(url, params);
    }

    getRestroTaxes(totalPrice) {
        var params = {
            'res_id': this.getRestaurantId(),
            'subtotal': totalPrice
        };
        let url = this.getApiUrl() + this.apiMethods().getRestroTaxes + ".json";
        return this.postRequest(url, params);
    }

    checkUserLogin(email, password) {
        var params = {
            'email': email,
            'password': password
        };
        let url = this.getApiUrl() + this.apiMethods().login;
        return this.postRequest(url, params);
    }

    addResUser(firstname, lastname, phone, city, email, password, dob) {
        var params = {
            'fname': firstname,
            'lname': lastname,
            'mobile': phone,
            'city': city,
            'email': email,
            'password': password,
            'dob': dob,
            'country_id': this.storage.getCountryId()
        };

        let url = this.getApiUrl() + this.apiMethods().signup;
        return this.postRequest(url, params);
    }

    placeOrder(tableId, orderId, userId, item, orExtra) {
        var params = {
            'table_id': tableId,
            'order_id': orderId,
            'user_id': userId,
            'item': JSON.stringify(item),
            'or_extra': orExtra
        };

        let url = this.getApiUrl() + this.apiMethods().placeOrder + ".json";
        return this.postRequest(url, params);
    }

    placeOrderForDelivery(addressId, date, time, userId, item, orExtra) {
        var params = {
            'address_id': addressId,
            'user_id': userId,
            'order_type_date': date,
            'order_type_time': time,
            'item': JSON.stringify(item),
            'or_extra': orExtra,
            'or_status': 6,
            'order_type': 'delivery'
        };

        let url = this.getApiUrl() + this.apiMethods().placeOrder + ".json";
        return this.postRequest(url, params);
    }

    placeOrderForTakeaway(date, time, userId, item, orExtra) {
        var params = {
            'user_id': userId,
            'order_type_date': date,
            'order_type_time': time,
            'item': JSON.stringify(item),
            'or_extra': orExtra,
            'or_status': 6,
            'order_type': 'takeaway'
        };

        let url = this.getApiUrl() + this.apiMethods().placeOrder + ".json";
        return this.postRequest(url, params);
    }

    requestForBill(orderId) {
        let params = {
            'r_id': this.getRestaurantId(),
            'or_id': orderId
        };
        let url = this.getApiUrl() + this.apiMethods().requestForBill + ".json";
        return this.postRequest(url, params);
    }

    getResUserOrderHistory(userId, userType) {
        let params = {
            'res_id': this.getRestaurantId(),
            'user_id': userId,
            'user_type': userType
        };
        let url = this.getApiUrl() + this.apiMethods().getResUserOrderHistory + ".json";
        return this.postRequest(url, params);
    }

    addFeedback(userId, serviceStar, delieveryTimeStar, staffBehaveStar, commentsTxt) {
        var params = {
            'user_id': userId,
            'service': serviceStar,
            'delievery_time': delieveryTimeStar,
            'staff_behaviour': staffBehaveStar,
            'comment': commentsTxt
        };
        let url = this.getApiUrl() + this.apiMethods().addFeedback;
        return this.postRequest(url, params);
    }

    updateProfile(uid, fname, lname, phone, cityid, dob) {
        var params = {
            'user_id': uid,
            'fname': fname,
            'lname': lname,
            'mobile': phone,
            'city_id': cityid,
            'dob': dob
        };
        let url = this.getApiUrl() + this.apiMethods().updateProfile;
        return this.postRequest(url, params);
    }


    updateProfilePhoto(uid, base64Img) {
        var params = {
            'user_id': uid,
            'photo': base64Img
        };
        let url = this.getApiUrl() + this.apiMethods().updateProfilePhoto;
        return this.postRequest(url, params);
    }

    resetPassword(email) {
        var params = {
            'email': email
        };
        let url = this.getApiUrl() + this.apiMethods().resetPassword;
        return this.postRequest(url, params);
    }


    updateUserPhoto(uid, base64Img) {
        var params = {
            'u_id': uid,
            'u_icon': base64Img
        };
        let url = this.getApiUrl() + this.apiMethods().userIcon + ".json";
        return this.postRequest(url, params);
    }

    /*add or update address(pass address_id for update address)*/
    userAddress(uid_r, fullname_r, city_r, phone_r, flatOrHouseNo, apartmentOrLocality, address, landmark, postalCode, company, geoLat, geoLong, type, addressId, primary) {
        var params = {
            'user_id': uid_r,
            'fullname': fullname_r,
            'city': city_r,
            'phone': phone_r,
            'flat_or_house_no': flatOrHouseNo,
            'apartment_or_locality': apartmentOrLocality,
            'address': address,
            'landmark': landmark,
            'postal_code': postalCode,
            'company': company,
            'geo_lat': geoLat,
            'geo_long': geoLong,
            'type': type,
            'primary': primary
        };

        if (addressId) { //update or delete otherwise undefined
            params['address_id'] = addressId;
        }
        let url = this.getApiUrl() + this.apiMethods().addressAdd;
        return this.postRequest(url, params);
    }

    deleteAddress(uid_r, addressId) {
        let url = this.getApiUrl() + this.apiMethods().deleteAddress + "?id=" + addressId;
        return this.getRequest(url);
    }

    /**
     * orderBillPayment
     * @param orderid(required)
     * @param type(required) - payu, paytm
     * @param status(required) - pending, success, failed
     * @param transaction_id
     * @param total_payment
     * @param response_data - serialize array of response from payment gateway
     * @returns Promise
     **/
    orderBillPayment(orderid_r, type_r, status_r, transaction_id, total_payment, response_data) {
        var params = {
            'order_id': orderid_r,
            'type': type_r,
            'status': status_r,
            'transaction_id': transaction_id,
            'total_payment': total_payment,
            'response_data': response_data
        };

        let url = this.getApiUrl() + this.apiMethods().orderBillPayment + ".json";
        return this.postRequest(url, params);
    }


    payFromWallet(password_r, phonenumber, user_id, parent_orderid) {
        var params = {
            'password': password_r,
            'user_id': user_id,
            'phonenumber': phonenumber,
            'parent_orderid': parent_orderid
        };

        let url = this.getApiUrl() + this.apiMethods().payFromWallet + ".json";
        return this.postRequest(url, params);
    }

    arPaymentRequest(data) {
        var params = data;
        let url = this.getApiUrl() + this.apiMethods().arPaymentRequest;
        return this.postRequest(url, params);
    }
}
