import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the Storage provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
    */
  @Injectable()
  export class Storage {

    public itemsCount = 0;

    static instance         : Storage;
    static isCreating       : Boolean = false;

    constructor() {

      console.log("cart constructor call");

      if (!Storage.isCreating)
      {
        throw new Error("no utilize getInstance()");
      }
    }

    static getInstance() : Storage
    {
      if (Storage.instance == null)
      {
        Storage.isCreating = true;
        Storage.instance = new Storage();
        Storage.isCreating = false;        
      }
      return Storage.instance;
    }

    // User

    setUser(user) {
    	window.localStorage.setItem('user', JSON.stringify(user));
    }

    getUser() {
    	let user = window.localStorage.getItem('user');
    	return (!user)?{}:JSON.parse(user);
    }

    removeUser() {
    	window.localStorage.removeItem('user');
    }


    setCountry(user) {
      window.localStorage.setItem('user_country', JSON.stringify(user));
    }

    getCountry() {
      let country = window.localStorage.getItem('user_country');
      return (!country)?undefined:JSON.parse(country);
    }

    getCountryId() {
      let country:any = this.getCountry();
      return country?country.id:0;
    }
    
    updateOrderId(orderId) {
      var user = this.getUser();
      user.main_order = orderId;
      this.setUser(user);
    }

    updateTableId(tableId) {
      var user = this.getUser();
      user.table_id = tableId;
      this.setUser(user);
    }

    // Login Status

    setLoggedIn() {
    	window.localStorage.setItem('isLoggedIn', "true");
    }

    isLoggedIn() {
    	return window.localStorage.getItem('isLoggedIn') == 'true' ? true : false;
    }

    removeLoggedIn() {
    	window.localStorage.removeItem('isLoggedIn');
    }

    // Login Details

    setLoginDetails(loginDetails) {
    	window.localStorage.setItem('loginDetails', JSON.stringify(loginDetails));
    }

    getLoginDetails() {
    	let loginDetails = window.localStorage.getItem('loginDetails');
    	return (!loginDetails)?{}:JSON.parse(loginDetails);
    }

    removeLoginDetails() {
    	window.localStorage.removeItem('loginDetails');
    }

    // Default Address
    setDefaultAddress(addId) {
      window.localStorage.setItem('defalut_address', addId);
    }

    getDefaultAddress() {
      let addId = window.localStorage.getItem('defalut_address');
      return addId;
    }
    
    removeDefaultAddress() {
      window.localStorage.removeItem('defalut_address');
    }

    // Localizations
    setLanguage(language) {
      window.localStorage.setItem('default_language', language);
    }

    getLanguage() {
      let language = window.localStorage.getItem('default_language');
      return language ? language : "es";
    }
  }
