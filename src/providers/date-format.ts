import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the DateFormat provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
  	*/
  @Injectable()
  export class DateFormat {


  	static instance         : DateFormat;
  	static isCreating       : Boolean = false;

  	constructor() {

  		console.log('Hello DateFormat Provider');

  		if (!DateFormat.isCreating)
  		{
  			throw new Error("no utilize getInstance()");
  		}
  	}

  	static shared() : DateFormat
  	{
  		if (DateFormat.instance == null)
  		{
  			DateFormat.isCreating = true;
  			DateFormat.instance = new DateFormat();
  			DateFormat.isCreating = false;        
  		}
  		return DateFormat.instance;
  	}

  	dateToStrDDMMYYYY(date) { 
      if (this.isValidDate(date)) {
        var day = date.getDate();
        var monthIndex = date.getMonth()+1;
        var year = date.getFullYear();
        var datStr = this.appendZero(day)+'-'+ this.appendZero(monthIndex) +'-'+year;
        return datStr; 
      }
      return ""; 
    }
    
    ddmmyyyyStrToDate(dateStr, seperater) {
      var pattern = /(\d{2})\-(\d{2})\-(\d{4})/;
      var dt = new Date(dateStr.replace(pattern,'$3-$2-$1'));
      return dt;
    }

    convertDateWithZeroTime(date) {
      date.setHours(0);
      date.setMinutes(0);
      date.setSeconds(0);
      date.setMilliseconds(0);
      return date;
    }

    getCurrentTimezoneDate() {
      var currDate = new Date();
      currDate.setHours(currDate.getHours() - currDate.getTimezoneOffset() / 60);
      currDate.setMinutes(currDate.getMinutes() - currDate.getTimezoneOffset() % 60);

      return currDate;
    }


    getMonthName(dt:Date) {
      var mlist = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];  
      return mlist[dt.getMonth()]; 
    }

    isValidDate(date) {
      if ( Object.prototype.toString.call(date) === "[object Date]" ) {
        if ( isNaN( date.getTime() ) ) {  
          return false;
        }
        else {
          return true;
        }
      }
      else {
        return false;
      }
    }

    appendZero(no) {
      return ((no < 10)?'0':'')+no;
    }

  }
